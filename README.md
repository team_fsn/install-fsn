# FSN - Verson 4.2.0

Environment :

- Ubuntu 14.04 LTS (Trusty)
- Access root

# Get Started

* Configuration

You can to edit prod.properties file for :
  * To configure domain name.
  * To configure MySQL database (name, user, password)
    * To change the values in fsn.sql (line 7 to 9)
  * To configure WebService settings.
  
# Download :

```
$ sudo apt-get update
$ sudo apt-get install -y unzip
$ cd /opt
$ wget https://gitlab.com/team_fsn/install-fsn/repository/archive.zip?ref=master -O install-fsn.zip
$ unzip install-fsn.zip
$ cd install-fsn*
```

or

```
$ sudo apt-get update
$ sudo apt-get install -y git
$ cd /opt
$ git clone https://gitlab.com/team_fsn/install-fsn.git -b master
$ cd install-fsn
```

# Installation

* Step 1 (Env Config)
  * Note : same user/password for MySQL in the "prod.properties" file.
  * Note : you must agree the oracle's license for java installation.

```
$ ./step_1.sh
```

* Step 2 (Domain Config)

```
$ ./step_2.sh
```

* Step 3 (Source Install)

```
$ ./step_3.sh
```

* Step 4 (Database Install)

```
$ ./step_4.sh
```

# RUN 

Go to http://fsn.com

Default user/password for Administrator account :
- user name : administ
- password : root

