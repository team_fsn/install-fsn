#!/bin/bash
set -e

source ./prod.properties
echo "Début de livraison de FSN ${FSN_ENV_NOM}"

echo "Initialisation des variables."
FSN_ARTIFACT_URL="http://artifactory.fichestrati.fr/artifactory/fsn-sogeti/sogeti/cir/fsn/fsn-livrable/${VERSION}/fsn-livrable-${VERSION}-multi.zip"
FSN="$FSN_ENV_NOM"
DEPLOY_FOLDER="/var/www/html/$FSN"
APP_FOLDER="${DEPLOY_FOLDER}/FSN"
APACHE_FOLDER="/etc/apache2"
VH_FOLDER_AVAILABLE="$APACHE_FOLDER/sites-available"
VH_FOLDER_ENABLED="$APACHE_FOLDER/sites-enabled"
VH_FILE="fsn_host.conf"
MEDIATHEQUE_FOLDER="${APP_FOLDER}/mediatheque"
PHOTOGRAMMETRIE_FOLDER="${APP_FOLDER}/photogrammetrie"
GEOLOCALISATION_FOLDER="${APP_FOLDER}/geolocalisation"
MEDIATHEQUE_INIT_UPLOAD_FOLDER="${MEDIATHEQUE_FOLDER}/upload"
MEDIATHEQUE_UPLOAD_FOLDER="${DEPLOY_FOLDER}/${FSN}_uploads/"
GEOLOCALISATION_SAVE_FOLDER="${DEPLOY_FOLDER}/${FSN}_geolocalisation/"
PHOTOGRAMMETRIE_SAVE_FOLDER="${DEPLOY_FOLDER}/${FSN}_photogrammetrie/"
QRCODE_FOLDER="${APP_FOLDER}/public/images/qrcode"
APACHE_USER="www-data"
FSN_WS_JAR="ws.jar"
JAR_PATH="$APP_FOLDER/$FSN_WS_JAR"
SERVICE_NAME="$FSN-ws"
SERVICE_PATH="/etc/init.d/$SERVICE_NAME"
echo "Fin d'initialisation des variables."

#Uninstall ws old
echo "Désinstallation de l'ancien web service s'il existe."
if [ -L "$SERVICE_PATH" ]; then
    echo "Arrêt de l'ancien webservice"
	sudo service "$SERVICE_NAME" stop || echo "WS $FSN_ENV_NOM déjà stoppé ou non installé."
	sudo rm $SERVICE_PATH
fi

echo "Delete $APP_FOLDER"
sudo rm -Rf $APP_FOLDER

if [ ! -d "$DEPLOY_FOLDER" ]
then
    echo "Création du répertoire $DEPLOY_FOLDER."
    sudo mkdir "$DEPLOY_FOLDER"
	sudo chown www-data:www-data "$DEPLOY_FOLDER"
fi

echo "Copie du fichier vhost"
sudo rm -f $VH_FOLDER_AVAILABLE/$VH_FILE
sudo cp $VH_FILE $VH_FOLDER_AVAILABLE/$VH_FILE

cd ~/

echo "mise à jour du virtual host"
if [ -f $VH_FOLDER_ENABLED/$VH_FILE ]; then
	echo "desactivation de l'ancien virtual host"
	sudo a2dissite $VH_FILE
fi
sudo sed -i -- "s#__DEPLOY_FOLDER__#$APP_FOLDER#g" $VH_FOLDER_AVAILABLE/$VH_FILE
sudo sed -i -- "s/__FSN_DOMAIN__/$FSN_DOMAIN/g" $VH_FOLDER_AVAILABLE/$VH_FILE
echo "activation du nouveau virtual host"
sudo a2ensite $VH_FILE
sudo service apache2 restart

echo "Préparation du téléchargement du livrable."
echo "URL de téléchargement : $FSN_ARTIFACT_URL"

# Download
echo "Lancement du téléchargement."
wget -q -N --output-document="$FSN.zip" $FSN_ARTIFACT_URL
echo "Fin du téléchargement."

echo "Déplacement des sources vers $DEPLOY_FOLDER."
sudo mv -f $FSN.zip $DEPLOY_FOLDER
cd $DEPLOY_FOLDER

# unzip 
echo "Unzip $FSN.zip"
unzip -qq $FSN.zip
echo "Unzip finished."

echo "Supression de zip"
sudo rm $FSN.zip

echo "Recursively changing mod of the files to 755"
sudo chmod -R 775 "$DEPLOY_FOLDER"

echo "Change group"
sudo chgrp -R www-data "$DEPLOY_FOLDER"

if [ ! -d "$MEDIATHEQUE_UPLOAD_FOLDER" ]
then
	echo "create mediatheque folder"
	sudo mkdir "$MEDIATHEQUE_UPLOAD_FOLDER"
fi
sudo chmod 775 "$MEDIATHEQUE_UPLOAD_FOLDER"
echo "remove the upload folder from meditheque"
sudo rm -Rf "$MEDIATHEQUE_INIT_UPLOAD_FOLDER"
echo "create mediatheque's symbolic link"
sudo ln -s "$MEDIATHEQUE_UPLOAD_FOLDER" "$MEDIATHEQUE_INIT_UPLOAD_FOLDER"

if [ ! -d "$GEOLOCALISATION_SAVE_FOLDER" ]
then
	echo "create geolocalisation folder"
	sudo mkdir "$GEOLOCALISATION_SAVE_FOLDER"
fi
sudo chmod 775 "$GEOLOCALISATION_SAVE_FOLDER"
echo "remove the geolocalisation folder"
sudo rm -Rf "$GEOLOCALISATION_FOLDER"
echo "create geolocalisation's symbolic link"
sudo ln -s "$GEOLOCALISATION_SAVE_FOLDER" "$GEOLOCALISATION_FOLDER"

if [ ! -d "$PHOTOGRAMMETRIE_SAVE_FOLDER" ]
then
	echo "create photogrammetrie folder"
	sudo mkdir "$PHOTOGRAMMETRIE_SAVE_FOLDER"
	sudo mv $PHOTOGRAMMETRIE_FOLDER/* $PHOTOGRAMMETRIE_SAVE_FOLDER
fi
sudo chmod 775 "$PHOTOGRAMMETRIE_SAVE_FOLDER"
echo "remove the photogrammetrie folder"
sudo rm -Rf "$PHOTOGRAMMETRIE_FOLDER"
echo "create photogrammetrie's symbolic link"
sudo ln -s "$PHOTOGRAMMETRIE_SAVE_FOLDER" "$PHOTOGRAMMETRIE_FOLDER"

echo "Changing particular access rights on $QRCODE_FOLDER"
sudo chmod g+rwx $QRCODE_FOLDER


sudo chown -R www-data:www-data "$DEPLOY_FOLDER"

#Install config file

echo "Configuring the environment"

PARAMETERS="fsn_env_nom=$FSN_ENV_NOM&fsn_ws_token=$FSN_WS_TOKEN&fsn_port_ws=$FSN_PORT_WS&fsn_bdd_server=$FSN_BDD_SERVER&fsn_bdd=$FSN_BDD&fsn_bdd_login=$FSN_BDD_LOGIN&fsn_bdd_password=$FSN_BDD_PASSWORD&fsn_bdd_port=$FSN_BDD_PORT"
echo "config parameters : $PARAMETERS"
URL_CONFIG="http://${FSN_DOMAIN}/public/install.php"
echo "url configuration : $URL_CONFIG"
curl -X POST --data "$PARAMETERS" $URL_CONFIG

#RUN WS

echo "Lancement Webservice pour $FSN_ENV_NOM"

echo "Application des droits sur le jar."
sudo chmod 776 "$JAR_PATH"
echo "Création du nouveau lien symbolique sur $JAR_PATH"
if [ ! -f "$SERVICE_PATH" ]; then
	sudo ln -s "$JAR_PATH" "$SERVICE_PATH"
fi

sudo update-rc.d -f "$SERVICE_NAME" remove
sudo update-rc.d -f "$SERVICE_NAME" defaults
echo "Lancement du webservice."
sudo service "$SERVICE_NAME" start