-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: fsn
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.8-MariaDB

DROP DATABASE if exists `fsn`;
CREATE DATABASE `fsn` DEFAULT CHARACTER SET utf8;
USE `fsn`;

-- ----------------------------------------------------------------------------------------------------
-- ----------------------------------------------------------------------------------------------------
-- --------------   C R E A T I O N   D E S   S T R U C T U R E S   V I D E S -------------------------
-- --------------   User : administ, mot de passe : root ----------------------------------------------
-- ----------------------------------------------------------------------------------------------------
-- ----------------------------------------------------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `_version`
--

DROP TABLE IF EXISTS `_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_version` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version_cible_appli` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `version_db` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_version`
--

LOCK TABLES `_version` WRITE;
/*!40000 ALTER TABLE `_version` DISABLE KEYS */;
/*!40000 ALTER TABLE `_version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `adm_ancien_comment`
--

DROP TABLE IF EXISTS `adm_ancien_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adm_ancien_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `ancien_commentaire` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `nouveau_commentaire` text COLLATE utf8_unicode_ci NOT NULL,
  `datechmnt` datetime NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `user` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNI_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adm_ancien_comment`
--

LOCK TABLES `adm_ancien_comment` WRITE;
/*!40000 ALTER TABLE `adm_ancien_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `adm_ancien_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `adm_table_log`
--

DROP TABLE IF EXISTS `adm_table_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adm_table_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nom_table` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_crea` datetime DEFAULT NULL,
  `date_modif` datetime DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `user` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNI_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adm_table_log`
--

LOCK TABLES `adm_table_log` WRITE;
/*!40000 ALTER TABLE `adm_table_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `adm_table_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `association_er`
--

DROP TABLE IF EXISTS `association_er`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `association_er` (
  `er_ancien_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `er_nouveau_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `typeasso_id` int(11) NOT NULL,
  `commentaire` varchar(100) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'commentaire',
  `traitement_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNI_ancien_id_nouveau_id` (`er_ancien_id`,`er_nouveau_id`),
  UNIQUE KEY `unique_key` (`er_ancien_id`,`er_nouveau_id`),
  KEY `IDX_association_er_er_nouveau_id` (`er_nouveau_id`),
  KEY `IDX_association_er_er_ancien_id` (`er_ancien_id`),
  KEY `IDX_association_er_typeasso_id` (`typeasso_id`),
  KEY `FK_association_er_traitement` (`traitement_id`),
  CONSTRAINT `FK_association_er_traitement` FOREIGN KEY (`traitement_id`) REFERENCES `traitement` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_association_er_typeasso` FOREIGN KEY (`typeasso_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_er_ancien_id` FOREIGN KEY (`er_ancien_id`) REFERENCES `elementrecueilli` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_er_nouveau_id` FOREIGN KEY (`er_nouveau_id`) REFERENCES `elementrecueilli` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `association_er`
--

LOCK TABLES `association_er` WRITE;
/*!40000 ALTER TABLE `association_er` DISABLE KEYS */;
/*!40000 ALTER TABLE `association_er` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `caracterepate`
--

DROP TABLE IF EXISTS `caracterepate`;
/*!50001 DROP VIEW IF EXISTS `caracterepate`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `caracterepate` (
  `id` tinyint NOT NULL,
  `caracterepate` tinyint NOT NULL,
  `visible` tinyint NOT NULL,
  `ordre` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `classe`
--

DROP TABLE IF EXISTS `classe`;
/*!50001 DROP VIEW IF EXISTS `classe`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `classe` (
  `id` tinyint NOT NULL,
  `classe` tinyint NOT NULL,
  `visible` tinyint NOT NULL,
  `ordre` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `classe_fonction`
--

DROP TABLE IF EXISTS `classe_fonction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classe_fonction` (
  `classe_id` int(11) NOT NULL,
  `fonction_id` int(11) NOT NULL,
  PRIMARY KEY (`classe_id`,`fonction_id`),
  UNIQUE KEY `UNI_classe_id_fonction_id` (`classe_id`,`fonction_id`),
  KEY `IDX_classe_fonction_classe_id` (`classe_id`),
  KEY `IDX_classe_fonction_fonction_id` (`fonction_id`),
  CONSTRAINT `FK_classe_fonction_classe_id` FOREIGN KEY (`classe_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_classe_fonction_fonction_id` FOREIGN KEY (`fonction_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classe_fonction`
--

LOCK TABLES `classe_fonction` WRITE;
/*!40000 ALTER TABLE `classe_fonction` DISABLE KEYS */;
/*!40000 ALTER TABLE `classe_fonction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classe_naturematiere`
--

DROP TABLE IF EXISTS `classe_naturematiere`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classe_naturematiere` (
  `classe_id` int(11) NOT NULL,
  `naturematiere_id` int(11) NOT NULL,
  PRIMARY KEY (`classe_id`,`naturematiere_id`),
  UNIQUE KEY `UNI_classe_id_naturematiere_id` (`classe_id`,`naturematiere_id`),
  KEY `IDX_classe_naturematiere_classe_id` (`classe_id`),
  KEY `IDX_classe_naturematiere_naturematiere_id` (`naturematiere_id`),
  CONSTRAINT `FK_classe_naturematiere_classe_id` FOREIGN KEY (`classe_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_classe_naturematiere_naturematiere_id` FOREIGN KEY (`naturematiere_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classe_naturematiere`
--

LOCK TABLES `classe_naturematiere` WRITE;
/*!40000 ALTER TABLE `classe_naturematiere` DISABLE KEYS */;
/*!40000 ALTER TABLE `classe_naturematiere` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `classification`
--

DROP TABLE IF EXISTS `classification`;
/*!50001 DROP VIEW IF EXISTS `classification`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `classification` (
  `id` tinyint NOT NULL,
  `classification` tinyint NOT NULL,
  `visible` tinyint NOT NULL,
  `ordre` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `consistance`
--

DROP TABLE IF EXISTS `consistance`;
/*!50001 DROP VIEW IF EXISTS `consistance`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `consistance` (
  `id` tinyint NOT NULL,
  `consistance` tinyint NOT NULL,
  `visible` tinyint NOT NULL,
  `ordre` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `conteneur`
--

DROP TABLE IF EXISTS `conteneur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conteneur` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identifiant',
  `nom` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Nom',
  `description` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'description',
  `codebarre` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `emplacement_id` int(11) NOT NULL COMMENT 'Emplacement',
  `entiteadmin_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNI_id` (`id`),
  UNIQUE KEY `nom_entiteadmin_id` (`nom`,`entiteadmin_id`),
  UNIQUE KEY `codebarre_entiteadmin_id` (`codebarre`,`entiteadmin_id`),
  KEY `FK_conteneur_type_id` (`type_id`),
  KEY `FK_conteneur_emplacement_id` (`emplacement_id`),
  KEY `FK_conteneur_entiteadmin_id` (`entiteadmin_id`),
  CONSTRAINT `FK_conteneur_emplacement_id` FOREIGN KEY (`emplacement_id`) REFERENCES `emplacement` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_conteneur_entiteadmin_id` FOREIGN KEY (`entiteadmin_id`) REFERENCES `fsn_entiteadmin` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_conteneur_type_id` FOREIGN KEY (`type_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Table des Conteneurs';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conteneur`
--

LOCK TABLES `conteneur` WRITE;
/*!40000 ALTER TABLE `conteneur` DISABLE KEYS */;
/*!40000 ALTER TABLE `conteneur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `contexte`
--

DROP TABLE IF EXISTS `contexte`;
/*!50001 DROP VIEW IF EXISTS `contexte`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contexte` (
  `id` tinyint NOT NULL,
  `contexte` tinyint NOT NULL,
  `visible` tinyint NOT NULL,
  `ordre` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `contour_fai`
--

DROP TABLE IF EXISTS `contour_fai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contour_fai` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `fai_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `x` float(20,14) NOT NULL,
  `y` float(20,14) NOT NULL,
  `z` float(20,14) NOT NULL,
  `sequence` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNI_ID` (`id`),
  UNIQUE KEY `UNI_POSITION` (`fai_id`,`x`,`y`,`z`),
  UNIQUE KEY `UNI_SEQUENCE` (`fai_id`,`sequence`),
  CONSTRAINT `FK_contour_fai_fai_id` FOREIGN KEY (`fai_id`) REFERENCES `fai` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contour_fai`
--

LOCK TABLES `contour_fai` WRITE;
/*!40000 ALTER TABLE `contour_fai` DISABLE KEYS */;
/*!40000 ALTER TABLE `contour_fai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contour_sitefouille`
--

DROP TABLE IF EXISTS `contour_sitefouille`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contour_sitefouille` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `sitefouille_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `x` float(20,14) NOT NULL,
  `y` float(20,14) NOT NULL,
  `z` float(20,14) NOT NULL,
  `sequence` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNI_ID` (`id`),
  UNIQUE KEY `UNI_POSITION` (`sitefouille_id`,`x`,`y`,`z`),
  UNIQUE KEY `UNI_SEQUENCE` (`sitefouille_id`,`sequence`),
  CONSTRAINT `FK_contour_sitefouille_sitefouille_id` FOREIGN KEY (`sitefouille_id`) REFERENCES `sitefouille` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contour_sitefouille`
--

LOCK TABLES `contour_sitefouille` WRITE;
/*!40000 ALTER TABLE `contour_sitefouille` DISABLE KEYS */;
/*!40000 ALTER TABLE `contour_sitefouille` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contour_us`
--

DROP TABLE IF EXISTS `contour_us`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contour_us` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `us_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `x` float(20,14) NOT NULL,
  `y` float(20,14) NOT NULL,
  `z` float(20,14) NOT NULL,
  `sequence` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNI_ID` (`id`),
  UNIQUE KEY `UNI_POSITION` (`us_id`,`x`,`y`,`z`),
  UNIQUE KEY `UNI_SEQUENCE` (`us_id`,`sequence`),
  CONSTRAINT `FK_contour_us_us_id` FOREIGN KEY (`us_id`) REFERENCES `us` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contour_us`
--

LOCK TABLES `contour_us` WRITE;
/*!40000 ALTER TABLE `contour_us` DISABLE KEYS */;
/*!40000 ALTER TABLE `contour_us` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `croquis`
--

DROP TABLE IF EXISTS `croquis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `croquis` (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `file_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` longblob,
  `svg` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `vg` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `croquis`
--

LOCK TABLES `croquis` WRITE;
/*!40000 ALTER TABLE `croquis` DISABLE KEYS */;
/*!40000 ALTER TABLE `croquis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `croquis_import`
--

DROP TABLE IF EXISTS `croquis_import`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `croquis_import` (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `croquis_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `file_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagepng` longblob,
  `imagesvg` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_croquis_import_croquis` (`croquis_id`),
  CONSTRAINT `FK_croquis_import_croquis` FOREIGN KEY (`croquis_id`) REFERENCES `croquis` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `croquis_import`
--

LOCK TABLES `croquis_import` WRITE;
/*!40000 ALTER TABLE `croquis_import` DISABLE KEYS */;
/*!40000 ALTER TABLE `croquis_import` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document`
--

DROP TABLE IF EXISTS `document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `copyright` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'UASD',
  `datecreation` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chemin` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `commentaire` text COLLATE utf8_unicode_ci,
  `type_doc` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `topo_id` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNI_id` (`id`),
  UNIQUE KEY `UNI_chemin` (`chemin`),
  KEY `IDX_document_type_doc_id` (`type_doc`),
  KEY `IDX_document_topo_id` (`topo_id`),
  CONSTRAINT `FK_document_topo_id` FOREIGN KEY (`topo_id`) REFERENCES `topographie` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='<double-click to overwrite multiple objects>';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document`
--

LOCK TABLES `document` WRITE;
/*!40000 ALTER TABLE `document` DISABLE KEYS */;
/*!40000 ALTER TABLE `document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elementpoterie`
--

DROP TABLE IF EXISTS `elementpoterie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elementpoterie` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `caracterepate_id` int(11) DEFAULT NULL,
  `forme_id` int(11) DEFAULT NULL,
  `classification_id` int(11) DEFAULT NULL,
  `production_id` int(11) DEFAULT NULL,
  `decor` text COLLATE utf8_unicode_ci,
  `traceusage` text COLLATE utf8_unicode_ci,
  `proportionconserve` float DEFAULT NULL,
  `precisionproduction_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_elementpoterie_caracterepate_id` (`caracterepate_id`),
  KEY `IDX_elementpoterie_production_id` (`production_id`),
  KEY `IDX_elementpoterie_forme_id` (`forme_id`),
  KEY `IDX_elementpoterie_classification_id` (`classification_id`),
  KEY `FK_elementpoterie_precisionproduction_id_idx` (`precisionproduction_id`),
  CONSTRAINT `FK_elementpoterie_caracterepate_id` FOREIGN KEY (`caracterepate_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_elementpoterie_classification_id` FOREIGN KEY (`classification_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_elementpoterie_forme_id` FOREIGN KEY (`forme_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_elementpoterie_precisionproduction_id` FOREIGN KEY (`precisionproduction_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_elementpoterie_production_id` FOREIGN KEY (`production_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='<double-click to overwrite multiple objects>';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elementpoterie`
--

LOCK TABLES `elementpoterie` WRITE;
/*!40000 ALTER TABLE `elementpoterie` DISABLE KEYS */;
/*!40000 ALTER TABLE `elementpoterie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elementrecueilli`
--

DROP TABLE IF EXISTS `elementrecueilli`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elementrecueilli` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `us_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `intervenant_id` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `elementisole` tinyint(1) NOT NULL,
  `numero` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `intact` tinyint(1) DEFAULT NULL,
  `datedecouverte` date DEFAULT NULL,
  `elementpoterie_id` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `precisionfonction` text COLLATE utf8_unicode_ci,
  `statut_id` int(11) DEFAULT NULL,
  `quantite` float unsigned DEFAULT NULL,
  `poids` float unsigned DEFAULT NULL,
  `longueur` float unsigned DEFAULT NULL,
  `largeur` float unsigned DEFAULT NULL,
  `hauteur` float unsigned DEFAULT NULL,
  `diametre` float unsigned DEFAULT NULL,
  `incertitude` float DEFAULT NULL,
  `fonction_id` int(11) DEFAULT NULL,
  `classe_id` int(11) DEFAULT NULL,
  `etat_id` int(11) DEFAULT NULL,
  `emplacement_id` int(11) DEFAULT NULL COMMENT 'Emplacement',
  `conteneur_id` int(11) DEFAULT NULL COMMENT 'Conteneur',
  `datationdebreal_certain` int(11) DEFAULT NULL,
  `datationfinutil_certain` int(11) DEFAULT NULL,
  `datationdebreal_estim` int(11) DEFAULT NULL,
  `datationfinutil_estim` int(11) DEFAULT NULL,
  `bary_x` float DEFAULT NULL,
  `bary_y` float DEFAULT NULL,
  `bary_z` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNI_id` (`id`),
  UNIQUE KEY `uk_numero` (`numero`),
  KEY `IDX_er_us_id` (`us_id`),
  KEY `IDX_er_statut_id` (`statut_id`),
  KEY `IDX_er_utilisateur_id` (`intervenant_id`),
  KEY `IDX_er_classe_id` (`classe_id`),
  KEY `IDX_er_fonction_id` (`fonction_id`),
  KEY `IDX_eri_elementpoterie` (`elementpoterie_id`),
  KEY `IDX_er_etat` (`etat_id`),
  KEY `emplacement_id` (`emplacement_id`,`conteneur_id`),
  KEY `FK_elementrecueilli_conteneur_id` (`conteneur_id`),
  CONSTRAINT `FK_elementrecueilli_conteneur_id` FOREIGN KEY (`conteneur_id`) REFERENCES `conteneur` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_elementrecueilli_emplacement_id` FOREIGN KEY (`emplacement_id`) REFERENCES `emplacement` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_er_etat` FOREIGN KEY (`etat_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_er_fonction` FOREIGN KEY (`fonction_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_er_intervenant_id` FOREIGN KEY (`intervenant_id`) REFERENCES `intervenant` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_er_status_id` FOREIGN KEY (`statut_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_er_us_id` FOREIGN KEY (`us_id`) REFERENCES `us` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_eri_classe` FOREIGN KEY (`classe_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='<double-click to overwrite multiple objects>';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elementrecueilli`
--

LOCK TABLES `elementrecueilli` WRITE;
/*!40000 ALTER TABLE `elementrecueilli` DISABLE KEYS */;
/*!40000 ALTER TABLE `elementrecueilli` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elementrecueilli_zpwg_images`
--

DROP TABLE IF EXISTS `elementrecueilli_zpwg_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elementrecueilli_zpwg_images` (
  `id` varchar(36) NOT NULL,
  `zpwg_images_id` mediumint(8) unsigned NOT NULL,
  `elementrecueilli_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `elementrecueilli_zpwg_images_UNI1` (`zpwg_images_id`,`elementrecueilli_id`),
  KEY `elementrecueilli_zpwg_images_elementrecueilli_id_idx` (`elementrecueilli_id`),
  CONSTRAINT `elementrecueilli_zpwg_images_elementrecueilli_id` FOREIGN KEY (`elementrecueilli_id`) REFERENCES `elementrecueilli` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `elementrecueilli_zpwg_images_zpwg_images_id` FOREIGN KEY (`zpwg_images_id`) REFERENCES `zpwg_images` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elementrecueilli_zpwg_images`
--

LOCK TABLES `elementrecueilli_zpwg_images` WRITE;
/*!40000 ALTER TABLE `elementrecueilli_zpwg_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `elementrecueilli_zpwg_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emplacement`
--

DROP TABLE IF EXISTS `emplacement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emplacement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Nom',
  `type_id` int(11) NOT NULL COMMENT 'Type',
  `parent_id` int(11) DEFAULT NULL,
  `entiteadmin_id` int(11) NOT NULL COMMENT 'Entite Admin',
  `description` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'description',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_nom_parent_id` (`nom`,`parent_id`),
  KEY `FK_emplacement_type_id` (`type_id`),
  KEY `fk_emplacement_parent_id` (`parent_id`),
  KEY `FK_emplacement_entiteadmin_id` (`entiteadmin_id`),
  CONSTRAINT `FK_emplacement_entiteadmin_id` FOREIGN KEY (`entiteadmin_id`) REFERENCES `fsn_entiteadmin` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_emplacement_type_id` FOREIGN KEY (`type_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_emplacement_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `emplacement` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=204 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Table des Emplacements';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emplacement`
--

LOCK TABLES `emplacement` WRITE;
/*!40000 ALTER TABLE `emplacement` DISABLE KEYS */;
/*!40000 ALTER TABLE `emplacement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `episodeurbain`
--

DROP TABLE IF EXISTS `episodeurbain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `episodeurbain` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `identification` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `datationdebutbasse` date DEFAULT NULL,
  `datationdebuthaute` date DEFAULT NULL,
  `datationfinbasse` date DEFAULT NULL,
  `datationfinhaute` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNI_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='<double-click to overwrite multiple objects>';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `episodeurbain`
--

LOCK TABLES `episodeurbain` WRITE;
/*!40000 ALTER TABLE `episodeurbain` DISABLE KEYS */;
/*!40000 ALTER TABLE `episodeurbain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `er_doc`
--

DROP TABLE IF EXISTS `er_doc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `er_doc` (
  `er_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `doc_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`er_id`,`doc_id`),
  UNIQUE KEY `UNI_er_id_doc_id` (`er_id`,`doc_id`),
  KEY `IDX_er_doc_er_id` (`er_id`),
  KEY `IDX_er_doc_doc_id` (`doc_id`),
  CONSTRAINT `FK_er_doc_doc` FOREIGN KEY (`doc_id`) REFERENCES `document` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_er_doc_er` FOREIGN KEY (`er_id`) REFERENCES `elementrecueilli` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `er_doc`
--

LOCK TABLES `er_doc` WRITE;
/*!40000 ALTER TABLE `er_doc` DISABLE KEYS */;
/*!40000 ALTER TABLE `er_doc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `er_naturematiere`
--

DROP TABLE IF EXISTS `er_naturematiere`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `er_naturematiere` (
  `er_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `naturematiere_id` int(11) NOT NULL,
  `principal` tinyint(1) NOT NULL DEFAULT '1',
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNI_er_id_naturematiere_id` (`er_id`,`naturematiere_id`),
  UNIQUE KEY `unique_key` (`er_id`,`naturematiere_id`),
  KEY `IDX_naturematiere_id` (`naturematiere_id`),
  KEY `IDX_er_id` (`er_id`),
  CONSTRAINT `FK_er_naturematiere_er` FOREIGN KEY (`er_id`) REFERENCES `elementrecueilli` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_er_naturematiere_naturematiere` FOREIGN KEY (`naturematiere_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `er_naturematiere`
--

LOCK TABLES `er_naturematiere` WRITE;
/*!40000 ALTER TABLE `er_naturematiere` DISABLE KEYS */;
/*!40000 ALTER TABLE `er_naturematiere` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `etat`
--

DROP TABLE IF EXISTS `etat`;
/*!50001 DROP VIEW IF EXISTS `etat`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `etat` (
  `id` tinyint NOT NULL,
  `etat` tinyint NOT NULL,
  `visible` tinyint NOT NULL,
  `ordre` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `etattraitemen`
--

DROP TABLE IF EXISTS `etattraitemen`;
/*!50001 DROP VIEW IF EXISTS `etattraitemen`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `etattraitemen` (
  `id` tinyint NOT NULL,
  `etat` tinyint NOT NULL,
  `visible` tinyint NOT NULL,
  `ordre` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `etattraitement`
--

DROP TABLE IF EXISTS `etattraitement`;
/*!50001 DROP VIEW IF EXISTS `etattraitement`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `etattraitement` (
  `id` tinyint NOT NULL,
  `etat` tinyint NOT NULL,
  `visible` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `extorganisme`
--

DROP TABLE IF EXISTS `extorganisme`;
/*!50001 DROP VIEW IF EXISTS `extorganisme`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `extorganisme` (
  `id` tinyint NOT NULL,
  `nom` tinyint NOT NULL,
  `adresse` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `fai`
--

DROP TABLE IF EXISTS `fai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fai` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `sitefouille_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `typefai_id` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `identification` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `deb_fourc_hte_certain` int(11) DEFAULT NULL,
  `deb_fourc_bas_certain` int(11) DEFAULT NULL,
  `fin_fourc_hte_certain` int(11) DEFAULT NULL,
  `fin_fourc_bas_certain` int(11) DEFAULT NULL,
  `deb_fourc_hte_estim` int(11) DEFAULT NULL,
  `deb_fourc_bas_estim` int(11) DEFAULT NULL,
  `fin_fourc_hte_estim` int(11) DEFAULT NULL,
  `fin_fourc_bas_estim` int(11) DEFAULT NULL,
  `bary_x` float DEFAULT NULL,
  `bary_y` float DEFAULT NULL,
  `bary_z` float DEFAULT NULL,
  `contour` polygon DEFAULT NULL,
  PRIMARY KEY (`id`,`identification`),
  UNIQUE KEY `identification` (`identification`,`sitefouille_id`),
  KEY `FK_sitefouille_id` (`sitefouille_id`),
  KEY `FK_typefai_id` (`typefai_id`),
  CONSTRAINT `FK_sitefouille_id` FOREIGN KEY (`sitefouille_id`) REFERENCES `sitefouille` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_typefai_id` FOREIGN KEY (`typefai_id`) REFERENCES `fsn_categorie` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fai`
--

LOCK TABLES `fai` WRITE;
/*!40000 ALTER TABLE `fai` DISABLE KEYS */;
/*!40000 ALTER TABLE `fai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fai_us`
--

DROP TABLE IF EXISTS `fai_us`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fai_us` (
  `fai_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `us_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `incertitude` tinyint(1) NOT NULL DEFAULT '0',
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_key` (`fai_id`,`us_id`),
  KEY `FK_fai_us_us_id_idx` (`us_id`),
  CONSTRAINT `FK_fai_us_fai_id` FOREIGN KEY (`fai_id`) REFERENCES `fai` (`id`) ON UPDATE NO ACTION,
  CONSTRAINT `FK_fai_us_us_id` FOREIGN KEY (`us_id`) REFERENCES `us` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fai_us`
--

LOCK TABLES `fai_us` WRITE;
/*!40000 ALTER TABLE `fai_us` DISABLE KEYS */;
/*!40000 ALTER TABLE `fai_us` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fai_zpwg_images`
--

DROP TABLE IF EXISTS `fai_zpwg_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fai_zpwg_images` (
  `id` varchar(36) NOT NULL,
  `zpwg_images_id` mediumint(8) unsigned NOT NULL,
  `fai_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `fai_zpwg_images_UNI1` (`zpwg_images_id`,`fai_id`),
  KEY `fai_zpwg_images_fai_id_idx` (`fai_id`),
  CONSTRAINT `fai_zpwg_images_fai_id` FOREIGN KEY (`fai_id`) REFERENCES `fai` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fai_zpwg_images_zpwg_images_id` FOREIGN KEY (`zpwg_images_id`) REFERENCES `zpwg_images` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fai_zpwg_images`
--

LOCK TABLES `fai_zpwg_images` WRITE;
/*!40000 ALTER TABLE `fai_zpwg_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `fai_zpwg_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `fonction`
--

DROP TABLE IF EXISTS `fonction`;
/*!50001 DROP VIEW IF EXISTS `fonction`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `fonction` (
  `id` tinyint NOT NULL,
  `fonction` tinyint NOT NULL,
  `visible` tinyint NOT NULL,
  `ordre` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `forme`
--

DROP TABLE IF EXISTS `forme`;
/*!50001 DROP VIEW IF EXISTS `forme`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `forme` (
  `id` tinyint NOT NULL,
  `description` tinyint NOT NULL,
  `visible` tinyint NOT NULL,
  `ordre` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `fsn_categorie`
--

DROP TABLE IF EXISTS `fsn_categorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fsn_categorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categorie_object` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `categorie_type` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `categorie_key` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `categorie_value` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `system` tinyint(1) NOT NULL DEFAULT '0',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `ordre` int(11) NOT NULL,
  `old_id` int(11) DEFAULT NULL COMMENT 'correspond à id précédente table de référence',
  `description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNI_categorie_type_categorie_key` (`categorie_type`,`categorie_key`)
) ENGINE=InnoDB AUTO_INCREMENT=2858 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Table des référenciels';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fsn_categorie`
--

LOCK TABLES `fsn_categorie` WRITE;
/*!40000 ALTER TABLE `fsn_categorie` DISABLE KEYS */;
INSERT INTO `fsn_categorie` VALUES (1,'categorie','categorie_type','categorie_type','Type de catégorie','179428',1,1,6,NULL,''),(2,'categorie','categorie_object','ea','Entité Administrative','125546',1,1,0,NULL,'Entité Administrative\r\nOrganisme Administratif\r\nUnité Archéologie'),(3,'categorie','categorie_object','sf','Site de Fouille','220910',1,1,0,NULL,''),(4,'categorie','categorie_object','oa','Opération Archéologique','284723',1,1,0,NULL,''),(5,'categorie','categorie_object','us','Unité Stratigraphique','688737',1,1,0,NULL,''),(6,'categorie','categorie_object','elementrecueilli','Elément recueilli','349258',1,1,0,NULL,''),(7,'categorie','categorie_object','intervenant','Intervenant','411543',1,1,0,NULL,''),(8,'categorie','categorie_object','intervention','Intervention','337496',1,1,0,NULL,''),(9,'categorie','categorie_object','participation','Participation','405514',1,1,0,NULL,''),(10,'categorie','categorie_object','document','Document','555739',1,1,0,NULL,''),(11,'categorie','categorie_object','fai','Fait Archéologique','271512',1,1,0,NULL,''),(12,'oa','categorie_type','oa_nature','Nature campagne de l\'Opération archéolog','442151',1,1,7,NULL,'Cette rubrique concerne uniquement les opérations non préventives'),(13,'oa','categorie_type','oa_statut','Statut de l\'Opération archéologique','104781',1,1,8,NULL,'Permet de distinguer les opérations préventives et programmées'),(15,'us','categorie_type','us_synchronisation_type','Type de Synchronisme','651433',1,1,4,NULL,'Par défaut le type est « Identité ».'),(16,'us','categorie_type','us_methodefouille','Méthode de fouille de L\'unité Stratigraphique','128184',1,1,3,NULL,''),(17,'us','categorie_type','sitefouille','Contexte de l\'unité stratigraphique','251662',1,1,11,NULL,''),(18,'us','categorie_type','us_consistance','Consistance de l\'unité stratigraphique','280846',1,1,13,NULL,''),(19,'us','categorie_type','us_superposition_type','Type de Superposition','381535',1,1,10,NULL,''),(20,'elementrecueilli','categorie_type','elementrecueilli_statut','Elément recueilli - Statut ','503698',1,1,6,NULL,''),(21,'elementrecueilli','categorie_type','elementrecueilli_etat','Elément recueilli - Etat','255388',1,1,16,NULL,''),(22,'elementrecueilli','categorie_type','elementrecueilli_classe','Elément recueilli - Classe','163844',1,1,22,NULL,'Voir les spécificités de la classe poterie : attributs supplémentaires'),(23,'elementrecueilli','categorie_type','elementrecueilli_naturematiere','Elément recueilli - Nature / Matière','215293',1,1,5,NULL,'Possibilité davoir plusieurs nature/matière dans le cas délément composite.'),(24,'elementrecueilli','categorie_type','elementrecueilli_fonction','Elément recueilli - Fonction','181379',1,1,6,NULL,''),(25,'elementrecueilli','categorie_type','elementpoterie_caracterepate','Caractèristique pâte Poterie','294336',1,1,19,NULL,''),(26,'elementrecueilli','categorie_type','elementpoterie_forme','Forme de poterie','254663',1,1,20,NULL,''),(27,'elementrecueilli','categorie_type','elementpoterie_production','Production pôterie','258460',1,1,15,NULL,''),(28,'elementrecueilli','categorie_type','elementpoterie_precisionproduction','Précision Production pôterie','266999',1,1,21,NULL,''),(29,'elementrecueilli','categorie_type','elementpoterie_classification','Classification de poterie','288586',1,1,23,NULL,''),(31,'intervenant','categorie_type','intervenant_metier','Métier Intervenants','354166',1,1,10,NULL,'Correspond à la fonction sociale de lintervenant vis-à-vis de lunité darchéologie.'),(32,'participation','categorie_type','participation_statut','Statut de Participation','645087',1,1,11,NULL,''),(33,'participation','categorie_type','participation_role','Rôlede Participation','253867',1,1,12,NULL,''),(34,'intervention','categorie_type','intervention_type','Type d\'Intervention','447249',1,1,6,NULL,'que le type de fouille :\r\n- Relevé\r\n- Enregistrement\r\n- Fouille fine\r\n'),(35,'traitement','categorie_type','traitement_type','Type de Traitement','377286',1,1,9,NULL,''),(36,'traitement','categorie_type','traitement_etat','Etat du Traitement','232155',1,1,12,NULL,''),(37,'document','categorie_type','document_type','Type de document','229893',1,1,14,NULL,''),(38,'fai','categorie_type','fai_fonction','Fait Archéologique - Fonction','282867',1,1,5,NULL,'Fonction issue du Thésaurus CNAU.'),(39,'fai','categorie_type','fai_rubrique','Fait Archéologique - Rubrique','775873',1,1,8,NULL,'Rubrique issue du Thésaurus CNAU.'),(40,'fai','categorie_type','fai_terme','Fait Archéologique - Terme','269759',1,1,17,NULL,'Terme issue du Thésaurus CNAU.'),(41,'categorie','categorie_relation','depend_de','Dépend de','165259',1,1,1,NULL,''),(42,'categorie','categorie_relation','secondaire_de','Terme secondaire de','116628',1,1,2,NULL,''),(43,'categorie','categorie_relation','remplacer_par','Terme à remplacer par','287641',1,1,3,NULL,''),(44,'categorie','categorie_relation','en_instance','Terme en instance','172325',1,1,4,NULL,''),(45,'fai','fai_fonction','voirie_amenagements','voirie aménagements','269444',0,1,8,1,NULL),(46,'fai','fai_fonction','structures_defensives_et_militaires','structures défensives et militaires','306322',0,1,7,2,NULL),(47,'fai','fai_fonction','constructions_civiles','constructions civiles','258503',0,1,2,3,NULL),(48,'fai','fai_fonction','edifices_religieux','édifices religieux','581035',0,1,3,4,NULL),(49,'fai','fai_fonction','funeraire','funéraire','383567',0,1,5,5,NULL),(50,'fai','fai_fonction','production','production','355557',0,1,6,6,NULL),(51,'fai','fai_fonction','formations_naturelles','formations naturelles','126057',0,1,4,7,NULL),(52,'fai','fai_fonction','voirie_espaces_libres','voirie, espaces libres et repères remarquables...','194073',0,0,0,1,'voirie, espaces libres et repères remarquables'),(53,'fai','fai_fonction','amenagements_des_berges_et_mouillage','aménagements des berges, du littoral et du relief, franchissements, mouillage','379334',0,0,0,2,'aménagements des berges, du littoral et du relief, franchissements, mouillage'),(54,'fai','fai_fonction','adductions_deau_et_collecteurs','adductions d\'eau et collecteurs','520639',0,0,0,3,'adductions d\'eau et collecteurs'),(55,'fai','fai_fonction','structures_defensives_et_militaires2','structures défensives et militaires','306322',0,0,0,4,'structures défensives et militaires'),(56,'fai','fai_fonction','lieux_de_sociabilite','édifices publics civils et religieux, lieux de sociabilité','291715',0,0,0,5,'édifices publics civils et religieux, lieux de sociabilité'),(57,'fai','fai_fonction','etablissements_daccueil','établissements d\'accueil (assistance, enseignements)','158182',0,0,0,6,'établissements d\'accueil (assistance, enseignements)'),(58,'fai','fai_fonction','production_commerce_et_artisanat','production, commerce et artisanat','644391',0,0,0,10,'production, commerce et artisanat'),(59,'fai','fai_fonction','habitat_prive','habitat privé','349019',0,0,0,7,'habitat privé'),(60,'fai','fai_fonction','etablissements_religieux','établissements religieux','281630',0,0,0,8,'établissements religieux'),(61,'fai','fai_fonction','funeraire2','funéraire','383567',0,0,0,9,'funéraire'),(62,'fai','fai_fonction','formations_naturelles2','formations naturelles','126057',0,0,0,11,'formations naturelles'),(63,'fai','fai_rubrique','voies','Voies','222641',0,1,0,1,NULL),(64,'fai','fai_rubrique','espaces_libres','Espaces libres','260053',0,1,0,2,NULL),(65,'fai','fai_rubrique','amenagements_des_berges_et_voies_deau','Aménagements des berges et voies d\'eau','331380',0,1,0,3,NULL),(66,'fai','fai_rubrique','amenagements_du_relief','Aménagements du relief','196961',0,1,0,4,NULL),(67,'fai','fai_rubrique','franchissements','Franchissements','361089',0,1,0,5,NULL),(68,'fai','fai_rubrique','adductions_deau','Adductions d\'eau','197694',0,1,0,6,NULL),(69,'fai','fai_rubrique','collecteurs_evacuations','Collecteurs, évacuations','239228',0,1,0,7,NULL),(70,'fai','fai_rubrique','systeme_defensif_urbain','Système défensif urbain','219373',0,1,0,8,NULL),(71,'fai','fai_rubrique','structures_fortifiees','Structures fortifiées','356134',0,1,0,9,NULL),(72,'fai','fai_rubrique','garnisons_casernements','Garnisons, casernements','352328',0,1,0,10,NULL),(73,'fai','fai_rubrique','espaces_publics_amenages','Espaces publics aménagés','754679',0,1,0,11,NULL),(74,'fai','fai_rubrique','pouvoir_civil_justice','Pouvoir civil, justice','381304',0,1,0,12,NULL),(75,'fai','fai_rubrique','education_culture','Education, culture','288448',0,1,0,13,NULL),(76,'fai','fai_rubrique','sante','Santé','148205',0,1,0,14,NULL),(77,'fai','fai_rubrique','spectacle_sport','Spectacle, sport','345174',0,1,0,15,NULL),(78,'fai','fai_rubrique','bains','Bains','318765',0,1,0,16,NULL),(79,'fai','fai_rubrique','commerce_echanges','Commerce, échanges','119311',0,1,0,17,NULL),(80,'fai','fai_rubrique','habitat_prive','Habitat privé','493638',0,1,0,18,NULL),(81,'fai','fai_rubrique','cultes_paiens','Cultes païens','334907',0,1,0,19,NULL),(82,'fai','fai_rubrique','edifices_cultuels_catholiques','Edifices cultuels catholiques','351156',0,1,0,20,NULL),(83,'fai','fai_rubrique','batiments_conventuels_ou_monastiques','Bâtiments conventuels ou monastiques','819338',0,1,0,21,NULL),(84,'fai','fai_rubrique','batiments_ecclesiastiques','Bâtiments écclésiastiques','175781',0,1,0,22,NULL),(85,'fai','fai_rubrique','cultes_autres_que_catholique','Cultes autres que catholique','251701',0,1,0,23,NULL),(86,'fai','fai_rubrique','funeraire','Funéraire','288166',0,1,0,24,NULL),(87,'fai','fai_rubrique','artisanat','Artisanat','272460',0,1,0,25,NULL),(88,'fai','fai_rubrique','agriculture_elevage','Agriculture, élevage','132629',0,1,0,26,NULL),(89,'fai','fai_rubrique','industrie','Industrie','405921',0,1,0,27,NULL),(90,'fai','fai_rubrique','extraction','Extraction','197143',0,1,0,28,NULL),(91,'fai','fai_rubrique','formations_naturelles','Formations naturelles','370638',0,1,0,29,NULL),(92,'fai','fai_terme','abattoir','abattoir','122864',0,1,0,1,' '),(93,'fai','fai_terme','abbatiale','abbatiale','396839',0,1,0,2,' '),(94,'fai','fai_terme','abbaye','abbaye','232414',0,1,0,3,' '),(95,'fai','fai_terme','abbaye_fortifiee','abbaye fortifiée','194676',0,1,0,4,' '),(96,'fai','fai_terme','abreuvoir','abreuvoir','717792',0,1,0,5,' '),(97,'fai','fai_terme','abri','abri','323659',0,0,0,6,' '),(98,'fai','fai_terme','abri_a_betail','abri à bétail','236776',0,0,0,7,' '),(99,'fai','fai_terme','abri_anti_aerien','abri anti-aérien','246814',0,0,0,8,' '),(100,'fai','fai_terme','abri_de_chantier','abri de chantier','147662',0,1,0,1829,' '),(101,'fai','fai_terme','abri_de_jardin','abri de jardin','338338',0,0,0,9,' '),(102,'fai','fai_terme','abside','abside','247238',0,0,0,10,' '),(103,'fai','fai_terme','academie_dequitation','académie d\'équitation','113904',0,0,0,11,' '),(104,'fai','fai_terme','acierie','aciérie','289421',0,0,0,12,' '),(105,'fai','fai_terme','activite_agricole','activité agricole','103221',0,0,0,13,' '),(106,'fai','fai_terme','adduction','adduction','389232',0,0,0,14,' '),(107,'fai','fai_terme','adduction_deau','adduction d\'eau','261279',0,1,0,15,' '),(108,'fai','fai_terme','agora','agora','269632',0,0,0,1686,' '),(109,'fai','fai_terme','agriculture','agriculture','409068',0,0,0,16,' '),(110,'fai','fai_terme','aire_a_ble','aire à blé','115045',0,0,0,17,' '),(111,'fai','fai_terme','aire_a_portique','aire à portique','314231',0,0,0,18,' '),(112,'fai','fai_terme','aire_amenagee','aire aménagée','306827',0,0,0,19,' '),(113,'fai','fai_terme','aire_boisee','aire boisée','659750',0,1,0,1864,' '),(114,'fai','fai_terme','aire_cultuelle','aire cultuelle','986464',0,0,0,20,' '),(115,'fai','fai_terme','aire_densilage','aire d\'ensilage','284082',0,0,0,31,' '),(116,'fai','fai_terme','aire_de_battage','aire de battage','315683',0,1,0,21,' '),(117,'fai','fai_terme','aire_de_circulation','aire de circulation','198931',0,1,0,22,' '),(118,'fai','fai_terme','aire_de_combustion','aire de combustion','217328',0,0,0,23,' '),(119,'fai','fai_terme','aire_de_construction','aire de construction','202530',0,1,0,24,'sols résultant d\'une activité de construction'),(120,'fai','fai_terme','aire_de_cremation','aire de crémation','148468',0,1,0,25,' '),(121,'fai','fai_terme','aire_de_dolia','aire de dolia','816380',0,0,0,26,' '),(122,'fai','fai_terme','aire_de_foulage','aire de foulage','366802',0,1,0,27,' '),(123,'fai','fai_terme','aire_de_mouture','aire de mouture','163691',0,1,0,1738,' '),(124,'fai','fai_terme','aire_de_sechage','aire de séchage','223609',0,1,0,28,' '),(125,'fai','fai_terme','aire_de_stockage','aire de stockage','304703',0,0,0,29,' '),(126,'fai','fai_terme','aire_de_travail','aire de travail','362722',0,1,0,30,' '),(127,'fai','fai_terme','aire_empierree','aire empierrée','197588',0,0,0,32,' '),(128,'fai','fai_terme','aire_marecageuse','aire marécageuse','418622',0,0,0,1901,' '),(129,'fai','fai_terme','alandier','alandier','223322',0,0,0,33,' '),(130,'fai','fai_terme','alimentation','alimentation','238904',0,1,0,34,' '),(131,'fai','fai_terme','alimentation_vivier','alimentation (vivier)','161383',0,0,0,35,' '),(132,'fai','fai_terme','allee','allée','239364',0,0,0,36,' '),(133,'fai','fai_terme','alliage_cuivreux','alliage cuivreux','382836',0,0,0,37,' '),(134,'fai','fai_terme','alluvions','alluvions','209569',0,1,0,38,' '),(135,'fai','fai_terme','alveole','alvéole','325080',0,0,0,39,' '),(136,'fai','fai_terme','amarrage','amarrage','129182',0,1,0,40,' '),(137,'fai','fai_terme','ambitus','ambitus','177258',0,0,0,41,' '),(138,'fai','fai_terme','amenagement_bas_de_pente','aménagement bas de pente','547583',0,0,0,42,' '),(139,'fai','fai_terme','amenagement_berge','aménagement berge','127222',0,0,0,43,' '),(140,'fai','fai_terme','amenagement_dune_voie_fluviale','aménagement d\'une voie fluviale','498106',0,0,0,1627,' '),(141,'fai','fai_terme','amenagement_de_berge','aménagement de berge','235180',0,1,0,44,' '),(142,'fai','fai_terme','amenagement_de_bras_mort','aménagement de bras mort','364162',0,0,0,45,' '),(143,'fai','fai_terme','amenagement_de_marais','aménagement de marais','110453',0,0,0,46,' '),(144,'fai','fai_terme','amenagement_de_pente','aménagement de pente','380622',0,0,0,47,' '),(145,'fai','fai_terme','amenagement_de_relief','aménagement de relief','963129',0,1,0,48,' '),(146,'fai','fai_terme','amenagement_de_source','aménagement de source','125339',0,1,0,1648,' '),(147,'fai','fai_terme','amenagement_portuaire','aménagement portuaire','899129',0,0,0,50,' '),(148,'fai','fai_terme','amenagement_zone_humide','aménagement zone humide','847982',0,0,0,51,' '),(149,'fai','fai_terme','amphitheatre','amphithéâtre','102764',0,1,0,52,' '),(150,'fai','fai_terme','amphore','amphore','162514',0,1,0,53,' '),(151,'fai','fai_terme','annexe','annexe','464739',0,0,0,54,' '),(152,'fai','fai_terme','annexe_de_lhotel_dieu','annexe de l\'hôtel-Dieu','252017',0,0,0,55,' '),(153,'fai','fai_terme','annexe_des_etuves','annexe des étuves','316034',0,0,0,56,' '),(154,'fai','fai_terme','annexe_du_palais_episcopal','annexe du palais épiscopal','149374',0,0,0,57,' '),(155,'fai','fai_terme','annexe_du_port','annexe du port','150172',0,0,0,58,' '),(156,'fai','fai_terme','anse','anse','364675',0,0,0,59,' '),(157,'fai','fai_terme','aplanissement','aplanissement','397776',0,0,0,60,' '),(158,'fai','fai_terme','apodyterium','apodyterium','221496',0,1,0,62,' '),(159,'fai','fai_terme','appartement','appartement','190675',0,0,0,63,' '),(160,'fai','fai_terme','appentis','appentis','986311',0,0,0,65,' '),(161,'fai','fai_terme','appontement','appontement','329373',0,1,0,66,' '),(162,'fai','fai_terme','aqueduc','aqueduc','221806',0,1,0,67,' '),(163,'fai','fai_terme','aqueduc_souterrain','aqueduc souterrain','263210',0,0,0,69,' '),(164,'fai','fai_terme','arasement','arasement','257576',0,1,0,70,' '),(165,'fai','fai_terme','arc','arc','214580',0,0,0,71,' '),(166,'fai','fai_terme','arc_de_triomphe','arc de triomphe','300475',0,1,0,72,' '),(167,'fai','fai_terme','arc_triomphal','arc triomphal','110774',0,1,0,1687,'réf : Fornassier (B.), 2003 : il est assuré qu\'aucun des arcs gallo-romains ne vit passer en son \"fornix\" le cortège d\'un triomphateur ; parallèlement, les caractéristiques décoratives de ces monuments mettent en valeur le caractère victorieux de Rome, ce qui permet de préférer à l\'adjectif \"honorifique\" celui de triomphal'),(168,'fai','fai_terme','arcade','arcade','291445',0,0,0,73,' '),(169,'fai','fai_terme','arche','arche','203291',0,0,0,74,' '),(170,'fai','fai_terme','arche_de_soutenement','arche de soutènement','225425',0,0,0,75,' '),(171,'fai','fai_terme','archere','archère','306945',0,0,0,76,' '),(172,'fai','fai_terme','archeveche','archevêché','701852',0,0,0,1787,' '),(173,'fai','fai_terme','archives','archives','379813',0,1,0,1711,' '),(174,'fai','fai_terme','ardoise','ardoise','227307',0,0,0,77,' '),(175,'fai','fai_terme','ardoisiere','ardoisière','249754',0,1,0,1886,' '),(176,'fai','fai_terme','arene','arène','221965',0,0,0,1723,'partie d\'édifice, voir donc l\'édifice concerné'),(177,'fai','fai_terme','argent','argent','304532',0,1,0,78,' '),(178,'fai','fai_terme','argile','argile','382725',0,1,0,79,' '),(179,'fai','fai_terme','argile_crue','argile crue','996118',0,0,0,80,' '),(180,'fai','fai_terme','arriere_cour','arrière-cour','422750',0,0,0,81,' '),(181,'fai','fai_terme','arriere_fosse','arrière-fossé','227509',0,0,0,82,' '),(182,'fai','fai_terme','arsenal','arsenal','104688',0,1,0,83,' '),(183,'fai','fai_terme','arsenal_militaire','arsenal militaire','324208',0,0,0,84,' '),(184,'fai','fai_terme','artisanat','artisanat','546815',0,0,0,85,' '),(185,'fai','fai_terme','artisanat_ceramique','artisanat céramique','170877',0,0,0,1830,' '),(186,'fai','fai_terme','artisanat_de_figurines_de_terre','artisanat de figurines de terre','627138',0,0,0,86,' '),(187,'fai','fai_terme','artisanat_de_lalimentation','artisanat de l\'alimentation','417130',0,0,0,1831,' '),(188,'fai','fai_terme','artisanat_de_los','artisanat de l\'os','251951',0,0,0,1832,' '),(189,'fai','fai_terme','artisanat_de_travail_du_fer','artisanat de travail du fer','227523',0,0,0,1833,' '),(190,'fai','fai_terme','artisanat_domestique','artisanat domestique','250142',0,1,0,87,' '),(191,'fai','fai_terme','artisanat_du_batiment','artisanat du bâtiment','379249',0,0,0,88,' '),(192,'fai','fai_terme','artisanat_du_bois','artisanat du bois','157645',0,0,0,1834,' '),(193,'fai','fai_terme','artisanat_du_cuir','artisanat du cuir','221438',0,0,0,1835,' '),(194,'fai','fai_terme','artisanat_du_metal','artisanat du métal','192619',0,0,0,1836,' '),(195,'fai','fai_terme','artisanat_du_verre','artisanat du verre','175347',0,0,0,1837,' '),(196,'fai','fai_terme','artisanat_metallurgique','artisanat métallurgique','305637',0,0,0,1838,' '),(197,'fai','fai_terme','artisanat_textile','artisanat textile','338490',0,0,0,1839,' '),(198,'fai','fai_terme','asile','asile','282570',0,0,0,89,' '),(199,'fai','fai_terme','assainissement','assainissement','150236',0,1,0,90,'à employer en rubrique 3 quand assainissement de zone marécageuse'),(200,'fai','fai_terme','assechement','assèchement','184679',0,0,0,91,' '),(201,'fai','fai_terme','atelier','atelier','378713',0,1,0,92,' '),(202,'fai','fai_terme','atelier_textile','atelier (textile)','133665',0,0,0,93,' '),(203,'fai','fai_terme','atelier_damphore','atelier d\'amphore','104671',0,0,0,94,' '),(204,'fai','fai_terme','atelier_de_bronzier','atelier de bronzier','331424',0,0,0,95,' '),(205,'fai','fai_terme','atelier_de_chaufournier','atelier de chaufournier','244082',0,0,0,96,' '),(206,'fai','fai_terme','atelier_de_dinandier','atelier de dinandier','423422',0,0,0,97,' '),(207,'fai','fai_terme','atelier_de_fondeur','atelier de fondeur','142636',0,0,0,98,' '),(208,'fai','fai_terme','atelier_de_forgeron','atelier de forgeron','581587',0,0,0,99,' '),(209,'fai','fai_terme','atelier_de_grillage_du_minerai_de_fer','atelier de grillage du minerai de fer','165420',0,0,0,1919,' '),(210,'fai','fai_terme','atelier_de_potier','atelier de potier','191769',0,1,0,100,' '),(211,'fai','fai_terme','atelier_de_sculpteur','atelier de sculpteur','295845',0,0,0,101,' '),(212,'fai','fai_terme','atelier_de_tabletterie','atelier de Tabletterie','178620',0,0,0,102,' '),(213,'fai','fai_terme','atelier_de_taille_de_pierre','atelier de taille de pierre','343597',0,1,0,103,' '),(214,'fai','fai_terme','atelier_de_teinturier','atelier de teinturier','611702',0,0,0,104,' '),(215,'fai','fai_terme','atelier_de_tuilier','atelier de tuilier','860104',0,1,0,105,' '),(216,'fai','fai_terme','atelier_de_verrier','atelier de verrier','164948',0,1,0,106,' '),(217,'fai','fai_terme','atelier_industriel','atelier industriel','126978',0,0,0,1882,' '),(218,'fai','fai_terme','atelier_mecanique','atelier mécanique','188742',0,0,0,107,' '),(219,'fai','fai_terme','atelier_monetaire','atelier monétaire','168588',0,1,0,108,' '),(220,'fai','fai_terme','atre','atre','352262',0,0,0,109,' '),(221,'fai','fai_terme','atre_cheminee','atre-cheminée','208659',0,0,0,110,' '),(222,'fai','fai_terme','atrium','atrium','257131',0,1,0,111,'cour entourée de portiques sur au moins trois côtés et située devant l\'entrée principale de l\'église (J.-C. Picard)'),(223,'fai','fai_terme','auberge','auberge','114674',0,1,0,112,' '),(224,'fai','fai_terme','auditoire','auditoire','291764',0,0,0,113,' '),(225,'fai','fai_terme','aula','aula','520070',0,1,0,114,' '),(226,'fai','fai_terme','aumonerie','aumônerie','276324',0,1,0,115,' '),(227,'fai','fai_terme','autel','autel','837258',0,0,0,116,' '),(228,'fai','fai_terme','autel_domestique','autel domestique','827158',0,1,0,1739,' '),(229,'fai','fai_terme','autel_funeraire','autel funéraire','424061',0,1,0,1796,' '),(230,'fai','fai_terme','autel_votif','autel votif','326333',0,0,0,1761,' '),(231,'fai','fai_terme','avant_corps','avant-corps','544720',0,0,0,117,' '),(232,'fai','fai_terme','avant_cour','avant-cour','306025',0,0,0,118,' '),(233,'fai','fai_terme','avant_mur','avant-mur','710394',0,0,0,119,' '),(234,'fai','fai_terme','avant_nef','avant-nef','201390',0,0,0,120,' '),(235,'fai','fai_terme','avant_port','avant-port','139100',0,0,0,121,' '),(236,'fai','fai_terme','avenue','avenue','635015',0,0,0,122,' '),(237,'fai','fai_terme','bac','bac','474983',0,1,0,123,' '),(238,'fai','fai_terme','bac_a_argile','bac à argile','384414',0,0,0,124,' '),(239,'fai','fai_terme','bac_a_chaux','bac à chaux','119818',0,1,0,125,' '),(240,'fai','fai_terme','bac_a_decantation','bac à décantation','283748',0,0,0,126,' '),(241,'fai','fai_terme','bac_a_fritte','bac à fritte','238440',0,1,0,127,' '),(242,'fai','fai_terme','bac_a_mortier','bac à mortier','290955',0,0,0,128,' '),(243,'fai','fai_terme','bac_a_pigment','bac à pigment','422160',0,1,0,129,' '),(244,'fai','fai_terme','bac_de_traitement_de_largile','bac de traitement de l\'argile','428806',0,0,0,130,' '),(245,'fai','fai_terme','baignoire','baignoire','271073',0,0,0,131,' '),(246,'fai','fai_terme','bain','bain','388050',0,0,0,132,' '),(247,'fai','fai_terme','bain_rituel','bain rituel','267018',0,1,0,133,' '),(248,'fai','fai_terme','bains','bains','354411',0,1,0,134,' '),(249,'fai','fai_terme','bains_prives','bains privés','139405',0,1,0,135,' '),(250,'fai','fai_terme','bains_publics','bains publics','381870',0,0,0,136,' '),(251,'fai','fai_terme','bains_douches','bains-douches','249134',0,0,0,137,' '),(252,'fai','fai_terme','balneaire','balnéaire','793498',0,0,0,138,' '),(253,'fai','fai_terme','balneum','balneum','393437',0,0,0,139,' '),(254,'fai','fai_terme','banc_de_sable','banc de sable','161113',0,0,0,140,' '),(255,'fai','fai_terme','banquette','banquette','234169',0,0,0,141,' '),(256,'fai','fai_terme','baptistere','baptistère','230132',0,1,0,142,' '),(257,'fai','fai_terme','baraquement','baraquement','410064',0,1,0,143,'logement des soldats'),(258,'fai','fai_terme','barbacane','barbacane','302645',0,1,0,144,' '),(259,'fai','fai_terme','barrage','barrage','158913',0,1,0,145,' '),(260,'fai','fai_terme','barriere','barrière','299687',0,0,0,146,' '),(261,'fai','fai_terme','bas_fourneau','bas-fourneau','986565',0,1,0,147,' '),(262,'fai','fai_terme','bas_foyer','bas-foyer','172762',0,0,0,148,' '),(263,'fai','fai_terme','bas_foyer_metallurgique','bas-foyer métallurgique','285642',0,0,0,149,' '),(264,'fai','fai_terme','basilica','basilica','216704',0,0,0,150,' '),(265,'fai','fai_terme','basilique','basilique','152526',0,1,0,151,' '),(266,'fai','fai_terme','basilique_funeraire','basilique funéraire','740311',0,1,0,152,' '),(267,'fai','fai_terme','basse_cour','basse-cour','170998',0,1,0,153,' '),(268,'fai','fai_terme','bassin','bassin','163814',0,1,0,154,' '),(269,'fai','fai_terme','bassin_a_chaux','bassin à chaux','348496',0,0,0,155,' '),(270,'fai','fai_terme','bassin_cultuel','bassin cultuel','128806',0,1,0,1762,' '),(271,'fai','fai_terme','bassin_dagrement','bassin d\'agrément','942112',0,0,0,156,' '),(272,'fai','fai_terme','bassin_dargile','bassin d\'argile','359894',0,0,0,157,' '),(273,'fai','fai_terme','bassin_datrium','bassin d\'atrium','174797',0,0,0,1649,' '),(274,'fai','fai_terme','bassin_de_captage','bassin de captage','314045',0,0,0,1650,' '),(275,'fai','fai_terme','bassin_de_decantation','bassin de décantation','405152',0,1,0,158,' '),(276,'fai','fai_terme','bassin_de_drainage','bassin de drainage','412729',0,0,0,159,' '),(277,'fai','fai_terme','bassin_de_foulon','bassin de foulon','102629',0,0,0,160,' '),(278,'fai','fai_terme','bassin_de_purification','bassin de purification','285553',0,0,0,1793,' '),(279,'fai','fai_terme','bassin_de_stockage','bassin de stockage','276483',0,0,0,161,' '),(280,'fai','fai_terme','bassin_de_tanneur','bassin de tanneur','846795',0,0,0,162,' '),(281,'fai','fai_terme','bassin_froid','bassin froid','219939',0,0,0,1740,' '),(282,'fai','fai_terme','bassin_impluvium','bassin impluvium','298532',0,0,0,163,' '),(283,'fai','fai_terme','bastille','bastille','283077',0,1,0,164,' '),(284,'fai','fai_terme','bastion','bastion','397096',0,1,0,165,' '),(285,'fai','fai_terme','bastion_dartillerie','bastion d\'artillerie','335113',0,0,0,1662,' '),(286,'fai','fai_terme','bastionnage','bastionnage','416341',0,0,0,166,' '),(287,'fai','fai_terme','batardeau','batardeau','198922',0,1,0,167,' '),(288,'fai','fai_terme','bateau','bateau','279161',0,0,0,168,' '),(289,'fai','fai_terme','bati_de_machine','bâti de machine','266650',0,0,0,169,' '),(290,'fai','fai_terme','batiment','bâtiment','267325',0,0,0,170,' '),(291,'fai','fai_terme','batiment_a_galeries','bâtiment à galeries','359321',0,0,0,171,' '),(292,'fai','fai_terme','batiment_a_portique','bâtiment à portique','320511',0,0,0,172,' '),(293,'fai','fai_terme','batiment_agricole','bâtiment agricole','341116',0,1,0,173,' '),(294,'fai','fai_terme','batiment_annexe','bâtiment annexe','353035',0,0,0,174,' '),(295,'fai','fai_terme','batiment_artisanal','bâtiment artisanal','203980',0,1,0,175,' '),(296,'fai','fai_terme','batiment_canonial','bâtiment canonial','728119',0,0,0,176,' '),(297,'fai','fai_terme','batiment_commercial','bâtiment commercial','398441',0,1,0,177,' '),(298,'fai','fai_terme','batiment_conventuel','bâtiment conventuel','530914',0,0,0,178,' '),(299,'fai','fai_terme','batiment_cultuel','bâtiment cultuel','206254',0,0,0,1763,' '),(300,'fai','fai_terme','batiment_culturel','bâtiment culturel','285790',0,0,0,1712,' '),(301,'fai','fai_terme','batiment_de_service','bâtiment de service','870023',0,0,0,179,' '),(302,'fai','fai_terme','batiment_ecclesiastique','bâtiment ecclésiastique','157329',0,1,0,180,' '),(303,'fai','fai_terme','batiment_fortifie','bâtiment fortifié','381083',0,0,0,181,' '),(304,'fai','fai_terme','batiment_funeraire','bâtiment funéraire','214231',0,0,0,182,' '),(305,'fai','fai_terme','batiment_industriel','bâtiment industriel','188513',0,1,0,183,' '),(306,'fai','fai_terme','batiment_militaire','bâtiment militaire','115951',0,1,0,1677,' '),(307,'fai','fai_terme','batiment_monastique','bâtiment monastique','340042',0,0,0,184,' '),(308,'fai','fai_terme','batiment_prioral','bâtiment prioral','520608',0,0,0,185,' '),(309,'fai','fai_terme','batiment_public','bâtiment public','406706',0,0,0,186,' '),(310,'fai','fai_terme','batiments_canoniaux','bâtiments canoniaux','282789',0,0,0,1773,' '),(311,'fai','fai_terme','batterie','batterie','249395',0,1,0,187,' '),(312,'fai','fai_terme','beffroi','beffroi','134618',0,1,0,188,' '),(313,'fai','fai_terme','beguinage','béguinage','109366',0,1,0,1774,' '),(314,'fai','fai_terme','berge','berge','176534',0,0,0,189,' '),(315,'fai','fai_terme','berge_amenagee','berge aménagée','968211',0,0,0,190,' '),(316,'fai','fai_terme','berge_non_amenagee','berge non aménagée','252600',0,0,0,1902,' '),(317,'fai','fai_terme','bergerie','bergerie','244848',0,1,0,191,' '),(318,'fai','fai_terme','berme','berme','248033',0,1,0,1663,'espace compris entre le pied d\'un rempart et l\'escarpe d\'un fossé'),(319,'fai','fai_terme','bibliotheque','bibliothèque','235008',0,1,0,192,' '),(320,'fai','fai_terme','bief','bief','126184',0,1,0,193,' '),(321,'fai','fai_terme','bijouterie','bijouterie','213661',0,0,0,194,' '),(322,'fai','fai_terme','blanchisserie','blanchisserie','234195',0,1,0,195,' '),(323,'fai','fai_terme','bois','bois','239623',0,1,0,196,' '),(324,'fai','fai_terme','bois_de_cerf','bois de cerf','150029',0,0,0,197,' '),(325,'fai','fai_terme','bois_de_cervide','bois de cervidé','372279',0,0,0,198,' '),(326,'fai','fai_terme','borne','borne','361171',0,1,0,199,' '),(327,'fai','fai_terme','borne_milliaire','borne milliaire','301886',0,0,0,1618,' '),(328,'fai','fai_terme','borne_protege_essieu','borne protège essieu','418343',0,0,0,200,' '),(329,'fai','fai_terme','borne_fontaine','borne-fontaine','128792',0,0,0,201,' '),(330,'fai','fai_terme','boucherie','boucherie','359552',0,1,0,202,' '),(331,'fai','fai_terme','boucherie_communale','boucherie communale','304003',0,0,0,203,' '),(332,'fai','fai_terme','boulanger','boulanger','336407',0,0,0,204,' '),(333,'fai','fai_terme','boulangerie','boulangerie','256511',0,1,0,205,' '),(334,'fai','fai_terme','bouleuterion','bouleuterion','429091',0,0,0,1691,' '),(335,'fai','fai_terme','boulevard','boulevard','156789',0,1,0,206,' '),(336,'fai','fai_terme','boulevard_dartillerie','boulevard d\'artillerie','417737',0,0,0,207,' '),(337,'fai','fai_terme','bourellerie','bourellerie','106549',0,1,0,208,' '),(338,'fai','fai_terme','boutique','boutique','270337',0,1,0,209,' '),(339,'fai','fai_terme','braie','braie','605314',0,0,0,210,' '),(340,'fai','fai_terme','brancard','brancard','159186',0,1,0,211,' '),(341,'fai','fai_terme','bras_deau','bras d\'eau','183518',0,0,0,212,' '),(342,'fai','fai_terme','brasserie','brasserie','306226',0,1,0,213,' '),(343,'fai','fai_terme','brasseur','brasseur','705979',0,0,0,214,' '),(344,'fai','fai_terme','brique','brique','190914',0,0,0,215,' '),(345,'fai','fai_terme','briqueterie','briqueterie','971551',0,1,0,216,' '),(346,'fai','fai_terme','bronze','bronze','320292',0,1,0,217,' '),(347,'fai','fai_terme','bronzier','bronzier','306596',0,0,0,218,' '),(348,'fai','fai_terme','buanderie','buanderie','351975',0,0,0,219,' '),(349,'fai','fai_terme','bucher','bûcher','310330',0,1,0,220,' '),(350,'fai','fai_terme','bucher_funeraire','bûcher funéraire','188806',0,0,0,221,' '),(351,'fai','fai_terme','bustum','bustum','239843',0,1,0,222,'lieu où l\'on brûle et ensevelit un corps'),(352,'fai','fai_terme','butte','butte','154785',0,0,0,223,' '),(353,'fai','fai_terme','cabane','cabane','223850',0,1,0,224,' '),(354,'fai','fai_terme','cabane_enterree','cabane enterrée','382164',0,0,0,225,' '),(355,'fai','fai_terme','cabane_depotoir','cabane-dépotoir','203562',0,0,0,226,' '),(356,'fai','fai_terme','cabinet_de_lecture','cabinet de lecture','232309',0,0,0,1714,' '),(357,'fai','fai_terme','cadastration','cadastration','354643',0,1,0,1614,' '),(358,'fai','fai_terme','cafe','café','256149',0,0,0,227,' '),(359,'fai','fai_terme','cailloutis','cailloutis','126783',0,0,0,228,' '),(360,'fai','fai_terme','caisson','caisson','401253',0,0,0,229,' '),(361,'fai','fai_terme','caisson_maconne','caisson maçonné','123220',0,0,0,230,' '),(362,'fai','fai_terme','caisson_non_maconne','caisson non maçonné','251045',0,0,0,231,' '),(363,'fai','fai_terme','calade','calade','267362',0,1,0,232,'voie ou aire aménagée en galets damés'),(364,'fai','fai_terme','calcaire','calcaire','139972',0,1,0,233,' '),(365,'fai','fai_terme','caldarium','caldarium','668600',0,1,0,234,' '),(366,'fai','fai_terme','cale','cale','304208',0,1,0,235,' '),(367,'fai','fai_terme','cale_de_halage','cale de halage','229283',0,0,0,236,' '),(368,'fai','fai_terme','calvaire','calvaire','421829',0,1,0,237,' '),(369,'fai','fai_terme','camp','camp','324771',0,1,0,238,' '),(370,'fai','fai_terme','camp_militaire','camp militaire','479912',0,0,0,239,' '),(371,'fai','fai_terme','campus','campus','263463',0,1,0,1724,'def. d\'A. Bouet, Caesarodunum, 35-36, 2001-2002 : aire découverte ceinte d\'un mur, comprenant parfois des portiques et un bassin pour la natation, espace à fonctions multiples, entraînement, sport, terrain d\'exercice. Des thermes peuvent également y être associés.'),(372,'fai','fai_terme','canal','canal','378339',0,1,0,240,'définition : voie d\'eau artificielle qui sert à l\'irrigation, au drainage ou à la navigation'),(373,'fai','fai_terme','canal_devacuation','canal d\'évacuation','234501',0,0,0,243,' '),(374,'fai','fai_terme','canal_dirrigation','canal d\'irrigation','303444',0,0,0,1865,' '),(375,'fai','fai_terme','canal_de_drainage','canal de drainage','382888',0,0,0,241,' '),(376,'fai','fai_terme','canal_de_force','Canal de force','425908',0,0,0,242,' '),(377,'fai','fai_terme','canal_maconne','canal maçonné','111973',0,0,0,244,' '),(378,'fai','fai_terme','canal_souterrain','canal souterrain','387905',0,1,0,1628,' '),(379,'fai','fai_terme','canalisation','canalisation','179201',0,1,0,245,' '),(380,'fai','fai_terme','canalisation_dun_cours_deau','canalisation d\'un cours d\'eau','281640',0,0,0,246,' '),(381,'fai','fai_terme','canalisation_dune_riviere','canalisation d\'une rivière','751648',0,0,0,1629,' '),(382,'fai','fai_terme','canaux_de_drainage','canaux de drainage','111563',0,0,0,1658,' '),(383,'fai','fai_terme','caniveau','caniveau','396954',0,1,0,247,' '),(384,'fai','fai_terme','canonniere','canonnière','402504',0,0,0,248,' '),(385,'fai','fai_terme','cantonnement','cantonnement','129252',0,0,0,249,' '),(386,'fai','fai_terme','caponniere','caponnière','234748',0,1,0,250,' '),(387,'fai','fai_terme','captage','captage','280836',0,1,0,251,' '),(388,'fai','fai_terme','captage_de_source','captage de source','352571',0,0,0,252,' '),(389,'fai','fai_terme','cardo','cardo','281216',0,1,0,253,' '),(390,'fai','fai_terme','cargaison','cargaison','218430',0,1,0,1733,' '),(391,'fai','fai_terme','cargaison_de_verrerie','cargaison de verrerie','365610',0,0,0,254,' '),(392,'fai','fai_terme','carrefour','carrefour','112277',0,0,0,255,' '),(393,'fai','fai_terme','carriere','carrière','278263',0,1,0,256,' '),(394,'fai','fai_terme','carriere_souterraine','carrière souterraine','149158',0,0,0,1887,' '),(395,'fai','fai_terme','case','case','201382',0,1,0,257,' '),(396,'fai','fai_terme','case_negre','case nègre','207734',0,0,0,258,' '),(397,'fai','fai_terme','casemate','casemate','100985',0,1,0,259,' '),(398,'fai','fai_terme','caserne','caserne','335148',0,1,0,260,' '),(399,'fai','fai_terme','caserne_de_pompiers','caserne de pompiers','256895',0,0,0,261,' '),(400,'fai','fai_terme','casernement','casernement','426436',0,0,0,262,' '),(401,'fai','fai_terme','castellum','castellum','352646',0,0,0,1664,' '),(402,'fai','fai_terme','castellum_divisiorum','castellum divisiorum','164781',0,0,0,1651,' '),(403,'fai','fai_terme','castrum','castrum','229086',0,0,0,263,' '),(404,'fai','fai_terme','cathedrale','cathédrale','377817',0,1,0,264,' '),(405,'fai','fai_terme','cavalier','cavalier','429245',0,0,0,265,' '),(406,'fai','fai_terme','cave','cave','922371',0,1,0,266,' '),(407,'fai','fai_terme','cave_a_vin','cave à vin','110282',0,0,0,267,' '),(408,'fai','fai_terme','cave_viticole','cave viticole','110904',0,0,0,268,' '),(409,'fai','fai_terme','cave_depotoir','cave-dépotoir','939710',0,0,0,275,' '),(410,'fai','fai_terme','cave_murissoir','cave-murissoir','836958',0,0,0,276,' '),(411,'fai','fai_terme','cave_silo','cave-silo','153925',0,0,0,278,' '),(412,'fai','fai_terme','cavea','cavea','391601',0,0,0,269,'partie d\'édifice voir donc l\'édifice concerné'),(413,'fai','fai_terme','caveau','caveau','334192',0,1,0,270,' '),(414,'fai','fai_terme','caveau_collectif','caveau collectif','285946',0,0,0,271,' '),(415,'fai','fai_terme','caveau_en_bois','caveau en bois','377219',0,0,0,272,' '),(416,'fai','fai_terme','caveau_maconne','caveau maçonné','404067',0,0,0,273,' '),(417,'fai','fai_terme','caveau_ossuaire','caveau-ossuaire','110127',0,0,0,274,' '),(418,'fai','fai_terme','caves','caves','449888',0,0,0,277,' '),(419,'fai','fai_terme','cavite_a_offrandes','cavité à offrandes','174505',0,1,0,1764,' '),(420,'fai','fai_terme','cella','cella','279292',0,1,0,279,' '),(421,'fai','fai_terme','cellier','cellier','301562',0,1,0,280,' '),(422,'fai','fai_terme','cellule','cellule','410333',0,1,0,281,' '),(423,'fai','fai_terme','cendres_volcaniques','cendres volcaniques','212360',0,1,0,282,' '),(424,'fai','fai_terme','cendrier','cendrier','262076',0,0,0,283,' '),(425,'fai','fai_terme','cenotaphe','cénotaphe','223112',0,1,0,284,' '),(426,'fai','fai_terme','centrale_electrique','centrale électrique','281503',0,0,0,285,' '),(427,'fai','fai_terme','ceramique','céramique','121438',0,1,0,286,' '),(428,'fai','fai_terme','ceramique_architecturale','céramique architecturale','527985',0,0,0,287,' '),(429,'fai','fai_terme','ceramique_commune','céramique commune','302535',0,0,0,288,' '),(430,'fai','fai_terme','ceramique_domestique','céramique domestique','620028',0,0,0,289,' '),(431,'fai','fai_terme','cercueil','cercueil','255787',0,1,0,290,' '),(432,'fai','fai_terme','cercueil_cloue','cercueil cloué','276876',0,0,0,1798,' '),(433,'fai','fai_terme','cercueil_de_bois','cercueil de bois','240937',0,1,0,291,' '),(434,'fai','fai_terme','cercueil_de_plomb','cercueil de plomb','301482',0,1,0,292,' '),(435,'fai','fai_terme','cercueil_de_zinc','cercueil de zinc','127996',0,0,0,293,' '),(436,'fai','fai_terme','cercueil_plomb','cercueil plomb','156268',0,0,0,294,' '),(437,'fai','fai_terme','chai','chai','110861',0,1,0,295,' '),(438,'fai','fai_terme','chais','chais','352350',0,0,0,296,' '),(439,'fai','fai_terme','chambre_de_tir','chambre de tir','989369',0,0,0,297,' '),(440,'fai','fai_terme','chambre_des_comptes','chambre des comptes','377178',0,0,0,298,' '),(441,'fai','fai_terme','chambre_funeraire','chambre funéraire','135640',0,1,0,1799,' '),(442,'fai','fai_terme','chambre_souterraine','chambre souterraine','119172',0,0,0,299,' '),(443,'fai','fai_terme','champ','champ','794943',0,1,0,300,' '),(444,'fai','fai_terme','champ_durne','champ d\'urne','324808',0,0,0,1800,' '),(445,'fai','fai_terme','champ_de_foire','champ de foire','297978',0,1,0,301,' '),(446,'fai','fai_terme','champ_de_manoeuvre','champ de manoeuvre','620298',0,1,0,302,' '),(447,'fai','fai_terme','champ_de_mars','champ de mars','263145',0,0,0,1678,' '),(448,'fai','fai_terme','champ_de_tir','champ de tir','946146',0,0,0,303,' '),(449,'fai','fai_terme','chancellerie','chancellerie','318658',0,1,0,1692,' '),(450,'fai','fai_terme','chantier','chantier','166822',0,0,0,304,' '),(451,'fai','fai_terme','chantier_de_construction','chantier de construction','223530',0,0,0,1840,' '),(452,'fai','fai_terme','chantier_de_construction_cathedrale','chantier de construction, cathédrale','243979',0,0,0,305,' '),(453,'fai','fai_terme','chantier_naval','chantier naval','380148',0,0,0,306,' '),(454,'fai','fai_terme','chapelle','chapelle','126327',0,1,0,307,' '),(455,'fai','fai_terme','chapelle_castrale','chapelle castrale','399188',0,1,0,308,' '),(456,'fai','fai_terme','chapelle_cemeteriale','chapelle cémétériale','781795',0,0,0,309,' '),(457,'fai','fai_terme','chapelle_conventuelle','chapelle conventuelle','296696',0,1,0,310,' '),(458,'fai','fai_terme','chapelle_funeraire','chapelle funéraire','315079',0,1,0,311,'chapelle à usage privé ou corporatif'),(459,'fai','fai_terme','chapelle_laterale','chapelle latérale','257833',0,0,0,312,' '),(460,'fai','fai_terme','chapelle_paroissiale','chapelle paroissiale','284728',0,0,0,313,' '),(461,'fai','fai_terme','chapelle_prieurale','chapelle prieurale','275403',0,0,0,314,' '),(462,'fai','fai_terme','chapellerie','chapellerie','295028',0,0,0,315,' '),(463,'fai','fai_terme','chapitre','chapitre','235527',0,0,0,316,' '),(464,'fai','fai_terme','chapitre_episcopal','chapitre épiscopal','361786',0,0,0,1775,' '),(465,'fai','fai_terme','charbonniere','charbonnière','169006',0,0,0,317,' '),(466,'fai','fai_terme','charcuterie','charcuterie','420095',0,1,0,318,' '),(467,'fai','fai_terme','charnier','charnier','419193',0,1,0,319,' '),(468,'fai','fai_terme','charpente','charpente','707449',0,1,0,320,' '),(469,'fai','fai_terme','chateau','château','389730',0,0,0,321,' '),(470,'fai','fai_terme','chateau_deau','château d\'eau','410976',0,1,0,322,' '),(471,'fai','fai_terme','chateau_fort','château fort','246580',0,1,0,323,'si château résidence, employer en 12 ou 18'),(472,'fai','fai_terme','chatelet','châtelet','659390',0,1,0,325,' '),(473,'fai','fai_terme','chaudiere','chaudière','150286',0,1,0,326,' '),(474,'fai','fai_terme','chaudronnerie','chaudronnerie','120349',0,1,0,327,' '),(475,'fai','fai_terme','chaufferie','chaufferie','225268',0,0,0,328,' '),(476,'fai','fai_terme','chauffoir','chauffoir','307492',0,1,0,329,' '),(477,'fai','fai_terme','chaussee','chaussée','377145',0,1,0,330,' '),(478,'fai','fai_terme','chaux','chaux','274529',0,0,0,331,' '),(479,'fai','fai_terme','chemin','chemin','329057',0,1,0,332,' '),(480,'fai','fai_terme','chemin_couvert','chemin couvert','646238',0,0,0,333,' '),(481,'fai','fai_terme','chemin_de_halage','chemin de halage','405047',0,0,0,334,' '),(482,'fai','fai_terme','chemin_de_ronde','chemin de ronde','397042',0,0,0,335,' '),(483,'fai','fai_terme','cheminee','cheminée','193162',0,1,0,336,' '),(484,'fai','fai_terme','chenal','chenal','376985',0,1,0,337,'def : un canal mais naturel ou peu aménagé : passage navigable, chemin de navigation repéré et balisé entre les hauts-fonds ; ligne des bas-fonds ...'),(485,'fai','fai_terme','chenal_decoulement','chenal d\'écoulement','256808',0,0,0,339,' '),(486,'fai','fai_terme','chenal_de_riviere','chenal de rivière','404632',0,0,0,338,' '),(487,'fai','fai_terme','cheneviere','chènevière','328928',0,0,0,340,' '),(488,'fai','fai_terme','chevet','chevet','224020',0,0,0,341,' '),(489,'fai','fai_terme','choeur','choeur','147603',0,0,0,342,' '),(490,'fai','fai_terme','cimetiere','cimetière','740929',0,1,0,343,'lieu d\'inhumation à partir du Moyen Age et jusqu\'à nos jours'),(491,'fai','fai_terme','cimetiere_abbatial','cimetière abbatial','921399',0,0,0,344,' '),(492,'fai','fai_terme','cimetiere_canonial','cimetière canonial','299373',0,0,0,345,' '),(493,'fai','fai_terme','cimetiere_claustral','cimetière claustral','172229',0,0,0,346,' '),(494,'fai','fai_terme','cimetiere_communal','cimetière communal','262223',0,0,0,347,' '),(495,'fai','fai_terme','cimetiere_conventuel','cimetière conventuel','109157',0,0,0,348,' '),(496,'fai','fai_terme','cimetiere_denfants','cimetière d\'enfants','233454',0,1,0,353,' '),(497,'fai','fai_terme','cimetiere_detablissement_hospitalier','cimetière d\'établissement hospitalier','422384',0,1,0,354,' '),(498,'fai','fai_terme','cimetiere_dhopital','cimetière d\'hôpital','301642',0,0,0,355,' '),(499,'fai','fai_terme','cimetiere_de_communaute','cimetière de communauté','917795',0,0,0,349,' '),(500,'fai','fai_terme','cimetiere_de_communaute_canonial','cimetière de communauté canonial','353232',0,0,0,1801,' '),(501,'fai','fai_terme','cimetiere_de_communaute_religieuse','cimetière de communauté religieuse','282774',0,1,0,350,' '),(502,'fai','fai_terme','cimetiere_de_leproserie','cimetière de léproserie','246263',0,1,0,351,' '),(503,'fai','fai_terme','cimetiere_de_mendiants','cimetière de mendiants','363202',0,0,0,352,' '),(504,'fai','fai_terme','cimetiere_du_couvent','cimetière du couvent','167451',0,0,0,1802,' '),(505,'fai','fai_terme','cimetiere_habite','cimetière habité','403704',0,0,0,356,' '),(506,'fai','fai_terme','cimetiere_juif','cimetière juif','815720',0,1,0,357,' '),(507,'fai','fai_terme','cimetiere_militaire','cimetière militaire','787389',0,1,0,358,' '),(508,'fai','fai_terme','cimetiere_monastique','cimetière monastique','248482',0,0,0,359,' '),(509,'fai','fai_terme','cimetiere_paroissial','cimetière paroissial','267047',0,1,0,360,' '),(510,'fai','fai_terme','cimetiere_prieural','cimetière prieural','360164',0,0,0,362,' '),(511,'fai','fai_terme','cimetiere_protestant','cimetière protestant','417677',0,1,0,363,' '),(512,'fai','fai_terme','cippe','cippe','411616',0,1,0,364,' '),(513,'fai','fai_terme','cippe_pomerial','cippe pomérial','300661',0,0,0,365,' '),(514,'fai','fai_terme','cirque','cirque','134360',0,1,0,366,' '),(515,'fai','fai_terme','ciste','ciste','212459',0,0,0,367,' '),(516,'fai','fai_terme','citadelle','citadelle','112167',0,1,0,368,' '),(517,'fai','fai_terme','citerne','citerne','257851',0,1,0,369,' '),(518,'fai','fai_terme','clayonnage','clayonnage','155324',0,1,0,370,' '),(519,'fai','fai_terme','clinique','clinique','359412',0,0,0,371,' '),(520,'fai','fai_terme','clocher','clocher','232685',0,1,0,372,' '),(521,'fai','fai_terme','clocher_donjon','clocher-donjon','397126',0,1,0,1674,' '),(522,'fai','fai_terme','clocher_porche','clocher-porche','311207',0,0,0,373,' '),(523,'fai','fai_terme','cloitre','cloître','427572',0,1,0,375,' '),(524,'fai','fai_terme','cloitre_a_galerie','cloître à galerie','319924',0,0,0,376,' '),(525,'fai','fai_terme','cloitre_a_galeries','cloître à galeries','367487',0,0,0,377,' '),(526,'fai','fai_terme','cloitre_canonial','cloître canonial','422818',0,1,0,378,' '),(527,'fai','fai_terme','cloitre_cathedral','cloître cathédral','382367',0,0,0,379,' '),(528,'fai','fai_terme','cloitre_episcopal','cloître épiscopal','108563',0,0,0,380,' '),(529,'fai','fai_terme','cloture','clôture','248077',0,1,0,381,' '),(530,'fai','fai_terme','coffrage','coffrage','287345',0,0,0,382,' '),(531,'fai','fai_terme','coffrage_de_bois','coffrage de bois','423685',0,0,0,383,' '),(532,'fai','fai_terme','coffrage_de_pierres','coffrage de pierres','277721',0,0,0,385,' '),(533,'fai','fai_terme','coffrage_en_platre','coffrage en plâtre','302377',0,0,0,1803,' '),(534,'fai','fai_terme','coffre','coffre','144327',0,1,0,387,' '),(535,'fai','fai_terme','coffre_dardoises','coffre d\'ardoises','305497',0,0,0,388,' '),(536,'fai','fai_terme','coffre_de_bois','coffre de bois','589439',0,1,0,389,' '),(537,'fai','fai_terme','coffre_de_briques','coffre de briques','259337',0,0,0,390,' '),(538,'fai','fai_terme','coffre_de_dalles','coffre de dalles','379028',0,1,0,391,' '),(539,'fai','fai_terme','coffre_de_lauzes','coffre de lauzes','348119',0,0,0,392,' '),(540,'fai','fai_terme','coffre_de_pierres','coffre de pierres','114393',0,0,0,393,' '),(541,'fai','fai_terme','coffre_de_platre','coffre de plâtre','940248',0,0,0,394,' '),(542,'fai','fai_terme','coffre_de_tegulae','coffre de tegulae','279690',0,0,0,395,' '),(543,'fai','fai_terme','coffre_de_tuiles','coffre de tuiles','308695',0,0,0,397,' '),(544,'fai','fai_terme','coffre_en_bois','coffre en bois','401937',0,0,0,398,' '),(545,'fai','fai_terme','coffre_en_briques','coffre en briques','165903',0,0,0,399,' '),(546,'fai','fai_terme','coffre_en_pierre','coffre en pierre','185115',0,0,0,400,' '),(547,'fai','fai_terme','coffre_en_tuiles','coffre en tuiles','426025',0,0,0,401,' '),(548,'fai','fai_terme','coffre_funeraire','coffre funéraire','270432',0,0,0,402,' '),(549,'fai','fai_terme','coffre_maconne','coffre maçonné','330066',0,1,0,403,' '),(550,'fai','fai_terme','coffre_non_maconne','coffre non maçonné','414468',0,0,0,1797,' '),(551,'fai','fai_terme','coffre_tegulae','coffre tegulae','157147',0,0,0,404,' '),(552,'fai','fai_terme','collecteur','collecteur','136703',0,1,0,405,' '),(553,'fai','fai_terme','collecteur_egout','collecteur-égout','986152',0,0,0,406,' '),(554,'fai','fai_terme','college','collège','325027',0,1,0,407,' '),(555,'fai','fai_terme','collegiale','collégiale','288680',0,1,0,408,' '),(556,'fai','fai_terme','colluvions','colluvions','233995',0,1,0,409,' '),(557,'fai','fai_terme','colmatage_bras_de_riviere','colmatage bras de rivière','121307',0,0,0,410,' '),(558,'fai','fai_terme','colombier','colombier','781349',0,0,0,411,' '),(559,'fai','fai_terme','colonnade','colonnade','307438',0,0,0,412,' '),(560,'fai','fai_terme','colorant','colorant','114905',0,0,0,413,' '),(561,'fai','fai_terme','comblement_detang','comblement d\'étang','168862',0,0,0,421,' '),(562,'fai','fai_terme','comblement_de_bras_deau','comblement de bras d\'eau','190182',0,0,0,414,' '),(563,'fai','fai_terme','comblement_de_fosse','comblement de fossé','376021',0,0,0,415,' '),(564,'fai','fai_terme','comblement_de_marecage','comblement de marécage','383235',0,0,0,416,' '),(565,'fai','fai_terme','comblement_de_riviere','comblement de rivière','491914',0,1,0,417,' '),(566,'fai','fai_terme','comblement_de_vallee','comblement de vallée','196542',0,1,0,418,' '),(567,'fai','fai_terme','comblement_de_vallon','comblement de vallon','114851',0,0,0,419,' '),(568,'fai','fai_terme','comblement_de_zone_marecageuse','comblement de zone marécageuse','249694',0,0,0,420,' '),(569,'fai','fai_terme','commanderie','commanderie','331302',0,1,0,422,' '),(570,'fai','fai_terme','commerce','commerce','315346',0,0,0,423,' '),(571,'fai','fai_terme','communs','communs','372663',0,1,0,424,' '),(572,'fai','fai_terme','complexe_thermal','complexe thermal','203173',0,0,0,425,' '),(573,'fai','fai_terme','conduit','conduit','425691',0,0,0,426,' '),(574,'fai','fai_terme','conduit_devacuation_des_eaux_usees','conduit d\'évacuation des eaux usées','351404',0,0,0,1659,' '),(575,'fai','fai_terme','conduit_de_chauffe','conduit de chauffe','260334',0,1,0,427,' '),(576,'fai','fai_terme','conduit_de_derivation','conduit de dérivation','867140',0,0,0,1630,' '),(577,'fai','fai_terme','conduit_souterrain_dune_riviere','conduit souterrain d\'une rivière','146159',0,0,0,1631,' '),(578,'fai','fai_terme','conduite','conduite','197292',0,0,0,428,' '),(579,'fai','fai_terme','confection','confection','437796',0,0,0,429,' '),(580,'fai','fai_terme','confection_de_chandelles','confection de chandelles','117861',0,0,0,430,' '),(581,'fai','fai_terme','confluence','confluence','709586',0,0,0,431,' '),(582,'fai','fai_terme','conservatoire','conservatoire','115803',0,0,0,432,' '),(583,'fai','fai_terme','conserverie','conserverie','264238',0,1,0,1841,' '),(584,'fai','fai_terme','construction','construction','370054',0,0,0,433,' '),(585,'fai','fai_terme','construction_navale','construction navale','927841',0,1,0,1842,' '),(586,'fai','fai_terme','contre_garde','contre-garde','339174',0,1,0,434,' '),(587,'fai','fai_terme','contreforts','contreforts','317441',0,0,0,1640,' '),(588,'fai','fai_terme','contrescarpe','contrescarpe','662960',0,1,0,435,' '),(589,'fai','fai_terme','contrevallation','contrevallation','429064',0,0,0,1665,' '),(590,'fai','fai_terme','cooperative','coopérative','339619',0,0,0,436,' '),(591,'fai','fai_terme','corail','corail','155766',0,1,0,437,' '),(592,'fai','fai_terme','cordonnerie','cordonnerie','301578',0,1,0,438,' '),(593,'fai','fai_terme','cordonnier','cordonnier','785575',0,0,0,439,' '),(594,'fai','fai_terme','corne','corne','392838',0,1,0,440,' '),(595,'fai','fai_terme','corps_de_garde','corps de garde','389961',0,0,0,441,' '),(596,'fai','fai_terme','corps_de_logis','corps de logis','620213',0,1,0,442,' '),(597,'fai','fai_terme','corps_de_place','corps de place','258187',0,0,0,443,' '),(598,'fai','fai_terme','corroyeur','corroyeur','406580',0,1,0,1843,' '),(599,'fai','fai_terme','cote','côte','336519',0,0,0,1903,' '),(600,'fai','fai_terme','couloir','couloir','362265',0,0,0,444,' '),(601,'fai','fai_terme','couloir_souterrain','couloir souterrain','132599',0,0,0,445,' '),(602,'fai','fai_terme','cour','cour','280386',0,1,0,446,' '),(603,'fai','fai_terme','cour_a_portique','cour à portique','299736',0,0,0,447,' '),(604,'fai','fai_terme','cour_de_ferme','cour de ferme','860117',0,0,0,448,' '),(605,'fai','fai_terme','cour_de_justice','cour de justice','259058',0,1,0,449,'terme utilisé jusqu\'au XVIIIe siècle, après : employer Palais de justice'),(606,'fai','fai_terme','cour_interieure','cour intérieure','224660',0,0,0,450,' '),(607,'fai','fai_terme','cours_deau','cours d\'eau','134128',0,1,0,451,' '),(608,'fai','fai_terme','cours_deau_fossile','cours d\'eau fossile','232068',0,0,0,452,' '),(609,'fai','fai_terme','courtine','courtine','422317',0,1,0,453,' '),(610,'fai','fai_terme','couture','couture','203137',0,0,0,454,' '),(611,'fai','fai_terme','couvent','couvent','114061',0,1,0,455,' '),(612,'fai','fai_terme','couvent_de_la_contre_reforme','couvent de la Contre Réforme','103737',0,0,0,1776,' '),(613,'fai','fai_terme','couvent_fortifie','couvent fortifié','384345',0,0,0,456,' '),(614,'fai','fai_terme','couvent_hospitalier','couvent hospitalier','158058',0,1,0,1719,' '),(615,'fai','fai_terme','couvent_mendiant','couvent mendiant','104595',0,1,0,1777,' '),(616,'fai','fai_terme','craie','craie','427062',0,1,0,457,' '),(617,'fai','fai_terme','crepido','crépido','151621',0,0,0,458,' '),(618,'fai','fai_terme','creusement','creusement','359807',0,1,0,459,' '),(619,'fai','fai_terme','creuset','creuset','197541',0,1,0,460,' '),(620,'fai','fai_terme','croix','croix','189452',0,1,0,1615,' '),(621,'fai','fai_terme','crypte','crypte','229833',0,1,0,461,' '),(622,'fai','fai_terme','crypto_corridor','crypto-corridor','955223',0,0,0,462,' '),(623,'fai','fai_terme','cryptoportique','cryptoportique','663584',0,1,0,463,' '),(624,'fai','fai_terme','cuir','cuir','135645',0,1,0,464,' '),(625,'fai','fai_terme','cuisine','cuisine','282405',0,1,0,465,' '),(626,'fai','fai_terme','cuivre','cuivre','940806',0,1,0,466,' '),(627,'fai','fai_terme','culte_des_eaux','culte des eaux','363037',0,1,0,467,' '),(628,'fai','fai_terme','culte_domestique','culte domestique','389950',0,0,0,1741,' '),(629,'fai','fai_terme','culte_prive','culte privé','127414',0,0,0,468,' '),(630,'fai','fai_terme','culture','culture','306456',0,0,0,469,' '),(631,'fai','fai_terme','culture_maraichere','culture maraîchère','155251',0,0,0,1866,' '),(632,'fai','fai_terme','curage','curage','244679',0,0,0,470,' '),(633,'fai','fai_terme','cure','cure','205079',0,0,0,471,' '),(634,'fai','fai_terme','curie','curie','259649',0,1,0,472,' '),(635,'fai','fai_terme','cuve','cuve','508585',0,0,0,473,' '),(636,'fai','fai_terme','cuve_foulon','cuve (foulon)','584981',0,0,0,474,' '),(637,'fai','fai_terme','cuve_tanneur','cuve (tanneur)','167931',0,0,0,475,' '),(638,'fai','fai_terme','cuve_vin_huile','cuve (vin, huile)','234634',0,0,0,476,' '),(639,'fai','fai_terme','cuve_a_chaux','cuve à chaux','192317',0,0,0,477,' '),(640,'fai','fai_terme','cuve_a_fermentation_du_vin','cuve à fermentation du vin','319887',0,0,0,478,' '),(641,'fai','fai_terme','cuve_a_vin','cuve à vin','187898',0,0,0,479,' '),(642,'fai','fai_terme','cuve_baptismale','cuve baptismale','152902',0,0,0,1768,' '),(643,'fai','fai_terme','cuve_dargile','cuve d\'argile','282555',0,0,0,480,' '),(644,'fai','fai_terme','cuve_de_decantation','cuve de décantation','360287',0,0,0,481,' '),(645,'fai','fai_terme','cuve_de_tanneur','cuve de tanneur','197166',0,1,0,482,' '),(646,'fai','fai_terme','cuve_de_teinturier','cuve de teinturier','130933',0,0,0,483,' '),(647,'fai','fai_terme','cuve_monolithe','cuve monolithe','208573',0,0,0,484,' '),(648,'fai','fai_terme','dallage','dallage','138401',0,0,0,485,' '),(649,'fai','fai_terme','dalle','dalle','262296',0,0,0,486,' '),(650,'fai','fai_terme','dalle_epigraphe','dalle épigraphe','118331',0,0,0,487,' '),(651,'fai','fai_terme','dalle_funeraire','dalle funéraire','417904',0,1,0,488,' '),(652,'fai','fai_terme','dalle_gravee','dalle gravée','106268',0,0,0,489,' '),(653,'fai','fai_terme','dalot','dalot','372102',0,0,0,490,' '),(654,'fai','fai_terme','darse','darse','120309',0,0,0,1632,' '),(655,'fai','fai_terme','deambulatoire','déambulatoire','277879',0,0,0,491,' '),(656,'fai','fai_terme','debarcadere','débarcadère','258875',0,0,0,492,' '),(657,'fai','fai_terme','debit_de_boisson','débit de boisson','117501',0,0,0,493,' '),(658,'fai','fai_terme','debitage','débitage','286355',0,0,0,494,' '),(659,'fai','fai_terme','debitage_de_silex','débitage de silex','294551',0,0,0,495,' '),(660,'fai','fai_terme','deblai','déblai','242775',0,0,0,496,' '),(661,'fai','fai_terme','decaissement','décaissement','417716',0,1,0,497,' '),(662,'fai','fai_terme','decharge','décharge','624236',0,1,0,498,' '),(663,'fai','fai_terme','decharge_organisee','décharge organisée','338358',0,0,0,499,' '),(664,'fai','fai_terme','decharge_publique','décharge publique','385673',0,0,0,1625,' '),(665,'fai','fai_terme','dechet_houiller','déchet houiller','246191',0,0,0,500,' '),(666,'fai','fai_terme','dechets_de_mine','déchets de mine','284327',0,0,0,501,' '),(667,'fai','fai_terme','decor_peint','décor peint','868472',0,0,0,502,' '),(668,'fai','fai_terme','decumanus','decumanus','276888',0,1,0,503,' '),(669,'fai','fai_terme','defense_avancee','défense avancée','362507',0,0,0,505,' '),(670,'fai','fai_terme','defrichement','défrichement','223672',0,1,0,506,' '),(671,'fai','fai_terme','dehors','dehors','443120',0,1,0,507,' '),(672,'fai','fai_terme','demeure','demeure','277018',0,0,0,508,' '),(673,'fai','fai_terme','demi_lune','demi-lune','171407',0,1,0,509,' '),(674,'fai','fai_terme','dentelliere','dentellière','320710',0,0,0,510,' '),(675,'fai','fai_terme','dependance','dépendance','337721',0,1,0,511,' '),(676,'fai','fai_terme','dependance_canoniale','dépendance canoniale','186178',0,0,0,512,' '),(677,'fai','fai_terme','dependances','dépendances','418591',0,0,0,513,' '),(678,'fai','fai_terme','depense','dépense','209374',0,0,0,514,' '),(679,'fai','fai_terme','depot','dépôt','667512',0,0,0,515,' '),(680,'fai','fai_terme','depot_cultuel','dépôt cultuel','290272',0,0,0,516,' '),(681,'fai','fai_terme','depot_dequides','dépôt d\'équidés','662032',0,0,0,520,' '),(682,'fai','fai_terme','depot_dhydrocarbures','dépôt d\'hydrocarbures','287409',0,0,0,521,' '),(683,'fai','fai_terme','depot_dossements_animaux','dépôt d\'ossements animaux','154518',0,0,0,522,' '),(684,'fai','fai_terme','depot_de_cremation','dépôt de crémation','666791',0,1,0,517,' '),(685,'fai','fai_terme','depot_de_fondation','dépôt de fondation','493923',0,1,0,518,'à mettre aussi dans le champ correspondant à la fonction de la structure concernée par le dépôt'),(686,'fai','fai_terme','depot_de_libation','dépôt de libation','234919',0,0,0,519,' '),(687,'fai','fai_terme','depot_funeraire','dépôt funéraire','349025',0,1,0,523,' '),(688,'fai','fai_terme','depot_rituel','dépôt rituel','278687',0,1,0,524,' '),(689,'fai','fai_terme','depot_votif','dépôt votif','127009',0,1,0,525,' '),(690,'fai','fai_terme','depotoir','dépotoir','208447',0,1,0,526,' '),(691,'fai','fai_terme','depotoir_surcuits_ceramiques','dépotoir (surcuits céramiques)','167620',0,0,0,527,' '),(692,'fai','fai_terme','depotoir_taille_de_pierre','dépotoir (taille de pierre)','276800',0,0,0,528,' '),(693,'fai','fai_terme','depotoir_tesselles','dépotoir (tesselles)','197551',0,0,0,529,' '),(694,'fai','fai_terme','depotoir_damphores','dépotoir d\'amphores','370426',0,0,0,530,' '),(695,'fai','fai_terme','depotoir_datelier_de_verrier','dépotoir d\'atelier de verrier','173824',0,0,0,531,' '),(696,'fai','fai_terme','depotoir_dorfevre_emailleur','dépotoir d\'orfèvre-émailleur','223987',0,0,0,551,' '),(697,'fai','fai_terme','depotoir_de_berge','dépotoir de berge','725875',0,0,0,532,' '),(698,'fai','fai_terme','depotoir_de_boucherie','dépotoir de boucherie','298992',0,0,0,533,' '),(699,'fai','fai_terme','depotoir_de_bronzier','dépotoir de bronzier','276721',0,0,0,534,' '),(700,'fai','fai_terme','depotoir_de_chaufournier','dépotoir de chaufournier','279505',0,0,0,535,' '),(701,'fai','fai_terme','depotoir_de_cordonnerie','dépotoir de cordonnerie','209097',0,0,0,536,' '),(702,'fai','fai_terme','depotoir_de_cordonnier','dépotoir de cordonnier','410767',0,0,0,537,' '),(703,'fai','fai_terme','depotoir_de_corne','dépotoir de corne','282463',0,0,0,538,' '),(704,'fai','fai_terme','depotoir_de_faiencerie','dépotoir de faïencerie','923543',0,0,0,539,' '),(705,'fai','fai_terme','depotoir_de_faiencier','dépotoir de faïencier','156938',0,0,0,540,' '),(706,'fai','fai_terme','depotoir_de_pipier','dépotoir de pipier','160932',0,0,0,541,' '),(707,'fai','fai_terme','depotoir_de_poelier','dépotoir de poëlier','278909',0,0,0,542,' '),(708,'fai','fai_terme','depotoir_de_potier','dépotoir de potier','160734',0,0,0,543,' '),(709,'fai','fai_terme','depotoir_de_saline','dépotoir de saline','248098',0,0,0,544,' '),(710,'fai','fai_terme','depotoir_de_savetier','dépotoir de savetier','404343',0,0,0,545,' '),(711,'fai','fai_terme','depotoir_de_tabletier','dépotoir de tabletier','740481',0,0,0,546,' '),(712,'fai','fai_terme','depotoir_de_tabletterie','dépotoir de tabletterie','306904',0,0,0,547,' '),(713,'fai','fai_terme','depotoir_de_tablettier','dépotoir de tablettier','396514',0,0,0,548,' '),(714,'fai','fai_terme','depotoir_de_tuilier','dépotoir de tuilier','598990',0,0,0,549,' '),(715,'fai','fai_terme','depotoir_de_verrier','dépotoir de verrier','269240',0,0,0,550,' '),(716,'fai','fai_terme','depotoir_municipal','dépotoir municipal','142900',0,0,0,1626,' '),(717,'fai','fai_terme','depression','dépression','386549',0,1,0,552,' '),(718,'fai','fai_terme','derivation','dérivation','843253',0,1,0,553,' '),(719,'fai','fai_terme','deversoir','déversoir','180201',0,1,0,554,' '),(720,'fai','fai_terme','digue','digue','346805',0,1,0,555,' '),(721,'fai','fai_terme','dinanderie','dinanderie','294861',0,1,0,556,' '),(722,'fai','fai_terme','dinandier','dinandier','113680',0,0,0,557,' '),(723,'fai','fai_terme','distillation','distillation','202659',0,0,0,558,' '),(724,'fai','fai_terme','distillerie','distillerie','283513',0,1,0,559,' '),(725,'fai','fai_terme','dock','dock','111120',0,1,0,560,'à utiliser en rubriqu et 173 dans le sens d\'entrepôt, sinon voir bassin'),(726,'fai','fai_terme','domaine_agricole','domaine agricole','381323',0,0,0,562,' '),(727,'fai','fai_terme','domus','domus','312358',0,1,0,563,' '),(728,'fai','fai_terme','donjon','donjon','401106',0,1,0,564,' '),(729,'fai','fai_terme','donjon_double','donjon double','318326',0,0,0,1675,' '),(730,'fai','fai_terme','dortoir','dortoir','902283',0,1,0,565,' '),(731,'fai','fai_terme','douane','douane','393577',0,1,0,1693,' '),(732,'fai','fai_terme','douve','douve','374670',0,0,0,566,' '),(733,'fai','fai_terme','douves','douves','383981',0,0,0,567,' '),(734,'fai','fai_terme','doyenne','doyenné','389120',0,0,0,568,' '),(735,'fai','fai_terme','drain','drain','101544',0,1,0,569,' '),(736,'fai','fai_terme','drainage','drainage','255788',0,1,0,570,' '),(737,'fai','fai_terme','draperie','draperie','272449',0,1,0,1844,' '),(738,'fai','fai_terme','duit','duit','611151',0,1,0,571,'def : chaussée formée de pieux et de cailloux, en travers d\'une rivière ou d\'un petit bras de mer, destinée à arrêter le poisson au moment du jusant ; lit artificiel, canal'),(739,'fai','fai_terme','dune','dune','270515',0,1,0,572,' '),(740,'fai','fai_terme','dunes','dunes','170696',0,0,0,573,' '),(741,'fai','fai_terme','e','é','235179',0,0,0,574,' '),(742,'fai','fai_terme','echafaudage','échafaudage','387947',0,0,0,575,' '),(743,'fai','fai_terme','echauguette','échauguette','276700',0,0,0,576,' '),(744,'fai','fai_terme','echoppe','échoppe','109531',0,0,0,577,' '),(745,'fai','fai_terme','ecluse','écluse','258528',0,1,0,578,' '),(746,'fai','fai_terme','ecole','école','330519',0,1,0,579,' '),(747,'fai','fai_terme','ecole_talmudique','école talmudique','138252',0,0,0,580,' '),(748,'fai','fai_terme','ecurie','écurie','406155',0,1,0,581,' '),(749,'fai','fai_terme','edicule','édicule','341036',0,0,0,582,' '),(750,'fai','fai_terme','edifice','édifice','184946',0,0,0,583,' '),(751,'fai','fai_terme','edifice_cultuel','édifice cultuel','197826',0,1,0,584,' '),(752,'fai','fai_terme','edifice_culturel','édifice culturel','272431',0,1,0,1713,' '),(753,'fai','fai_terme','edifice_dassistance','édifice d\'assistance','128004',0,1,0,585,' '),(754,'fai','fai_terme','edifice_de_culte','édifice de culte','150350',0,0,0,586,' '),(755,'fai','fai_terme','edifice_de_spectacle','édifice de spectacle','199116',0,1,0,587,' '),(756,'fai','fai_terme','edifice_ecclesiastique','édifice ecclésiastique','380644',0,0,0,1788,' '),(757,'fai','fai_terme','edifice_funeraire','édifice funéraire','217584',0,1,0,588,' '),(758,'fai','fai_terme','edifice_monumental','édifice monumental','252465',0,0,0,589,' '),(759,'fai','fai_terme','edifice_officiel','édifice officiel','427937',0,0,0,590,' '),(760,'fai','fai_terme','edifice_prive','édifice privé','294169',0,0,0,591,' '),(761,'fai','fai_terme','edifice_public','édifice public','410410',0,1,0,592,' '),(762,'fai','fai_terme','edifice_religieux','édifice religieux','194095',0,0,0,1770,' '),(763,'fai','fai_terme','eglise','église','205761',0,1,0,593,' '),(764,'fai','fai_terme','eglise_abbatiale','église abbatiale','352027',0,0,0,594,' '),(765,'fai','fai_terme','eglise_castrale','église castrale','247358',0,0,0,595,' '),(766,'fai','fai_terme','eglise_cathedrale','église cathédrale','126509',0,0,0,596,' '),(767,'fai','fai_terme','eglise_cemeteriale','église cémetériale','122071',0,1,0,597,' '),(768,'fai','fai_terme','eglise_collegiale','église collégiale','105156',0,0,0,598,' '),(769,'fai','fai_terme','eglise_conventuelle','église conventuelle','321185',0,1,0,599,' '),(770,'fai','fai_terme','eglise_detablissement_hospitalier','église d\'établissement hospitalier','329907',0,0,0,600,' '),(771,'fai','fai_terme','eglise_fortifiee','église fortifiée','314435',0,1,0,601,' '),(772,'fai','fai_terme','eglise_funeraire','église funéraire','314338',0,1,0,602,' '),(773,'fai','fai_terme','eglise_paroissiale','église paroissiale','663058',0,1,0,604,' '),(775,'fai','fai_terme','eglise_prieurale','église prieurale','893233',0,1,0,605,' '),(776,'fai','fai_terme','eglise_priorale','église priorale','218213',0,0,0,606,' '),(777,'fai','fai_terme','eglise_reformee','église réformée','404041',0,0,0,607,' '),(778,'fai','fai_terme','egout','égout','315118',0,1,0,608,' '),(779,'fai','fai_terme','egout_collecteur','égout-collecteur','253532',0,0,0,1660,' '),(780,'fai','fai_terme','element_darchitecture','élément d\'architecture','725239',0,0,0,609,' '),(781,'fai','fai_terme','element_de_charpente','élément de charpente','343777',0,0,0,610,' '),(782,'fai','fai_terme','elevage','élevage','211347',0,0,0,611,' '),(783,'fai','fai_terme','emailleur','émailleur','427373',0,1,0,1845,' '),(784,'fai','fai_terme','embarcadere','embarcadère','278693',0,0,0,612,' '),(785,'fai','fai_terme','embrasure_de_tir','embrasure de tir','349736',0,0,0,613,' '),(786,'fai','fai_terme','emmarchement','emmarchement','411864',0,0,0,614,' '),(787,'fai','fai_terme','emmottement','emmottement','955926',0,1,0,615,' '),(788,'fai','fai_terme','empierrement','empierrement','230577',0,0,0,616,' '),(789,'fai','fai_terme','enceinte','enceinte','234872',0,1,0,617,' '),(790,'fai','fai_terme','enceinte_cathedrale','enceinte (cathédrale)','347811',0,0,0,618,' '),(791,'fai','fai_terme','enceinte_abbatiale','enceinte abbatiale','150113',0,0,0,619,' '),(792,'fai','fai_terme','enceinte_avancee','enceinte avancée','833572',0,0,0,620,' '),(793,'fai','fai_terme','enceinte_bastionnee','enceinte bastionnée','352109',0,0,0,621,' '),(794,'fai','fai_terme','enceinte_canoniale','enceinte canoniale','788191',0,1,0,622,' '),(795,'fai','fai_terme','enceinte_circulaire','enceinte circulaire','135888',0,0,0,623,' '),(796,'fai','fai_terme','enceinte_claustrale','enceinte claustrale','406667',0,0,0,624,' '),(797,'fai','fai_terme','enceinte_conventuelle','enceinte conventuelle','384990',0,1,0,625,' '),(798,'fai','fai_terme','enceinte_cultuelle','enceinte cultuelle','210944',0,1,0,626,' '),(799,'fai','fai_terme','enceinte_en_bois','enceinte en bois','325173',0,1,0,627,' '),(800,'fai','fai_terme','enceinte_en_terre','enceinte en terre','236107',0,0,0,628,' '),(801,'fai','fai_terme','enceinte_episcopale','enceinte épiscopale','301623',0,1,0,629,' '),(802,'fai','fai_terme','enceinte_fossoyee','enceinte fossoyée','494399',0,0,0,630,' '),(803,'fai','fai_terme','enceinte_monastique','enceinte monastique','267911',0,0,0,631,' '),(804,'fai','fai_terme','enceinte_priorale','enceinte priorale','538339',0,0,0,632,' '),(805,'fai','fai_terme','enceintebastionnee','enceintebastionnée','307152',0,0,0,633,' '),(806,'fai','fai_terme','enclos','enclos','236236',0,1,0,634,' '),(807,'fai','fai_terme','enclos_a_bestiaux','enclos à bestiaux','157736',0,0,0,635,' '),(808,'fai','fai_terme','enclos_canonial','enclos canonial','372100',0,0,0,636,' '),(809,'fai','fai_terme','enclos_conventuel','enclos conventuel','969280',0,0,0,637,' '),(810,'fai','fai_terme','enclos_cultuel','enclos cultuel','187880',0,0,0,638,' '),(811,'fai','fai_terme','enclos_episcopal','enclos épiscopal','179963',0,0,0,639,' '),(812,'fai','fai_terme','enclos_funeraire','enclos funéraire','150693',0,1,0,640,' '),(813,'fai','fai_terme','enclos_prieural','enclos prieural','310522',0,0,0,1778,' '),(814,'fai','fai_terme','endiguement','endiguement','610998',0,0,0,641,' '),(815,'fai','fai_terme','endiguement_en_clayonnage','endiguement en clayonnage','198133',0,0,0,642,' '),(816,'fai','fai_terme','endiguement_maconne','endiguement maçonné','400340',0,0,0,643,' '),(817,'fai','fai_terme','enduit_peint','enduit peint','170576',0,0,0,644,' '),(818,'fai','fai_terme','enduits_peints','enduits peints','954647',0,1,0,645,' '),(819,'fai','fai_terme','enfeu','enfeu','951460',0,1,0,646,' '),(820,'fai','fai_terme','enseigne_de_pelerinage','enseigne de pèlerinage','374814',0,0,0,647,' '),(821,'fai','fai_terme','ensemble_balneaire','ensemble balnéaire','309912',0,0,0,1728,' '),(822,'fai','fai_terme','ensemble_cathedral','ensemble cathédral','275846',0,0,0,1771,' '),(823,'fai','fai_terme','ensemble_fossoye','ensemble fossoyé','918308',0,0,0,648,' '),(824,'fai','fai_terme','ensemble_monumental','ensemble monumental','425827',0,0,0,649,' '),(825,'fai','fai_terme','ensemble_thermal','ensemble thermal','195657',0,0,0,650,' '),(826,'fai','fai_terme','entree_fortifiee','entrée fortifiée','389251',0,0,0,651,' '),(827,'fai','fai_terme','entrepot','entrepôt','362058',0,1,0,652,' '),(828,'fai','fai_terme','entrepot_agricole','entrepôt agricole','393893',0,0,0,653,' '),(829,'fai','fai_terme','entrepot_de_moulin','entrepôt de moulin','229952',0,0,0,654,' '),(830,'fai','fai_terme','entrepot_fortifie','entrepôt fortifié','101330',0,0,0,655,' '),(831,'fai','fai_terme','envasement','envasement','403029',0,1,0,656,' '),(832,'fai','fai_terme','epandage','épandage','834059',0,0,0,657,' '),(833,'fai','fai_terme','epave','épave','102540',0,1,0,658,' '),(834,'fai','fai_terme','eperon','éperon','303211',0,1,0,659,' '),(835,'fai','fai_terme','epier','épier','350971',0,0,0,660,' '),(836,'fai','fai_terme','epierrement','épierrement','265557',0,0,0,661,' '),(837,'fai','fai_terme','epigraphie','épigraphie','825449',0,0,0,662,' '),(838,'fai','fai_terme','epitaphe','épitaphe','151149',0,0,0,663,' '),(839,'fai','fai_terme','erosion','érosion','175030',0,1,0,664,' '),(840,'fai','fai_terme','escalier','escalier','259920',0,1,0,665,' '),(841,'fai','fai_terme','escarpe','escarpe','351994',0,1,0,666,' '),(842,'fai','fai_terme','escarpement','escarpement','613697',0,0,0,667,' '),(843,'fai','fai_terme','espace','espace','111872',0,0,0,668,' '),(844,'fai','fai_terme','espace_agricole','espace agricole','153501',0,1,0,669,' '),(845,'fai','fai_terme','espace_artisanal','espace artisanal','170135',0,0,0,670,' '),(846,'fai','fai_terme','espace_commercial','espace commercial','320505',0,0,0,671,' '),(847,'fai','fai_terme','espace_cultive','espace cultivé','241076',0,0,0,672,' '),(848,'fai','fai_terme','espace_dalle','espace dallé','412644',0,0,0,673,' '),(849,'fai','fai_terme','espace_de_circulation','espace de circulation','553202',0,0,0,674,' '),(850,'fai','fai_terme','espace_domestique','espace domestique','504331',0,0,0,675,' '),(851,'fai','fai_terme','espace_funeraire','espace funéraire','767886',0,1,0,676,' '),(852,'fai','fai_terme','espace_libre','espace libre','376842',0,1,0,677,' '),(853,'fai','fai_terme','espace_marecageux','espace marécageux','529915',0,0,0,678,' '),(854,'fai','fai_terme','espace_non_bati','espace non bâti','354201',0,0,0,679,' '),(855,'fai','fai_terme','espace_prive','espace privé','182222',0,0,0,1742,' '),(856,'fai','fai_terme','espace_public','espace public','928376',0,1,0,680,' '),(857,'fai','fai_terme','espace_urbain_peripherique','espace urbain périphérique','725967',0,0,0,681,' '),(858,'fai','fai_terme','espace_vert','espace vert','352474',0,1,0,682,' '),(859,'fai','fai_terme','espace_rue','espace-rue','703912',0,0,0,683,' '),(860,'fai','fai_terme','esplanade','esplanade','126740',0,1,0,684,' '),(861,'fai','fai_terme','essart','essart','762815',0,0,0,685,' '),(862,'fai','fai_terme','estacade','estacade','121683',0,0,0,1633,' '),(863,'fai','fai_terme','etable','étable','185397',0,1,0,686,' '),(864,'fai','fai_terme','etablissement_agricole','établissement agricole','248630',0,0,0,687,' '),(865,'fai','fai_terme','etablissement_balneaire','établissement balnéaire','347445',0,0,0,688,' '),(866,'fai','fai_terme','etablissement_de_cure','établissement de cure','412480',0,1,0,1720,' '),(867,'fai','fai_terme','etablissement_rural','établissement rural','478918',0,1,0,689,' '),(868,'fai','fai_terme','etablissement_thermal','établissement thermal','154638',0,1,0,690,' '),(869,'fai','fai_terme','etablissement_viticole','établissement viticole','168051',0,0,0,691,' '),(870,'fai','fai_terme','etang','étang','373142',0,1,0,692,' '),(871,'fai','fai_terme','etuve','étuve','284084',0,0,0,693,' '),(872,'fai','fai_terme','etuves','étuves','241196',0,1,0,694,' '),(873,'fai','fai_terme','euripe','euripe','127704',0,0,0,695,' '),(874,'fai','fai_terme','evacuation','évacuation','934193',0,1,0,697,' '),(875,'fai','fai_terme','eveche','évêché','349965',0,1,0,698,' '),(876,'fai','fai_terme','evelage_de_porc','évelage de porc','162828',0,0,0,699,' '),(877,'fai','fai_terme','ex_voto','ex-voto','211955',0,1,0,706,' '),(878,'fai','fai_terme','exedre','exèdre','394202',0,0,0,700,' '),(879,'fai','fai_terme','exhaussement','exhaussement','337220',0,1,0,701,' '),(880,'fai','fai_terme','exhaussement_de_terrain','exhaussement de terrain','553522',0,0,0,702,' '),(881,'fai','fai_terme','exploitation_agricole','exploitation agricole','397732',0,0,0,703,' '),(882,'fai','fai_terme','extraction','extraction','987542',0,1,0,704,' '),(883,'fai','fai_terme','extraction_dardoise','extraction d\'ardoise','123565',0,0,0,1888,' '),(884,'fai','fai_terme','extraction_dargile','extraction d\'argile','159537',0,0,0,1889,' '),(885,'fai','fai_terme','extraction_de_calcaire','extraction de calcaire','167700',0,0,0,1890,' '),(886,'fai','fai_terme','extraction_de_craie','extraction de craie','275543',0,0,0,1891,' '),(887,'fai','fai_terme','extraction_de_gneiss','extraction de gneiss','111783',0,0,0,1892,' '),(888,'fai','fai_terme','extraction_de_granit','extraction de granit','113798',0,0,0,1893,' '),(889,'fai','fai_terme','extraction_de_gravier','extraction de gravier','296777',0,0,0,1894,' '),(890,'fai','fai_terme','extraction_de_gres','extraction de grès','149753',0,0,0,1895,' '),(891,'fai','fai_terme','extraction_de_limon','extraction de limon','343085',0,0,0,1896,' '),(892,'fai','fai_terme','extraction_de_loess','extraction de loess','165200',0,0,0,1897,' '),(893,'fai','fai_terme','extraction_de_pierre','extraction de pierre','334488',0,0,0,1898,' '),(894,'fai','fai_terme','extraction_de_sable','extraction de sable','155774',0,0,0,1899,' '),(895,'fai','fai_terme','extraction_de_sel','extraction de sel','181054',0,0,0,1900,' '),(896,'fai','fai_terme','exutoire','exutoire','190432',0,0,0,705,' '),(897,'fai','fai_terme','fabrication_de_bracelets','fabrication de bracelets','173612',0,0,0,707,' '),(898,'fai','fai_terme','fabrication_de_collagene','fabrication de collagène','148150',0,0,0,708,' '),(899,'fai','fai_terme','fabrication_du_garum','fabrication du garum','372546',0,1,0,1846,' '),(900,'fai','fai_terme','fabrique','fabrique','383808',0,1,0,709,' '),(901,'fai','fai_terme','fabrique_deglise','fabrique d\'église','390897',0,1,0,1789,' '),(902,'fai','fai_terme','faience','faïence','114629',0,0,0,710,' '),(903,'fai','fai_terme','faiencerie','faïencerie','416787',0,1,0,711,' '),(904,'fai','fai_terme','fanum','fanum','189219',0,1,0,712,' '),(905,'fai','fai_terme','fausse_monnaie','fausse monnaie','191042',0,1,0,714,' '),(906,'fai','fai_terme','fausse_braie','fausse-braie','145933',0,0,0,715,' '),(907,'fai','fai_terme','fenil','fenil','185980',0,1,0,1867,' '),(908,'fai','fai_terme','fer','fer','362471',0,0,0,716,' '),(909,'fai','fai_terme','ferme','ferme','171693',0,1,0,717,' '),(910,'fai','fai_terme','ferme_fortifiee','ferme fortifiée','221901',0,0,0,718,' '),(911,'fai','fai_terme','ferrier','ferrier','239184',0,0,0,719,' '),(912,'fai','fai_terme','figlina','figlina','226276',0,0,0,720,' '),(913,'fai','fai_terme','filature','filature','542834',0,1,0,721,' '),(914,'fai','fai_terme','flan_monetaire','flan monétaire','343923',0,0,0,722,' '),(915,'fai','fai_terme','flanc','flanc','334606',0,0,0,1666,' '),(916,'fai','fai_terme','fleuve','fleuve','701641',0,1,0,1904,' '),(917,'fai','fai_terme','foirail','foirail','162019',0,0,0,723,' '),(918,'fai','fai_terme','foire','foire','356954',0,0,0,724,' '),(919,'fai','fai_terme','fond_de_cabane','fond de cabane','215611',0,1,0,725,' '),(920,'fai','fai_terme','fond_de_parcelle','fond de parcelle','410404',0,0,0,726,' '),(921,'fai','fai_terme','fondation','fondation','334191',0,0,0,727,' '),(922,'fai','fai_terme','fonderie','fonderie','218929',0,1,0,728,' '),(923,'fai','fai_terme','fonderie_de_canons','fonderie de canons','900910',0,0,0,729,' '),(924,'fai','fai_terme','fonderie_de_cloches','fonderie de cloches','189134',0,0,0,730,' '),(925,'fai','fai_terme','fontaine','fontaine','852414',0,1,0,732,' '),(926,'fai','fai_terme','fontaine_publique','fontaine publique','134924',0,0,0,733,' '),(927,'fai','fai_terme','fonte_de_cloche','fonte de cloche','398319',0,0,0,734,' '),(928,'fai','fai_terme','fonts_baptismaux','fonts baptismaux','133622',0,1,0,1769,' '),(929,'fai','fai_terme','foret','forêt','626134',0,0,0,735,' '),(930,'fai','fai_terme','forge','forge','407714',0,1,0,736,' '),(931,'fai','fai_terme','forgeron','forgeron','188342',0,0,0,737,' '),(932,'fai','fai_terme','fort','fort','920868',0,1,0,738,' '),(933,'fai','fai_terme','forteresse','forteresse','406568',0,1,0,739,' '),(934,'fai','fai_terme','fortification','fortification','260284',0,0,0,740,' '),(935,'fai','fai_terme','fortification_bastionnee','fortification bastionnée','332185',0,0,0,741,' '),(936,'fai','fai_terme','fortin','fortin','417691',0,1,0,742,' '),(937,'fai','fai_terme','forum','forum','223423',0,1,0,743,' '),(938,'fai','fai_terme','fosse','fosse','369786',0,0,0,744,' '),(939,'fai','fai_terme','fosse2','fossé','351136',0,1,0,745,'à utiliser en 7 quand fossé d\'une rue'),(940,'fai','fai_terme','fosse_de_rue_ou_de_voie','fossé (de rue ou de voie)','133850',0,0,0,1623,' '),(941,'fai','fai_terme','fosse_a_amphores','fosse à amphores','218549',0,0,0,746,' '),(942,'fai','fai_terme','fosse_a_argile','fosse à argile','385966',0,1,0,747,' '),(943,'fai','fai_terme','fosse_a_ball_trap','fosse à ball-trap','535333',0,0,0,748,' '),(944,'fai','fai_terme','fosse_a_chaux','fosse à chaux','370154',0,1,0,749,' '),(945,'fai','fai_terme','fosse_a_dechets','fosse à déchets','121959',0,0,0,750,' '),(946,'fai','fai_terme','fosse_a_dolium','fosse à dolium','181893',0,1,0,1743,' '),(947,'fai','fai_terme','fosse_a_feu','fosse à feu','348381',0,0,0,751,' '),(948,'fai','fai_terme','fosse_a_fumier','fosse à fumier','195486',0,0,0,752,' '),(949,'fai','fai_terme','fosse_a_incineration','fosse à incinération','172485',0,0,0,753,' '),(950,'fai','fai_terme','fosse_a_lisier','fosse à lisier','323443',0,1,0,754,' '),(951,'fai','fai_terme','fosse_a_offrandes','fosse à offrandes','578765',0,1,0,756,' '),(952,'fai','fai_terme','fosse_anthropomorphe','fosse anthropomorphe','190598',0,0,0,757,' '),(953,'fai','fai_terme','fosse_arme','fossé armé','377110',0,0,0,1667,' '),(954,'fai','fai_terme','fosse_circulaire','fossé circulaire','250083',0,0,0,758,' '),(955,'fai','fai_terme','fosse_collecteur','fossé collecteur','152078',0,0,0,759,' '),(956,'fai','fai_terme','fosse_commune','fosse commune','286659',0,1,0,760,' '),(957,'fai','fai_terme','fosse_cultuelle','fosse cultuelle','218146',0,1,0,761,' '),(958,'fai','fai_terme','fosse_daisance','fosse d\'aisance','288182',0,0,0,762,' '),(959,'fai','fai_terme','fosse_denclos','fossé d\'enclos','305271',0,0,0,1868,' '),(960,'fai','fai_terme','fosse_dequarrissage','fosse d\'équarrissage','236900',0,0,0,770,' '),(961,'fai','fai_terme','fosse_devacuation','fossé d\'évacuation','108977',0,0,0,771,' '),(962,'fai','fai_terme','fosse_dextraction','fosse d\'extraction','102143',0,1,0,772,' '),(963,'fai','fai_terme','fosse_dirrigation','fossé d\'irrigation','388880',0,0,0,773,' '),(964,'fai','fai_terme','fosse_dun_camp_normand','fossé d\'un camp normand','290608',0,0,0,774,' '),(965,'fai','fai_terme','fosse_de_coulee_de_cloche','fosse de coulée de cloche','148708',0,0,0,1847,' '),(966,'fai','fai_terme','fosse_de_decantation','fosse de décantation','280880',0,1,0,763,' '),(967,'fai','fai_terme','fosse_de_drainage','fossé de drainage','107752',0,0,0,764,' '),(968,'fai','fai_terme','fosse_de_marcottage','fosse de marcottage','342890',0,1,0,765,' '),(969,'fai','fai_terme','fosse_de_plantation','fosse de plantation','396922',0,1,0,766,' '),(970,'fai','fai_terme','fosse_de_rouissage','fosse de rouissage','421855',0,1,0,767,' '),(971,'fai','fai_terme','fosse_de_vidange','fosse de vidange','440945',0,0,0,768,' '),(972,'fai','fai_terme','fosse_depotoir','fosse dépotoir','344544',0,0,0,769,' '),(973,'fai','fai_terme','fosse_funeraire','fosse funéraire','815129',0,1,0,1804,' '),(974,'fai','fai_terme','fosse_palissade','fossé palissadé','208366',0,0,0,1744,' '),(975,'fai','fai_terme','fosse_parcellaire','fossé parcellaire','305963',0,1,0,775,' '),(976,'fai','fai_terme','fosse_rituelle','fosse rituelle','384956',0,0,0,776,' '),(977,'fai','fai_terme','fosse_sec','fossé sec','246685',0,0,0,777,' '),(978,'fai','fai_terme','fosse_septique','fosse septique','237610',0,0,0,778,' '),(979,'fai','fai_terme','fosse_votive','fosse votive','371718',0,0,0,779,' '),(980,'fai','fai_terme','fosse_collecteur2','fossé-collecteur','243591',0,0,0,780,' '),(981,'fai','fai_terme','fosse_depotoir2','fosse-dépotoir','950257',0,0,0,781,' '),(982,'fai','fai_terme','fosse_silo','fosse-silo','139975',0,0,0,782,' '),(983,'fai','fai_terme','fouloir','fouloir','224535',0,1,0,1869,' '),(984,'fai','fai_terme','foulon','foulon','558822',0,1,0,783,' '),(985,'fai','fai_terme','foulonnerie','foulonnerie','196417',0,0,0,784,' '),(986,'fai','fai_terme','four','four','242859',0,0,0,785,' '),(987,'fai','fai_terme','four_a_amphores','four à amphores','266105',0,0,0,786,' '),(988,'fai','fai_terme','four_a_briques','four à briques','249853',0,1,0,787,' '),(989,'fai','fai_terme','four_a_ceramique','four à céramique','149238',0,1,0,788,' '),(990,'fai','fai_terme','four_a_chaux','four à chaux','357676',0,1,0,789,' '),(991,'fai','fai_terme','four_a_cloche','four à cloche','145839',0,1,0,790,' '),(992,'fai','fai_terme','four_a_grain','four à grain','230093',0,1,0,791,' '),(993,'fai','fai_terme','four_a_grains','Four à grains','241688',0,0,0,792,' '),(994,'fai','fai_terme','four_a_pain','four à pain','137380',0,1,0,793,' '),(995,'fai','fai_terme','four_a_platre','four à plâtre','274429',0,0,0,794,' '),(996,'fai','fai_terme','four_a_plomb','four à plomb','150224',0,1,0,1848,' '),(997,'fai','fai_terme','four_a_recuisson','four à recuisson','393769',0,1,0,795,' '),(998,'fai','fai_terme','four_a_sel','four à sel','631677',0,0,0,796,' '),(999,'fai','fai_terme','four_a_teinture','four à teinture','110214',0,0,0,797,' '),(1000,'fai','fai_terme','four_banal','four banal','104865',0,1,0,798,' '),(1001,'fai','fai_terme','four_ceramique','four céramique','796952',0,0,0,799,' '),(1002,'fai','fai_terme','four_de_boulanger','four de boulanger','127830',0,0,0,801,' '),(1003,'fai','fai_terme','four_de_brasseur','four de brasseur','646772',0,0,0,800,' '),(1004,'fai','fai_terme','four_de_brasseur2','four de brasseur','646772',0,0,0,802,' '),(1005,'fai','fai_terme','four_de_briquetier','four de briquetier','222238',0,0,0,803,' '),(1006,'fai','fai_terme','four_de_bronzier','four de bronzier','312307',0,0,0,804,' '),(1007,'fai','fai_terme','four_de_faiencier','four de faïencier','163433',0,1,0,805,' '),(1008,'fai','fai_terme','four_de_grillage','four de grillage','249740',0,0,0,806,' '),(1009,'fai','fai_terme','four_de_potier','four de potier','259196',0,1,0,807,' '),(1010,'fai','fai_terme','four_de_reduction','four de réduction','352590',0,0,0,808,' '),(1011,'fai','fai_terme','four_de_teinturier','four de teinturier','203258',0,0,0,809,' '),(1012,'fai','fai_terme','four_de_tuilier','four de tuilier','410175',0,1,0,810,' '),(1013,'fai','fai_terme','four_de_tuilier_briquetier','four de tuilier-briquetier','338427',0,0,0,812,' '),(1014,'fai','fai_terme','four_de_tuilier_briquetier2','four de tuilier, briquetier','699775',0,0,0,811,' '),(1015,'fai','fai_terme','four_de_verrier','four de verrier','335152',0,1,0,813,' '),(1016,'fai','fai_terme','four_domestique','four domestique','296480',0,1,0,814,' '),(1017,'fai','fai_terme','four_en_batterie','four en batterie','270640',0,1,0,1849,' '),(1018,'fai','fai_terme','fournaise','fournaise','381400',0,0,0,815,' '),(1019,'fai','fai_terme','fourneau','fourneau','533279',0,0,0,816,' '),(1020,'fai','fai_terme','fourneau_de_bronzier','fourneau de bronzier','136047',0,0,0,817,' '),(1021,'fai','fai_terme','fournette','fournette','931670',0,0,0,818,' '),(1022,'fai','fai_terme','fournil','fournil','347065',0,0,0,819,' '),(1023,'fai','fai_terme','foyer','foyer','132043',0,1,0,820,' '),(1024,'fai','fai_terme','foyer_de_bronzier','foyer de bronzier','253245',0,0,0,821,' '),(1025,'fai','fai_terme','foyer_domestique','foyer domestique','317156',0,0,0,822,' '),(1026,'fai','fai_terme','foyer_cheminee','foyer-cheminée','176219',0,0,0,823,' '),(1027,'fai','fai_terme','foyer_construit','foyer-construit','312370',0,0,0,1745,' '),(1028,'fai','fai_terme','foyer_plaque','foyer-plaque','345563',0,0,0,1746,' '),(1029,'fai','fai_terme','franchissement','franchissement','413062',0,0,0,824,' '),(1030,'fai','fai_terme','frayere','frayère','289362',0,0,0,1870,' '),(1031,'fai','fai_terme','frette','frette','332123',0,0,0,1652,' '),(1032,'fai','fai_terme','friche','friche','188312',0,1,0,825,' '),(1033,'fai','fai_terme','friche_urbaine','friche urbaine','922874',0,0,0,826,' '),(1034,'fai','fai_terme','frigidarium','frigidarium','331914',0,1,0,827,' '),(1035,'fai','fai_terme','front_de_carriere','front de carrière','161322',0,0,0,828,' '),(1036,'fai','fai_terme','front_de_scene','front de scène','628677',0,0,0,829,' '),(1037,'fai','fai_terme','front_de_taille','front de taille','330479',0,0,0,830,' '),(1038,'fai','fai_terme','fumoir','fumoir','373596',0,1,0,831,' '),(1039,'fai','fai_terme','galerie','galerie','265899',0,0,0,832,' '),(1040,'fai','fai_terme','galerie_portique','Galerie - portique','128990',0,0,0,833,' '),(1041,'fai','fai_terme','galerie_dextraction','galerie d\'extraction','234341',0,1,0,837,' '),(1042,'fai','fai_terme','galerie_de_boutiques','galerie de boutiques','358280',0,0,0,834,' '),(1043,'fai','fai_terme','galerie_de_cloitre','galerie de cloître','531646',0,0,0,835,' '),(1044,'fai','fai_terme','galerie_de_contre_mine','galerie de contre-mine','144875',0,1,0,836,' '),(1045,'fai','fai_terme','galerie_sanitaire','galerie sanitaire','339816',0,0,0,838,' '),(1046,'fai','fai_terme','galerie_technique','galerie technique','851173',0,1,0,839,' '),(1047,'fai','fai_terme','garage','garage','267009',0,0,0,840,' '),(1048,'fai','fai_terme','garde_manger','garde-manger','140613',0,0,0,841,' '),(1049,'fai','fai_terme','gare','gare','400039',0,1,0,842,' '),(1050,'fai','fai_terme','gare_maritime','gare maritime','642060',0,0,0,843,' '),(1051,'fai','fai_terme','garenne','garenne','466677',0,1,0,1871,' '),(1052,'fai','fai_terme','garnison','garnison','286595',0,0,0,844,' '),(1053,'fai','fai_terme','garum','garum','148615',0,0,0,1850,' '),(1054,'fai','fai_terme','gazometre','gazomètre','988717',0,0,0,845,' '),(1055,'fai','fai_terme','gendarmerie','gendarmerie','102797',0,0,0,846,' '),(1056,'fai','fai_terme','geole','geôle','281639',0,0,0,847,' '),(1057,'fai','fai_terme','gibet','gibet','294753',0,1,0,1694,' '),(1058,'fai','fai_terme','gisant','gisant','289569',0,1,0,848,' '),(1059,'fai','fai_terme','glaciere','glacière','425398',0,1,0,849,' '),(1060,'fai','fai_terme','glaciere_communale','glacière communale','383346',0,1,0,1695,' '),(1061,'fai','fai_terme','glacis','glacis','128824',0,1,0,850,' '),(1062,'fai','fai_terme','gleza','gleza','103559',0,1,0,1794,'tombe d\'objets isolée, juive'),(1063,'fai','fai_terme','gneiss','gneiss','808494',0,1,0,851,' '),(1064,'fai','fai_terme','gradins','gradins','404263',0,0,0,852,' '),(1065,'fai','fai_terme','grange','grange','405315',0,1,0,853,' '),(1066,'fai','fai_terme','grange_aux_dimes','grange aux dîmes','247745',0,1,0,1790,' '),(1067,'fai','fai_terme','grange_dimiere','grange dîmière','331862',0,0,0,854,' '),(1068,'fai','fai_terme','grange_monastique','grange monastique','230234',0,0,0,855,' '),(1069,'fai','fai_terme','granit','granit','947154',0,1,0,856,' '),(1070,'fai','fai_terme','granite','granite','368581',0,0,0,857,' '),(1071,'fai','fai_terme','granulat','granulat','401813',0,0,0,858,' '),(1072,'fai','fai_terme','grave','grave','565108',0,0,0,859,' '),(1073,'fai','fai_terme','gravier','gravier','265229',0,0,0,860,' '),(1074,'fai','fai_terme','graviere','gravière','334941',0,1,0,861,' '),(1075,'fai','fai_terme','gravillon','gravillon','750031',0,0,0,862,' '),(1076,'fai','fai_terme','grenier','grenier','426528',0,0,0,863,' '),(1077,'fai','fai_terme','grenier_a_ble','grenier à blé','144606',0,0,0,864,' '),(1078,'fai','fai_terme','grenier_fortifie','grenier fortifié','180193',0,0,0,865,' '),(1079,'fai','fai_terme','grenier_entrepot','grenier-entrepôt','575030',0,0,0,866,' '),(1080,'fai','fai_terme','gres','grès','272435',0,1,0,867,' '),(1081,'fai','fai_terme','groupe_cathedral','groupe cathédral','387019',0,0,0,868,' '),(1082,'fai','fai_terme','groupe_episcopal','groupe épiscopal','544226',0,1,0,870,' '),(1083,'fai','fai_terme','gue','gué','258907',0,1,0,871,' '),(1084,'fai','fai_terme','guichet','guichet','100701',0,0,0,872,' '),(1085,'fai','fai_terme','gymnase','gymnase','903142',0,1,0,873,' '),(1086,'fai','fai_terme','gypse','gypse','252054',0,0,0,874,' '),(1087,'fai','fai_terme','habitat','habitat','993506',0,1,0,875,' '),(1088,'fai','fai_terme','habitat2','habitat','993506',0,0,0,876,' '),(1089,'fai','fai_terme','habitat_aristocratique','habitat aristocratique','311228',0,0,0,877,' '),(1090,'fai','fai_terme','habitat_fortifie','habitat fortifié','222078',0,1,0,1668,' '),(1091,'fai','fai_terme','habitat_religieux','habitat religieux','323965',0,1,0,1779,' '),(1092,'fai','fai_terme','habitation','habitation','966261',0,1,0,878,' '),(1093,'fai','fai_terme','habitation2','habitation','966261',0,0,0,879,' '),(1094,'fai','fai_terme','habitation_canoniale','habitation canoniale','392664',0,0,0,1780,' '),(1095,'fai','fai_terme','habitation_du_prieur','habitation du prieur','719713',0,0,0,1781,' '),(1096,'fai','fai_terme','habitation_ecclesiastique','habitation ecclésiastique','182929',0,0,0,1791,' '),(1097,'fai','fai_terme','habitation_sucrerie','habitation-sucrerie','263720',0,0,0,880,' '),(1098,'fai','fai_terme','haie','haie','522511',0,1,0,881,' '),(1099,'fai','fai_terme','halle','halle','150417',0,0,0,882,' '),(1100,'fai','fai_terme','halle_au_poisson','halle au poisson','327137',0,0,0,883,' '),(1101,'fai','fai_terme','halle_aux_grains','halle aux grains','221576',0,0,0,884,' '),(1102,'fai','fai_terme','halle_echevinale','halle échevinale','262463',0,0,0,885,' '),(1103,'fai','fai_terme','halle_marchande','halle marchande','235481',0,0,0,886,' '),(1104,'fai','fai_terme','halles','halles','277657',0,1,0,887,' '),(1105,'fai','fai_terme','hangar','hangar','278052',0,0,0,888,' '),(1106,'fai','fai_terme','hangar_a_bateaux','hangar à bateaux','684438',0,1,0,1634,' '),(1107,'fai','fai_terme','heroum','heroum','387983',0,0,0,1805,'sépulture d\'un héros'),(1108,'fai','fai_terme','hopital','hôpital','346268',0,1,0,889,' '),(1109,'fai','fai_terme','hopital_militaire','hôpital militaire','126675',0,1,0,890,' '),(1110,'fai','fai_terme','hopital_militaire_thermal','hôpital militaire thermal','807671',0,0,0,891,' '),(1111,'fai','fai_terme','horreum','horreum','298451',0,1,0,1734,'dans l\'architecture romaine : grenier, entrepôt'),(1112,'fai','fai_terme','horticulture','horticulture','758481',0,0,0,892,' '),(1113,'fai','fai_terme','hospice','hospice','103730',0,1,0,893,' '),(1114,'fai','fai_terme','hotel','hôtel','172320',0,0,0,894,' '),(1115,'fai','fai_terme','hotel_de_lacademie','hôtel de l\'académie','199388',0,0,0,1715,' '),(1116,'fai','fai_terme','hotel_de_lintendance','hôtel de l\'intendance','124369',0,0,0,1697,' '),(1117,'fai','fai_terme','hotel_de_la_monnaie','hôtel de la monnaie','774962',0,0,0,895,' '),(1118,'fai','fai_terme','hotel_de_ville','hôtel de ville','328637',0,1,0,896,' '),(1119,'fai','fai_terme','hotel_des_chanoines','hôtel des chanoines','161181',0,0,0,1782,' '),(1120,'fai','fai_terme','hotel_des_tresoriers','hôtel des trésoriers','212648',0,0,0,1699,' '),(1121,'fai','fai_terme','hotel_du_gouverneur','hôtel du gouverneur','204680',0,1,0,1701,' '),(1122,'fai','fai_terme','hotel_particulier','hôtel particulier','143237',0,1,0,897,' '),(1123,'fai','fai_terme','hotel_prioral','hôtel prioral','297985',0,0,0,898,' '),(1124,'fai','fai_terme','hotel_dieu','hôtel-Dieu','369034',0,1,0,899,' '),(1125,'fai','fai_terme','hotellerie','hôtellerie','127579',0,1,0,900,' '),(1126,'fai','fai_terme','hotellerie_monastique','hôtellerie monastique','289204',0,0,0,901,' '),(1127,'fai','fai_terme','houblon','houblon','196557',0,0,0,902,' '),(1128,'fai','fai_terme','houblonnerie','houblonnerie','336373',0,1,0,1872,' '),(1129,'fai','fai_terme','huile','huile','214904',0,0,0,903,' '),(1130,'fai','fai_terme','huilerie','huilerie','203316',0,1,0,904,' '),(1131,'fai','fai_terme','hydrocarbures','hydrocarbures','214140',0,0,0,905,' '),(1132,'fai','fai_terme','hypocauste','hypocauste','421078',0,1,0,906,' '),(1133,'fai','fai_terme','hypogee','hypogée','202331',0,1,0,907,' '),(1134,'fai','fai_terme','ile','île','165742',0,1,0,1905,' '),(1135,'fai','fai_terme','ilot','îlot','837024',0,1,0,909,' '),(1136,'fai','fai_terme','ilot_dhabitation','îlot d\'habitation','367358',0,1,0,911,' '),(1137,'fai','fai_terme','ilot_urbain','Îlot urbain','335838',0,0,0,913,' '),(1138,'fai','fai_terme','immeuble','immeuble','118261',0,1,0,914,' '),(1139,'fai','fai_terme','immeuble_de_rapport','immeuble de rapport','307853',0,0,0,1747,' '),(1140,'fai','fai_terme','impasse','impasse','572380',0,0,0,915,' '),(1141,'fai','fai_terme','imprimerie','imprimerie','683696',0,0,0,916,' '),(1142,'fai','fai_terme','incinerateur','incinérateur','304848',0,0,0,917,' '),(1143,'fai','fai_terme','incineration','incinération','411202',0,1,0,918,' '),(1144,'fai','fai_terme','incineration_animale','incinération animale','332290',0,0,0,919,' '),(1145,'fai','fai_terme','incineration_denfant','incinération d\'enfant','949731',0,1,0,921,' '),(1146,'fai','fai_terme','incineration_de_nourrisson','incinération de nourrisson','147033',0,1,0,920,' '),(1147,'fai','fai_terme','incineration_isolee','incinération isolée','287698',0,1,0,922,' '),(1148,'fai','fai_terme','incineration_primaire','incinération primaire','266593',0,0,0,1806,' '),(1149,'fai','fai_terme','incineration_secondaire','incinération secondaire','155906',0,0,0,1807,'incinération déplacée après crémation'),(1150,'fai','fai_terme','industrie','industrie','192958',0,0,0,923,' '),(1151,'fai','fai_terme','infirmerie','infirmerie','280712',0,1,0,924,' '),(1152,'fai','fai_terme','inhumation','inhumation','478930',0,1,0,925,'à n\'utiliser que pour la période antique, sinon employer sépulture'),(1153,'fai','fai_terme','inhumation_animale','inhumation animale','167584',0,0,0,926,' '),(1154,'fai','fai_terme','inhumation_assise','inhumation assise','127083',0,1,0,1808,' '),(1155,'fai','fai_terme','inhumation_denfant','inhumation d\'enfant','249159',0,1,0,929,' '),(1156,'fai','fai_terme','inhumation_de_nouveau_ne','inhumation de nouveau-né','117191',0,0,0,927,' '),(1157,'fai','fai_terme','inhumation_de_religieuses','inhumation de religieuses','343698',0,0,0,1809,' '),(1158,'fai','fai_terme','inhumation_en_amphore','inhumation en amphore','188398',0,0,0,928,' '),(1159,'fai','fai_terme','inhumation_isolee','inhumation isolée','303308',0,0,0,930,' '),(1160,'fai','fai_terme','inhumation_perinatale','inhumation périnatale','408062',0,0,0,931,' '),(1161,'fai','fai_terme','inhumation_pleine_terre','inhumation pleine terre','143054',0,0,0,1810,' '),(1162,'fai','fai_terme','inhumation_privilegiee','inhumation privilégiée','369670',0,0,0,932,' '),(1163,'fai','fai_terme','inhumation_sous_tuiles','inhumation sous tuiles','238482',0,0,0,1811,' '),(1164,'fai','fai_terme','inhumation_tuile','inhumation tuile','271143',0,0,0,933,' '),(1165,'fai','fai_terme','inondation','inondation','141927',0,0,0,1906,' '),(1166,'fai','fai_terme','inscription','inscription','158655',0,0,0,934,' '),(1167,'fai','fai_terme','inscription_funeraire','inscription funéraire','259310',0,1,0,935,' '),(1168,'fai','fai_terme','inscription_grecque','inscription grecque','234597',0,0,0,936,' '),(1169,'fai','fai_terme','installation_agraire','installation agraire','390700',0,0,0,937,' '),(1170,'fai','fai_terme','installation_agricole','installation agricole','249647',0,0,0,938,' '),(1171,'fai','fai_terme','installation_artisanale','installation artisanale','366331',0,1,0,939,' '),(1172,'fai','fai_terme','installation_industrielle','installation industrielle','110288',0,0,0,940,' '),(1173,'fai','fai_terme','installation_vinicole','installation vinicole','333189',0,0,0,941,' '),(1174,'fai','fai_terme','insula','insula','161840',0,1,0,942,'îlot urbain dans l\'Antiquité'),(1175,'fai','fai_terme','intendance','intendance','120550',0,1,0,1698,' '),(1176,'fai','fai_terme','irrigation','irrigation','313535',0,1,0,943,' '),(1177,'fai','fai_terme','jardin','jardin','740443',0,1,0,944,' '),(1178,'fai','fai_terme','jardin_a_portique','jardin à portique','342705',0,0,0,945,' '),(1179,'fai','fai_terme','jardin_a_portiques','jardin à portiques','380625',0,0,0,946,' '),(1180,'fai','fai_terme','jardin_conventuel','jardin conventuel','236953',0,0,0,947,' '),(1181,'fai','fai_terme','jardin_dagrement','jardin d\'agrément','158530',0,0,0,948,' '),(1182,'fai','fai_terme','jardin_monastique','jardin monastique','148001',0,0,0,949,' '),(1183,'fai','fai_terme','jardin_public','jardin public','224864',0,1,0,950,' '),(1184,'fai','fai_terme','jetee','jetée','209446',0,0,0,951,' '),(1185,'fai','fai_terme','jeu_de_mail','jeu de mail','991166',0,1,0,1725,' '),(1186,'fai','fai_terme','jeu_de_paume','jeu de paume','117179',0,1,0,952,' '),(1187,'fai','fai_terme','jube','jubé','130543',0,1,0,953,' '),(1188,'fai','fai_terme','karst_effondre','karst effondré','122965',0,1,0,954,' '),(1189,'fai','fai_terme','kiosque','kiosque','166811',0,0,0,955,' '),(1190,'fai','fai_terme','laboratoire','laboratoire','329967',0,0,0,956,' '),(1191,'fai','fai_terme','labour','labour','141355',0,1,0,957,' '),(1192,'fai','fai_terme','lac','lac','382828',0,0,0,958,' '),(1193,'fai','fai_terme','lagune','lagune','148973',0,1,0,1907,' '),(1194,'fai','fai_terme','laine','laine','256448',0,0,0,959,' '),(1195,'fai','fai_terme','laiton','laiton','924808',0,0,0,960,' '),(1196,'fai','fai_terme','lame_funeraire','lame funéraire','311095',0,0,0,1812,' '),(1197,'fai','fai_terme','landes','landes','789015',0,1,0,1873,' '),(1198,'fai','fai_terme','lanterne_des_morts','lanterne des morts','344411',0,1,0,961,' '),(1199,'fai','fai_terme','laraire','laraire','197220',0,1,0,962,' '),(1200,'fai','fai_terme','latrines','latrines','166146',0,1,0,963,'quand il s\'agit de latrines publiques'),(1201,'fai','fai_terme','latrines_militaires','latrines militaires','448778',0,1,0,1679,' '),(1202,'fai','fai_terme','latrines_publiques','latrines publiques','805631',0,0,0,964,' '),(1203,'fai','fai_terme','lavabo','lavabo','387770',0,1,0,965,' '),(1204,'fai','fai_terme','lavoir','lavoir','183379',0,1,0,966,'à utiliser en 16 quand lavoir public'),(1205,'fai','fai_terme','lavoir_public','lavoir public','117124',0,0,0,967,' '),(1206,'fai','fai_terme','lazaret','lazaret','985521',0,1,0,968,' '),(1207,'fai','fai_terme','leproserie','léproserie','283297',0,1,0,969,' '),(1208,'fai','fai_terme','levee','levée','883598',0,0,0,970,' '),(1209,'fai','fai_terme','levee_de_berge','levée de berge','429249',0,0,0,971,' '),(1210,'fai','fai_terme','levee_de_terre','levée de terre','667932',0,1,0,972,'à utiliser en 8 uniquement pour structure de défense'),(1211,'fai','fai_terme','lice','lice','168996',0,1,0,974,'à utiliser en 8 ou 9 quand = espace compris entre 2 enceintes employer en 15 quand = espace réservé aux joutes'),(1212,'fai','fai_terme','lieu_dexecution','lieu d\'exécution','397787',0,1,0,1702,' '),(1213,'fai','fai_terme','lieu_de_culte','lieu de culte','272433',0,1,0,975,' '),(1214,'fai','fai_terme','lieu_de_pelerinage','lieu de pèlerinage','110652',0,0,0,976,' '),(1215,'fai','fai_terme','lignite','lignite','107212',0,0,0,977,' '),(1216,'fai','fai_terme','limite_dagglomeration','limite d\'agglomération','178785',0,0,0,978,' '),(1217,'fai','fai_terme','limite_de_cimetiere','limite de cimetière','322260',0,1,0,979,' '),(1218,'fai','fai_terme','limite_de_necropole','limite de nécropole','347515',0,0,0,980,' '),(1219,'fai','fai_terme','limite_de_parcelle','limite de parcelle','227212',0,0,0,981,' '),(1220,'fai','fai_terme','limite_de_pomerium','limite de pomerium','409864',0,0,0,983,' '),(1221,'fai','fai_terme','limite_de_propriete','limite de propriété','921145',0,1,0,984,' '),(1222,'fai','fai_terme','limite_parcellaire','limite parcellaire','739383',0,1,0,985,' '),(1223,'fai','fai_terme','limon','limon','356821',0,1,0,986,' '),(1224,'fai','fai_terme','linceul','linceul','283958',0,1,0,988,' '),(1225,'fai','fai_terme','linceul_avec_chaux','linceul avec chaux','103162',0,0,0,1813,' '),(1226,'fai','fai_terme','lingerie','lingerie','936270',0,0,0,989,' '),(1227,'fai','fai_terme','lingotiere','lingotière','308869',0,0,0,990,' '),(1228,'fai','fai_terme','lit','lit','157466',0,0,0,1908,' '),(1229,'fai','fai_terme','lit_fossile','lit fossile','757627',0,0,0,991,' '),(1230,'fai','fai_terme','lit_funeraire','lit funéraire','147956',0,1,0,1814,' '),(1231,'fai','fai_terme','lithographie','lithographie','168525',0,0,0,992,' '),(1232,'fai','fai_terme','littoral','littoral','182001',0,0,0,993,' '),(1233,'fai','fai_terme','livree_cardinalice','livrée cardinalice','469086',0,1,0,994,' '),(1234,'fai','fai_terme','local_a_fonction_commerciale','local à fonction commerciale','394062',0,0,0,1735,' '),(1235,'fai','fai_terme','local_de_vente','local de vente','495447',0,0,0,1736,' '),(1236,'fai','fai_terme','loess','loess','205780',0,1,0,995,' '),(1237,'fai','fai_terme','loge','loge','731020',0,0,0,996,' '),(1238,'fai','fai_terme','loge_des_marchands','loge des marchands','382493',0,0,0,1703,' '),(1239,'fai','fai_terme','logement','logement','404312',0,0,0,997,' '),(1240,'fai','fai_terme','logement_dofficier','logement d\'officier','400498',0,0,0,998,' '),(1241,'fai','fai_terme','logis','logis','337318',0,0,0,999,' '),(1242,'fai','fai_terme','logis_abbatial','logis abbatial','343010',0,1,0,1000,' '),(1243,'fai','fai_terme','logis_du_gouverneur','logis du gouverneur','269553',0,0,0,1001,' '),(1244,'fai','fai_terme','logis_prioral','logis prioral','254677',0,0,0,1002,' '),(1245,'fai','fai_terme','logis_seigneurial','logis seigneurial','179476',0,0,0,1003,' '),(1246,'fai','fai_terme','lot_urbain_non_bati','lot urbain non bâti','421846',0,0,0,1004,' '),(1247,'fai','fai_terme','lycee','lycée','326009',0,0,0,1005,' '),(1248,'fai','fai_terme','macellum','macellum','237877',0,1,0,1006,' '),(1249,'fai','fai_terme','machine_hydraulique','machine hydraulique','330805',0,0,0,1007,' '),(1250,'fai','fai_terme','magasin','magasin','142077',0,0,0,1008,' '),(1251,'fai','fai_terme','magasin_a_poudre','magasin à poudre','347907',0,0,0,1009,' '),(1252,'fai','fai_terme','magasin_de_faience','magasin de faïence','277054',0,0,0,1010,' '),(1253,'fai','fai_terme','mairie','mairie','960930',0,0,0,1704,' '),(1254,'fai','fai_terme','maison','maison','417835',0,0,0,1011,' '),(1255,'fai','fai_terme','maison_abbatiale','maison abbatiale','383093',0,0,0,1783,' '),(1256,'fai','fai_terme','maison_canoniale','maison canoniale','248019',0,1,0,1012,' '),(1257,'fai','fai_terme','maison_communale','maison communale','188913',0,1,0,1013,' '),(1258,'fai','fai_terme','maison_commune','maison commune','364849',0,0,0,1014,' '),(1259,'fai','fai_terme','maison_comtale','maison comtale','371615',0,0,0,1705,' '),(1260,'fai','fai_terme','maison_corporative','maison corporative','270439',0,1,0,1015,' '),(1261,'fai','fai_terme','maison_dartisan','maison d\'artisan','118760',0,0,0,1016,' '),(1262,'fai','fai_terme','maison_de_charite','maison de charité','395842',0,1,0,1017,' '),(1263,'fai','fai_terme','maison_de_commercant','maison de commerçant','387903',0,0,0,1018,' '),(1264,'fai','fai_terme','maison_de_detention','maison de détention','132132',0,0,0,1706,' '),(1265,'fai','fai_terme','maison_de_maitre','maison de maître','264743',0,0,0,1019,' '),(1266,'fai','fai_terme','maison_de_marchand','maison de marchand','401062',0,1,0,1020,' '),(1267,'fai','fai_terme','maison_de_potier','maison de potier','144879',0,0,0,1021,' '),(1268,'fai','fai_terme','maison_de_retraite','maison de retraite','216751',0,0,0,1022,' '),(1269,'fai','fai_terme','maison_de_source','maison de source','291141',0,1,0,1653,'au Moyen Age et à l\'époque moderne, ensemble hydraulique comprenant tout élément construit ou creusé, permettant le captage, le stockage et l\'approvisionnement en eau'),(1270,'fai','fai_terme','maison_de_vigneron','maison de vigneron','113220',0,0,0,1023,' '),(1271,'fai','fai_terme','maison_de_ville','maison de ville','588442',0,0,0,1024,' '),(1272,'fai','fai_terme','maison_des_archers','maison des archers','196135',0,0,0,1707,' '),(1273,'fai','fai_terme','maison_des_chanoinesses','maison des chanoinesses','359508',0,0,0,1784,' '),(1274,'fai','fai_terme','maison_des_jesuites','maison des Jésuites','249763',0,0,0,1025,' '),(1275,'fai','fai_terme','maison_des_orphelines','maison des orphelines','445706',0,0,0,1721,' '),(1276,'fai','fai_terme','maison_du_fermier_general','maison du fermier général','488596',0,0,0,1026,' '),(1277,'fai','fai_terme','maison_du_prieur','maison du prieur','139636',0,1,0,1027,' '),(1278,'fai','fai_terme','maison_du_tresorier','maison du trésorier','245351',0,0,0,1028,' '),(1279,'fai','fai_terme','maison_forte','maison forte','965296',0,1,0,1029,' '),(1280,'fai','fai_terme','maison_hospitaliere','maison hospitalière','331299',0,0,0,1030,' '),(1281,'fai','fai_terme','maison_noble','maison noble','254621',0,0,0,1748,' '),(1282,'fai','fai_terme','maison_seigneuriale','maison seigneuriale','349565',0,0,0,1031,' '),(1283,'fai','fai_terme','maison_vigneronne','maison vigneronne','330630',0,0,0,1032,' '),(1284,'fai','fai_terme','maison_forte2','maison-forte','223289',0,0,0,1033,' '),(1285,'fai','fai_terme','maison_tour','maison-tour','557399',0,1,0,1034,' '),(1286,'fai','fai_terme','maitrise','maîtrise','976176',0,0,0,1035,' '),(1287,'fai','fai_terme','maladrerie','maladrerie','382648',0,1,0,1036,' '),(1288,'fai','fai_terme','malterie','malterie','271812',0,0,0,1037,' '),(1289,'fai','fai_terme','manecanterie','manécanterie','300435',0,1,0,1716,' '),(1290,'fai','fai_terme','manege','manège','911262',0,1,0,1038,'si pas militaire, employer en 15'),(1291,'fai','fai_terme','manoir','manoir','867970',0,0,0,1039,' '),(1292,'fai','fai_terme','manufacture','manufacture','370943',0,1,0,1040,' '),(1293,'fai','fai_terme','manufacture_de_faience','manufacture de faïence','962449',0,0,0,1041,' '),(1294,'fai','fai_terme','manufacture_de_porcelaine','manufacture de porcelaine','424670',0,1,0,1883,' '),(1295,'fai','fai_terme','manutention','manutention','870031',0,0,0,1680,' '),(1296,'fai','fai_terme','manutention_de_vivres','manutention de vivres','139765',0,0,0,1042,' '),(1297,'fai','fai_terme','manutention_militaire','manutention militaire','184469',0,0,0,1043,' '),(1298,'fai','fai_terme','maraichage','maraîchage','289227',0,1,0,1044,' '),(1299,'fai','fai_terme','maraicher','maraîcher','373310',0,0,0,1045,' '),(1300,'fai','fai_terme','marais','marais','142467',0,1,0,1046,' '),(1301,'fai','fai_terme','marais_amenage','marais aménagé','112914',0,0,0,1047,' '),(1302,'fai','fai_terme','marais_asseche','marais asséché','485558',0,1,0,1048,' '),(1303,'fai','fai_terme','marais_littoral','marais littoral','288785',0,1,0,1909,' '),(1304,'fai','fai_terme','marche','marche','313114',0,0,0,1049,' '),(1305,'fai','fai_terme','marche2','marché','389923',0,1,0,1050,' '),(1306,'fai','fai_terme','marche_couvert','marché couvert','224159',0,0,0,1051,' '),(1307,'fai','fai_terme','mare','mare','217768',0,1,0,1052,' '),(1308,'fai','fai_terme','marecage','marécage','297337',0,1,0,1053,' '),(1309,'fai','fai_terme','marne','marne','304989',0,0,0,1054,' '),(1310,'fai','fai_terme','martyrium','martyrium','294609',0,1,0,1055,' '),(1311,'fai','fai_terme','materiau_de_construction','matériau de construction','226797',0,0,0,1056,' '),(1312,'fai','fai_terme','matricule','matricule','313711',0,0,0,1785,' '),(1313,'fai','fai_terme','mausolee','mausolée','375627',0,1,0,1057,' '),(1314,'fai','fai_terme','meandre','méandre','425398',0,1,0,1910,' '),(1315,'fai','fai_terme','melangeur_a_mortier','mélangeur à mortier','740172',0,1,0,1851,' '),(1316,'fai','fai_terme','memoria','memoria','409652',0,1,0,1058,' '),(1317,'fai','fai_terme','mensa','mensa','361698',0,1,0,1815,'petit autel sur un tombeau'),(1318,'fai','fai_terme','menuiserie','menuiserie','392097',0,0,0,1059,' '),(1319,'fai','fai_terme','metal','métal','403313',0,1,0,1060,' '),(1320,'fai','fai_terme','metallurgie','metallurgie','190191',0,0,0,1061,' '),(1321,'fai','fai_terme','metallurgie2','métallurgie','377193',0,0,0,1062,' '),(1322,'fai','fai_terme','metier_a_tisser','métier à tisser','153059',0,1,0,1063,' '),(1323,'fai','fai_terme','meule','meule','231888',0,1,0,1064,' '),(1324,'fai','fai_terme','meuliere','meulière','354957',0,0,0,1065,' '),(1325,'fai','fai_terme','milliaire','milliaire','335222',0,1,0,1616,' '),(1326,'fai','fai_terme','mine','mine','177267',0,1,0,1066,' '),(1327,'fai','fai_terme','mithraeum','mithraeum','147416',0,1,0,1765,' '),(1328,'fai','fai_terme','mole','môle','412299',0,0,0,1067,' '),(1329,'fai','fai_terme','monastere','monastère','319289',0,0,0,1068,' '),(1330,'fai','fai_terme','monnaie','monnaie','301405',0,1,0,1696,' '),(1331,'fai','fai_terme','monument','monument','207568',0,0,0,1069,' '),(1332,'fai','fai_terme','monument_a_portique','monument à portique','108413',0,0,0,1070,' '),(1333,'fai','fai_terme','monument_a_portiques','monument à portiques','230875',0,0,0,1071,' '),(1334,'fai','fai_terme','monument_commemoratif','monument commémoratif','319875',0,1,0,1072,' '),(1335,'fai','fai_terme','monument_des_eaux','monument des eaux','349612',0,0,0,1073,' '),(1336,'fai','fai_terme','monument_funeraire','monument funéraire','383329',0,1,0,1074,' '),(1337,'fai','fai_terme','monument_reliquaire','monument reliquaire','233709',0,0,0,1075,' '),(1338,'fai','fai_terme','monuments_aux_morts','monuments aux morts','397836',0,0,0,1619,' '),(1339,'fai','fai_terme','mosaique','mosaïque','372194',0,1,0,1076,' '),(1340,'fai','fai_terme','mosquee','mosquée','253565',0,1,0,1795,' '),(1341,'fai','fai_terme','motte','motte','378682',0,1,0,1077,' '),(1342,'fai','fai_terme','mouillage','mouillage','268477',0,0,0,1078,' '),(1343,'fai','fai_terme','moulage','moulage','241779',0,0,0,1079,' '),(1344,'fai','fai_terme','moule','moule','165148',0,1,0,1080,' '),(1345,'fai','fai_terme','moule_a_cloche','moule à cloche','363693',0,0,0,1081,' '),(1346,'fai','fai_terme','moule_a_flans_monetaire','moule à flans monétaire','362960',0,0,0,1082,' '),(1347,'fai','fai_terme','moule_de_cloche','moule de cloche','178288',0,0,0,1083,' '),(1348,'fai','fai_terme','moulin','moulin','500409',0,1,0,1084,' '),(1349,'fai','fai_terme','moulin_a_ble','moulin à blé','374199',0,1,0,1085,' '),(1350,'fai','fai_terme','moulin_a_broyer','moulin à broyer','196238',0,1,0,1086,' '),(1351,'fai','fai_terme','moulin_a_eau','moulin à eau','414627',0,0,0,1087,' '),(1352,'fai','fai_terme','moulin_a_maree','moulin à marée','751248',0,1,0,1635,' '),(1353,'fai','fai_terme','moulin_banal','moulin banal','290988',0,0,0,1088,' '),(1354,'fai','fai_terme','mur','mur','139336',0,0,0,1089,' '),(1355,'fai','fai_terme','mur_denclos','mur d\'enclos','136991',0,0,0,1095,' '),(1356,'fai','fai_terme','mur_de_berge','mur de berge','394837',0,0,0,1090,' '),(1357,'fai','fai_terme','mur_de_cloture','mur de clôture','171933',0,0,0,1091,' '),(1358,'fai','fai_terme','mur_de_defense','mur de défense','172457',0,0,0,1669,' '),(1359,'fai','fai_terme','mur_de_jardin','mur de jardin','363280',0,0,0,1092,' '),(1360,'fai','fai_terme','mur_de_soutenement','mur de soutènement','258532',0,0,0,1093,'à utiliser en 3 quand mur de soutènement de berge 2 termes génériques dans 2 rubriques'),(1361,'fai','fai_terme','mur_de_terrasse','mur de terrasse','120896',0,0,0,1094,' '),(1362,'fai','fai_terme','mur_de_ville','mur de ville','103090',0,0,0,1670,' '),(1363,'fai','fai_terme','mur_du_castrum','mur du castrum','236582',0,0,0,1096,' '),(1364,'fai','fai_terme','muraille','muraille','216998',0,0,0,1097,' '),(1365,'fai','fai_terme','muret_de_jardin','muret de jardin','280243',0,0,0,1749,' '),(1366,'fai','fai_terme','murette','murette','225622',0,0,0,1750,' '),(1367,'fai','fai_terme','murus_gallicus','murus gallicus','359326',0,1,0,1098,' '),(1368,'fai','fai_terme','musee','musée','263221',0,1,0,1099,' '),(1369,'fai','fai_terme','nacre','nacre','167400',0,1,0,1100,' '),(1370,'fai','fai_terme','nappe_phreatique','nappe phréatique','424381',0,1,0,1911,' '),(1371,'fai','fai_terme','narthex','narthex','380123',0,1,0,1772,' '),(1372,'fai','fai_terme','natatio','natatio','810520',0,0,0,1101,' '),(1373,'fai','fai_terme','necropole','nécropole','589174',0,1,0,1102,'lieu d\'inhumation extérieur et antique jusqu\'au haut Moyen Age'),(1374,'fai','fai_terme','necropole_conventuelle','nécropole conventuelle','110851',0,0,0,1103,' '),(1375,'fai','fai_terme','necropole_danimaux','nécropole d\'animaux','157306',0,1,0,1816,' '),(1376,'fai','fai_terme','necropole_denfants','nécropole d\'enfants','383567',0,0,0,1104,' '),(1377,'fai','fai_terme','necropole_dhopital','nécropole d\'hôpital','219894',0,0,0,1105,' '),(1378,'fai','fai_terme','necropole_de_communaute_religieuse','nécropole de communauté religieuse','327955',0,0,0,1817,' '),(1379,'fai','fai_terme','necropole_monastique','nécropole monastique','276412',0,0,0,1106,' '),(1380,'fai','fai_terme','nef','nef','222424',0,0,0,1107,' '),(1381,'fai','fai_terme','niveau_de_construction_cathedrale','niveau de construction (cathédrale)','841456',0,0,0,1108,' '),(1382,'fai','fai_terme','niveau_de_crue','niveau de crue','213907',0,1,0,1109,' '),(1383,'fai','fai_terme','niveau_marin','niveau marin','191743',0,1,0,1110,' '),(1384,'fai','fai_terme','nivellement','nivellement','382858',0,1,0,1111,' '),(1385,'fai','fai_terme','noria','noria','152010',0,1,0,1112,' '),(1386,'fai','fai_terme','noviciat','noviciat','352321',0,1,0,1113,' '),(1387,'fai','fai_terme','nymphee','nymphée','314450',0,1,0,1114,' '),(1388,'fai','fai_terme','occupation','occupation','498394',0,0,0,1115,' '),(1389,'fai','fai_terme','occupation_agricole','occupation agricole','852335',0,0,0,1116,' '),(1390,'fai','fai_terme','octroi','octroi','409057',0,1,0,1117,' '),(1391,'fai','fai_terme','odeon','odéon','374006',0,1,0,1118,' '),(1392,'fai','fai_terme','officine','officine','171465',0,0,0,1852,' '),(1393,'fai','fai_terme','offrande','offrande','615122',0,0,0,1119,' '),(1394,'fai','fai_terme','offrande_cultuelle','offrande cultuelle','324385',0,0,0,1120,' '),(1395,'fai','fai_terme','oliveraie','oliveraie','190975',0,1,0,1874,' '),(1396,'fai','fai_terme','oppidum','oppidum','206105',0,0,0,1121,' '),(1397,'fai','fai_terme','or','or','498562',0,1,0,1122,' '),(1398,'fai','fai_terme','oratoire','oratoire','369876',0,1,0,1123,' '),(1399,'fai','fai_terme','orfevre','orfèvre','267521',0,0,0,1124,' '),(1400,'fai','fai_terme','orfevrerie','orfèvrerie','203004',0,1,0,1125,' '),(1401,'fai','fai_terme','orniere','ornière','578819',0,1,0,1126,' '),(1402,'fai','fai_terme','orphelinat','orphelinat','573110',0,1,0,1722,' '),(1403,'fai','fai_terme','os','os','178993',0,1,0,1127,' '),(1404,'fai','fai_terme','oseraie','oseraie','102674',0,1,0,1875,' '),(1405,'fai','fai_terme','ossement','ossement','117938',0,1,0,1128,' '),(1406,'fai','fai_terme','ossements','ossements','187861',0,0,0,1129,' '),(1407,'fai','fai_terme','ossements_humains','ossements humains','358962',0,0,0,1130,' '),(1408,'fai','fai_terme','ossuaire','ossuaire','391959',0,1,0,1131,' '),(1409,'fai','fai_terme','oubliette','oubliette','160783',0,0,0,1708,' '),(1410,'fai','fai_terme','ouvrage_avance','ouvrage avancé','312839',0,1,0,1132,' '),(1411,'fai','fai_terme','ouvrage_de_defense','ouvrage de défense','427057',0,1,0,1133,' '),(1412,'fai','fai_terme','ouvrage_de_retenue','ouvrage de retenue','284511',0,0,0,1134,' '),(1413,'fai','fai_terme','ouvrage_defensif','ouvrage défensif','573271',0,0,0,1135,' '),(1414,'fai','fai_terme','ouvrage_hydraulique','ouvrage hydraulique','697085',0,0,0,1136,' '),(1415,'fai','fai_terme','ouvrage_hydraulique_souterrain','ouvrage hydraulique souterrain','446759',0,0,0,1654,' '),(1416,'fai','fai_terme','palais','palais','153334',0,1,0,1137,' '),(1417,'fai','fai_terme','palais_abbatial','palais abbatial','692739',0,0,0,1786,' '),(1418,'fai','fai_terme','palais_cardinalice','palais cardinalice','299534',0,0,0,1138,' '),(1419,'fai','fai_terme','palais_comtal','palais comtal','121496',0,0,0,1139,' '),(1420,'fai','fai_terme','palais_consulaire','palais consulaire','852413',0,0,0,1140,' '),(1421,'fai','fai_terme','palais_de_justice','palais de justice','157311',0,1,0,1141,'terme utilisé à partir du XVIIIe siècle, avant : utiliser Cour de justice'),(1422,'fai','fai_terme','palais_de_la_commune','palais de la commune','301016',0,0,0,1142,' '),(1423,'fai','fai_terme','palais_des_beaux_arts','palais des Beaux-Arts','257765',0,0,0,1717,' '),(1424,'fai','fai_terme','palais_des_comtes','palais des comtes','238129',0,0,0,1143,' '),(1425,'fai','fai_terme','palais_episcopal','palais épiscopal','117061',0,0,0,1144,' '),(1426,'fai','fai_terme','palais_seigneurial','palais seigneurial','195683',0,0,0,1145,' '),(1427,'fai','fai_terme','paleo_ruisseau','paléo-ruisseau','167169',0,0,0,1912,' '),(1428,'fai','fai_terme','paleochenal','paléochenal','500810',0,1,0,1146,' '),(1429,'fai','fai_terme','paleosol','paléosol','105943',0,1,0,1148,' '),(1430,'fai','fai_terme','palestre','palestre','323724',0,1,0,1149,' '),(1431,'fai','fai_terme','palissade','palissade','368482',0,1,0,1150,' '),(1432,'fai','fai_terme','palissage','palissage','403592',0,0,0,1151,' '),(1433,'fai','fai_terme','palmeraie','palmeraie','272637',0,1,0,1876,' '),(1434,'fai','fai_terme','papeterie','papeterie','161916',0,1,0,1152,' '),(1435,'fai','fai_terme','parapet','parapet','283899',0,1,0,1153,' '),(1436,'fai','fai_terme','parc','parc','340347',0,0,0,1154,' '),(1437,'fai','fai_terme','parc_a_betail','parc à bétail','387440',0,1,0,1155,' '),(1438,'fai','fai_terme','parc_a_volaille','parc à volaille','114332',0,0,0,1877,' '),(1439,'fai','fai_terme','parc_public','parc public','185351',0,0,0,1156,' '),(1440,'fai','fai_terme','parcellaire','parcellaire','897980',0,1,0,1158,' '),(1441,'fai','fai_terme','parcheminier','parcheminier','256912',0,1,0,1853,' '),(1442,'fai','fai_terme','pars_rustica','pars rustica','116534',0,1,0,1159,' '),(1443,'fai','fai_terme','pars_urbana','pars urbana','150545',0,1,0,1751,' '),(1444,'fai','fai_terme','parvis','parvis','407342',0,1,0,1160,' '),(1445,'fai','fai_terme','passage','passage','723881',0,1,0,1161,' '),(1446,'fai','fai_terme','passage_souterrain','passage souterrain','323054',0,0,0,1162,' '),(1447,'fai','fai_terme','passage_voute_souterrain','passage voûté souterrain','201411',0,0,0,1163,' '),(1448,'fai','fai_terme','passerelle','passerelle','327224',0,0,0,1164,' '),(1449,'fai','fai_terme','patenoterie','patenôterie','108030',0,1,0,1166,' '),(1450,'fai','fai_terme','patenotrier','patenôtrier','299891',0,0,0,1167,' '),(1451,'fai','fai_terme','patio','patio','225869',0,0,0,1168,' '),(1452,'fai','fai_terme','pature','pâture','204955',0,0,0,1169,' '),(1453,'fai','fai_terme','pavillon','pavillon','358565',0,0,0,1752,' '),(1454,'fai','fai_terme','peausserie','peausserie','108268',0,0,0,1170,' '),(1455,'fai','fai_terme','peaussier','peaussier','374028',0,0,0,1171,' '),(1456,'fai','fai_terme','peche','pêche','397507',0,0,0,1172,' '),(1457,'fai','fai_terme','pecherie','pêcherie','409473',0,1,0,1173,' '),(1458,'fai','fai_terme','peinture_murale','peinture murale','321121',0,1,0,1174,' '),(1459,'fai','fai_terme','pelleterie','pelleterie','131521',0,1,0,1175,' '),(1460,'fai','fai_terme','pelletier','pelletier','391807',0,0,0,1176,' '),(1461,'fai','fai_terme','penitencier','pénitencier','334838',0,0,0,1177,' '),(1462,'fai','fai_terme','pente_amenagee','pente aménagée','498647',0,0,0,1178,' '),(1463,'fai','fai_terme','pepiniere','pépinière','338547',0,0,0,1179,' '),(1464,'fai','fai_terme','peribole','péribole','329336',0,1,0,1180,' '),(1465,'fai','fai_terme','peristyle','péristyle','214912',0,1,0,1182,' '),(1466,'fai','fai_terme','perron','perron','891206',0,0,0,1183,' '),(1467,'fai','fai_terme','peson','peson','768671',0,0,0,1184,' '),(1468,'fai','fai_terme','pessiere','pessière','365685',0,0,0,1185,' '),(1469,'fai','fai_terme','piece_dhabitation','pièce d\'habitation','226719',0,0,0,1753,' '),(1470,'fai','fai_terme','piece_residentielle','pièce résidentielle','105016',0,0,0,1186,' '),(1471,'fai','fai_terme','piedestal','piédestal','322830',0,0,0,1187,' '),(1472,'fai','fai_terme','pierre','pierre','143645',0,0,0,1188,' '),(1473,'fai','fai_terme','pierre_tombale','pierre tombale','364825',0,0,0,1189,' '),(1474,'fai','fai_terme','pierre_tombale_inscrite','pierre tombale inscrite','405094',0,0,0,1190,' '),(1475,'fai','fai_terme','pierree','pierrée','481671',0,0,0,1191,' '),(1476,'fai','fai_terme','pigeonnier','pigeonnier','585633',0,1,0,1192,' '),(1477,'fai','fai_terme','pile','pile','419307',0,0,0,1193,' '),(1478,'fai','fai_terme','pilier','pilier','319956',0,0,0,1194,' '),(1479,'fai','fai_terme','pilori','pilori','903790',0,1,0,1195,' '),(1480,'fai','fai_terme','pilotis','pilotis','133713',0,0,0,1196,' '),(1481,'fai','fai_terme','pipe','pipe','530211',0,0,0,1197,' '),(1482,'fai','fai_terme','piquet_de_soutien','piquet de soutien','423834',0,0,0,1198,' '),(1483,'fai','fai_terme','piscine','piscine','122045',0,1,0,1199,' '),(1484,'fai','fai_terme','piscine_baptismale','piscine baptismale','906186',0,1,0,1200,' '),(1485,'fai','fai_terme','piste','piste','150800',0,0,0,1201,' '),(1486,'fai','fai_terme','place','place','194807',0,1,0,1202,' '),(1487,'fai','fai_terme','place_darmes','place d\'armes','129904',0,0,0,1203,' '),(1488,'fai','fai_terme','place_du_marche','place du marché','297894',0,0,0,1688,' '),(1489,'fai','fai_terme','place_publique','place publique','386976',0,0,0,1204,' '),(1490,'fai','fai_terme','place_royale','place royale','427967',0,1,0,1617,' '),(1491,'fai','fai_terme','placette','placette','298354',0,0,0,1620,' '),(1492,'fai','fai_terme','plafond_peint','plafond peint','412190',0,0,0,1205,' '),(1493,'fai','fai_terme','plage','plage','275879',0,1,0,1206,' '),(1494,'fai','fai_terme','plaine_alluviale','plaine alluviale','143465',0,1,0,1207,' '),(1495,'fai','fai_terme','plaine_dinondation','plaine d\'inondation','184771',0,1,0,1208,' '),(1496,'fai','fai_terme','plaine_fluviale','plaine fluviale','719375',0,1,0,1209,' '),(1497,'fai','fai_terme','plancher_alluvial','plancher alluvial','182774',0,1,0,1210,' '),(1498,'fai','fai_terme','plantation','plantation','307926',0,1,0,1211,' '),(1499,'fai','fai_terme','plaque_de_plomb','plaque de plomb','329401',0,0,0,1212,' '),(1500,'fai','fai_terme','plaque_tombale','plaque tombale','189013',0,0,0,1213,' '),(1501,'fai','fai_terme','plaque_foyer','plaque-foyer','352679',0,0,0,1214,' '),(1502,'fai','fai_terme','plate_forme','plate-forme','241651',0,1,0,1216,' '),(1503,'fai','fai_terme','plate_forme_de_tir','plate-forme de tir','255899',0,0,0,1671,' '),(1504,'fai','fai_terme','plate_tombe','plate-tombe','449518',0,0,0,1217,' '),(1505,'fai','fai_terme','platee_de_temple','platée de temple','400815',0,0,0,1215,' '),(1506,'fai','fai_terme','pleine_terre','pleine terre','292042',0,1,0,1219,' '),(1507,'fai','fai_terme','plomb','plomb','212053',0,1,0,1220,' '),(1508,'fai','fai_terme','podium','podium','864767',0,1,0,1221,' '),(1509,'fai','fai_terme','poelier','poêlier','229957',0,0,0,1222,' '),(1510,'fai','fai_terme','poids_de_tisserand','poids de tisserand','219898',0,0,0,1223,' '),(1511,'fai','fai_terme','point_deau','point d\'eau','370156',0,0,0,1224,' '),(1512,'fai','fai_terme','point_deau_amenage','point d\'eau aménagé','366856',0,1,0,1655,' '),(1513,'fai','fai_terme','poissonnerie','poissonnerie','378903',0,0,0,1225,' '),(1514,'fai','fai_terme','pomerium','pomerium','305867',0,1,0,1226,' '),(1515,'fai','fai_terme','pompe','pompe','240993',0,1,0,1636,' '),(1516,'fai','fai_terme','pont','pont','278331',0,1,0,1227,' '),(1517,'fai','fai_terme','pont_de_bateaux','pont de bateaux','407929',0,1,0,1645,' '),(1518,'fai','fai_terme','pont_dormant','pont dormant','782432',0,1,0,1228,' '),(1519,'fai','fai_terme','pont_levis','pont-levis','236719',0,1,0,1230,' '),(1520,'fai','fai_terme','pontet','pontet','171764',0,0,0,1229,' '),(1521,'fai','fai_terme','ponton','ponton','164287',0,1,0,1231,' '),(1522,'fai','fai_terme','porcelain','porcelain','265003',0,0,0,1232,' '),(1523,'fai','fai_terme','porcelaine','porcelaine','330431',0,0,0,1233,' '),(1524,'fai','fai_terme','porche','porche','313479',0,1,0,1234,' '),(1525,'fai','fai_terme','porcherie','porcherie','244725',0,0,0,1235,' '),(1526,'fai','fai_terme','port','port','113360',0,1,0,1236,' '),(1527,'fai','fai_terme','portail','portail','145590',0,0,0,1237,' '),(1528,'fai','fai_terme','porte','porte','210209',0,1,0,1238,'à employer en 12 quand porte monumentale non défensive'),(1529,'fai','fai_terme','porte_deau','porte d\'eau','373963',0,1,0,1240,' '),(1530,'fai','fai_terme','porte_de_ville_fortifiee','porte de ville fortifiée','103373',0,0,0,1239,' '),(1531,'fai','fai_terme','porte_fortifiee','porte fortifiée','170131',0,0,0,1241,' '),(1532,'fai','fai_terme','porterie','porterie','744384',0,1,0,1242,' '),(1533,'fai','fai_terme','portique','portique','219317',0,1,0,1243,' '),(1534,'fai','fai_terme','portique_a_colonades','portique à colonades','164661',0,0,0,1244,' '),(1535,'fai','fai_terme','portique_monumental','portique monumental','140290',0,0,0,1245,' '),(1536,'fai','fai_terme','portus','portus','281911',0,0,0,1246,' '),(1537,'fai','fai_terme','poste','poste','208935',0,1,0,1709,'à n\'utiliser que si bureau de poste moderne'),(1538,'fai','fai_terme','poste_de_garde','poste de garde','408423',0,0,0,1247,' '),(1539,'fai','fai_terme','poste_de_tir','poste de tir','108193',0,0,0,1248,' '),(1540,'fai','fai_terme','potager','potager','396723',0,0,0,1249,' '),(1541,'fai','fai_terme','poterie','poterie','145319',0,0,0,1250,' '),(1542,'fai','fai_terme','poterne','poterne','434084',0,1,0,1251,' '),(1543,'fai','fai_terme','potier','potier','275723',0,0,0,1252,' '),(1544,'fai','fai_terme','poudre','poudre','245661',0,0,0,1253,' '),(1545,'fai','fai_terme','poudrerie','poudrerie','166386',0,1,0,1884,' '),(1546,'fai','fai_terme','poudriere','poudrière','128936',0,1,0,1254,' '),(1547,'fai','fai_terme','poulailler','poulailler','428717',0,1,0,1255,' '),(1548,'fai','fai_terme','pourrissoir','pourrissoir','994306',0,1,0,1256,' '),(1549,'fai','fai_terme','praefurnium','praefurnium','203248',0,1,0,1257,' '),(1550,'fai','fai_terme','prairie','prairie','184597',0,0,0,1258,' '),(1551,'fai','fai_terme','pre','pré','138958',0,1,0,1259,' '),(1552,'fai','fai_terme','preau','préau','360991',0,0,0,1260,'jardin clos'),(1553,'fai','fai_terme','prefecture','préfecture','287169',0,1,0,1261,' '),(1554,'fai','fai_terme','presbytere','presbytère','423037',0,1,0,1262,' '),(1555,'fai','fai_terme','presbyterium','presbyterium','339037',0,0,0,1263,' '),(1556,'fai','fai_terme','pressoir','pressoir','169422',0,1,0,1264,' '),(1557,'fai','fai_terme','pretoire','prétoire','871980',0,1,0,1265,' '),(1558,'fai','fai_terme','prevote','prévôté','276245',0,1,0,1266,' '),(1559,'fai','fai_terme','prieure','prieuré','229673',0,1,0,1267,' '),(1560,'fai','fai_terme','principia','principia','252972',0,1,0,1681,'quartier général dans un camp romain'),(1561,'fai','fai_terme','prison','prison','162767',0,1,0,1268,' '),(1562,'fai','fai_terme','prison_militaire','prison militaire','119024',0,1,0,1682,' '),(1563,'fai','fai_terme','procure','procure','201249',0,0,0,1269,' '),(1564,'fai','fai_terme','production_domestique','production domestique','369405',0,1,0,1754,' '),(1565,'fai','fai_terme','production_salicole','production salicole','336887',0,1,0,1854,' '),(1566,'fai','fai_terme','progradation','progradation','427033',0,1,0,1913,' '),(1567,'fai','fai_terme','promenade','promenade','346007',0,1,0,1270,' '),(1568,'fai','fai_terme','promenade_publique','promenade publique','205924',0,0,0,1271,' '),(1569,'fai','fai_terme','propylees','propylées','102427',0,0,0,1689,' '),(1570,'fai','fai_terme','puisard','puisard','364908',0,1,0,1272,' '),(1571,'fai','fai_terme','puisart','puisart','329197',0,0,0,1273,' '),(1572,'fai','fai_terme','puisart_prive','puisart privé','255621',0,0,0,1274,' '),(1573,'fai','fai_terme','puisoir','puisoir','239893',0,0,0,1275,' '),(1574,'fai','fai_terme','puits','puits','278974',0,1,0,1276,' '),(1575,'fai','fai_terme','puits_a_dromos','puits à dromos','330686',0,0,0,1277,' '),(1576,'fai','fai_terme','puits_a_offrande','puits à offrande','219040',0,0,0,1278,' '),(1577,'fai','fai_terme','puits_a_offrandes','puits à offrandes','808983',0,0,0,1279,' '),(1578,'fai','fai_terme','puits_a_usage_cultuel','puits à usage cultuel','329873',0,0,0,1766,' '),(1579,'fai','fai_terme','puits_citerne','puits citerne','392318',0,0,0,1280,' '),(1580,'fai','fai_terme','puits_cultuel','puits cultuel','313004',0,1,0,1281,' '),(1581,'fai','fai_terme','puits_dacces','puits d\'accès','293036',0,1,0,1282,' '),(1582,'fai','fai_terme','puits_daeration','puits d\'aération','419261',0,0,0,1283,' '),(1583,'fai','fai_terme','puits_dextraction','puits d\'extraction','782665',0,1,0,1285,' '),(1584,'fai','fai_terme','puits_de_contremine','puits de contremine','241032',0,0,0,1284,' '),(1585,'fai','fai_terme','puits_domestique','puits domestique','321018',0,0,0,1756,' '),(1586,'fai','fai_terme','puits_funeraire','puits funéraire','235916',0,1,0,1286,' '),(1587,'fai','fai_terme','puits_monumental','puits monumental','357483',0,1,0,1656,' '),(1588,'fai','fai_terme','puits_perdu','puits perdu','767247',0,0,0,1287,' '),(1589,'fai','fai_terme','puits_prive','puits privé','182099',0,0,0,1288,' '),(1590,'fai','fai_terme','puits_public','puits public','927147',0,0,0,1289,' '),(1591,'fai','fai_terme','puits_rituel','puits rituel','223269',0,0,0,1290,' '),(1592,'fai','fai_terme','puits_votif','puits votif','352235',0,0,0,1291,' '),(1593,'fai','fai_terme','puits_depotoir','puits-dépotoir','144685',0,0,0,1755,' '),(1594,'fai','fai_terme','puteus','puteus','313467',0,0,0,1292,' '),(1595,'fai','fai_terme','puteus_public','puteus public','291061',0,0,0,1657,' '),(1596,'fai','fai_terme','qasba','qasba','313335',0,1,0,1676,' '),(1597,'fai','fai_terme','quai','quai','389999',0,1,0,1293,' '),(1598,'fai','fai_terme','quartier_artisanal','quartier artisanal','511546',0,1,0,1294,' '),(1599,'fai','fai_terme','quartier_canonial','quartier canonial','526038',0,0,0,1295,' '),(1600,'fai','fai_terme','quartier_militaire','quartier militaire','586525',0,0,0,1683,' '),(1601,'fai','fai_terme','quartier_thermal','quartier thermal','180599',0,0,0,1729,' '),(1602,'fai','fai_terme','raffinerie','raffinerie','213693',0,1,0,1885,' '),(1603,'fai','fai_terme','raffinerie_a_sucre','raffinerie à sucre','177986',0,0,0,1296,' '),(1604,'fai','fai_terme','rail','rail','149926',0,0,0,1297,' '),(1605,'fai','fai_terme','rampe','rampe','248737',0,1,0,1298,' '),(1606,'fai','fai_terme','rampe_dacces','rampe d\'accès','271937',0,0,0,1641,' '),(1607,'fai','fai_terme','rampe_dartillerie','rampe d\'artillerie','190594',0,0,0,1299,' '),(1608,'fai','fai_terme','rampe_de_halage','rampe de halage','139021',0,1,0,1637,' '),(1609,'fai','fai_terme','rampe_de_soutenement','rampe de soutènement','141399',0,0,0,1642,' '),(1610,'fai','fai_terme','ravine','ravine','262658',0,0,0,1300,' '),(1611,'fai','fai_terme','rebut_de_cuisson','rebut de cuisson','233541',0,0,0,1301,' '),(1612,'fai','fai_terme','rebuts','rebuts','388713',0,0,0,1302,' '),(1613,'fai','fai_terme','rebuts_de_cuisson','rebuts de cuisson','876684',0,0,0,1303,' '),(1614,'fai','fai_terme','recevrie_de_leveche','recevrie de l\'Evêché','347793',0,0,0,1304,' '),(1615,'fai','fai_terme','recreusement_de_vallon','recreusement de vallon','276422',0,0,0,1305,' '),(1616,'fai','fai_terme','recuperation_de_materiau','récupération de matériau','209636',0,1,0,1306,' '),(1617,'fai','fai_terme','recuperation_de_materiaux','récupération de matériaux','308378',0,0,0,1307,' '),(1618,'fai','fai_terme','redan','redan','350605',0,0,0,1308,' '),(1619,'fai','fai_terme','redoute','redoute','251476',0,1,0,1309,' '),(1620,'fai','fai_terme','reduction','réduction','885980',0,1,0,1310,' '),(1621,'fai','fai_terme','reduction_de_minerai','réduction de minerai','196813',0,0,0,1311,' '),(1622,'fai','fai_terme','reduction_de_tombes','réduction de tombes','162350',0,0,0,1312,' '),(1623,'fai','fai_terme','reduit','réduit','167321',0,0,0,1672,' '),(1624,'fai','fai_terme','reduit_fortifie','réduit fortifié','153783',0,0,0,1313,' '),(1625,'fai','fai_terme','refectoire','réfectoire','160934',0,1,0,1314,' '),(1626,'fai','fai_terme','refosse','refossé','416011',0,0,0,1316,' '),(1627,'fai','fai_terme','refuge','refuge','425994',0,0,0,1317,' '),(1628,'fai','fai_terme','regard','regard','516433',0,0,0,1318,' '),(1629,'fai','fai_terme','rehaussement_de_niveau','rehaussement de niveau','141491',0,0,0,1643,' '),(1630,'fai','fai_terme','relais','relais','381137',0,0,0,1319,' '),(1631,'fai','fai_terme','relais_de_poste','relais de poste','353946',0,1,0,1710,' '),(1632,'fai','fai_terme','relief_cultuel_mithriaque','relief cultuel mithriaque','384028',0,0,0,1320,' '),(1633,'fai','fai_terme','reliquaire','reliquaire','234311',0,0,0,1321,' '),(1634,'fai','fai_terme','remblai','remblai','119817',0,1,0,1322,' '),(1635,'fai','fai_terme','remblaiement','remblaiement','994133',0,0,0,1323,' '),(1636,'fai','fai_terme','remblaiement_de_riviere','remblaiement de rivière','304144',0,0,0,1324,' '),(1637,'fai','fai_terme','remblayage','remblayage','327415',0,0,0,1644,' '),(1638,'fai','fai_terme','remise','remise','293246',0,0,0,1325,' '),(1639,'fai','fai_terme','rempart','rempart','307518',0,0,0,1326,' '),(1640,'fai','fai_terme','rempart_de_terre','rempart de terre','351380',0,1,0,1327,' '),(1641,'fai','fai_terme','reparation_de_navires','réparation de navires','146610',0,0,0,1855,' '),(1642,'fai','fai_terme','reserve_a_grains','réserve à grains','183970',0,0,0,1328,' '),(1643,'fai','fai_terme','reserve_deau','réserve d\'eau','386375',0,0,0,1330,' '),(1644,'fai','fai_terme','reserve_de_terre','réserve de terre','217437',0,0,0,1329,' '),(1645,'fai','fai_terme','reserve_domestique','réserve domestique','122881',0,0,0,1331,' '),(1646,'fai','fai_terme','reservoir','réservoir','608086',0,0,0,1332,' '),(1647,'fai','fai_terme','residence','résidence','224912',0,1,0,1333,' '),(1648,'fai','fai_terme','residence_abbatiale','résidence abbatiale','137950',0,0,0,1334,' '),(1649,'fai','fai_terme','residence_cardinalice','résidence cardinalice','981485',0,0,0,1335,' '),(1650,'fai','fai_terme','residence_episcopale','résidence épiscopale','117665',0,1,0,1336,' '),(1651,'fai','fai_terme','residence_pontificale','résidence pontificale','685362',0,1,0,1792,' '),(1652,'fai','fai_terme','residence_princiere','résidence princière','329123',0,0,0,1337,' '),(1653,'fai','fai_terme','resserre','resserre','440531',0,0,0,1338,' '),(1654,'fai','fai_terme','restauration','restauration','230759',0,0,0,1339,' '),(1655,'fai','fai_terme','retenue','retenue','247996',0,0,0,1638,'2 termes génériques : bassin ou étang'),(1656,'fai','fai_terme','rigole','rigole','192734',0,1,0,1341,' '),(1657,'fai','fai_terme','rigole_devacuation','rigole d\'évacuation','350436',0,0,0,1342,' '),(1658,'fai','fai_terme','rivage','rivage','193247',0,1,0,1343,' '),(1659,'fai','fai_terme','rive','rive','382160',0,1,0,1344,' '),(1660,'fai','fai_terme','riviere','rivière','133185',0,1,0,1345,' '),(1661,'fai','fai_terme','rotonde','rotonde','237953',0,0,0,1346,' '),(1662,'fai','fai_terme','rouissage','rouissage','968208',0,1,0,1856,' '),(1663,'fai','fai_terme','route','route','464078',0,0,0,1347,' '),(1664,'fai','fai_terme','ru_amenage','rû aménagé','278269',0,0,0,1348,' '),(1665,'fai','fai_terme','rue','rue','334944',0,1,0,1349,' '),(1666,'fai','fai_terme','ruelle','ruelle','214328',0,1,0,1350,' '),(1667,'fai','fai_terme','ruisseau','ruisseau','348243',0,1,0,1351,' '),(1668,'fai','fai_terme','ruisseau_canalise','ruisseau canalisé','155422',0,0,0,1352,' '),(1669,'fai','fai_terme','sable','sable','114146',0,0,0,1353,' '),(1670,'fai','fai_terme','sable_de_fonderie','sable de fonderie','263371',0,0,0,1354,' '),(1671,'fai','fai_terme','sabliere','sablière','146126',0,1,0,1355,' '),(1672,'fai','fai_terme','sablon','sablon','112534',0,0,0,1356,' '),(1673,'fai','fai_terme','sacellum','sacellum','384148',0,0,0,1357,' '),(1674,'fai','fai_terme','sacristie','sacristie','416738',0,1,0,1358,' '),(1675,'fai','fai_terme','salaison','salaison','392054',0,1,0,1857,' '),(1676,'fai','fai_terme','saline','saline','175262',0,1,0,1359,' '),(1677,'fai','fai_terme','salines','salines','174568',0,0,0,1360,' '),(1678,'fai','fai_terme','salle','salle','131855',0,0,0,1361,' '),(1679,'fai','fai_terme','salle_capitulaire','salle capitulaire','403844',0,1,0,1362,' '),(1680,'fai','fai_terme','salle_chaude','salle chaude','143724',0,0,0,1363,' '),(1681,'fai','fai_terme','salle_dapparat','salle d\'apparat','356538',0,0,0,1757,' '),(1682,'fai','fai_terme','salle_darmes','salle d\'armes','141923',0,1,0,1364,' '),(1683,'fai','fai_terme','salle_de_bain','salle de bain','384440',0,0,0,1365,' '),(1684,'fai','fai_terme','salle_de_billard','salle de billard','353068',0,0,0,1366,' '),(1685,'fai','fai_terme','salle_de_chauffe','salle de chauffe','909967',0,0,0,1367,' '),(1686,'fai','fai_terme','salle_de_garde','salle de garde','525266',0,0,0,1368,' '),(1687,'fai','fai_terme','salle_de_justice','salle de justice','336135',0,0,0,1369,' '),(1688,'fai','fai_terme','salle_de_spectacle','salle de spectacle','357020',0,0,0,1370,' '),(1689,'fai','fai_terme','salle_des_malades','salle des malades','128690',0,0,0,1371,' '),(1690,'fai','fai_terme','salle_du_chapitre','salle du chapitre','148022',0,0,0,1372,' '),(1691,'fai','fai_terme','salle_froide','salle froide','367137',0,0,0,1730,' '),(1692,'fai','fai_terme','salle_funeraire','salle funéraire','380488',0,0,0,1373,' '),(1693,'fai','fai_terme','salle_souterraine','salle souterraine','145261',0,1,0,1374,' '),(1694,'fai','fai_terme','salle_tiede','salle tiède','338358',0,0,0,1731,' '),(1695,'fai','fai_terme','saloir','saloir','269126',0,0,0,1375,' '),(1696,'fai','fai_terme','salpetriere','salpêtrière','409893',0,0,0,1376,' '),(1697,'fai','fai_terme','sanctuaire','sanctuaire','240123',0,1,0,1377,' '),(1698,'fai','fai_terme','sanctuaire_de_carrefour','sanctuaire de carrefour','106236',0,1,0,1767,' '),(1699,'fai','fai_terme','sanctuaire_des_eaux','sanctuaire des eaux','878827',0,0,0,1378,' '),(1700,'fai','fai_terme','sanctuaire_domestique','sanctuaire domestique','434109',0,0,0,1758,' '),(1701,'fai','fai_terme','sanctuaire_germanique','sanctuaire germanique','251635',0,1,0,1379,' '),(1702,'fai','fai_terme','sanctuaire_metroaque','sanctuaire métroaque','325105',0,1,0,1380,' '),(1703,'fai','fai_terme','sanctuaire_prive','sanctuaire privé','340392',0,0,0,1381,' '),(1704,'fai','fai_terme','sarcophage','sarcophage','109604',0,1,0,1382,' '),(1705,'fai','fai_terme','sarcophage_de_platre','sarcophage de plâtre','194852',0,1,0,1383,' '),(1706,'fai','fai_terme','sarcophage_de_plomb','sarcophage de plomb','128790',0,1,0,1384,' '),(1707,'fai','fai_terme','sarcophage_en_plomb','sarcophage en plomb','218828',0,0,0,1385,' '),(1708,'fai','fai_terme','sarcophage_en_remploi','sarcophage en remploi','243669',0,0,0,1387,' '),(1709,'fai','fai_terme','sarcophage_monolithe','sarcophage monolithe','240266',0,0,0,1818,' '),(1710,'fai','fai_terme','sarcophage_plomb','sarcophage plomb','336269',0,0,0,1388,' '),(1711,'fai','fai_terme','sarcophage_reutilise','sarcophage réutilisé','322251',0,0,0,1389,' '),(1712,'fai','fai_terme','sarcophages','sarcophages','191740',0,0,0,1390,' '),(1713,'fai','fai_terme','savon','savon','382270',0,0,0,1391,' '),(1714,'fai','fai_terme','savonnerie','savonnerie','168999',0,1,0,1392,' '),(1715,'fai','fai_terme','schiste','schiste','312000',0,0,0,1393,' '),(1716,'fai','fai_terme','schola','schola','288587',0,1,0,1394,' '),(1717,'fai','fai_terme','scierie','scierie','229458',0,0,0,1395,' '),(1718,'fai','fai_terme','scorie','scorie','159709',0,0,0,1396,' '),(1719,'fai','fai_terme','scories','scories','218685',0,1,0,1397,' '),(1720,'fai','fai_terme','scories_metalliques','scories métalliques','727915',0,0,0,1398,' '),(1721,'fai','fai_terme','sechage','séchage','420128',0,0,0,1878,' '),(1722,'fai','fai_terme','sechoir','séchoir','398695',0,1,0,1399,' '),(1723,'fai','fai_terme','sediment_fluviatile','sédiment fluviatile','332658',0,0,0,1400,' '),(1724,'fai','fai_terme','sediments','sédiments','938634',0,1,0,1401,' '),(1725,'fai','fai_terme','sel','sel','411833',0,0,0,1402,' '),(1726,'fai','fai_terme','seminaire','séminaire','344626',0,1,0,1403,' '),(1727,'fai','fai_terme','senechaussee','sénéchaussée','108211',0,0,0,1404,' '),(1728,'fai','fai_terme','sepulture','sépulture','307542',0,1,0,1406,' '),(1729,'fai','fai_terme','sepulture_animale','sépulture animale','349111',0,1,0,1407,'à employer pour toute sépulture d\'animal'),(1730,'fai','fai_terme','sepulture_canoniale','sépulture canoniale','271824',0,0,0,1408,' '),(1731,'fai','fai_terme','sepulture_collective','sépulture collective','153590',0,1,0,1409,' '),(1732,'fai','fai_terme','sepulture_creusee_dans_le_rocher','sépulture creusée dans le rocher','976335',0,0,0,1819,' '),(1733,'fai','fai_terme','sepulture_danimaux','sépulture d\'animaux','351945',0,0,0,1410,' '),(1734,'fai','fai_terme','sepulture_denfant','sépulture d\'enfant','122311',0,1,0,1415,' '),(1735,'fai','fai_terme','sepulture_denfants','sépulture d\'enfants','175493',0,0,0,1416,' '),(1736,'fai','fai_terme','sepulture_durgence','sépulture d\'urgence','939913',0,0,0,1419,' '),(1737,'fai','fai_terme','sepulture_de_nourrisson','sépulture de nourrisson','571723',0,1,0,1411,' '),(1738,'fai','fai_terme','sepulture_de_nouveau_ne','sépulture de nouveau né','296999',0,0,0,1412,' '),(1739,'fai','fai_terme','sepulture_de_pelerin','sépulture de pélerin','376997',0,0,0,1413,' '),(1740,'fai','fai_terme','sepulture_de_religieux','sépulture de religieux','100576',0,0,0,1414,' '),(1741,'fai','fai_terme','sepulture_deplacee','sépulture déplacée','180713',0,0,0,1417,' '),(1742,'fai','fai_terme','sepulture_double','sépulture double','337294',0,0,0,1418,' '),(1743,'fai','fai_terme','sepulture_ecclesiastique','sépulture ecclésiastique','299427',0,0,0,1420,' '),(1744,'fai','fai_terme','sepulture_en_fosse','sépulture en fosse','344442',0,0,0,1421,' '),(1745,'fai','fai_terme','sepulture_en_pleine_terre','sépulture en pleine terre','118269',0,0,0,1820,' '),(1746,'fai','fai_terme','sepulture_isolee','sépulture isolée','121396',0,1,0,1422,'quand sépulture seule hors d\'un cimetière'),(1747,'fai','fai_terme','sepulture_juive','sépulture juive','339945',0,1,0,1423,' '),(1748,'fai','fai_terme','sepulture_maconnee','sépulture maçonnée','221567',0,0,0,1424,' '),(1749,'fai','fai_terme','sepulture_multiple','sépulture multiple','408924',0,1,0,1425,' '),(1750,'fai','fai_terme','sepulture_perinatale','sépulture périnatale','365383',0,0,0,1426,' '),(1751,'fai','fai_terme','sepulture_privilegiee','sépulture privilégiée','385631',0,1,0,1427,' '),(1752,'fai','fai_terme','sepulture_protestante','sépulture protestante','233463',0,1,0,1428,' '),(1753,'fai','fai_terme','sepulture_reutilisee','sépulture réutilisée','509671',0,1,0,1821,' '),(1754,'fai','fai_terme','sepulture_rupestre','sépulture rupestre','247251',0,0,0,1429,' '),(1755,'fai','fai_terme','sepulture_secondaire','sépulture secondaire','366636',0,1,0,1822,' '),(1756,'fai','fai_terme','sepulture_sous_lauzes','sépulture sous lauzes','369653',0,1,0,1430,' '),(1757,'fai','fai_terme','sepulture_sous_tuiles','sépulture sous tuiles','276543',0,1,0,1431,' '),(1758,'fai','fai_terme','serre à zero','serre','511407',0,0,0,1432,' '),(1759,'fai','fai_terme','seuil','seuil','360141',0,0,0,1433,' '),(1760,'fai','fai_terme','siderurgie','sidérurgie','195779',0,0,0,1434,' '),(1761,'fai','fai_terme','siege_dassemblee_politique','siège d\'assemblée politique','120511',0,0,0,1435,' '),(1762,'fai','fai_terme','siege_de_bailliage','siège de bailliage','426543',0,0,0,1436,' '),(1763,'fai','fai_terme','siege_du_pouvoir_public','siège du pouvoir public','858913',0,1,0,1700,' '),(1764,'fai','fai_terme','sigillee','sigillée','210862',0,0,0,1437,' '),(1765,'fai','fai_terme','silex','silex','971473',0,0,0,1438,' '),(1766,'fai','fai_terme','silo','silo','193820',0,1,0,1440,' '),(1767,'fai','fai_terme','silo_aerien','silo aérien','110450',0,0,0,1759,' '),(1768,'fai','fai_terme','socle','Socle','169170',0,0,0,1441,' '),(1769,'fai','fai_terme','sol','sol','261575',0,0,0,1442,' '),(1770,'fai','fai_terme','sol_de_circulation','sol de circulation','247235',0,0,0,1443,' '),(1771,'fai','fai_terme','soufre','soufre','394024',0,0,0,1444,' '),(1772,'fai','fai_terme','source','source','160291',0,0,0,1445,' '),(1773,'fai','fai_terme','sous_sol','sous-sol','209823',0,0,0,1446,' '),(1774,'fai','fai_terme','soutenement','soutènement','265117',0,1,0,1447,' '),(1775,'fai','fai_terme','soutenement_de_berge','soutènement de berge','129584',0,0,0,1448,' '),(1776,'fai','fai_terme','souterrain','souterrain','343120',0,1,0,1449,' '),(1777,'fai','fai_terme','spina','spina','251043',0,0,0,1450,' '),(1778,'fai','fai_terme','square','square','345423',0,1,0,1451,' '),(1779,'fai','fai_terme','squelette_danimal','squelette d\'animal','268118',0,0,0,1452,' '),(1780,'fai','fai_terme','squelette_denfant','squelette d\'enfant','325187',0,0,0,1453,' '),(1781,'fai','fai_terme','stade','stade','391444',0,1,0,1726,' '),(1782,'fai','fai_terme','statuaire','statuaire','186548',0,0,0,1454,' '),(1783,'fai','fai_terme','statue','statue','241308',0,0,0,1455,' '),(1784,'fai','fai_terme','stele','stèle','199893',0,0,0,1456,' '),(1785,'fai','fai_terme','stele_funeraire','stèle funéraire','310226',0,0,0,1457,' '),(1786,'fai','fai_terme','stele_juive','stèle juive','214227',0,0,0,1458,' '),(1787,'fai','fai_terme','stockage','stockage','340136',0,1,0,1459,' '),(1788,'fai','fai_terme','stockage_de_grains','stockage de grains','273477',0,0,0,1460,' '),(1789,'fai','fai_terme','structure_a_fonction_cultuelle','structure à fonction cultuelle','105106',0,0,0,1461,' '),(1790,'fai','fai_terme','structure_agricole','structure agricole','868677',0,1,0,1879,' '),(1791,'fai','fai_terme','structure_artisanale','structure artisanale','633493',0,0,0,1462,' '),(1792,'fai','fai_terme','structure_de_combustion','structure de combustion','421991',0,0,0,1463,' '),(1793,'fai','fai_terme','structure_de_cuisson','structure de cuisson','257227',0,1,0,1858,' '),(1794,'fai','fai_terme','structure_de_sechage','structure de séchage','171897',0,1,0,1464,' '),(1795,'fai','fai_terme','structure_fortifiee','structure fortifiée','314130',0,0,0,1465,' '),(1796,'fai','fai_terme','stuc','stuc','237854',0,0,0,1466,' '),(1797,'fai','fai_terme','stylobate','stylobate','302255',0,0,0,1467,' '),(1798,'fai','fai_terme','subsistances_militaires','subsistances militaires','193669',0,0,0,1468,' '),(1799,'fai','fai_terme','sucrerie','sucrerie','649430',0,0,0,1469,' '),(1800,'fai','fai_terme','surface_de_circulation','surface de circulation','314723',0,0,0,1470,' '),(1801,'fai','fai_terme','synagogue','synagogue','399756',0,1,0,1471,' '),(1802,'fai','fai_terme','systeme_cadastral','système cadastral','383571',0,0,0,1621,' '),(1803,'fai','fai_terme','systeme_de_defense','système de défense','101239',0,0,0,1673,' '),(1804,'fai','fai_terme','tabac','tabac','286709',0,0,0,1472,' '),(1805,'fai','fai_terme','table_doffrande','table d\'offrande','289682',0,0,0,1473,' '),(1806,'fai','fai_terme','tabletier','tabletier','161166',0,0,0,1474,' '),(1807,'fai','fai_terme','tabletterie','tabletterie','203931',0,1,0,1475,' '),(1808,'fai','fai_terme','taille_de_la_pierre','taille de la pierre','314897',0,0,0,1476,' '),(1809,'fai','fai_terme','taille_de_pierre','taille de pierre','152727',0,0,0,1477,' '),(1810,'fai','fai_terme','talus','talus','247278',0,1,0,1478,' '),(1811,'fai','fai_terme','talus_dartillerie','talus d\'artillerie','208833',0,0,0,1479,' '),(1812,'fai','fai_terme','talweg','talweg','184876',0,1,0,1480,' '),(1813,'fai','fai_terme','tannerie','tannerie','279231',0,1,0,1481,' '),(1814,'fai','fai_terme','tanneur','tanneur','424094',0,0,0,1482,' '),(1815,'fai','fai_terme','taverne','taverne','290839',0,0,0,1737,' '),(1816,'fai','fai_terme','teinture','teinture','200910',0,0,0,1483,' '),(1817,'fai','fai_terme','teinturerie','teinturerie','899940',0,1,0,1484,' '),(1818,'fai','fai_terme','teinturier','teinturier','268023',0,0,0,1485,' '),(1819,'fai','fai_terme','temple','temple','428794',0,1,0,1486,' '),(1820,'fai','fai_terme','temple_a_galerie','temple à galerie','386686',0,0,0,1487,' '),(1821,'fai','fai_terme','temple_protestant','temple protestant','305066',0,1,0,1488,' '),(1822,'fai','fai_terme','tepidarium','tepidarium','176043',0,1,0,1489,' '),(1823,'fai','fai_terme','terrain_agricole','terrain agricole','235495',0,0,0,1490,' '),(1824,'fai','fai_terme','terrain_de_manoeuvre','terrain de manoeuvre','153978',0,0,0,1684,' '),(1825,'fai','fai_terme','terrain_inondable','terrain inondable','968591',0,1,0,1491,' '),(1826,'fai','fai_terme','terrain_libre','terrain libre','310046',0,0,0,1492,' '),(1827,'fai','fai_terme','terrain_marecageux','terrain marécageux','114884',0,0,0,1493,' '),(1828,'fai','fai_terme','terrain_militaire','terrain militaire','420345',0,1,0,1685,' '),(1829,'fai','fai_terme','terrain_vague','terrain vague','514042',0,1,0,1494,' '),(1830,'fai','fai_terme','terrasse','terrasse','318202',0,1,0,1495,' '),(1831,'fai','fai_terme','terrasse_dartillerie','terrasse d\'artillerie','299664',0,0,0,1496,' '),(1832,'fai','fai_terme','terrasse_fluviale','terrasse fluviale','386890',0,0,0,1497,' '),(1833,'fai','fai_terme','terrasse_naturelle','terrasse naturelle','250968',0,1,0,1915,' '),(1834,'fai','fai_terme','terrassement','terrassement','172915',0,0,0,1498,' '),(1835,'fai','fai_terme','terre','terre','297248',0,0,0,1499,' '),(1836,'fai','fai_terme','terre_agricole','terre agricole','410547',0,0,0,1880,' '),(1837,'fai','fai_terme','terre_cultivee','terre cultivée','204601',0,0,0,1500,' '),(1838,'fai','fai_terme','terre_libre','terre libre','238019',0,0,0,1501,' '),(1839,'fai','fai_terme','terre_plein','terre-plein','255258',0,0,0,1502,' '),(1840,'fai','fai_terme','terres','terres','929463',0,0,0,1503,' '),(1841,'fai','fai_terme','terres_noires','terres noires','221339',0,1,0,1504,' '),(1842,'fai','fai_terme','tertre','tertre','326711',0,0,0,1505,' '),(1843,'fai','fai_terme','tessoniere','tessonière','297190',0,1,0,1506,' '),(1844,'fai','fai_terme','tessonniere','tessonnière','131850',0,1,0,1507,' '),(1845,'fai','fai_terme','tete_de_pont','tête de pont','861027',0,0,0,1646,' '),(1846,'fai','fai_terme','textile','textile','112586',0,1,0,1508,' '),(1847,'fai','fai_terme','thalweg','thalweg','783066',0,0,0,1914,' '),(1848,'fai','fai_terme','theatre','théâtre','297487',0,1,0,1509,' '),(1849,'fai','fai_terme','theatre_amphitheatre','théâtre-amphithéâtre','149903',0,1,0,1727,' '),(1850,'fai','fai_terme','thermes','thermes','161912',0,1,0,1510,' '),(1851,'fai','fai_terme','thermes_prives','thermes privés','414137',0,0,0,1511,' '),(1852,'fai','fai_terme','thermes_publics','thermes publics','117985',0,0,0,1512,' '),(1853,'fai','fai_terme','tissage','tissage','859384',0,1,0,1513,' '),(1854,'fai','fai_terme','tisserand','tisserand','316279',0,0,0,1514,' '),(1855,'fai','fai_terme','tombe','tombe','220053',0,0,0,1515,' '),(1856,'fai','fai_terme','tombe_a_char','tombe à char','113064',0,1,0,1823,' '),(1857,'fai','fai_terme','tombe_a_encadrement','tombe à encadrement','321329',0,0,0,1516,' '),(1858,'fai','fai_terme','tombe_avec_lauzes','tombe avec lauzes','140635',0,0,0,1824,' '),(1859,'fai','fai_terme','tombe_batie','tombe bâtie','121607',0,0,0,1825,' '),(1860,'fai','fai_terme','tombe_canoniale','tombe canoniale','384191',0,0,0,1517,' '),(1861,'fai','fai_terme','tombe_coffree','tombe coffrée','421149',0,0,0,1518,' '),(1862,'fai','fai_terme','tombe_collective','tombe collective','217542',0,0,0,1519,' '),(1863,'fai','fai_terme','tombe_construite','tombe construite','130192',0,0,0,1520,' '),(1864,'fai','fai_terme','tombe_double','tombe double','412162',0,1,0,1521,' '),(1865,'fai','fai_terme','tombe_en_amphore','tombe en amphore','890742',0,0,0,1522,' '),(1866,'fai','fai_terme','tombe_isolee','tombe isolée','279874',0,0,0,1523,' '),(1867,'fai','fai_terme','tombe_maconnee','tombe maçonnée','158065',0,0,0,1524,' '),(1868,'fai','fai_terme','tombe_perinatale','tombe périnatale','287024',0,0,0,1525,' '),(1869,'fai','fai_terme','tombe_privilegiee','tombe privilégiée','247647',0,0,0,1826,' '),(1870,'fai','fai_terme','tombe_rupestre','tombe rupestre','162264',0,1,0,1526,' '),(1871,'fai','fai_terme','tombe_sous_lauzes','tombe sous lauzes','292827',0,0,0,1527,' '),(1872,'fai','fai_terme','tombe_sous_tuiles','tombe sous tuiles','359087',0,0,0,1528,' '),(1873,'fai','fai_terme','tombeau','tombeau','266664',0,1,0,1529,' '),(1874,'fai','fai_terme','tombeau_maconne','tombeau maçonné','160997',0,0,0,1530,' '),(1875,'fai','fai_terme','tonnellerie','tonnellerie','208463',0,1,0,1859,' '),(1876,'fai','fai_terme','torrefaction_de_cereales','torréfaction de céréales','923074',0,0,0,1531,' '),(1877,'fai','fai_terme','tour','tour','179214',0,1,0,1533,' '),(1878,'fai','fai_terme','tour_de_potier','tour de potier','497202',0,1,0,1534,' '),(1879,'fai','fai_terme','tour_clocher','tour-clocher','607123',0,0,0,1536,' '),(1880,'fai','fai_terme','tour_porche','tour-porche','410412',0,0,0,1538,' '),(1881,'fai','fai_terme','tour_porte','tour-porte','156532',0,0,0,1539,' '),(1882,'fai','fai_terme','tour_poterne','tour-poterne','307446',0,0,0,1540,' '),(1883,'fai','fai_terme','tour_residence','tour-résidence','333211',0,0,0,1541,' '),(1884,'fai','fai_terme','tourbe','tourbe','153645',0,0,0,1535,' '),(1885,'fai','fai_terme','tourbiere','tourbière','112257',0,1,0,1916,' '),(1886,'fai','fai_terme','tourelle','tourelle','136936',0,0,0,1537,' '),(1887,'fai','fai_terme','traitement_de_largile','traitement de l\'argile','215962',0,1,0,1542,' '),(1888,'fai','fai_terme','trame_urbaine','trame urbaine','427792',0,1,0,1543,' '),(1889,'fai','fai_terme','tranchee','tranchée','127456',0,0,0,1544,' '),(1890,'fai','fai_terme','tranchee_de_drainage','tranchée de drainage','215287',0,0,0,1545,' '),(1891,'fai','fai_terme','tranchee_chemin','tranchée-chemin','216664',0,0,0,1622,' '),(1892,'fai','fai_terme','transept','transept','262801',0,0,0,1546,' '),(1893,'fai','fai_terme','transgression','transgression','459498',0,1,0,1917,' '),(1894,'fai','fai_terme','transgression_marine','transgression marine','133972',0,0,0,1547,' '),(1895,'fai','fai_terme','travailleur_de_metaux','travailleur de métaux','229049',0,0,0,1548,' '),(1896,'fai','fai_terme','trefilerie','tréfilerie','286453',0,0,0,1549,' '),(1897,'fai','fai_terme','treille','treille','162740',0,0,0,1550,' '),(1898,'fai','fai_terme','tribunal','tribunal','370017',0,0,0,1551,' '),(1899,'fai','fai_terme','triclinium','triclinium','270371',0,0,0,1552,' '),(1900,'fai','fai_terme','trophee','trophée','433699',0,1,0,1690,' '),(1901,'fai','fai_terme','trottoir','trottoir','185997',0,1,0,1553,' '),(1902,'fai','fai_terme','trou_de_poteau','trou de poteau','353263',0,0,0,1760,' '),(1903,'fai','fai_terme','tuffeau','tuffeau','328927',0,0,0,1554,' '),(1904,'fai','fai_terme','tuile','tuile','621256',0,0,0,1555,' '),(1905,'fai','fai_terme','tuilerie','tuilerie','137963',0,1,0,1556,' '),(1906,'fai','fai_terme','tuilier','tuilier','367326',0,0,0,1557,' '),(1907,'fai','fai_terme','tumulus','tumulus','963562',0,1,0,1558,' '),(1908,'fai','fai_terme','tunnel','tunnel','358176',0,1,0,1559,' '),(1909,'fai','fai_terme','tuyau','tuyau','243639',0,0,0,1560,' '),(1910,'fai','fai_terme','tuyau_de_plomb','tuyau de plomb','223235',0,0,0,1561,' '),(1911,'fai','fai_terme','tuyere_de_forge','tuyère de forge','382608',0,0,0,1562,' '),(1912,'fai','fai_terme','universite','université','339308',0,1,0,1718,' '),(1913,'fai','fai_terme','urinoir','urinoir','167263',0,1,0,1661,'uniquement pour urinoir public'),(1914,'fai','fai_terme','urne','urne','396721',0,1,0,1563,' '),(1915,'fai','fai_terme','urne_cineraire','urne cinéraire','215966',0,0,0,1827,' '),(1916,'fai','fai_terme','urne_funeraire','urne funéraire','744786',0,0,0,1828,' '),(1917,'fai','fai_terme','usine','usine','255505',0,1,0,1564,' '),(1918,'fai','fai_terme','usine_de_salaison','usine de salaison','235033',0,0,0,1860,' '),(1919,'fai','fai_terme','usine_electrique','usine électrique','130156',0,0,0,1565,' '),(1920,'fai','fai_terme','ustrinum','ustrinum','715110',0,1,0,1566,'lieu où l\'on brûle un corps'),(1921,'fai','fai_terme','vallee_secondaire','vallée secondaire','113575',0,1,0,1568,' '),(1922,'fai','fai_terme','vallon','vallon','296493',0,0,0,1569,' '),(1923,'fai','fai_terme','vallum','vallum','257726',0,1,0,1570,' '),(1924,'fai','fai_terme','vanne','vanne','308315',0,1,0,1571,' '),(1925,'fai','fai_terme','vannerie','vannerie','383007',0,0,0,1572,' '),(1926,'fai','fai_terme','vase','vase','533675',0,0,0,1573,' '),(1927,'fai','fai_terme','vase_de_foulon','vase de foulon','395607',0,1,0,1861,' '),(1928,'fai','fai_terme','vase_funeraire','vase funéraire','357153',0,0,0,1574,' '),(1929,'fai','fai_terme','venelle','venelle','357845',0,0,0,1575,' '),(1930,'fai','fai_terme','verger','verger','390368',0,0,0,1576,' '),(1931,'fai','fai_terme','verre','verre','342129',0,1,0,1577,' '),(1932,'fai','fai_terme','verrerie','verrerie','323811',0,0,0,1578,' '),(1933,'fai','fai_terme','verrier','verrier','391420',0,0,0,1579,' '),(1934,'fai','fai_terme','vestiaire','vestiaire','323639',0,0,0,1732,' '),(1935,'fai','fai_terme','viaduc','viaduc','298923',0,1,0,1647,' '),(1936,'fai','fai_terme','vidange','vidange','226772',0,1,0,1580,' '),(1937,'fai','fai_terme','vidange_de_bassin','vidange de bassin','200670',0,0,0,1581,' '),(1938,'fai','fai_terme','vide_sanitaire','vide sanitaire','333818',0,1,0,1582,' '),(1939,'fai','fai_terme','vigne','vigne','210765',0,1,0,1583,' '),(1940,'fai','fai_terme','viguerie','viguerie','388568',0,0,0,1584,' '),(1941,'fai','fai_terme','villa','villa','115226',0,1,0,1585,'à n\'utiliser que pour l\'Antiquité et le haut Moyen Age, sinon employer habitation'),(1942,'fai','fai_terme','villa_pars_rustica','villa (pars rustica)','372544',0,0,0,1586,' '),(1943,'fai','fai_terme','villa_suburbaine','villa suburbaine','214727',0,0,0,1587,' '),(1944,'fai','fai_terme','vin','vin','297011',0,0,0,1588,' '),(1945,'fai','fai_terme','vinee','vinée','406994',0,0,0,1589,' '),(1946,'fai','fai_terme','vitrail','vitrail','123136',0,1,0,1862,' '),(1947,'fai','fai_terme','vivier','vivier','146439',0,1,0,1590,' '),(1948,'fai','fai_terme','voie','voie','277642',0,1,0,1591,' '),(1949,'fai','fai_terme','voie_deau','voie d\'eau','337665',0,0,0,1593,' '),(1950,'fai','fai_terme','voie_de_chemin_de_fer','voie de chemin de fer','401187',0,0,0,1592,' '),(1951,'fai','fai_terme','voie_ferree','voie ferrée','245691',0,0,0,1594,' '),(1952,'fai','fai_terme','voie_secondaire','voie secondaire','696163',0,0,0,1624,' '),(1953,'fai','fai_terme','voierie','voierie','290654',0,0,0,1595,' '),(1954,'fai','fai_terme','voirie','voirie','401656',0,0,0,1596,' '),(1955,'fai','fai_terme','voutement_de_riviere','voûtement de rivière','302327',0,0,0,1639,' '),(1956,'fai','fai_terme','zone_agraire','zone agraire','155679',0,0,0,1597,' '),(1957,'fai','fai_terme','zone_agricole','zone agricole','209009',0,0,0,1598,' '),(1958,'fai','fai_terme','zone_densilage','zone d\'ensilage','385423',0,0,0,1607,' '),(1959,'fai','fai_terme','zone_depandage','zone d\'épandage','121699',0,0,0,1608,' '),(1960,'fai','fai_terme','zone_de_chargement','zone de chargement','216536',0,0,0,1599,' '),(1961,'fai','fai_terme','zone_de_circulation','zone de circulation','112528',0,0,0,1600,' '),(1962,'fai','fai_terme','zone_de_cuisson','zone de cuisson','234873',0,0,0,1601,' '),(1963,'fai','fai_terme','zone_de_culture','zone de culture','300276',0,0,0,1602,' '),(1964,'fai','fai_terme','zone_de_debitage','zone de débitage','901566',0,0,0,1603,' '),(1965,'fai','fai_terme','zone_de_decharge','zone de décharge','312315',0,0,0,1604,' '),(1966,'fai','fai_terme','zone_de_rejets','zone de rejets','305371',0,0,0,1863,' '),(1967,'fai','fai_terme','zone_de_stockage','zone de stockage','174269',0,0,0,1605,' '),(1968,'fai','fai_terme','zone_de_stockage_chaufournier','zone de stockage (chaufournier)','135083',0,0,0,1606,' '),(1969,'fai','fai_terme','zone_humide','zone humide','408362',0,1,0,1609,' '),(1970,'fai','fai_terme','zone_inondable','zone inondable','116756',0,0,0,1610,' '),(1971,'fai','fai_terme','zone_maraichere','zone maraîchère','411562',0,0,0,1611,' '),(1972,'fai','fai_terme','zone_marecageuse','zone marécageuse','389536',0,1,0,1612,' '),(1973,'fai','fai_terme','zone_palustre','zone palustre','281348',0,0,0,1918,' '),(1974,'fai','fai_terme','zone_pastorale','zone pastorale','284989',0,0,0,1881,' '),(1975,'fai','fai_terme','zone_portuaire','zone portuaire','282793',0,0,0,1613,' '),(1976,'elementpoterie','elementpoterie_caracterepate','pas_dinclusion_visible','Pas d\'inclusion visible','375443',0,1,1,1,NULL),(1977,'elementpoterie','elementpoterie_caracterepate','inclusions_fines','Inclusions fines','211141',0,1,1,2,NULL),(1978,'elementpoterie','elementpoterie_caracterepate','inclusions_moyenne','Inclusions moyenne','240462',0,1,1,3,NULL),(1979,'elementpoterie','elementpoterie_caracterepate','inclusions_grosse','Inclusions grosse','978961',0,1,1,4,NULL),(1980,'elementpoterie','elementpoterie_caracterepate','inclusions_de_taille_exceptionnelle','Inclusions de taille exceptionnelle','282405',0,1,1,5,NULL),(1981,'elementpoterie','elementpoterie_caracterepate','inclusions_mineral_non_silicieuses','Inclusions minéral non silicieuses','113568',0,1,1,6,NULL),(1982,'elementpoterie','elementpoterie_caracterepate','terre_chamotte','Terre chamotté','313932',0,1,1,7,NULL),(1983,'elementpoterie','elementpoterie_caracterepate','pate_grasse','Pâte grasse','138762',0,1,1,8,NULL),(1984,'elementpoterie','elementpoterie_caracterepate','pate_a_tuile','Pâte à tuile','185597',0,1,1,9,NULL),(1985,'elementpoterie','elementpoterie_caracterepate','pate_a_inclusions_coquillieres','Pâte à inclusions coquillières','192484',0,1,1,10,NULL),(1986,'elementpoterie','elementpoterie_caracterepate','_a_determiner','* à déterminer','211002',0,1,1,999,NULL),(1991,'elementrecueilli','elementrecueilli_classe','n_naturel','N - Naturel','280129',0,1,1,1,NULL),(1992,'elementrecueilli','elementrecueilli_classe','c_element_de_construction','C - Elément de construction','202890',0,1,2,2,NULL),(1993,'elementrecueilli','elementrecueilli_classe','p_poterie_de_terre','P - Poterie de terre','391826',0,1,1,3,NULL),(1994,'elementrecueilli','elementrecueilli_classe','m_mesures','M - Mesures','285708',0,1,1,4,NULL),(1995,'elementrecueilli','elementrecueilli_classe','o_autres','O - Autres','882670',0,1,1,5,NULL),(1996,'elementrecueilli','elementrecueilli_classe','_a_determiner','* à déterminer','211002',0,1,2,999,NULL),(1998,'elementpoterie','elementpoterie_classification','prehistorique','Préhistorique','240171',0,1,1,1,NULL),(1999,'elementpoterie','elementpoterie_classification','protohistorique','Protohistorique','244304',0,1,1,2,NULL),(2000,'elementpoterie','elementpoterie_classification','antique','Antique','102663',0,1,1,3,NULL),(2001,'elementpoterie','elementpoterie_classification','haut_moyen_age','Haut Moyen âge','489220',0,1,1,4,NULL),(2002,'elementpoterie','elementpoterie_classification','medievale','Médiévale','427410',0,1,1,5,NULL),(2003,'elementpoterie','elementpoterie_classification','bas_moyen_age','Bas Moyen âge','293715',0,1,1,6,NULL),(2004,'elementpoterie','elementpoterie_classification','post_medievale','Post médiévale','359011',0,1,1,7,NULL),(2005,'elementpoterie','elementpoterie_classification','moderne','Moderne','296195',0,1,1,8,NULL),(2006,'elementpoterie','elementpoterie_classification','contemporaine','Contemporaine','162829',0,1,1,9,NULL),(2007,'elementpoterie','elementpoterie_classification','_a_determiner','* à déterminer','211002',0,1,1,999,NULL),(2013,'us','us_consistance','compacte','Compacte','130797',0,1,1,1,NULL),(2014,'us','us_consistance','meuble','Meuble','296882',0,1,1,2,NULL),(2015,'us','us_consistance','non_applicable','Non applicable','359173',0,1,1,3,NULL),(2016,'us','us_consistance','_a_determiner','* à déterminer','211002',0,1,1,999,NULL),(2020,'us','us_contexte','sous_sol_naturel','Sous-sol naturel','301283',0,1,1,1,NULL),(2021,'us','us_contexte','construction','Construction','329883',0,1,2,2,NULL),(2022,'us','us_contexte','remblais','Remblais','311855',0,1,1,3,NULL),(2023,'us','us_contexte','occupation','Occupation','130269',0,1,3,4,NULL),(2024,'us','us_contexte','remblais_doccupation','Remblais d\'occupation','277835',0,1,1,5,NULL),(2025,'us','us_contexte','demolition','Démolition','376923',0,1,4,6,NULL),(2026,'us','us_contexte','negatif','Négatif','190088',0,1,2,7,NULL),(2027,'us','us_contexte','alteration','Altération','893639',0,1,2,8,NULL),(2028,'us','us_contexte','reste_humain_en_connexion','Reste humain en connexion','175571',0,1,1,9,NULL),(2029,'us','us_contexte','technique','Technique','143930',0,1,1,10,NULL),(2030,'us','us_contexte','regroupement','Regroupement','270674',1,1,1,11,NULL),(2031,'us','us_contexte','_a_determiner','* à déterminer','211002',0,1,2,999,NULL),(2035,'elementrecueilli','elementrecueilli_etat','disponible','Disponible','182674',0,1,1,1,NULL),(2036,'elementrecueilli','elementrecueilli_etat','indisponible','Indisponible','344670',0,1,1,2,NULL),(2037,'elementrecueilli','elementrecueilli_etat','obsolete','Obsolète','335681',0,1,1,3,NULL),(2038,'elementrecueilli','elementrecueilli_etat','detruit','Détruit','199820',0,1,1,4,NULL),(2040,'elementrecueilli','elementrecueilli_etat','_a_determiner','* à déterminer','211002',0,1,1,999,NULL),(2042,'traitement','traitement_etat','a_faire','A faire','232389',0,1,1,1,NULL),(2043,'traitement','traitement_etat','en_cours','En cours','381801',0,1,1,2,NULL),(2044,'traitement','traitement_etat','realise','Réalisé','155353',0,1,1,3,NULL),(2045,'traitement','traitement_etat','abandonne','Abandonné','204298',0,1,1,4,NULL),(2046,'traitement','traitement_etat','_a_determiner','* à déterminer','211002',0,1,1,999,NULL),(2049,'elementrecueilli','elementrecueilli_fonction','lot_verre_plat','lot verre plat','526776',0,1,1,1,NULL),(2050,'elementrecueilli','elementrecueilli_fonction','lot_verre_creux','lot verre creux','420009',0,1,1,2,NULL),(2051,'elementrecueilli','elementrecueilli_fonction','lot_fer_indetermine','lot fer indéterminé','242927',0,1,1,3,NULL),(2052,'elementrecueilli','elementrecueilli_fonction','objet_fer_indetermine','objet fer indéterminé','535383',0,1,1,4,NULL),(2053,'elementrecueilli','elementrecueilli_fonction','objet_ceramique_indetermine','objet céramique indéterminé','184242',0,1,1,5,NULL),(2054,'elementrecueilli','elementrecueilli_fonction','objet_pierre_indetermine','objet pierre indéterminé','158241',0,1,1,6,NULL),(2055,'elementrecueilli','elementrecueilli_fonction','objet_bois_indetermine','objet bois indéterminé','177647',0,1,1,7,NULL),(2056,'elementrecueilli','elementrecueilli_fonction','objet_os_indetermine','objet os indéterminé','409287',0,1,1,8,NULL),(2057,'elementrecueilli','elementrecueilli_fonction','objet_cuir_indetermine','objet cuir indéterminé','439810',0,1,1,9,NULL),(2058,'elementrecueilli','elementrecueilli_fonction','objet_alliage_cuivreux','objet alliage cuivreux','232479',0,1,1,10,NULL),(2059,'elementrecueilli','elementrecueilli_fonction','autre','autre','130395',0,1,1,11,NULL),(2060,'elementrecueilli','elementrecueilli_fonction','_a_determiner','* à déterminer','211002',0,0,2,999,NULL),(2064,'elementpoterie','elementpoterie_forme','je_ne_sais_pas','Je ne sais pas','570146',0,1,1,1,NULL),(2065,'elementpoterie','elementpoterie_forme','coquemarss','Coquemarss','394461',0,1,1,2,NULL),(2066,'elementpoterie','elementpoterie_forme','pichets','Pichets','209722',0,1,1,3,NULL),(2067,'elementpoterie','elementpoterie_forme','cruches','Cruches','562433',0,1,1,4,NULL),(2068,'elementpoterie','elementpoterie_forme','pots','Pots','731758',0,1,1,5,NULL),(2069,'elementpoterie','elementpoterie_forme','autre','Autre','235677',0,1,1,6,NULL),(2071,'us','us_homogeneite','homogene','Homogène','334578',0,1,1,1,NULL),(2072,'us','us_homogeneite','heterogene','Hétérogène','138255',0,1,1,2,NULL),(2073,'us','us_homogeneite','_a_determiner','* à déterminer','211002',0,1,1,999,NULL),(2074,'us','us_methodefouille','mecanique','Mécanique','268433',0,1,1,1,NULL),(2075,'us','us_methodefouille','manuelle','Manuelle','107497',0,1,1,2,NULL),(2076,'us','us_methodefouille','fine','Fine','513538',0,1,1,3,NULL),(2077,'us','us_methodefouille','autre','Autre','235677',0,1,1,4,NULL),(2078,'us','us_methodefouille','_a_determiner','* à déterminer','211002',0,1,1,999,NULL),(2081,'intervenant','intervenant_metier','archeologue','Archéologue','176171',0,1,1,1,NULL),(2082,'intervenant','intervenant_metier','etudiant','Etudiant','228224',0,1,1,2,NULL),(2083,'intervenant','intervenant_metier','benevole','Bénévole','223137',0,1,1,3,NULL),(2084,'intervenant','intervenant_metier','restaurateur','Restaurateur','101584',0,1,1,4,NULL),(2085,'intervenant','intervenant_metier','gestionnaire_de_collection','Gestionnaire de collection','201654',0,1,1,5,NULL),(2086,'intervenant','intervenant_metier','autre','Autre','235677',0,1,1,6,NULL),(2087,'intervenant','intervenant_metier','_a_determiner','* à déterminer','211002',0,1,1,999,NULL),(2088,'oa','oa_nature','diagnostic','Diagnostic','303887',0,1,1,1,NULL),(2089,'oa','oa_nature','fouilles','Fouilles','364644',0,1,1,2,NULL),(2090,'oa','oa_nature','autre','Autre','235677',0,1,1,3,NULL),(2091,'oa','oa_nature','campagne_de_sondages','Campagne de sondages','274256',0,1,1,4,NULL),(2092,'oa','oa_nature','campagne_de_fouilles','Campagne de fouilles','127073',0,1,1,5,NULL),(2095,'elementrecueilli','elementrecueilli_naturematiere','h_restes_humains','H - Restes humains','337525',0,1,22,1,NULL),(2096,'elementrecueilli','elementrecueilli_naturematiere','a_restes_animaux','A - Restes animaux','110767',0,1,1,2,NULL),(2097,'elementrecueilli','elementrecueilli_naturematiere','v_vegetaux','V - Végétaux','952685',0,1,4,3,NULL),(2098,'elementrecueilli','elementrecueilli_naturematiere','c_coquillages','C - Coquillages','166319',0,1,3,4,NULL),(2099,'elementrecueilli','elementrecueilli_naturematiere','M__Mineraux','M - Minéraux','313313',0,1,2,5,NULL),(2100,'elementrecueilli','elementrecueilli_naturematiere','verre','Verre','170784',0,1,20,6,NULL),(2101,'elementrecueilli','elementrecueilli_naturematiere','Metal','Métal','415713',0,1,12,7,NULL),(2102,'elementrecueilli','elementrecueilli_naturematiere','ceramique','Céramique','121576',0,1,8,8,NULL),(2103,'elementrecueilli','elementrecueilli_naturematiere','pierre','Pierre','137904',0,1,18,9,NULL),(2104,'elementrecueilli','elementrecueilli_naturematiere','platre','Plâtre','421854',0,1,23,10,NULL),(2105,'elementrecueilli','elementrecueilli_naturematiere','bois','Bois','786510',0,1,6,11,NULL),(2106,'elementrecueilli','elementrecueilli_naturematiere','mortier','Mortier','335298',0,1,7,12,NULL),(2107,'elementrecueilli','elementrecueilli_naturematiere','monnaies','Monnaies','287954',0,1,13,13,NULL),(2108,'elementrecueilli','elementrecueilli_naturematiere','jetons_mereaux','Jetons, méreaux','293373',0,1,10,14,NULL),(2109,'elementrecueilli','elementrecueilli_naturematiere','medailles_insignes','Médailles, insignes','152375',0,1,11,15,NULL),(2110,'elementrecueilli','elementrecueilli_naturematiere','sceaux_cachets','Sceaux, cachets','205190',0,1,17,16,NULL),(2111,'elementrecueilli','elementrecueilli_naturematiere','poids_et_mesures','Poids et mesures','318887',0,1,15,17,NULL),(2112,'elementrecueilli','elementrecueilli_naturematiere','os_ivoire_corne_bois_de_cervide','Os, ivoire, corne, bois de cervide','208023',0,1,16,18,NULL),(2113,'elementrecueilli','elementrecueilli_naturematiere','tissus','Tissus','109480',0,1,19,19,NULL),(2114,'elementrecueilli','elementrecueilli_naturematiere','cuire_et_parchemin','Cuire et parchemin','886117',0,1,14,20,NULL),(2115,'elementrecueilli','elementrecueilli_naturematiere','coquillage','Coquillage','254697',0,1,9,21,NULL),(2116,'elementrecueilli','elementrecueilli_naturematiere','Autre_materieux','Autre matérieux','116150',0,1,5,22,NULL),(2117,'elementrecueilli','elementrecueilli_naturematiere','_a_determiner','* à déterminer','211002',0,1,21,999,NULL),(2126,'oa','oa_statut','_a_determiner','* à déterminer','211002',0,1,1,0,NULL),(2127,'oa','oa_statut','non_preventif','non préventif','380432',0,1,1,1,NULL),(2128,'oa','oa_statut','preventif','préventif','419080',0,1,1,2,NULL),(2129,'elementpoterie','elementpoterie_precisionproduction','17_cuisson_plutot_oxydante_contient_10_11_','17-Cuisson plutôt oxydante (contient 10, 11, 12, 14, 16)','267742',0,1,1,1,NULL),(2130,'elementpoterie','elementpoterie_precisionproduction','18_cuisson_plutot_reductrice_contient_13_et1','18-Cuisson plutôt réductrice (contient 13 et15)','289913',0,1,1,2,NULL),(2131,'elementpoterie','elementpoterie_precisionproduction','20_polissage_exterieur','20-Polissage extérieur','354042',0,1,1,3,NULL),(2132,'elementpoterie','elementpoterie_precisionproduction','21_polissage_exterieur_interieur','21-Polissage extérieur - intérieur','275501',0,1,1,4,NULL),(2133,'elementpoterie','elementpoterie_precisionproduction','23_polissage_par_outil_metalique_laissant_des','23-Polissage par outil métalique laissant des traînées d\'oxyde brunes','308807',0,1,1,5,NULL),(2134,'elementpoterie','elementpoterie_precisionproduction','24_polissage_interieur','24-Polissage intérieur','124582',0,1,1,6,NULL),(2135,'elementpoterie','elementpoterie_precisionproduction','30_polissage_exterieur','30-Polissage extérieur','121565',0,1,1,7,NULL),(2136,'elementpoterie','elementpoterie_precisionproduction','31_polissage_exterieur_interieur','31-Polissage extérieur - intérieur','223314',0,1,1,8,NULL),(2137,'elementpoterie','elementpoterie_precisionproduction','32_polissage_interieur','32-Polissage intérieur','274749',0,1,1,9,NULL),(2138,'elementpoterie','elementpoterie_precisionproduction','33_polissage_exterieur_peinture_exterieur','33-Polissage extérieur - peinture extérieur','125190',0,1,1,10,NULL),(2139,'elementpoterie','elementpoterie_precisionproduction','331_production_dite_de_tating','331-Production dite de \"Tating\"','332442',0,1,1,11,NULL),(2140,'elementpoterie','elementpoterie_precisionproduction','40_engobage_exterieur','40-Engobage extérieur','218866',0,1,1,12,NULL),(2141,'elementpoterie','elementpoterie_precisionproduction','41_engobage_interieur','41-Engobage intérieur','362859',0,1,1,13,NULL),(2142,'elementpoterie','elementpoterie_precisionproduction','42_engobage_exterieur_et_polissage','42-Engobage extérieur et polissage','145934',0,1,1,14,NULL),(2143,'elementpoterie','elementpoterie_precisionproduction','43_engobage_interieur_exterieur','43-Engobage intérieur - extérieur','395792',0,1,1,15,NULL),(2144,'elementpoterie','elementpoterie_precisionproduction','50_peinture_visible_non_caracterisee','50-Peinture visible, non caractérisée','187039',0,1,1,16,NULL),(2145,'elementpoterie','elementpoterie_precisionproduction','51_motif','51-Motif','263569',0,1,1,17,NULL),(2146,'elementpoterie','elementpoterie_precisionproduction','52_coulures','52-\"Coulures\"','287365',0,1,1,18,NULL),(2147,'elementpoterie','elementpoterie_precisionproduction','53_virgules','53-\"Virgules\"','259249',0,1,1,19,NULL),(2148,'elementpoterie','elementpoterie_precisionproduction','54_flammules_en_faisceux_croises','54-\"Flammules\" en faisceux croisés','390133',0,1,1,20,NULL),(2149,'elementpoterie','elementpoterie_precisionproduction','55_flammules_en_faisceux','55-\"Flammules\" en faisceux','313808',0,1,1,21,NULL),(2150,'elementpoterie','elementpoterie_precisionproduction','56_decore_peint_non_caracterise_puis_poliss','56-Décore peint, non caractérisé, puis polissage','130167',0,1,1,22,NULL),(2151,'elementpoterie','elementpoterie_precisionproduction','58_motif_peint_puis_polissage','58-Motif peint, puis polissage','398440',0,1,1,23,NULL),(2152,'elementpoterie','elementpoterie_precisionproduction','60_glacure_exterieur','60-Glaçure extérieur','415010',0,1,1,24,NULL),(2153,'elementpoterie','elementpoterie_precisionproduction','61_glacure_exterieur_interieur','61-Glaçure extérieur - intérieur','212759',0,1,1,25,NULL),(2154,'elementpoterie','elementpoterie_precisionproduction','62_glacure_interieur','62-Glaçure intérieur','359866',0,1,1,26,NULL),(2155,'elementpoterie','elementpoterie_precisionproduction','63_glacure_sur_decor_peint_ou_modele_a_la_bar','63-Glaçure sur décor peint ou modelé à la barbotine colorée','338406',0,1,1,27,NULL),(2156,'elementpoterie','elementpoterie_precisionproduction','69_glacure_sur_engobe','69-Glaçure sur engobe','339842',0,1,1,28,NULL),(2157,'elementpoterie','elementpoterie_precisionproduction','70_pate_gresee_non_glacuree_ouverte','70-Pâte grésée non glaçurée (\"ouverte\")','783105',0,1,1,29,NULL),(2158,'elementpoterie','elementpoterie_precisionproduction','71_gres_en_pate_claire','71-Grès en pâte claire','566649',0,1,1,30,NULL),(2159,'elementpoterie','elementpoterie_precisionproduction','72_pate_gresee_glacuree_regroupe_72_et_73','72-Pâte grésée, glaçurée (regroupe 72 et 73)','239911',0,1,1,31,NULL),(2160,'elementpoterie','elementpoterie_precisionproduction','73_gres_glacure_sel_cendre','73-grès glaçuré (sel, cendre)','322549',0,1,1,32,NULL),(2161,'elementpoterie','elementpoterie_precisionproduction','74_pate_glacure_et_oxydes_colores_gres_azur','74-Pâte glaçuré et oxydes colorés (\"grès azuré\")','225578',0,1,1,33,NULL),(2162,'elementpoterie','elementpoterie_precisionproduction','76_gres_en_pate_brune','76-Grès en pâte brune','111136',0,1,1,34,NULL),(2163,'elementpoterie','elementpoterie_precisionproduction','77_pate_gresee_decor_a_sgraffiato_glacue','77-Pâte grésée, décor \"a sgraffiato\" glaçué','982617',0,1,1,35,NULL),(2164,'elementpoterie','elementpoterie_precisionproduction','79_pate_gresee_decor_a_la_corne','79-Pâte grésée, décor \"à la corne\"','874829',0,1,1,36,NULL),(2165,'elementpoterie','elementpoterie_precisionproduction','999__a_determiner','999-* à déterminer','322660',0,1,1,999,NULL),(2166,'elementpoterie','elementpoterie_precisionproduction','80_le_test_a_steph','80-Le test à steph','215498',0,1,1,1000,NULL),(2192,'elementpoterie','elementpoterie_production','0_production_non_caracterise','0-Production non caractérisé','236091',0,1,1,1,NULL),(2193,'elementpoterie','elementpoterie_production','1_vernis_argileux_greses','1-Vernis argileux, greses','296666',0,1,2,2,NULL),(2194,'elementpoterie','elementpoterie_production','2_polissage_sur_pate_cuite_en_atmosphere_oxyd','2-Polissage sur pâte cuite en atmosphère oxydante','414425',0,1,3,3,NULL),(2195,'elementpoterie','elementpoterie_production','3_polissage_sur_pate_cuite_en_atmosphere_redu','3-Polissage sur pâte cuite en atmosphère réductrice','256888',0,1,4,4,NULL),(2196,'elementpoterie','elementpoterie_production','4_engobage_et_film','4-Engobage et film','313342',0,1,5,5,NULL),(2197,'elementpoterie','elementpoterie_production','5_decor_peint','5-Décor peint','152224',0,1,6,6,NULL),(2198,'elementpoterie','elementpoterie_production','6_glacure_plombifere','6-Glaçure plombifère','402196',0,1,7,7,NULL),(2199,'elementpoterie','elementpoterie_production','7_gres_gresage','7-Gres, gresage','771283',0,1,8,8,NULL),(2200,'elementpoterie','elementpoterie_production','8_faience','8-Faïence','190535',0,1,9,9,NULL),(2201,'elementpoterie','elementpoterie_production','9_porcelaine','9-Porcelaine','361507',0,1,10,10,NULL),(2202,'elementpoterie','elementpoterie_production','999__a_determiner','999-* à déterminer','322660',0,0,2,999,NULL),(2207,'participation','participation_role','_a_determiner','* à déterminer','211002',0,1,1,0,NULL),(2208,'participation','participation_role','responsable_d’operation','Responsable d’opération','593369',0,1,4,1,NULL),(2209,'participation','participation_role','responsable_de_secteur','Responsable de secteur','362816',0,1,2,2,NULL),(2210,'participation','participation_role','technicien_de_fouille','Technicien de fouille','334164',0,1,1,3,NULL),(2211,'participation','participation_role','stagiaire','Stagiaire','344899',0,1,5,4,NULL),(2214,'superposition','us_superposition_type','superposition','Superposition','862627',0,1,1,1,NULL),(2215,'superposition','us_superposition_type','recoupement','Recoupement','374713',0,1,1,2,NULL),(2216,'superposition','us_superposition_type','remplissage','Remplissage','227356',0,1,1,3,NULL),(2217,'superposition','us_superposition_type','derasement_erosion_horizontale','Dérasement (érosion horizontale)','163216',0,1,1,4,NULL),(2218,'superposition','us_superposition_type','appui_laterale','Appui latérale','784461',0,1,1,5,NULL),(2219,'superposition','us_superposition_type','alteration','Altération','893639',0,1,1,6,NULL),(2220,'superposition','us_superposition_type','occlusion','Occlusion','100658',0,1,1,7,NULL),(2221,'superposition','us_superposition_type','reprise','Reprise','926383',0,1,1,8,NULL),(2222,'superposition','us_superposition_type','autre','Autre','235677',0,1,1,9,NULL),(2229,'elementrecueilli','elementrecueilli_statut','sur_site_/_en_cours_dexcavation','Sur site / en cours d\'excavation','135960',0,1,1,1,NULL),(2230,'elementrecueilli','elementrecueilli_statut','non_lave','Non lavé','384714',0,1,1,2,NULL),(2231,'elementrecueilli','elementrecueilli_statut','lave_et_trie','lavé et trié','325961',0,1,1,3,NULL),(2232,'elementrecueilli','elementrecueilli_statut','enregistre','Enregistré','172004',0,1,1,4,NULL),(2233,'elementrecueilli','elementrecueilli_statut','restaure','Restauré','240847',0,1,1,5,NULL),(2234,'elementrecueilli','elementrecueilli_statut','etudie','Etudié','252088',0,1,1,6,NULL),(2235,'elementrecueilli','elementrecueilli_statut','stocke','Stocké','642539',0,1,1,7,NULL),(2236,'elementrecueilli','elementrecueilli_statut','_a_determiner','* à déterminer','211002',0,1,1,999,NULL),(2244,'synchronisation','us_synchronisation_type','identite','Identité','210872',0,1,1,1,NULL),(2245,'synchronisation','us_synchronisation_type','formation_liee','Formation liée','151023',0,1,1,2,NULL),(2246,'synchronisation','us_synchronisation_type','harpage_mur_imbrique','Harpage (mur imbriqué)','400258',0,1,1,3,NULL),(2247,'synchronisation','us_synchronisation_type','autre','Autre','235677',0,1,1,9999,NULL),(2251,'association_er','elementrecueilli_association_type','dissociation','Dissociation','168514',0,1,1,1,NULL),(2252,'association_er','elementrecueilli_association_type','reconstitution','Reconstitution','379815',0,1,1,2,NULL),(2253,'association_er','elementrecueilli_association_type','autre','Autre','187955',0,1,1,9999,NULL),(2254,'document','document_type','photo','Photo','358132',0,1,1,1,NULL),(2255,'document','document_type','video','Video','317134',0,1,1,2,NULL),(2256,'document','document_type','audio','Audio','365298',0,1,1,3,NULL),(2257,'document','document_type','croquis','croquis','225131',0,1,1,4,NULL),(2258,'document','document_type','texte','Texte','723552',0,1,1,5,NULL),(2259,'document','document_type','_a_determiner','* à déterminer','211002',0,1,1,999,NULL),(2261,'intervention','intervention_type','a_determinier','A déterminier','289643',0,1,1,0,NULL),(2262,'intervention','intervention_type','releve','Relevé','104888',0,1,1,1,NULL),(2263,'intervention','intervention_type','enregistrement','Enregistrement','342647',0,1,1,2,NULL),(2264,'intervention','intervention_type','fouille_fine','Fouille fine','124573',0,1,1,3,NULL),(2265,'intervention','intervention_type','_a_determiner','* à déterminer','211002',0,1,1,999,NULL),(2268,'traitement','traitement_type','datation_c14','Datation c14','230781',0,1,1,1,NULL),(2269,'traitement','traitement_type','dendrochronologie','Dendrochronologie','463472',0,1,1,2,NULL),(2270,'traitement','traitement_type','thermoluminescence','Thermoluminescence','252552',0,1,1,3,NULL),(2271,'traitement','traitement_type','prelevement_mobilier','Prélèvement mobilier','104242',0,1,1,4,NULL),(2272,'traitement','traitement_type','prelevement_sedimentaire','Prélèvement sédimentaire','412284',0,1,1,5,NULL),(2273,'traitement','traitement_type','restauration','Restauration','244493',0,1,1,6,NULL),(2274,'traitement','traitement_type','pret_pour_etude','Prêt pour étude','250327',0,1,1,7,NULL),(2275,'traitement','traitement_type','autre','Autre','235677',0,1,1,8,NULL),(2276,'traitement','traitement_type','_a_determiner','* à déterminer','211002',0,1,1,999,NULL),(2283,'fai','categorie_type','fai_interfai_type','Fait Archéologique - Interfai - Type ','318759',1,1,7,NULL,'Type de relation entre 2 Fait Archéologique'),(2284,'fai','fai_interfai_type','proche_de','Proche de','672056',0,1,1,NULL,'Fait Archéologique proche de'),(2285,'fai','fai_interfai_type','fait_partie_de','Fait partie de','249142',0,1,2,NULL,'Fait Archéologique fait partie de'),(2289,'elementrecueilli','categorie_type','elementrecueilli_association_type','Type d\'association entre ER','125614',1,1,1,NULL,NULL),(2290,'elementrecueilli','categorie_type','elementrecueilli_forme','Elément recueilli - Forme ','956250',1,1,5,NULL,''),(2291,'elementrecueilli','elementrecueilli_forme','verre_16eme-17eme','Verre 16ème-17ème','410109',0,1,3,1,''),(2292,'elementrecueilli','elementrecueilli_forme','forme_ouverte','Forme ouverte','148544',0,0,37,2,''),(2293,'elementrecueilli','elementrecueilli_forme','coupe','Coupe','420960',0,1,6,3,''),(2294,'elementrecueilli','elementrecueilli_forme','cruche','Cruche','103286',0,1,27,30,''),(2295,'elementrecueilli','elementrecueilli_forme','gourde','Gourde','240128',0,1,21,33,''),(2296,'elementrecueilli','elementrecueilli_forme','bouteille_a_pied','Bouteille à pied','300011',0,1,12,39,''),(2297,'fai','categorie_type','fai_type','Fait Archéologique - Type ','201208',1,1,9,NULL,'Type de Fait Archéologique'),(2298,'fai','fai_type','fait_archeologique','Fait Archéologique','271512',1,1,1,NULL,'Fait Archéologique'),(2299,'fai','fai_type','structure_archeologique_complexe','Structure Archéologique Complexe','327039',1,1,2,NULL,'Structure Archéologique Complexe'),(2300,'us','categorie_type','us_homogeneite','Homogénéité','247798',1,1,2,NULL,NULL),(2301,'us','categorie_type','us_contexte','Type de contexte','166334',1,1,18,NULL,NULL),(2302,'categorie','categorie_object','categorie_object','Catgorie des objets foncitonnels','222060',1,1,0,NULL,NULL),(2303,'categorie','categorie_object','categorie_relation','Catégorie des relations','318870',1,1,0,NULL,NULL),(2304,'categorie','categorie_relation','presence_de','Présence de','632167',1,1,5,NULL,NULL),(2305,'categorie','categorie_relation','absence_de','Absence de','428908',1,1,6,NULL,NULL),(2306,'elementrecueilli','elementrecueilli_forme','gobelet','Gobelet','162290',0,1,20,4,''),(2307,'elementrecueilli','elementrecueilli_forme','vase','Vase','227699',0,1,29,5,''),(2308,'elementrecueilli','elementrecueilli_forme','verre_a_pied','Verre à pied','311422',0,1,13,6,''),(2309,'elementrecueilli','elementrecueilli_forme','verre_a_jambe','Verre à jambe','120305',0,1,11,7,''),(2310,'elementrecueilli','elementrecueilli_forme','plat','Plat','306874',0,1,25,8,''),(2311,'elementrecueilli','elementrecueilli_forme','drageoir','Drageoir','377097',0,1,33,9,''),(2312,'elementrecueilli','elementrecueilli_forme','tasse','Tasse','238805',0,1,28,10,''),(2313,'elementrecueilli','elementrecueilli_forme','coupe_a_pied','Coupe à pied','129427',0,1,19,11,''),(2314,'elementrecueilli','elementrecueilli_forme','coupelle','Coupelle','428896',0,1,23,12,''),(2315,'elementrecueilli','elementrecueilli_forme','verre_biconique','Verre biconique','382152',0,1,18,13,''),(2316,'elementrecueilli','elementrecueilli_forme','verre_bitronconique','Verre bitronconique','125240',0,1,22,14,''),(2317,'elementrecueilli','elementrecueilli_forme','verre_conique','Verre conique','378674',0,1,30,15,''),(2318,'elementrecueilli','elementrecueilli_forme','verre_tronconique','Verre tronconique','625661',0,1,36,16,''),(2319,'elementrecueilli','elementrecueilli_forme','verre_campagniforme','Verre campagniforme','183474',0,1,26,17,''),(2320,'elementrecueilli','elementrecueilli_forme','verre_cylindrique','Verre cylindrique','480969',0,1,32,18,''),(2321,'elementrecueilli','elementrecueilli_forme','verre_hemispherique','Verre hémisphérique','241619',0,1,34,19,''),(2322,'elementrecueilli','elementrecueilli_forme','verre_a_tige','Verre à tige','145123',0,1,17,20,''),(2323,'elementrecueilli','elementrecueilli_forme','verre_a_bouton','Verre à bouton','369382',0,1,9,21,''),(2324,'elementrecueilli','elementrecueilli_forme','verre_a_balustre','Verre à balustre','418743',0,1,7,22,''),(2325,'elementrecueilli','elementrecueilli_forme','verre_a_serpents','Verre à serpents','176758',0,1,15,23,''),(2326,'elementrecueilli','elementrecueilli_forme','forme_fermee','Forme fermée','373673',0,1,4,24,''),(2327,'elementrecueilli','elementrecueilli_forme','aiguière','Aiguière','206861',0,1,5,25,''),(2328,'elementrecueilli','elementrecueilli_forme','albarello','Albarello','177241',0,1,8,26,''),(2329,'elementrecueilli','elementrecueilli_forme','bouteille','Bouteille','364555',0,1,10,27,''),(2330,'elementrecueilli','elementrecueilli_forme','burette','Burette','867432',0,1,14,28,''),(2331,'elementrecueilli','elementrecueilli_forme','carafe','Carafe','114198',0,1,16,29,''),(2332,'elementrecueilli','elementrecueilli_forme','curcubite','Curcubite','413086',0,1,31,31,''),(2333,'elementrecueilli','elementrecueilli_forme','kuttrolf','Kuttrolf','214284',0,1,24,32,''),(2334,'elementrecueilli','elementrecueilli_forme','flasque','Flasque','423732',0,1,35,34,''),(2335,'categorie','categorie_type','organisme_type','Organisme - Type',NULL,1,1,13,2443,NULL),(2336,'organisme','organisme_type','organisme_interne','Organisme Interne',NULL,0,1,1,2444,NULL),(2337,'organisme','organisme_type','organisme_externe','Organisme Externe',NULL,0,1,2,2445,NULL),(2338,'categorie','categorie_type','emplacement_type','Emplacement - Type',NULL,1,1,13,2338,NULL),(2339,'emplacement','emplacement_type','site','Site',NULL,0,1,1,2339,NULL),(2340,'emplacement','emplacement_type','batiment','Batiment',NULL,0,1,2,2340,NULL),(2341,'emplacement','emplacement_type','etage','Etage',NULL,0,1,2,2341,NULL),(2342,'emplacement','emplacement_type','salle','Salle',NULL,0,1,2,2342,NULL),(2343,'emplacement','emplacement_type','armoire','Armoire',NULL,0,1,2,2343,NULL),(2344,'emplacement','emplacement_type','etagere','Etagère',NULL,0,1,2,2344,NULL),(2530,'categorie','categorie_type','conteneur_type','Conteneur - Type',NULL,1,1,13,2530,NULL),(2531,'conteneur','conteneur_type','boite_curver','Boite Curver',NULL,0,1,1,2531,NULL),(2532,'conteneur','conteneur_type','test','test',NULL,0,1,2,2532,NULL),(2533,'elementpoterie','elementpoterie_forme','la_forme_a_steph','La forme à steph',NULL,0,1,2,2533,NULL),(2534,'emplacement','emplacement_type','racine','Racine',NULL,0,1,0,NULL,'Type emplacement racine, pour instancier une racine par entité admin dans la table emplacement'),(2535,'categorie','categorie_type','essais','Essais',NULL,0,1,24,2535,NULL),(2536,'categorie','categorie_type','performance','Performance',NULL,0,1,25,2536,NULL),(2537,'elementpoterie','elementpoterie_forme','la_forme_a_ridge','La forme à Ridge',NULL,0,1,3,2537,NULL),(2550,'elementrecueilli','elementrecueilli_fonction','asdz','asdz',NULL,0,1,3,2550,NULL),(2559,'elementrecueilli','elementrecueilli_naturematiere','jjjjjjjjjjjjj','jjjjjjjjjjjjj',NULL,0,1,24,2559,NULL),(2579,'elementrecueilli','elementrecueilli_forme','n','n',NULL,0,1,38,2579,NULL),(2581,'elementrecueilli','elementrecueilli_naturematiere','kkkk','kkkk',NULL,0,1,25,2581,NULL),(2785,'Categorie','categorie_type','table_categorie','Table vers catégorie',NULL,1,1,0,NULL,'Aucune'),(2786,'association_er','table_categorie','association_er-typeasso_id','typeasso',NULL,1,1,0,NULL,'Aucune'),(2787,'fsn_categorie_relation','table_categorie','fsn_categorie_relation-element_b','elementB',NULL,1,1,0,NULL,'Aucune'),(2788,'fsn_categorie_relation','table_categorie','fsn_categorie_relation-type_relation_id','typeRelation',NULL,1,1,0,NULL,'Aucune'),(2789,'fsn_categorie_relation','table_categorie','fsn_categorie_relation-element_a','elementA',NULL,1,1,0,NULL,'Aucune'),(2790,'conteneur','table_categorie','conteneur-type_id','type',NULL,1,1,0,NULL,'Aucune'),(2791,'elementrecueilli','table_categorie','elementrecueilli-statut_id','statut',NULL,1,1,0,NULL,'Aucune'),(2792,'elementrecueilli','table_categorie','elementrecueilli-fonction_id','fonction',NULL,1,1,0,NULL,'Aucune'),(2793,'elementrecueilli','table_categorie','elementrecueilli-classe_id','classe',NULL,1,1,0,NULL,'Aucune'),(2794,'elementrecueilli','table_categorie','elementrecueilli-etat_id','etat',NULL,1,1,0,NULL,'Aucune'),(2795,'elementpoterie','table_categorie','elementpoterie-caracterepate_id','caracterepate',NULL,1,1,0,NULL,'Aucune'),(2796,'elementpoterie','table_categorie','elementpoterie-forme_id','forme',NULL,1,1,0,NULL,'Aucune'),(2797,'elementpoterie','table_categorie','elementpoterie-classification_id','classification',NULL,1,1,0,NULL,'Aucune'),(2798,'elementpoterie','table_categorie','elementpoterie-production_id','production',NULL,1,1,0,NULL,'Aucune'),(2799,'elementpoterie','table_categorie','elementpoterie-precisionproduction_id','precisionproduction',NULL,1,1,0,NULL,'Aucune'),(2800,'emplacement','table_categorie','emplacement-type_id','type',NULL,1,1,0,NULL,'Aucune'),(2801,'er_naturematiere','table_categorie','er_naturematiere-naturematiere_id','naturematiere',NULL,1,1,0,NULL,'Aucune'),(2802,'fai','table_categorie','fai-typefai_id','typeFAI',NULL,1,1,0,NULL,'Aucune'),(2803,'interfait','table_categorie','interfait-typeinterfait_id','typeInterFAI',NULL,1,1,0,NULL,'Aucune'),(2804,'intervenant','table_categorie','intervenant-metier_id','metier',NULL,1,1,0,NULL,'Aucune'),(2805,'intervention_us','table_categorie','intervention_us-type_id','typeintervenant',NULL,1,1,0,NULL,'Aucune'),(2806,'oa','table_categorie','oa-nature_id','nature',NULL,1,1,0,NULL,'Aucune'),(2807,'oa','table_categorie','oa-oastatut_id','oastatut',NULL,1,1,0,NULL,'Aucune'),(2808,'fsn_organisme','table_categorie','fsn_organisme-type_id','type',NULL,1,1,0,NULL,'Aucune'),(2809,'participation','table_categorie','participation-role_id','role',NULL,1,1,0,NULL,'Aucune'),(2810,'superposition','table_categorie','superposition-sptype_id','sptype',NULL,1,1,0,NULL,'Aucune'),(2811,'synchronisation','table_categorie','synchronisation-synchrotype_id','synchrotype',NULL,1,1,0,NULL,'Aucune'),(2812,'traitement','table_categorie','traitement-type_id','type',NULL,1,1,0,NULL,'Aucune'),(2813,'traitement','table_categorie','traitement-etat_id','etat',NULL,1,1,0,NULL,'Aucune'),(2814,'us','table_categorie','us-contexte_id','contexte',NULL,1,1,0,NULL,'Aucune'),(2815,'us','table_categorie','us-consistance_id','consistance',NULL,1,1,0,NULL,'Aucune'),(2816,'us','table_categorie','us-methodefouille_id','methodefouille',NULL,1,1,0,NULL,'Aucune'),(2817,'us','table_categorie','us-homogeneite_id','homogeneite',NULL,1,1,0,NULL,'Aucune'),(2818,'association_er','association_er-typeasso_id','elementrecueilli_association_type','typeasso',NULL,1,1,0,NULL,NULL),(2819,'fsn_categorie_relation','fsn_categorie_relation-element_b','','elementB',NULL,1,1,0,NULL,NULL),(2820,'fsn_categorie_relation','fsn_categorie_relation-type_relation_id','','typeRelation',NULL,1,1,0,NULL,NULL),(2821,'fsn_categorie_relation','fsn_categorie_relation-element_a','','elementA',NULL,1,1,0,NULL,NULL),(2822,'conteneur','conteneur-type_id','conteneur_type','type',NULL,1,1,0,NULL,NULL),(2823,'elementrecueilli','elementrecueilli-statut_id','elementrecueilli_statut','statut',NULL,1,1,0,NULL,NULL),(2824,'elementrecueilli','elementrecueilli-fonction_id','elementrecueilli_fonction','fonction',NULL,1,1,0,NULL,NULL),(2825,'elementrecueilli','elementrecueilli-classe_id','elementrecueilli_classe','classe',NULL,1,1,0,NULL,NULL),(2826,'elementrecueilli','elementrecueilli-etat_id','elementrecueilli_etat','etat',NULL,1,1,0,NULL,NULL),(2827,'elementpoterie','elementpoterie-caracterepate_id','elementpoterie_caracterepate','caracterepate',NULL,1,1,0,NULL,NULL),(2828,'elementpoterie','elementpoterie-forme_id','elementpoterie_forme','forme',NULL,1,1,0,NULL,NULL),(2829,'elementpoterie','elementpoterie-classification_id','elementpoterie_classification','classification',NULL,1,1,0,NULL,NULL),(2830,'elementpoterie','elementpoterie-production_id','elementpoterie_production','production',NULL,1,1,0,NULL,NULL),(2831,'elementpoterie','elementpoterie-precisionproduction_id','elementpoterie_precisionproduction','precisionproduction',NULL,1,1,0,NULL,NULL),(2832,'emplacement','emplacement-type_id','emplacement_type','type',NULL,1,1,0,NULL,NULL),(2833,'er_naturematiere','er_naturematiere-naturematiere_id','elementrecueilli_naturematiere','naturematiere',NULL,1,1,0,NULL,NULL),(2834,'fai','fai-typefai_id','fai_type','typeFAI',NULL,1,1,0,NULL,NULL),(2835,'fai','fai-typefai_id','fai_terme','typeFAI',NULL,1,1,0,NULL,NULL),(2836,'fai','fai-typefai_id','fai_fonction','typeFAI',NULL,1,1,0,NULL,NULL),(2837,'fai','fai-typefai_id','fai_rubrique','typeFAI',NULL,1,1,0,NULL,NULL),(2838,'interfait','interfait-typeinterfait_id','fai_interfai_type','typeInterFAI',NULL,1,1,0,NULL,NULL),(2839,'intervenant','intervenant-metier_id','intervenant_metier','metier',NULL,1,1,0,NULL,NULL),(2840,'intervention_us','intervention_us-type_id','intervention_type','typeIntervention',NULL,1,1,0,NULL,NULL),(2841,'oa','oa-nature_id','oa_nature','nature',NULL,1,1,0,NULL,NULL),(2842,'oa','oa-oastatut_id','oa_statut','oaStatut',NULL,1,1,0,NULL,NULL),(2843,'fsn_organisme','fsn_organisme-type_id','organisme_type','type',NULL,1,1,0,NULL,NULL),(2844,'participation','participation-role_id','participation_role','role',NULL,1,1,0,NULL,NULL),(2845,'superposition','superposition-sptype_id','us_superposition_type','superpositionType',NULL,1,1,0,NULL,NULL),(2846,'synchronisation','synchronisation-synchrotype_id','us_synchronisation_type','synchronisationType',NULL,1,1,0,NULL,NULL),(2847,'traitement','traitement-type_id','traitement_type','type',NULL,1,1,0,NULL,NULL),(2848,'traitement','traitement-etat_id','traitement_etat','etat',NULL,1,1,0,NULL,NULL),(2849,'us','us-contexte_id','us_contexte','contexte',NULL,1,1,0,NULL,NULL),(2850,'us','us-consistance_id','us_consistance','consistance',NULL,1,1,0,NULL,NULL),(2851,'us','us-methodefouille_id','us_methodefouille','methodefouille',NULL,1,1,0,NULL,NULL),(2852,'us','us-homogeneite_id','us_homogeneite','homogeneite',NULL,1,1,0,NULL,NULL);
/*!40000 ALTER TABLE `fsn_categorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fsn_categorie_relation`
--

DROP TABLE IF EXISTS `fsn_categorie_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fsn_categorie_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `element_a` int(11) NOT NULL COMMENT 'Categorie A',
  `type_relation` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `type_relation_id` int(11) DEFAULT NULL,
  `element_b` int(11) NOT NULL COMMENT 'Categorie B',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `ordre` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `FK_fsn_categorie_relation_fsn_categorie_idx` (`type_relation_id`),
  KEY `FK_fsn_categorie_relation_element_a_idx` (`element_a`),
  KEY `FK_fsn_categorie_relation_element_b_idx` (`element_b`),
  CONSTRAINT `FK_fsn_categorie_relation_element_a` FOREIGN KEY (`element_a`) REFERENCES `fsn_categorie` (`id`) ON UPDATE NO ACTION,
  CONSTRAINT `FK_fsn_categorie_relation_element_b` FOREIGN KEY (`element_b`) REFERENCES `fsn_categorie` (`id`) ON UPDATE NO ACTION,
  CONSTRAINT `FK_fsn_categorie_relation_fsn_categorie` FOREIGN KEY (`type_relation_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4029 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fsn_categorie_relation`
--

LOCK TABLES `fsn_categorie_relation` WRITE;
/*!40000 ALTER TABLE `fsn_categorie_relation` DISABLE KEYS */;
INSERT INTO `fsn_categorie_relation` VALUES (1,12,'depend_de',41,13,1,0,'Nature campagne de l\'Opération archéolog depend de Statut de l\'Opération archéologique'),(2,23,'depend_de',41,22,1,0,'Elément recueilli - Nature / Matière depend de Elément recueilli - Classe'),(3,24,'depend_de',41,23,1,0,'Elément recueilli - Fonction depend de Elément recueilli - Nature / Matière'),(4,28,'depend_de',41,27,1,0,'Précision Production pôterie depend de Production pôterie'),(5,39,'depend_de',41,38,1,0,'Fait Archéologique - Rubrique depend de Fait Archéologique - Fonction'),(6,40,'depend_de',41,39,1,0,'Fait Archéologique - Terme depend de Fait Archéologique - Rubrique'),(7,40,'secondaire_de',42,40,1,0,'Fait Archéologique - Terme secondaire de Fait Archéologique - Terme'),(8,40,'remplacer_par',43,40,1,0,'Fait Archéologique - Terme remplacer par Fait Archéologique - Terme'),(9,63,'depend_de',41,45,1,1,'La Rubrique Voies depend de La Fonction voirie aménagements'),(10,64,'depend_de',41,45,1,2,'La Rubrique Espaces libres depend de La Fonction voirie aménagements'),(11,65,'depend_de',41,45,1,3,'La Rubrique Aménagements des berges et voies d\'eau depend de La Fonction voirie aménagements'),(12,66,'depend_de',41,45,1,4,'La Rubrique Aménagements du relief depend de La Fonction voirie aménagements'),(13,67,'depend_de',41,45,1,5,'La Rubrique Franchissements depend de La Fonction voirie aménagements'),(14,68,'depend_de',41,45,1,6,'La Rubrique Adductions d\'eau depend de La Fonction voirie aménagements'),(15,69,'depend_de',41,45,1,7,'La Rubrique Collecteurs, évacuations depend de La Fonction voirie aménagements'),(16,70,'depend_de',41,46,1,8,'La Rubrique Système défensif urbain depend de La Fonction structures défensives et militaires'),(17,71,'depend_de',41,46,1,9,'La Rubrique Structures fortifiées depend de La Fonction structures défensives et militaires'),(18,72,'depend_de',41,46,1,10,'La Rubrique Garnisons, casernements depend de La Fonction structures défensives et militaires'),(19,73,'depend_de',41,47,1,11,'La Rubrique Espaces publics aménagés depend de La Fonction constructions civiles'),(20,74,'depend_de',41,47,1,12,'La Rubrique Pouvoir civil, justice depend de La Fonction constructions civiles'),(21,75,'depend_de',41,47,1,13,'La Rubrique Education, culture depend de La Fonction constructions civiles'),(22,76,'depend_de',41,47,1,14,'La Rubrique Santé depend de La Fonction constructions civiles'),(23,77,'depend_de',41,47,1,15,'La Rubrique Spectacle, sport depend de La Fonction constructions civiles'),(24,78,'depend_de',41,47,1,16,'La Rubrique Bains depend de La Fonction constructions civiles'),(25,79,'depend_de',41,47,1,17,'La Rubrique Commerce, échanges depend de La Fonction constructions civiles'),(26,80,'depend_de',41,47,1,18,'La Rubrique Habitat privé depend de La Fonction constructions civiles'),(27,81,'depend_de',41,48,1,19,'La Rubrique Cultes païens depend de La Fonction édifices religieux'),(28,82,'depend_de',41,48,1,20,'La Rubrique Edifices cultuels catholiques depend de La Fonction édifices religieux'),(29,83,'depend_de',41,48,1,21,'La Rubrique Bâtiments conventuels ou monastiques depend de La Fonction édifices religieux'),(30,84,'depend_de',41,48,1,22,'La Rubrique Bâtiments écclésiastiques depend de La Fonction édifices religieux'),(31,85,'depend_de',41,48,1,23,'La Rubrique Cultes autres que catholique depend de La Fonction édifices religieux'),(32,86,'depend_de',41,49,1,24,'La Rubrique Funéraire depend de La Fonction funéraire'),(33,87,'depend_de',41,50,1,25,'La Rubrique Artisanat depend de La Fonction production'),(34,88,'depend_de',41,50,1,26,'La Rubrique Agriculture, élevage depend de La Fonction production'),(35,89,'depend_de',41,50,1,27,'La Rubrique Industrie depend de La Fonction production'),(36,90,'depend_de',41,50,1,28,'La Rubrique Extraction depend de La Fonction production'),(37,91,'depend_de',41,51,1,29,'La Rubrique Formations naturelles depend de La Fonction formations naturelles'),(67,92,'depend_de',41,79,1,1,'Le Terme abattoir depend de La Rubrique Commerce, échanges '),(69,92,'depend_de',41,89,1,1,'Le Terme abattoir depend de La Rubrique Industrie '),(70,93,'depend_de',41,82,1,2,'Le Terme abbatiale depend de La Rubrique Edifices cultuels catholiques '),(73,94,'depend_de',41,83,1,3,'Le Terme abbaye depend de La Rubrique Bâtiments conventuels ou monastiques '),(75,95,'depend_de',41,71,1,4,'Le Terme abbaye fortifiée depend de La Rubrique Structures fortifiées '),(77,96,'depend_de',41,68,1,5,'Le Terme abreuvoir depend de La Rubrique Adductions d\'eau '),(93,107,'depend_de',41,68,1,15,'Le Terme adduction d\'eau depend de La Rubrique Adductions d\'eau '),(100,116,'depend_de',41,88,1,21,'Le Terme aire de battage depend de La Rubrique Agriculture, élevage '),(101,117,'depend_de',41,63,1,22,'Le Terme aire de circulation depend de La Rubrique Voies si espace public'),(103,117,'depend_de',41,80,1,22,'Le Terme aire de circulation depend de La Rubrique Habitat privé si espace privé'),(107,119,'depend_de',41,87,1,24,'Le Terme aire de construction depend de La Rubrique Artisanat '),(108,120,'depend_de',41,86,1,25,'Le Terme aire de crémation depend de La Rubrique Funéraire '),(110,122,'depend_de',41,88,1,27,'Le Terme aire de foulage depend de La Rubrique Agriculture, élevage '),(111,124,'depend_de',41,88,1,28,'Le Terme aire de séchage depend de La Rubrique Agriculture, élevage '),(115,126,'depend_de',41,87,1,30,'Le Terme aire de travail depend de La Rubrique Artisanat '),(121,130,'depend_de',41,87,1,34,'Le Terme alimentation depend de La Rubrique Artisanat '),(127,134,'depend_de',41,91,1,38,'Le Terme alluvions depend de La Rubrique Formations naturelles '),(129,136,'depend_de',41,65,1,40,'Le Terme amarrage depend de La Rubrique Aménagements des berges et voies d\'eau '),(134,141,'depend_de',41,65,1,44,'Le Terme aménagement de berge depend de La Rubrique Aménagements des berges et voies d\'eau '),(141,145,'depend_de',41,66,1,48,'Le Terme aménagement de relief depend de La Rubrique Aménagements du relief '),(145,149,'depend_de',41,77,1,52,'Le Terme amphithéâtre depend de La Rubrique Spectacle, sport '),(146,150,'depend_de',41,86,1,53,'Le Terme amphore depend de La Rubrique Funéraire '),(160,158,'depend_de',41,78,1,62,'Le Terme apodyterium depend de La Rubrique Bains '),(166,161,'depend_de',41,65,1,66,'Le Terme appontement depend de La Rubrique Aménagements des berges et voies d\'eau '),(171,162,'depend_de',41,68,1,67,'Le Terme aqueduc depend de La Rubrique Adductions d\'eau '),(178,164,'depend_de',41,66,1,70,'Le Terme arasement depend de La Rubrique Aménagements du relief '),(180,166,'depend_de',41,73,1,72,'Le Terme arc de triomphe depend de La Rubrique Espaces publics aménagés '),(187,177,'depend_de',41,87,1,78,'Le Terme argent depend de La Rubrique Artisanat '),(188,178,'depend_de',41,90,1,79,'Le Terme argile depend de La Rubrique Extraction '),(194,182,'depend_de',41,72,1,83,'Le Terme arsenal depend de La Rubrique Garnisons, casernements '),(199,190,'depend_de',41,87,1,87,'Le Terme artisanat domestique depend de La Rubrique Artisanat '),(202,199,'depend_de',41,65,1,90,'Le Terme assainissement depend de La Rubrique Aménagements des berges et voies d\'eau '),(204,199,'depend_de',41,69,1,90,'Le Terme assainissement depend de La Rubrique Collecteurs, évacuations '),(210,201,'depend_de',41,87,1,92,'Le Terme atelier depend de La Rubrique Artisanat '),(220,210,'depend_de',41,87,1,100,'Le Terme atelier de potier depend de La Rubrique Artisanat '),(224,213,'depend_de',41,87,1,103,'Le Terme atelier de taille de pierre depend de La Rubrique Artisanat '),(226,215,'depend_de',41,87,1,105,'Le Terme atelier de tuilier depend de La Rubrique Artisanat '),(227,216,'depend_de',41,87,1,106,'Le Terme atelier de verrier depend de La Rubrique Artisanat '),(230,219,'depend_de',41,87,1,108,'Le Terme atelier monétaire depend de La Rubrique Artisanat '),(234,222,'depend_de',41,80,1,111,'Le Terme atrium depend de La Rubrique Habitat privé '),(235,222,'depend_de',41,82,1,111,'Le Terme atrium depend de La Rubrique Edifices cultuels catholiques '),(237,223,'depend_de',41,79,1,112,'Le Terme auberge depend de La Rubrique Commerce, échanges '),(240,225,'depend_de',41,74,1,114,'Le Terme aula depend de La Rubrique Pouvoir civil, justice '),(243,226,'depend_de',41,83,1,115,'Le Terme aumônerie depend de La Rubrique Bâtiments conventuels ou monastiques '),(254,237,'depend_de',41,67,1,123,'Le Terme bac depend de La Rubrique Franchissements '),(258,239,'depend_de',41,87,1,125,'Le Terme bac à chaux depend de La Rubrique Artisanat '),(260,241,'depend_de',41,87,1,127,'Le Terme bac à fritte depend de La Rubrique Artisanat '),(267,247,'depend_de',41,85,1,133,'Le Terme bain rituel depend de La Rubrique Cultes autres que catholique '),(269,248,'depend_de',41,78,1,134,'Le Terme bains depend de La Rubrique Bains '),(270,248,'depend_de',41,80,1,134,'Le Terme bains depend de La Rubrique Habitat privé '),(271,249,'depend_de',41,78,1,135,'Le Terme bains privés depend de La Rubrique Bains '),(280,256,'depend_de',41,82,1,142,'Le Terme baptistère depend de La Rubrique Edifices cultuels catholiques '),(281,257,'depend_de',41,72,1,143,'Le Terme baraquement depend de La Rubrique Garnisons, casernements '),(282,258,'depend_de',41,70,1,144,'Le Terme barbacane depend de La Rubrique Système défensif urbain '),(283,258,'depend_de',41,71,1,144,'Le Terme barbacane depend de La Rubrique Structures fortifiées '),(284,259,'depend_de',41,65,1,145,'Le Terme barrage depend de La Rubrique Aménagements des berges et voies d\'eau '),(286,261,'depend_de',41,87,1,147,'Le Terme bas-fourneau depend de La Rubrique Artisanat '),(290,265,'depend_de',41,74,1,151,'Le Terme basilique depend de La Rubrique Pouvoir civil, justice '),(291,265,'depend_de',41,82,1,151,'Le Terme basilique depend de La Rubrique Edifices cultuels catholiques '),(292,266,'depend_de',41,82,1,152,'Le Terme basilique funéraire depend de La Rubrique Edifices cultuels catholiques '),(293,266,'depend_de',41,86,1,152,'Le Terme basilique funéraire depend de La Rubrique Funéraire '),(294,267,'depend_de',41,71,1,153,'Le Terme basse-cour depend de La Rubrique Structures fortifiées '),(297,268,'depend_de',41,65,1,154,'Le Terme bassin depend de La Rubrique Aménagements des berges et voies d\'eau '),(299,268,'depend_de',41,68,1,154,'Le Terme bassin depend de La Rubrique Adductions d\'eau '),(304,268,'depend_de',41,78,1,154,'Le Terme bassin depend de La Rubrique Bains '),(306,268,'depend_de',41,80,1,154,'Le Terme bassin depend de La Rubrique Habitat privé '),(309,268,'depend_de',41,87,1,154,'Le Terme bassin depend de La Rubrique Artisanat '),(310,268,'depend_de',41,88,1,154,'Le Terme bassin depend de La Rubrique Agriculture, élevage '),(315,275,'depend_de',41,69,1,158,'Le Terme bassin de décantation depend de La Rubrique Collecteurs, évacuations '),(317,275,'depend_de',41,87,1,158,'Le Terme bassin de décantation depend de La Rubrique Artisanat '),(324,283,'depend_de',41,70,1,164,'Le Terme bastille depend de La Rubrique Système défensif urbain '),(325,284,'depend_de',41,70,1,165,'Le Terme bastion depend de La Rubrique Système défensif urbain '),(326,284,'depend_de',41,71,1,165,'Le Terme bastion depend de La Rubrique Structures fortifiées '),(331,287,'depend_de',41,65,1,167,'Le Terme batardeau depend de La Rubrique Aménagements des berges et voies d\'eau '),(354,293,'depend_de',41,88,1,173,'Le Terme bâtiment agricole depend de La Rubrique Agriculture, élevage '),(357,295,'depend_de',41,87,1,175,'Le Terme bâtiment artisanal depend de La Rubrique Artisanat '),(360,297,'depend_de',41,79,1,177,'Le Terme bâtiment commercial depend de La Rubrique Commerce, échanges '),(364,302,'depend_de',41,84,1,180,'Le Terme bâtiment ecclésiastique depend de La Rubrique Bâtiments écclésiastiques '),(367,305,'depend_de',41,89,1,183,'Le Terme bâtiment industriel depend de La Rubrique Industrie '),(374,311,'depend_de',41,70,1,187,'Le Terme batterie depend de La Rubrique Système défensif urbain '),(375,311,'depend_de',41,71,1,187,'Le Terme batterie depend de La Rubrique Structures fortifiées '),(376,312,'depend_de',41,74,1,188,'Le Terme beffroi depend de La Rubrique Pouvoir civil, justice '),(381,317,'depend_de',41,88,1,191,'Le Terme bergerie depend de La Rubrique Agriculture, élevage '),(382,319,'depend_de',41,75,1,192,'Le Terme bibliothèque depend de La Rubrique Education, culture '),(383,320,'depend_de',41,65,1,193,'Le Terme bief depend de La Rubrique Aménagements des berges et voies d\'eau '),(386,322,'depend_de',41,87,1,195,'Le Terme blanchisserie depend de La Rubrique Artisanat '),(390,323,'depend_de',41,87,1,196,'Le Terme bois depend de La Rubrique Artisanat '),(391,323,'depend_de',41,88,1,196,'Le Terme bois depend de La Rubrique Agriculture, élevage '),(392,323,'depend_de',41,91,1,196,'Le Terme bois depend de La Rubrique Formations naturelles '),(395,326,'depend_de',41,63,1,199,'Le Terme borne depend de La Rubrique Voies '),(400,330,'depend_de',41,87,1,202,'Le Terme boucherie depend de La Rubrique Artisanat '),(406,333,'depend_de',41,87,1,205,'Le Terme boulangerie depend de La Rubrique Artisanat '),(408,335,'depend_de',41,70,1,206,'Le Terme boulevard depend de La Rubrique Système défensif urbain '),(409,335,'depend_de',41,71,1,206,'Le Terme boulevard depend de La Rubrique Structures fortifiées '),(412,337,'depend_de',41,87,1,208,'Le Terme bourellerie depend de La Rubrique Artisanat '),(415,338,'depend_de',41,79,1,209,'Le Terme boutique depend de La Rubrique Commerce, échanges '),(420,340,'depend_de',41,86,1,211,'Le Terme brancard depend de La Rubrique Funéraire '),(424,342,'depend_de',41,87,1,213,'Le Terme brasserie depend de La Rubrique Artisanat '),(425,342,'depend_de',41,89,1,213,'Le Terme brasserie depend de La Rubrique Industrie '),(428,345,'depend_de',41,87,1,216,'Le Terme briqueterie depend de La Rubrique Artisanat '),(430,346,'depend_de',41,87,1,217,'Le Terme bronze depend de La Rubrique Artisanat '),(433,349,'depend_de',41,86,1,220,'Le Terme bûcher depend de La Rubrique Funéraire '),(435,351,'depend_de',41,86,1,222,'Le Terme bustum depend de La Rubrique Funéraire '),(437,353,'depend_de',41,80,1,224,'Le Terme cabane depend de La Rubrique Habitat privé '),(446,363,'depend_de',41,63,1,232,'Le Terme calade depend de La Rubrique Voies '),(447,363,'depend_de',41,65,1,232,'Le Terme calade depend de La Rubrique Aménagements des berges et voies d\'eau '),(449,364,'depend_de',41,90,1,233,'Le Terme calcaire depend de La Rubrique Extraction '),(450,365,'depend_de',41,78,1,234,'Le Terme caldarium depend de La Rubrique Bains '),(451,366,'depend_de',41,65,1,235,'Le Terme cale depend de La Rubrique Aménagements des berges et voies d\'eau '),(455,368,'depend_de',41,63,1,237,'Le Terme calvaire depend de La Rubrique Voies '),(458,369,'depend_de',41,72,1,238,'Le Terme camp depend de La Rubrique Garnisons, casernements '),(461,372,'depend_de',41,65,1,240,'Le Terme canal depend de La Rubrique Aménagements des berges et voies d\'eau '),(475,379,'depend_de',41,68,1,245,'Le Terme canalisation depend de La Rubrique Adductions d\'eau '),(476,379,'depend_de',41,69,1,245,'Le Terme canalisation depend de La Rubrique Collecteurs, évacuations '),(480,379,'depend_de',41,78,1,245,'Le Terme canalisation depend de La Rubrique Bains '),(481,379,'depend_de',41,80,1,245,'Le Terme canalisation depend de La Rubrique Habitat privé '),(483,379,'depend_de',41,83,1,245,'Le Terme canalisation depend de La Rubrique Bâtiments conventuels ou monastiques '),(484,379,'depend_de',41,84,1,245,'Le Terme canalisation depend de La Rubrique Bâtiments écclésiastiques '),(485,379,'depend_de',41,87,1,245,'Le Terme canalisation depend de La Rubrique Artisanat '),(487,383,'depend_de',41,63,1,247,'Le Terme caniveau depend de La Rubrique Voies '),(490,383,'depend_de',41,69,1,247,'Le Terme caniveau depend de La Rubrique Collecteurs, évacuations '),(500,386,'depend_de',41,70,1,250,'Le Terme caponnière depend de La Rubrique Système défensif urbain '),(501,387,'depend_de',41,68,1,251,'Le Terme captage depend de La Rubrique Adductions d\'eau '),(505,389,'depend_de',41,63,1,253,'Le Terme cardo depend de La Rubrique Voies '),(511,393,'depend_de',41,90,1,256,'Le Terme carrière depend de La Rubrique Extraction '),(512,395,'depend_de',41,80,1,257,'Le Terme case depend de La Rubrique Habitat privé '),(515,397,'depend_de',41,70,1,259,'Le Terme casemate depend de La Rubrique Système défensif urbain '),(516,397,'depend_de',41,71,1,259,'Le Terme casemate depend de La Rubrique Structures fortifiées '),(519,398,'depend_de',41,72,1,260,'Le Terme caserne depend de La Rubrique Garnisons, casernements '),(525,404,'depend_de',41,82,1,264,'Le Terme cathédrale depend de La Rubrique Edifices cultuels catholiques '),(537,406,'depend_de',41,80,1,266,'Le Terme cave depend de La Rubrique Habitat privé '),(538,406,'depend_de',41,83,1,266,'Le Terme cave depend de La Rubrique Bâtiments conventuels ou monastiques '),(539,406,'depend_de',41,84,1,266,'Le Terme cave depend de La Rubrique Bâtiments écclésiastiques '),(549,413,'depend_de',41,86,1,270,'Le Terme caveau depend de La Rubrique Funéraire '),(558,420,'depend_de',41,81,1,279,'Le Terme cella depend de La Rubrique Cultes païens '),(560,421,'depend_de',41,79,1,280,'Le Terme cellier depend de La Rubrique Commerce, échanges quand cellier public'),(561,421,'depend_de',41,80,1,280,'Le Terme cellier depend de La Rubrique Habitat privé '),(562,421,'depend_de',41,83,1,280,'Le Terme cellier depend de La Rubrique Bâtiments conventuels ou monastiques '),(563,421,'depend_de',41,84,1,280,'Le Terme cellier depend de La Rubrique Bâtiments écclésiastiques '),(566,422,'depend_de',41,83,1,281,'Le Terme cellule depend de La Rubrique Bâtiments conventuels ou monastiques '),(568,423,'depend_de',41,91,1,282,'Le Terme cendres volcaniques depend de La Rubrique Formations naturelles '),(570,425,'depend_de',41,86,1,284,'Le Terme cénotaphe depend de La Rubrique Funéraire '),(575,427,'depend_de',41,87,1,286,'Le Terme céramique depend de La Rubrique Artisanat '),(582,431,'depend_de',41,86,1,290,'Le Terme cercueil depend de La Rubrique Funéraire '),(584,433,'depend_de',41,86,1,291,'Le Terme cercueil de bois depend de La Rubrique Funéraire '),(585,434,'depend_de',41,86,1,292,'Le Terme cercueil de plomb depend de La Rubrique Funéraire '),(591,437,'depend_de',41,88,1,295,'Le Terme chai depend de La Rubrique Agriculture, élevage '),(599,443,'depend_de',41,88,1,300,'Le Terme champ depend de La Rubrique Agriculture, élevage '),(600,445,'depend_de',41,79,1,301,'Le Terme champ de foire depend de La Rubrique Commerce, échanges '),(601,446,'depend_de',41,72,1,302,'Le Terme champ de manoeuvre depend de La Rubrique Garnisons, casernements '),(613,454,'depend_de',41,82,1,307,'Le Terme chapelle depend de La Rubrique Edifices cultuels catholiques '),(619,455,'depend_de',41,82,1,308,'Le Terme chapelle castrale depend de La Rubrique Edifices cultuels catholiques '),(621,457,'depend_de',41,82,1,310,'Le Terme chapelle conventuelle depend de La Rubrique Edifices cultuels catholiques '),(622,458,'depend_de',41,82,1,311,'Le Terme chapelle funéraire depend de La Rubrique Edifices cultuels catholiques '),(624,458,'depend_de',41,86,1,311,'Le Terme chapelle funéraire depend de La Rubrique Funéraire '),(631,466,'depend_de',41,87,1,318,'Le Terme charcuterie depend de La Rubrique Artisanat '),(632,467,'depend_de',41,86,1,319,'Le Terme charnier depend de La Rubrique Funéraire '),(633,468,'depend_de',41,80,1,320,'Le Terme charpente depend de La Rubrique Habitat privé '),(641,470,'depend_de',41,68,1,322,'Le Terme château d\'eau depend de La Rubrique Adductions d\'eau '),(642,471,'depend_de',41,71,1,323,'Le Terme château fort depend de La Rubrique Structures fortifiées '),(644,472,'depend_de',41,70,1,325,'Le Terme châtelet depend de La Rubrique Système défensif urbain '),(645,472,'depend_de',41,71,1,325,'Le Terme châtelet depend de La Rubrique Structures fortifiées '),(646,473,'depend_de',41,78,1,326,'Le Terme chaudière depend de La Rubrique Bains '),(648,474,'depend_de',41,87,1,327,'Le Terme chaudronnerie depend de La Rubrique Artisanat '),(650,476,'depend_de',41,83,1,329,'Le Terme chauffoir depend de La Rubrique Bâtiments conventuels ou monastiques '),(651,477,'depend_de',41,63,1,330,'Le Terme chaussée depend de La Rubrique Voies '),(661,479,'depend_de',41,63,1,332,'Le Terme chemin depend de La Rubrique Voies '),(672,483,'depend_de',41,80,1,336,'Le Terme cheminée depend de La Rubrique Habitat privé '),(673,483,'depend_de',41,83,1,336,'Le Terme cheminée depend de La Rubrique Bâtiments conventuels ou monastiques '),(674,484,'depend_de',41,65,1,337,'Le Terme chenal depend de La Rubrique Aménagements des berges et voies d\'eau '),(675,484,'depend_de',41,91,1,337,'Le Terme chenal depend de La Rubrique Formations naturelles '),(684,490,'depend_de',41,86,1,343,'Le Terme cimetière depend de La Rubrique Funéraire '),(693,501,'depend_de',41,86,1,350,'Le Terme cimetière de communauté religieuse depend de La Rubrique Funéraire '),(694,502,'depend_de',41,86,1,351,'Le Terme cimetière de léproserie depend de La Rubrique Funéraire '),(697,496,'depend_de',41,86,1,353,'Le Terme cimetière d\'enfants depend de La Rubrique Funéraire '),(698,497,'depend_de',41,86,1,354,'Le Terme cimetière d\'établissement hospitalier depend de La Rubrique Funéraire '),(701,506,'depend_de',41,86,1,357,'Le Terme cimetière juif depend de La Rubrique Funéraire '),(702,507,'depend_de',41,86,1,358,'Le Terme cimetière militaire depend de La Rubrique Funéraire '),(709,509,'depend_de',41,86,1,360,'Le Terme cimetière paroissial depend de La Rubrique Funéraire '),(711,511,'depend_de',41,86,1,363,'Le Terme cimetière protestant depend de La Rubrique Funéraire '),(712,512,'depend_de',41,86,1,364,'Le Terme cippe depend de La Rubrique Funéraire '),(714,514,'depend_de',41,77,1,366,'Le Terme cirque depend de La Rubrique Spectacle, sport '),(716,516,'depend_de',41,70,1,368,'Le Terme citadelle depend de La Rubrique Système défensif urbain '),(717,516,'depend_de',41,71,1,368,'Le Terme citadelle depend de La Rubrique Structures fortifiées '),(720,517,'depend_de',41,68,1,369,'Le Terme citerne depend de La Rubrique Adductions d\'eau '),(723,517,'depend_de',41,80,1,369,'Le Terme citerne depend de La Rubrique Habitat privé '),(729,518,'depend_de',41,80,1,370,'Le Terme clayonnage depend de La Rubrique Habitat privé '),(731,520,'depend_de',41,82,1,372,'Le Terme clocher depend de La Rubrique Edifices cultuels catholiques '),(735,523,'depend_de',41,83,1,375,'Le Terme cloître depend de La Rubrique Bâtiments conventuels ou monastiques '),(740,526,'depend_de',41,83,1,378,'Le Terme cloître canonial depend de La Rubrique Bâtiments conventuels ou monastiques '),(749,529,'depend_de',41,80,1,381,'Le Terme clôture depend de La Rubrique Habitat privé '),(752,529,'depend_de',41,88,1,381,'Le Terme clôture depend de La Rubrique Agriculture, élevage '),(757,534,'depend_de',41,86,1,387,'Le Terme coffre depend de La Rubrique Funéraire '),(759,536,'depend_de',41,86,1,389,'Le Terme coffre de bois depend de La Rubrique Funéraire '),(761,538,'depend_de',41,86,1,391,'Le Terme coffre de dalles depend de La Rubrique Funéraire '),(773,549,'depend_de',41,86,1,403,'Le Terme coffre maçonné depend de La Rubrique Funéraire '),(776,552,'depend_de',41,69,1,405,'Le Terme collecteur depend de La Rubrique Collecteurs, évacuations '),(781,554,'depend_de',41,75,1,407,'Le Terme collège depend de La Rubrique Education, culture '),(784,555,'depend_de',41,82,1,408,'Le Terme collégiale depend de La Rubrique Edifices cultuels catholiques '),(785,556,'depend_de',41,91,1,409,'Le Terme colluvions depend de La Rubrique Formations naturelles '),(793,565,'depend_de',41,65,1,417,'Le Terme comblement de rivière depend de La Rubrique Aménagements des berges et voies d\'eau si comblement anthropique'),(794,565,'depend_de',41,91,1,417,'Le Terme comblement de rivière depend de La Rubrique Formations naturelles si comblement naturel'),(796,566,'depend_de',41,91,1,418,'Le Terme comblement de vallée depend de La Rubrique Formations naturelles '),(800,569,'depend_de',41,83,1,422,'Le Terme commanderie depend de La Rubrique Bâtiments conventuels ou monastiques '),(804,571,'depend_de',41,80,1,424,'Le Terme communs depend de La Rubrique Habitat privé '),(808,575,'depend_de',41,78,1,427,'Le Terme conduit de chauffe depend de La Rubrique Bains '),(809,575,'depend_de',41,80,1,427,'Le Terme conduit de chauffe depend de La Rubrique Habitat privé '),(819,586,'depend_de',41,70,1,434,'Le Terme contre-garde depend de La Rubrique Système défensif urbain '),(821,588,'depend_de',41,70,1,435,'Le Terme contrescarpe depend de La Rubrique Système défensif urbain '),(822,588,'depend_de',41,71,1,435,'Le Terme contrescarpe depend de La Rubrique Structures fortifiées '),(824,591,'depend_de',41,87,1,437,'Le Terme corail depend de La Rubrique Artisanat '),(826,592,'depend_de',41,87,1,438,'Le Terme cordonnerie depend de La Rubrique Artisanat '),(830,594,'depend_de',41,87,1,440,'Le Terme corne depend de La Rubrique Artisanat '),(835,596,'depend_de',41,80,1,442,'Le Terme corps de logis depend de La Rubrique Habitat privé '),(852,602,'depend_de',41,78,1,446,'Le Terme cour depend de La Rubrique Bains '),(854,602,'depend_de',41,80,1,446,'Le Terme cour depend de La Rubrique Habitat privé '),(860,602,'depend_de',41,88,1,446,'Le Terme cour depend de La Rubrique Agriculture, élevage '),(866,605,'depend_de',41,74,1,449,'Le Terme cour de justice depend de La Rubrique Pouvoir civil, justice '),(869,607,'depend_de',41,91,1,451,'Le Terme cours d\'eau depend de La Rubrique Formations naturelles '),(872,609,'depend_de',41,70,1,453,'Le Terme courtine depend de La Rubrique Système défensif urbain '),(873,609,'depend_de',41,71,1,453,'Le Terme courtine depend de La Rubrique Structures fortifiées '),(880,611,'depend_de',41,83,1,455,'Le Terme couvent depend de La Rubrique Bâtiments conventuels ou monastiques '),(884,616,'depend_de',41,90,1,457,'Le Terme craie depend de La Rubrique Extraction '),(886,618,'depend_de',41,66,1,459,'Le Terme creusement depend de La Rubrique Aménagements du relief '),(887,619,'depend_de',41,87,1,460,'Le Terme creuset depend de La Rubrique Artisanat '),(888,621,'depend_de',41,82,1,461,'Le Terme crypte depend de La Rubrique Edifices cultuels catholiques '),(891,623,'depend_de',41,73,1,463,'Le Terme cryptoportique depend de La Rubrique Espaces publics aménagés '),(895,624,'depend_de',41,87,1,464,'Le Terme cuir depend de La Rubrique Artisanat '),(898,625,'depend_de',41,80,1,465,'Le Terme cuisine depend de La Rubrique Habitat privé '),(899,625,'depend_de',41,83,1,465,'Le Terme cuisine depend de La Rubrique Bâtiments conventuels ou monastiques '),(901,626,'depend_de',41,87,1,466,'Le Terme cuivre depend de La Rubrique Artisanat '),(903,627,'depend_de',41,81,1,467,'Le Terme culte des eaux depend de La Rubrique Cultes païens '),(912,634,'depend_de',41,74,1,472,'Le Terme curie depend de La Rubrique Pouvoir civil, justice '),(925,645,'depend_de',41,87,1,482,'Le Terme cuve de tanneur depend de La Rubrique Artisanat '),(932,651,'depend_de',41,86,1,488,'Le Terme dalle funéraire depend de La Rubrique Funéraire '),(944,661,'depend_de',41,66,1,497,'Le Terme décaissement depend de La Rubrique Aménagements du relief '),(945,662,'depend_de',41,64,1,498,'Le Terme décharge depend de La Rubrique Espaces libres '),(951,668,'depend_de',41,63,1,503,'Le Terme decumanus depend de La Rubrique Voies '),(953,670,'depend_de',41,88,1,506,'Le Terme défrichement depend de La Rubrique Agriculture, élevage '),(954,671,'depend_de',41,70,1,507,'Le Terme dehors depend de La Rubrique Système défensif urbain '),(956,673,'depend_de',41,70,1,509,'Le Terme demi-lune depend de La Rubrique Système défensif urbain '),(957,673,'depend_de',41,71,1,509,'Le Terme demi-lune depend de La Rubrique Structures fortifiées '),(967,675,'depend_de',41,80,1,511,'Le Terme dépendance depend de La Rubrique Habitat privé '),(969,675,'depend_de',41,83,1,511,'Le Terme dépendance depend de La Rubrique Bâtiments conventuels ou monastiques '),(970,675,'depend_de',41,84,1,511,'Le Terme dépendance depend de La Rubrique Bâtiments écclésiastiques '),(972,675,'depend_de',41,87,1,511,'Le Terme dépendance depend de La Rubrique Artisanat '),(982,684,'depend_de',41,86,1,517,'Le Terme dépôt de crémation depend de La Rubrique Funéraire '),(983,685,'depend_de',41,81,1,518,'Le Terme dépôt de fondation depend de La Rubrique Cultes païens '),(988,687,'depend_de',41,86,1,523,'Le Terme dépôt funéraire depend de La Rubrique Funéraire '),(990,688,'depend_de',41,81,1,524,'Le Terme dépôt rituel depend de La Rubrique Cultes païens '),(991,689,'depend_de',41,81,1,525,'Le Terme dépôt votif depend de La Rubrique Cultes païens '),(1003,690,'depend_de',41,80,1,526,'Le Terme dépotoir depend de La Rubrique Habitat privé '),(1006,690,'depend_de',41,83,1,526,'Le Terme dépotoir depend de La Rubrique Bâtiments conventuels ou monastiques '),(1007,690,'depend_de',41,84,1,526,'Le Terme dépotoir depend de La Rubrique Bâtiments écclésiastiques '),(1009,690,'depend_de',41,87,1,526,'Le Terme dépotoir depend de La Rubrique Artisanat '),(1038,717,'depend_de',41,91,1,552,'Le Terme dépression depend de La Rubrique Formations naturelles '),(1039,718,'depend_de',41,65,1,553,'Le Terme dérivation depend de La Rubrique Aménagements des berges et voies d\'eau '),(1042,720,'depend_de',41,65,1,555,'Le Terme digue depend de La Rubrique Aménagements des berges et voies d\'eau '),(1044,721,'depend_de',41,87,1,556,'Le Terme dinanderie depend de La Rubrique Artisanat '),(1049,724,'depend_de',41,89,1,559,'Le Terme distillerie depend de La Rubrique Industrie '),(1050,725,'depend_de',41,65,1,560,'Le Terme dock depend de La Rubrique Aménagements des berges et voies d\'eau '),(1053,727,'depend_de',41,80,1,563,'Le Terme domus depend de La Rubrique Habitat privé '),(1058,728,'depend_de',41,71,1,564,'Le Terme donjon depend de La Rubrique Structures fortifiées '),(1060,730,'depend_de',41,83,1,565,'Le Terme dortoir depend de La Rubrique Bâtiments conventuels ou monastiques '),(1068,735,'depend_de',41,69,1,569,'Le Terme drain depend de La Rubrique Collecteurs, évacuations '),(1070,735,'depend_de',41,80,1,569,'Le Terme drain depend de La Rubrique Habitat privé '),(1071,735,'depend_de',41,88,1,569,'Le Terme drain depend de La Rubrique Agriculture, élevage '),(1076,736,'depend_de',41,69,1,570,'Le Terme drainage depend de La Rubrique Collecteurs, évacuations '),(1077,736,'depend_de',41,80,1,570,'Le Terme drainage depend de La Rubrique Habitat privé '),(1078,736,'depend_de',41,88,1,570,'Le Terme drainage depend de La Rubrique Agriculture, élevage '),(1079,738,'depend_de',41,65,1,571,'Le Terme duit depend de La Rubrique Aménagements des berges et voies d\'eau '),(1080,739,'depend_de',41,91,1,572,'Le Terme dune depend de La Rubrique Formations naturelles '),(1085,745,'depend_de',41,65,1,578,'Le Terme écluse depend de La Rubrique Aménagements des berges et voies d\'eau '),(1086,745,'depend_de',41,67,1,578,'Le Terme écluse depend de La Rubrique Franchissements '),(1087,746,'depend_de',41,75,1,579,'Le Terme école depend de La Rubrique Education, culture '),(1090,748,'depend_de',41,72,1,581,'Le Terme écurie depend de La Rubrique Garnisons, casernements '),(1092,748,'depend_de',41,80,1,581,'Le Terme écurie depend de La Rubrique Habitat privé '),(1093,748,'depend_de',41,83,1,581,'Le Terme écurie depend de La Rubrique Bâtiments conventuels ou monastiques '),(1095,748,'depend_de',41,88,1,581,'Le Terme écurie depend de La Rubrique Agriculture, élevage '),(1103,751,'depend_de',41,81,1,584,'Le Terme édifice cultuel depend de La Rubrique Cultes païens '),(1104,751,'depend_de',41,82,1,584,'Le Terme édifice cultuel depend de La Rubrique Edifices cultuels catholiques '),(1105,753,'depend_de',41,76,1,585,'Le Terme édifice d\'assistance depend de La Rubrique Santé '),(1107,755,'depend_de',41,77,1,587,'Le Terme édifice de spectacle depend de La Rubrique Spectacle, sport '),(1109,757,'depend_de',41,86,1,588,'Le Terme édifice funéraire depend de La Rubrique Funéraire '),(1114,761,'depend_de',41,74,1,592,'Le Terme édifice public depend de La Rubrique Pouvoir civil, justice '),(1120,763,'depend_de',41,82,1,593,'Le Terme église depend de La Rubrique Edifices cultuels catholiques '),(1129,769,'depend_de',41,82,1,599,'Le Terme église conventuelle depend de La Rubrique Edifices cultuels catholiques '),(1131,771,'depend_de',41,71,1,601,'Le Terme église fortifiée depend de La Rubrique Structures fortifiées '),(1133,772,'depend_de',41,82,1,602,'Le Terme église funéraire depend de La Rubrique Edifices cultuels catholiques '),(1134,772,'depend_de',41,86,1,602,'Le Terme église funéraire depend de La Rubrique Funéraire '),(1137,773,'depend_de',41,82,1,604,'Le Terme église paroissiale depend de La Rubrique Edifices cultuels catholiques '),(1139,775,'depend_de',41,82,1,605,'Le Terme église prieurale depend de La Rubrique Edifices cultuels catholiques '),(1146,778,'depend_de',41,69,1,608,'Le Terme égout depend de La Rubrique Collecteurs, évacuations '),(1161,787,'depend_de',41,66,1,615,'Le Terme emmottement depend de La Rubrique Aménagements du relief '),(1166,789,'depend_de',41,70,1,617,'Le Terme enceinte depend de La Rubrique Système défensif urbain '),(1167,789,'depend_de',41,71,1,617,'Le Terme enceinte depend de La Rubrique Structures fortifiées '),(1185,794,'depend_de',41,83,1,622,'Le Terme enceinte canoniale depend de La Rubrique Bâtiments conventuels ou monastiques '),(1188,797,'depend_de',41,71,1,625,'Le Terme enceinte conventuelle depend de La Rubrique Structures fortifiées '),(1189,797,'depend_de',41,83,1,625,'Le Terme enceinte conventuelle depend de La Rubrique Bâtiments conventuels ou monastiques '),(1190,798,'depend_de',41,81,1,626,'Le Terme enceinte cultuelle depend de La Rubrique Cultes païens '),(1193,801,'depend_de',41,71,1,629,'Le Terme enceinte épiscopale depend de La Rubrique Structures fortifiées '),(1204,806,'depend_de',41,88,1,634,'Le Terme enclos depend de La Rubrique Agriculture, élevage '),(1212,812,'depend_de',41,86,1,640,'Le Terme enclos funéraire depend de La Rubrique Funéraire '),(1220,818,'depend_de',41,80,1,645,'Le Terme enduits peints depend de La Rubrique Habitat privé '),(1225,819,'depend_de',41,86,1,646,'Le Terme enfeu depend de La Rubrique Funéraire '),(1234,827,'depend_de',41,72,1,652,'Le Terme entrepôt depend de La Rubrique Garnisons, casernements '),(1235,827,'depend_de',41,79,1,652,'Le Terme entrepôt depend de La Rubrique Commerce, échanges '),(1237,827,'depend_de',41,87,1,652,'Le Terme entrepôt depend de La Rubrique Artisanat '),(1239,827,'depend_de',41,89,1,652,'Le Terme entrepôt depend de La Rubrique Industrie '),(1243,831,'depend_de',41,91,1,656,'Le Terme envasement depend de La Rubrique Formations naturelles '),(1246,833,'depend_de',41,65,1,658,'Le Terme épave depend de La Rubrique Aménagements des berges et voies d\'eau '),(1248,834,'depend_de',41,91,1,659,'Le Terme éperon depend de La Rubrique Formations naturelles '),(1253,839,'depend_de',41,91,1,664,'Le Terme érosion depend de La Rubrique Formations naturelles '),(1255,840,'depend_de',41,66,1,665,'Le Terme escalier depend de La Rubrique Aménagements du relief '),(1265,841,'depend_de',41,70,1,666,'Le Terme escarpe depend de La Rubrique Système défensif urbain '),(1270,844,'depend_de',41,88,1,669,'Le Terme espace agricole depend de La Rubrique Agriculture, élevage '),(1277,851,'depend_de',41,86,1,676,'Le Terme espace funéraire depend de La Rubrique Funéraire '),(1279,852,'depend_de',41,64,1,677,'Le Terme espace libre depend de La Rubrique Espaces libres '),(1285,856,'depend_de',41,73,1,680,'Le Terme espace public depend de La Rubrique Espaces publics aménagés '),(1289,860,'depend_de',41,63,1,684,'Le Terme esplanade depend de La Rubrique Voies '),(1293,860,'depend_de',41,73,1,684,'Le Terme esplanade depend de La Rubrique Espaces publics aménagés '),(1294,860,'depend_de',41,81,1,684,'Le Terme esplanade depend de La Rubrique Cultes païens '),(1297,863,'depend_de',41,88,1,686,'Le Terme étable depend de La Rubrique Agriculture, élevage '),(1300,867,'depend_de',41,88,1,689,'Le Terme établissement rural depend de La Rubrique Agriculture, élevage '),(1301,868,'depend_de',41,78,1,690,'Le Terme établissement thermal depend de La Rubrique Bains '),(1303,870,'depend_de',41,65,1,692,'Le Terme étang depend de La Rubrique Aménagements des berges et voies d\'eau '),(1306,870,'depend_de',41,91,1,692,'Le Terme étang depend de La Rubrique Formations naturelles '),(1310,872,'depend_de',41,78,1,694,'Le Terme étuves depend de La Rubrique Bains '),(1313,874,'depend_de',41,69,1,697,'Le Terme évacuation depend de La Rubrique Collecteurs, évacuations '),(1317,875,'depend_de',41,84,1,698,'Le Terme évêché depend de La Rubrique Bâtiments écclésiastiques '),(1322,879,'depend_de',41,66,1,701,'Le Terme exhaussement depend de La Rubrique Aménagements du relief '),(1325,882,'depend_de',41,90,1,704,'Le Terme extraction depend de La Rubrique Extraction '),(1328,877,'depend_de',41,81,1,706,'Le Terme ex-voto depend de La Rubrique Cultes païens '),(1332,900,'depend_de',41,89,1,709,'Le Terme fabrique depend de La Rubrique Industrie '),(1336,903,'depend_de',41,89,1,711,'Le Terme faïencerie depend de La Rubrique Industrie '),(1337,904,'depend_de',41,81,1,712,'Le Terme fanum depend de La Rubrique Cultes païens '),(1338,905,'depend_de',41,87,1,714,'Le Terme fausse monnaie depend de La Rubrique Artisanat '),(1345,909,'depend_de',41,88,1,717,'Le Terme ferme depend de La Rubrique Agriculture, élevage '),(1349,913,'depend_de',41,89,1,721,'Le Terme filature depend de La Rubrique Industrie '),(1355,919,'depend_de',41,80,1,725,'Le Terme fond de cabane depend de La Rubrique Habitat privé '),(1356,919,'depend_de',41,87,1,725,'Le Terme fond de cabane depend de La Rubrique Artisanat '),(1359,922,'depend_de',41,87,1,728,'Le Terme fonderie depend de La Rubrique Artisanat '),(1360,922,'depend_de',41,89,1,728,'Le Terme fonderie depend de La Rubrique Industrie '),(1363,925,'depend_de',41,68,1,732,'Le Terme fontaine depend de La Rubrique Adductions d\'eau '),(1365,925,'depend_de',41,81,1,732,'Le Terme fontaine depend de La Rubrique Cultes païens si en relation avec une pratique cultuelle'),(1369,930,'depend_de',41,87,1,736,'Le Terme forge depend de La Rubrique Artisanat '),(1370,930,'depend_de',41,89,1,736,'Le Terme forge depend de La Rubrique Industrie '),(1373,932,'depend_de',41,71,1,738,'Le Terme fort depend de La Rubrique Structures fortifiées '),(1374,933,'depend_de',41,71,1,739,'Le Terme forteresse depend de La Rubrique Structures fortifiées '),(1378,936,'depend_de',41,70,1,742,'Le Terme fortin depend de La Rubrique Système défensif urbain '),(1379,936,'depend_de',41,71,1,742,'Le Terme fortin depend de La Rubrique Structures fortifiées '),(1380,937,'depend_de',41,73,1,743,'Le Terme forum depend de La Rubrique Espaces publics aménagés '),(1395,939,'depend_de',41,65,1,745,'Le Terme fossé depend de La Rubrique Aménagements des berges et voies d\'eau '),(1398,939,'depend_de',41,69,1,745,'Le Terme fossé depend de La Rubrique Collecteurs, évacuations '),(1399,939,'depend_de',41,70,1,745,'Le Terme fossé depend de La Rubrique Système défensif urbain '),(1400,939,'depend_de',41,71,1,745,'Le Terme fossé depend de La Rubrique Structures fortifiées '),(1404,939,'depend_de',41,80,1,745,'Le Terme fossé depend de La Rubrique Habitat privé '),(1413,942,'depend_de',41,87,1,747,'Le Terme fosse à argile depend de La Rubrique Artisanat '),(1416,944,'depend_de',41,87,1,749,'Le Terme fosse à chaux depend de La Rubrique Artisanat '),(1421,950,'depend_de',41,88,1,754,'Le Terme fosse à lisier depend de La Rubrique Agriculture, élevage '),(1422,951,'depend_de',41,81,1,756,'Le Terme fosse à offrandes depend de La Rubrique Cultes païens '),(1423,951,'depend_de',41,86,1,756,'Le Terme fosse à offrandes depend de La Rubrique Funéraire '),(1427,956,'depend_de',41,86,1,760,'Le Terme fosse commune depend de La Rubrique Funéraire '),(1428,957,'depend_de',41,81,1,761,'Le Terme fosse cultuelle depend de La Rubrique Cultes païens '),(1430,966,'depend_de',41,87,1,763,'Le Terme fosse de décantation depend de La Rubrique Artisanat '),(1432,968,'depend_de',41,88,1,765,'Le Terme fosse de marcottage depend de La Rubrique Agriculture, élevage '),(1433,969,'depend_de',41,88,1,766,'Le Terme fosse de plantation depend de La Rubrique Agriculture, élevage '),(1434,970,'depend_de',41,87,1,767,'Le Terme fosse de rouissage depend de La Rubrique Artisanat '),(1443,962,'depend_de',41,90,1,772,'Le Terme fosse d\'extraction depend de La Rubrique Extraction '),(1446,975,'depend_de',41,63,1,775,'Le Terme fossé parcellaire depend de La Rubrique Voies '),(1447,975,'depend_de',41,80,1,775,'Le Terme fossé parcellaire depend de La Rubrique Habitat privé '),(1448,975,'depend_de',41,88,1,775,'Le Terme fossé parcellaire depend de La Rubrique Agriculture, élevage '),(1466,984,'depend_de',41,87,1,783,'Le Terme foulon depend de La Rubrique Artisanat '),(1474,988,'depend_de',41,87,1,787,'Le Terme four à briques depend de La Rubrique Artisanat '),(1476,989,'depend_de',41,87,1,788,'Le Terme four à céramique depend de La Rubrique Artisanat '),(1477,990,'depend_de',41,87,1,789,'Le Terme four à chaux depend de La Rubrique Artisanat '),(1478,991,'depend_de',41,87,1,790,'Le Terme four à cloche depend de La Rubrique Artisanat '),(1479,992,'depend_de',41,88,1,791,'Le Terme four à grain depend de La Rubrique Agriculture, élevage '),(1486,994,'depend_de',41,87,1,793,'Le Terme four à pain depend de La Rubrique Artisanat '),(1488,997,'depend_de',41,87,1,795,'Le Terme four à recuisson depend de La Rubrique Artisanat '),(1491,1000,'depend_de',41,74,1,798,'Le Terme four banal depend de La Rubrique Pouvoir civil, justice '),(1500,1007,'depend_de',41,89,1,805,'Le Terme four de faïencier depend de La Rubrique Industrie '),(1502,1009,'depend_de',41,87,1,807,'Le Terme four de potier depend de La Rubrique Artisanat '),(1505,1012,'depend_de',41,87,1,810,'Le Terme four de tuilier depend de La Rubrique Artisanat '),(1509,1015,'depend_de',41,87,1,813,'Le Terme four de verrier depend de La Rubrique Artisanat '),(1512,1016,'depend_de',41,80,1,814,'Le Terme four domestique depend de La Rubrique Habitat privé quand usage privé sinon voir four en 25'),(1522,1023,'depend_de',41,78,1,820,'Le Terme foyer depend de La Rubrique Bains '),(1523,1023,'depend_de',41,80,1,820,'Le Terme foyer depend de La Rubrique Habitat privé '),(1528,1023,'depend_de',41,87,1,820,'Le Terme foyer depend de La Rubrique Artisanat '),(1536,1032,'depend_de',41,64,1,825,'Le Terme friche depend de La Rubrique Espaces libres '),(1539,1034,'depend_de',41,78,1,827,'Le Terme frigidarium depend de La Rubrique Bains '),(1543,1038,'depend_de',41,80,1,831,'Le Terme fumoir depend de La Rubrique Habitat privé '),(1544,1038,'depend_de',41,87,1,831,'Le Terme fumoir depend de La Rubrique Artisanat '),(1563,1044,'depend_de',41,70,1,836,'Le Terme galerie de contre-mine depend de La Rubrique Système défensif urbain '),(1566,1041,'depend_de',41,90,1,837,'Le Terme galerie d\'extraction depend de La Rubrique Extraction '),(1568,1046,'depend_de',41,68,1,839,'Le Terme galerie technique depend de La Rubrique Adductions d\'eau '),(1581,1058,'depend_de',41,86,1,848,'Le Terme gisant depend de La Rubrique Funéraire '),(1588,1059,'depend_de',41,80,1,849,'Le Terme glacière depend de La Rubrique Habitat privé '),(1592,1061,'depend_de',41,70,1,850,'Le Terme glacis depend de La Rubrique Système défensif urbain '),(1593,1061,'depend_de',41,71,1,850,'Le Terme glacis depend de La Rubrique Structures fortifiées '),(1595,1063,'depend_de',41,90,1,851,'Le Terme gneiss depend de La Rubrique Extraction '),(1600,1065,'depend_de',41,83,1,853,'Le Terme grange depend de La Rubrique Bâtiments conventuels ou monastiques '),(1601,1065,'depend_de',41,88,1,853,'Le Terme grange depend de La Rubrique Agriculture, élevage '),(1604,1069,'depend_de',41,90,1,856,'Le Terme granit depend de La Rubrique Extraction '),(1609,1074,'depend_de',41,90,1,861,'Le Terme gravière depend de La Rubrique Extraction '),(1621,1080,'depend_de',41,90,1,867,'Le Terme grès depend de La Rubrique Extraction '),(1623,1082,'depend_de',41,82,1,870,'Le Terme groupe épiscopal depend de La Rubrique Edifices cultuels catholiques '),(1624,1083,'depend_de',41,67,1,871,'Le Terme gué depend de La Rubrique Franchissements '),(1628,1087,'depend_de',41,80,1,875,'Le Terme habitat depend de La Rubrique Habitat privé '),(1644,1092,'depend_de',41,80,1,878,'Le Terme habitation depend de La Rubrique Habitat privé '),(1670,1098,'depend_de',41,70,1,881,'Le Terme haie depend de La Rubrique Système défensif urbain '),(1677,1104,'depend_de',41,79,1,887,'Le Terme halles depend de La Rubrique Commerce, échanges '),(1683,1108,'depend_de',41,76,1,889,'Le Terme hôpital depend de La Rubrique Santé '),(1687,1109,'depend_de',41,76,1,890,'Le Terme hôpital militaire depend de La Rubrique Santé '),(1690,1113,'depend_de',41,76,1,893,'Le Terme hospice depend de La Rubrique Santé '),(1696,1118,'depend_de',41,74,1,896,'Le Terme hôtel de ville depend de La Rubrique Pouvoir civil, justice '),(1697,1122,'depend_de',41,80,1,897,'Le Terme hôtel particulier depend de La Rubrique Habitat privé '),(1703,1124,'depend_de',41,76,1,899,'Le Terme hôtel-Dieu depend de La Rubrique Santé '),(1705,1125,'depend_de',41,79,1,900,'Le Terme hôtellerie depend de La Rubrique Commerce, échanges '),(1710,1130,'depend_de',41,87,1,904,'Le Terme huilerie depend de La Rubrique Artisanat '),(1715,1132,'depend_de',41,78,1,906,'Le Terme hypocauste depend de La Rubrique Bains '),(1717,1132,'depend_de',41,80,1,906,'Le Terme hypocauste depend de La Rubrique Habitat privé '),(1719,1133,'depend_de',41,86,1,907,'Le Terme hypogée depend de La Rubrique Funéraire '),(1720,1135,'depend_de',41,63,1,909,'Le Terme îlot depend de La Rubrique Voies '),(1721,1135,'depend_de',41,80,1,909,'Le Terme îlot depend de La Rubrique Habitat privé '),(1722,1136,'depend_de',41,80,1,911,'Le Terme îlot d\'habitation depend de La Rubrique Habitat privé '),(1725,1138,'depend_de',41,80,1,914,'Le Terme immeuble depend de La Rubrique Habitat privé '),(1733,1143,'depend_de',41,86,1,918,'Le Terme incinération depend de La Rubrique Funéraire '),(1735,1146,'depend_de',41,86,1,920,'Le Terme incinération de nourrisson depend de La Rubrique Funéraire '),(1736,1145,'depend_de',41,86,1,921,'Le Terme incinération d\'enfant depend de La Rubrique Funéraire '),(1737,1147,'depend_de',41,86,1,922,'Le Terme incinération isolée depend de La Rubrique Funéraire '),(1739,1151,'depend_de',41,76,1,924,'Le Terme infirmerie depend de La Rubrique Santé '),(1740,1151,'depend_de',41,83,1,924,'Le Terme infirmerie depend de La Rubrique Bâtiments conventuels ou monastiques '),(1741,1152,'depend_de',41,86,1,925,'Le Terme inhumation depend de La Rubrique Funéraire '),(1745,1155,'depend_de',41,86,1,929,'Le Terme inhumation d\'enfant depend de La Rubrique Funéraire '),(1754,1167,'depend_de',41,86,1,935,'Le Terme inscription funéraire depend de La Rubrique Funéraire '),(1759,1171,'depend_de',41,87,1,939,'Le Terme installation artisanale depend de La Rubrique Artisanat '),(1763,1174,'depend_de',41,80,1,942,'Le Terme insula depend de La Rubrique Habitat privé '),(1766,1176,'depend_de',41,88,1,943,'Le Terme irrigation depend de La Rubrique Agriculture, élevage '),(1779,1177,'depend_de',41,80,1,944,'Le Terme jardin depend de La Rubrique Habitat privé '),(1782,1177,'depend_de',41,83,1,944,'Le Terme jardin depend de La Rubrique Bâtiments conventuels ou monastiques '),(1783,1177,'depend_de',41,84,1,944,'Le Terme jardin depend de La Rubrique Bâtiments écclésiastiques '),(1786,1177,'depend_de',41,88,1,944,'Le Terme jardin depend de La Rubrique Agriculture, élevage '),(1793,1183,'depend_de',41,73,1,950,'Le Terme jardin public depend de La Rubrique Espaces publics aménagés '),(1795,1186,'depend_de',41,77,1,952,'Le Terme jeu de paume depend de La Rubrique Spectacle, sport '),(1796,1187,'depend_de',41,82,1,953,'Le Terme jubé depend de La Rubrique Edifices cultuels catholiques '),(1797,1188,'depend_de',41,91,1,954,'Le Terme karst effondré depend de La Rubrique Formations naturelles '),(1800,1191,'depend_de',41,88,1,957,'Le Terme labour depend de La Rubrique Agriculture, élevage '),(1804,1198,'depend_de',41,86,1,961,'Le Terme lanterne des morts depend de La Rubrique Funéraire '),(1805,1199,'depend_de',41,80,1,962,'Le Terme laraire depend de La Rubrique Habitat privé '),(1806,1199,'depend_de',41,81,1,962,'Le Terme laraire depend de La Rubrique Cultes païens '),(1807,1200,'depend_de',41,69,1,963,'Le Terme latrines depend de La Rubrique Collecteurs, évacuations '),(1814,1200,'depend_de',41,78,1,963,'Le Terme latrines depend de La Rubrique Bains '),(1816,1200,'depend_de',41,80,1,963,'Le Terme latrines depend de La Rubrique Habitat privé '),(1818,1200,'depend_de',41,83,1,963,'Le Terme latrines depend de La Rubrique Bâtiments conventuels ou monastiques '),(1826,1203,'depend_de',41,83,1,965,'Le Terme lavabo depend de La Rubrique Bâtiments conventuels ou monastiques '),(1827,1204,'depend_de',41,68,1,966,'Le Terme lavoir depend de La Rubrique Adductions d\'eau '),(1828,1204,'depend_de',41,78,1,966,'Le Terme lavoir depend de La Rubrique Bains '),(1831,1206,'depend_de',41,76,1,968,'Le Terme lazaret depend de La Rubrique Santé '),(1832,1207,'depend_de',41,76,1,969,'Le Terme léproserie depend de La Rubrique Santé '),(1840,1210,'depend_de',41,70,1,972,'Le Terme levée de terre depend de La Rubrique Système défensif urbain '),(1841,1210,'depend_de',41,71,1,972,'Le Terme levée de terre depend de La Rubrique Structures fortifiées '),(1843,1211,'depend_de',41,70,1,974,'Le Terme lice depend de La Rubrique Système défensif urbain '),(1844,1211,'depend_de',41,71,1,974,'Le Terme lice depend de La Rubrique Structures fortifiées '),(1845,1211,'depend_de',41,77,1,974,'Le Terme lice depend de La Rubrique Spectacle, sport '),(1846,1213,'depend_de',41,81,1,975,'Le Terme lieu de culte depend de La Rubrique Cultes païens '),(1847,1213,'depend_de',41,82,1,975,'Le Terme lieu de culte depend de La Rubrique Edifices cultuels catholiques '),(1852,1217,'depend_de',41,86,1,979,'Le Terme limite de cimetière depend de La Rubrique Funéraire '),(1857,1221,'depend_de',41,80,1,984,'Le Terme limite de propriété depend de La Rubrique Habitat privé '),(1858,1222,'depend_de',41,63,1,985,'Le Terme limite parcellaire depend de La Rubrique Voies '),(1863,1222,'depend_de',41,80,1,985,'Le Terme limite parcellaire depend de La Rubrique Habitat privé '),(1868,1222,'depend_de',41,88,1,985,'Le Terme limite parcellaire depend de La Rubrique Agriculture, élevage '),(1870,1223,'depend_de',41,90,1,986,'Le Terme limon depend de La Rubrique Extraction '),(1871,1224,'depend_de',41,86,1,988,'Le Terme linceul depend de La Rubrique Funéraire '),(1880,1233,'depend_de',41,84,1,994,'Le Terme livrée cardinalice depend de La Rubrique Bâtiments écclésiastiques '),(1882,1236,'depend_de',41,90,1,995,'Le Terme loess depend de La Rubrique Extraction '),(1891,1242,'depend_de',41,83,1,1000,'Le Terme logis abbatial depend de La Rubrique Bâtiments conventuels ou monastiques '),(1899,1248,'depend_de',41,79,1,1006,'Le Terme macellum depend de La Rubrique Commerce, échanges '),(1910,1256,'depend_de',41,83,1,1012,'Le Terme maison canoniale depend de La Rubrique Bâtiments conventuels ou monastiques '),(1913,1257,'depend_de',41,74,1,1013,'Le Terme maison communale depend de La Rubrique Pouvoir civil, justice '),(1915,1260,'depend_de',41,74,1,1015,'Le Terme maison corporative depend de La Rubrique Pouvoir civil, justice '),(1918,1262,'depend_de',41,76,1,1017,'Le Terme maison de charité depend de La Rubrique Santé '),(1922,1266,'depend_de',41,79,1,1020,'Le Terme maison de marchand depend de La Rubrique Commerce, échanges '),(1923,1266,'depend_de',41,80,1,1020,'Le Terme maison de marchand depend de La Rubrique Habitat privé '),(1933,1277,'depend_de',41,83,1,1027,'Le Terme maison du prieur depend de La Rubrique Bâtiments conventuels ou monastiques '),(1935,1279,'depend_de',41,71,1,1029,'Le Terme maison forte depend de La Rubrique Structures fortifiées '),(1936,1279,'depend_de',41,80,1,1029,'Le Terme maison forte depend de La Rubrique Habitat privé '),(1944,1285,'depend_de',41,80,1,1034,'Le Terme maison-tour depend de La Rubrique Habitat privé '),(1947,1287,'depend_de',41,76,1,1036,'Le Terme maladrerie depend de La Rubrique Santé '),(1949,1290,'depend_de',41,72,1,1038,'Le Terme manège depend de La Rubrique Garnisons, casernements '),(1951,1292,'depend_de',41,89,1,1040,'Le Terme manufacture depend de La Rubrique Industrie '),(1955,1298,'depend_de',41,88,1,1044,'Le Terme maraîchage depend de La Rubrique Agriculture, élevage '),(1958,1300,'depend_de',41,91,1,1046,'Le Terme marais depend de La Rubrique Formations naturelles '),(1961,1302,'depend_de',41,65,1,1048,'Le Terme marais asséché depend de La Rubrique Aménagements des berges et voies d\'eau '),(1963,1305,'depend_de',41,79,1,1050,'Le Terme marché depend de La Rubrique Commerce, échanges '),(1965,1307,'depend_de',41,65,1,1052,'Le Terme mare depend de La Rubrique Aménagements des berges et voies d\'eau '),(1970,1308,'depend_de',41,91,1,1053,'Le Terme marécage depend de La Rubrique Formations naturelles '),(1974,1310,'depend_de',41,86,1,1055,'Le Terme martyrium depend de La Rubrique Funéraire '),(1976,1313,'depend_de',41,86,1,1057,'Le Terme mausolée depend de La Rubrique Funéraire '),(1978,1316,'depend_de',41,86,1,1058,'Le Terme memoria depend de La Rubrique Funéraire '),(1987,1319,'depend_de',41,87,1,1060,'Le Terme métal depend de La Rubrique Artisanat '),(1988,1319,'depend_de',41,89,1,1060,'Le Terme métal depend de La Rubrique Industrie '),(1992,1322,'depend_de',41,87,1,1063,'Le Terme métier à tisser depend de La Rubrique Artisanat '),(1993,1323,'depend_de',41,87,1,1064,'Le Terme meule depend de La Rubrique Artisanat '),(1995,1326,'depend_de',41,90,1,1066,'Le Terme mine depend de La Rubrique Extraction '),(2002,1334,'depend_de',41,63,1,1072,'Le Terme monument commémoratif depend de La Rubrique Voies '),(2003,1334,'depend_de',41,73,1,1072,'Le Terme monument commémoratif depend de La Rubrique Espaces publics aménagés '),(2005,1336,'depend_de',41,86,1,1074,'Le Terme monument funéraire depend de La Rubrique Funéraire '),(2011,1339,'depend_de',41,80,1,1076,'Le Terme mosaïque depend de La Rubrique Habitat privé '),(2013,1341,'depend_de',41,71,1,1077,'Le Terme motte depend de La Rubrique Structures fortifiées '),(2018,1344,'depend_de',41,87,1,1080,'Le Terme moule depend de La Rubrique Artisanat '),(2024,1348,'depend_de',41,87,1,1084,'Le Terme moulin depend de La Rubrique Artisanat '),(2027,1349,'depend_de',41,87,1,1085,'Le Terme moulin à blé depend de La Rubrique Artisanat '),(2028,1350,'depend_de',41,89,1,1086,'Le Terme moulin à broyer depend de La Rubrique Industrie '),(2044,1367,'depend_de',41,70,1,1098,'Le Terme murus gallicus depend de La Rubrique Système défensif urbain '),(2045,1368,'depend_de',41,75,1,1099,'Le Terme musée depend de La Rubrique Education, culture '),(2046,1369,'depend_de',41,87,1,1100,'Le Terme nacre depend de La Rubrique Artisanat '),(2052,1373,'depend_de',41,86,1,1102,'Le Terme nécropole depend de La Rubrique Funéraire '),(2060,1382,'depend_de',41,91,1,1109,'Le Terme niveau de crue depend de La Rubrique Formations naturelles '),(2061,1383,'depend_de',41,91,1,1110,'Le Terme niveau marin depend de La Rubrique Formations naturelles '),(2062,1384,'depend_de',41,66,1,1111,'Le Terme nivellement depend de La Rubrique Aménagements du relief '),(2063,1385,'depend_de',41,68,1,1112,'Le Terme noria depend de La Rubrique Adductions d\'eau '),(2065,1386,'depend_de',41,83,1,1113,'Le Terme noviciat depend de La Rubrique Bâtiments conventuels ou monastiques '),(2066,1387,'depend_de',41,68,1,1114,'Le Terme nymphée depend de La Rubrique Adductions d\'eau '),(2067,1387,'depend_de',41,81,1,1114,'Le Terme nymphée depend de La Rubrique Cultes païens '),(2070,1390,'depend_de',41,63,1,1117,'Le Terme octroi depend de La Rubrique Voies '),(2072,1390,'depend_de',41,74,1,1117,'Le Terme octroi depend de La Rubrique Pouvoir civil, justice '),(2073,1391,'depend_de',41,77,1,1118,'Le Terme odéon depend de La Rubrique Spectacle, sport '),(2078,1397,'depend_de',41,87,1,1122,'Le Terme or depend de La Rubrique Artisanat '),(2080,1398,'depend_de',41,82,1,1123,'Le Terme oratoire depend de La Rubrique Edifices cultuels catholiques '),(2082,1400,'depend_de',41,87,1,1125,'Le Terme orfèvrerie depend de La Rubrique Artisanat '),(2083,1401,'depend_de',41,63,1,1126,'Le Terme ornière depend de La Rubrique Voies '),(2089,1403,'depend_de',41,87,1,1127,'Le Terme os depend de La Rubrique Artisanat '),(2090,1405,'depend_de',41,86,1,1128,'Le Terme ossement depend de La Rubrique Funéraire '),(2093,1408,'depend_de',41,86,1,1131,'Le Terme ossuaire depend de La Rubrique Funéraire '),(2094,1410,'depend_de',41,70,1,1132,'Le Terme ouvrage avancé depend de La Rubrique Système défensif urbain '),(2095,1410,'depend_de',41,71,1,1132,'Le Terme ouvrage avancé depend de La Rubrique Structures fortifiées '),(2096,1411,'depend_de',41,70,1,1133,'Le Terme ouvrage de défense depend de La Rubrique Système défensif urbain '),(2101,1416,'depend_de',41,74,1,1137,'Le Terme palais depend de La Rubrique Pouvoir civil, justice '),(2105,1421,'depend_de',41,74,1,1141,'Le Terme palais de justice depend de La Rubrique Pouvoir civil, justice '),(2111,1428,'depend_de',41,91,1,1146,'Le Terme paléochenal depend de La Rubrique Formations naturelles '),(2112,1429,'depend_de',41,91,1,1148,'Le Terme paléosol depend de La Rubrique Formations naturelles '),(2114,1430,'depend_de',41,77,1,1149,'Le Terme palestre depend de La Rubrique Spectacle, sport '),(2115,1430,'depend_de',41,78,1,1149,'Le Terme palestre depend de La Rubrique Bains '),(2118,1431,'depend_de',41,70,1,1150,'Le Terme palissade depend de La Rubrique Système défensif urbain '),(2119,1431,'depend_de',41,71,1,1150,'Le Terme palissade depend de La Rubrique Structures fortifiées '),(2122,1431,'depend_de',41,80,1,1150,'Le Terme palissade depend de La Rubrique Habitat privé '),(2126,1434,'depend_de',41,89,1,1152,'Le Terme papeterie depend de La Rubrique Industrie '),(2127,1435,'depend_de',41,70,1,1153,'Le Terme parapet depend de La Rubrique Système défensif urbain '),(2128,1435,'depend_de',41,71,1,1153,'Le Terme parapet depend de La Rubrique Structures fortifiées '),(2133,1437,'depend_de',41,88,1,1155,'Le Terme parc à bétail depend de La Rubrique Agriculture, élevage '),(2135,1440,'depend_de',41,63,1,1158,'Le Terme parcellaire depend de La Rubrique Voies '),(2137,1440,'depend_de',41,80,1,1158,'Le Terme parcellaire depend de La Rubrique Habitat privé '),(2139,1440,'depend_de',41,88,1,1158,'Le Terme parcellaire depend de La Rubrique Agriculture, élevage '),(2140,1442,'depend_de',41,88,1,1159,'Le Terme pars rustica depend de La Rubrique Agriculture, élevage '),(2143,1444,'depend_de',41,73,1,1160,'Le Terme parvis depend de La Rubrique Espaces publics aménagés '),(2144,1444,'depend_de',41,82,1,1160,'Le Terme parvis depend de La Rubrique Edifices cultuels catholiques '),(2145,1445,'depend_de',41,63,1,1161,'Le Terme passage depend de La Rubrique Voies '),(2152,1449,'depend_de',41,87,1,1166,'Le Terme patenôterie depend de La Rubrique Artisanat '),(2159,1457,'depend_de',41,65,1,1173,'Le Terme pêcherie depend de La Rubrique Aménagements des berges et voies d\'eau '),(2162,1458,'depend_de',41,80,1,1174,'Le Terme peinture murale depend de La Rubrique Habitat privé '),(2164,1459,'depend_de',41,87,1,1175,'Le Terme pelleterie depend de La Rubrique Artisanat '),(2169,1464,'depend_de',41,73,1,1180,'Le Terme péribole depend de La Rubrique Espaces publics aménagés '),(2170,1464,'depend_de',41,81,1,1180,'Le Terme péribole depend de La Rubrique Cultes païens '),(2171,1465,'depend_de',41,78,1,1182,'Le Terme péristyle depend de La Rubrique Bains '),(2172,1465,'depend_de',41,80,1,1182,'Le Terme péristyle depend de La Rubrique Habitat privé '),(2173,1465,'depend_de',41,81,1,1182,'Le Terme péristyle depend de La Rubrique Cultes païens '),(2184,1476,'depend_de',41,88,1,1192,'Le Terme pigeonnier depend de La Rubrique Agriculture, élevage '),(2187,1479,'depend_de',41,74,1,1195,'Le Terme pilori depend de La Rubrique Pouvoir civil, justice '),(2192,1483,'depend_de',41,78,1,1199,'Le Terme piscine depend de La Rubrique Bains '),(2195,1484,'depend_de',41,82,1,1200,'Le Terme piscine baptismale depend de La Rubrique Edifices cultuels catholiques '),(2197,1486,'depend_de',41,63,1,1202,'Le Terme place depend de La Rubrique Voies '),(2201,1486,'depend_de',41,73,1,1202,'Le Terme place depend de La Rubrique Espaces publics aménagés '),(2212,1493,'depend_de',41,91,1,1206,'Le Terme plage depend de La Rubrique Formations naturelles '),(2213,1494,'depend_de',41,91,1,1207,'Le Terme plaine alluviale depend de La Rubrique Formations naturelles '),(2214,1495,'depend_de',41,91,1,1208,'Le Terme plaine d\'inondation depend de La Rubrique Formations naturelles '),(2215,1496,'depend_de',41,91,1,1209,'Le Terme plaine fluviale depend de La Rubrique Formations naturelles '),(2216,1497,'depend_de',41,91,1,1210,'Le Terme plancher alluvial depend de La Rubrique Formations naturelles '),(2220,1498,'depend_de',41,88,1,1211,'Le Terme plantation depend de La Rubrique Agriculture, élevage '),(2225,1502,'depend_de',41,66,1,1216,'Le Terme plate-forme depend de La Rubrique Aménagements du relief '),(2226,1502,'depend_de',41,70,1,1216,'Le Terme plate-forme depend de La Rubrique Système défensif urbain '),(2227,1502,'depend_de',41,71,1,1216,'Le Terme plate-forme depend de La Rubrique Structures fortifiées '),(2229,1506,'depend_de',41,86,1,1219,'Le Terme pleine terre depend de La Rubrique Funéraire '),(2231,1507,'depend_de',41,87,1,1220,'Le Terme plomb depend de La Rubrique Artisanat '),(2233,1508,'depend_de',41,74,1,1221,'Le Terme podium depend de La Rubrique Pouvoir civil, justice '),(2235,1508,'depend_de',41,81,1,1221,'Le Terme podium depend de La Rubrique Cultes païens '),(2241,1514,'depend_de',41,81,1,1226,'Le Terme pomerium depend de La Rubrique Cultes païens '),(2245,1516,'depend_de',41,67,1,1227,'Le Terme pont depend de La Rubrique Franchissements '),(2250,1518,'depend_de',41,71,1,1228,'Le Terme pont dormant depend de La Rubrique Structures fortifiées '),(2254,1519,'depend_de',41,71,1,1230,'Le Terme pont-levis depend de La Rubrique Structures fortifiées '),(2255,1521,'depend_de',41,65,1,1231,'Le Terme ponton depend de La Rubrique Aménagements des berges et voies d\'eau '),(2260,1526,'depend_de',41,65,1,1236,'Le Terme port depend de La Rubrique Aménagements des berges et voies d\'eau '),(2263,1528,'depend_de',41,70,1,1238,'Le Terme porte depend de La Rubrique Système défensif urbain '),(2264,1528,'depend_de',41,71,1,1238,'Le Terme porte depend de La Rubrique Structures fortifiées '),(2265,1528,'depend_de',41,72,1,1238,'Le Terme porte depend de La Rubrique Garnisons, casernements '),(2270,1529,'depend_de',41,70,1,1240,'Le Terme porte d\'eau depend de La Rubrique Système défensif urbain '),(2275,1532,'depend_de',41,83,1,1242,'Le Terme porterie depend de La Rubrique Bâtiments conventuels ou monastiques '),(2276,1533,'depend_de',41,63,1,1243,'Le Terme portique depend de La Rubrique Voies '),(2280,1533,'depend_de',41,73,1,1243,'Le Terme portique depend de La Rubrique Espaces publics aménagés '),(2281,1533,'depend_de',41,74,1,1243,'Le Terme portique depend de La Rubrique Pouvoir civil, justice '),(2283,1533,'depend_de',41,78,1,1243,'Le Terme portique depend de La Rubrique Bains '),(2285,1533,'depend_de',41,80,1,1243,'Le Terme portique depend de La Rubrique Habitat privé '),(2286,1533,'depend_de',41,81,1,1243,'Le Terme portique depend de La Rubrique Cultes païens '),(2287,1533,'depend_de',41,82,1,1243,'Le Terme portique depend de La Rubrique Edifices cultuels catholiques '),(2299,1542,'depend_de',41,70,1,1251,'Le Terme poterne depend de La Rubrique Système défensif urbain '),(2300,1542,'depend_de',41,71,1,1251,'Le Terme poterne depend de La Rubrique Structures fortifiées '),(2303,1546,'depend_de',41,72,1,1254,'Le Terme poudrière depend de La Rubrique Garnisons, casernements '),(2304,1547,'depend_de',41,88,1,1255,'Le Terme poulailler depend de La Rubrique Agriculture, élevage '),(2305,1548,'depend_de',41,86,1,1256,'Le Terme pourrissoir depend de La Rubrique Funéraire '),(2306,1549,'depend_de',41,78,1,1257,'Le Terme praefurnium depend de La Rubrique Bains '),(2311,1551,'depend_de',41,88,1,1259,'Le Terme pré depend de La Rubrique Agriculture, élevage '),(2313,1553,'depend_de',41,74,1,1261,'Le Terme préfecture depend de La Rubrique Pouvoir civil, justice '),(2315,1554,'depend_de',41,84,1,1262,'Le Terme presbytère depend de La Rubrique Bâtiments écclésiastiques '),(2319,1556,'depend_de',41,87,1,1264,'Le Terme pressoir depend de La Rubrique Artisanat '),(2320,1557,'depend_de',41,74,1,1265,'Le Terme prétoire depend de La Rubrique Pouvoir civil, justice '),(2321,1558,'depend_de',41,74,1,1266,'Le Terme prévôté depend de La Rubrique Pouvoir civil, justice '),(2325,1559,'depend_de',41,83,1,1267,'Le Terme prieuré depend de La Rubrique Bâtiments conventuels ou monastiques '),(2331,1561,'depend_de',41,74,1,1268,'Le Terme prison depend de La Rubrique Pouvoir civil, justice '),(2334,1567,'depend_de',41,63,1,1270,'Le Terme promenade depend de La Rubrique Voies '),(2341,1570,'depend_de',41,69,1,1272,'Le Terme puisard depend de La Rubrique Collecteurs, évacuations '),(2343,1570,'depend_de',41,80,1,1272,'Le Terme puisard depend de La Rubrique Habitat privé '),(2345,1570,'depend_de',41,83,1,1272,'Le Terme puisard depend de La Rubrique Bâtiments conventuels ou monastiques '),(2346,1570,'depend_de',41,87,1,1272,'Le Terme puisard depend de La Rubrique Artisanat '),(2352,1574,'depend_de',41,68,1,1276,'Le Terme puits depend de La Rubrique Adductions d\'eau '),(2361,1574,'depend_de',41,80,1,1276,'Le Terme puits depend de La Rubrique Habitat privé '),(2364,1574,'depend_de',41,83,1,1276,'Le Terme puits depend de La Rubrique Bâtiments conventuels ou monastiques '),(2365,1574,'depend_de',41,84,1,1276,'Le Terme puits depend de La Rubrique Bâtiments écclésiastiques '),(2367,1574,'depend_de',41,87,1,1276,'Le Terme puits depend de La Rubrique Artisanat '),(2368,1574,'depend_de',41,88,1,1276,'Le Terme puits depend de La Rubrique Agriculture, élevage '),(2375,1580,'depend_de',41,81,1,1281,'Le Terme puits cultuel depend de La Rubrique Cultes païens '),(2376,1581,'depend_de',41,90,1,1282,'Le Terme puits d\'accès depend de La Rubrique Extraction '),(2381,1583,'depend_de',41,90,1,1285,'Le Terme puits d\'extraction depend de La Rubrique Extraction '),(2382,1586,'depend_de',41,86,1,1286,'Le Terme puits funéraire depend de La Rubrique Funéraire '),(2393,1597,'depend_de',41,65,1,1293,'Le Terme quai depend de La Rubrique Aménagements des berges et voies d\'eau '),(2395,1598,'depend_de',41,87,1,1294,'Le Terme quartier artisanal depend de La Rubrique Artisanat '),(2401,1605,'depend_de',41,66,1,1298,'Le Terme rampe depend de La Rubrique Aménagements du relief '),(2411,1616,'depend_de',41,90,1,1306,'Le Terme récupération de matériau depend de La Rubrique Extraction '),(2414,1619,'depend_de',41,70,1,1309,'Le Terme redoute depend de La Rubrique Système défensif urbain '),(2415,1620,'depend_de',41,86,1,1310,'Le Terme réduction depend de La Rubrique Funéraire '),(2419,1625,'depend_de',41,83,1,1314,'Le Terme réfectoire depend de La Rubrique Bâtiments conventuels ou monastiques '),(2430,1634,'depend_de',41,66,1,1322,'Le Terme remblai depend de La Rubrique Aménagements du relief '),(2441,1640,'depend_de',41,70,1,1327,'Le Terme rempart de terre depend de La Rubrique Système défensif urbain '),(2449,1647,'depend_de',41,74,1,1333,'Le Terme résidence depend de La Rubrique Pouvoir civil, justice quand résidence du pouvoir en place'),(2450,1647,'depend_de',41,80,1,1333,'Le Terme résidence depend de La Rubrique Habitat privé '),(2455,1650,'depend_de',41,84,1,1336,'Le Terme résidence épiscopale depend de La Rubrique Bâtiments écclésiastiques '),(2462,1656,'depend_de',41,69,1,1341,'Le Terme rigole depend de La Rubrique Collecteurs, évacuations '),(2464,1658,'depend_de',41,91,1,1343,'Le Terme rivage depend de La Rubrique Formations naturelles '),(2465,1659,'depend_de',41,91,1,1344,'Le Terme rive depend de La Rubrique Formations naturelles '),(2466,1660,'depend_de',41,91,1,1345,'Le Terme rivière depend de La Rubrique Formations naturelles '),(2470,1665,'depend_de',41,63,1,1349,'Le Terme rue depend de La Rubrique Voies '),(2474,1666,'depend_de',41,63,1,1350,'Le Terme ruelle depend de La Rubrique Voies '),(2477,1667,'depend_de',41,91,1,1351,'Le Terme ruisseau depend de La Rubrique Formations naturelles '),(2483,1671,'depend_de',41,90,1,1355,'Le Terme sablière depend de La Rubrique Extraction '),(2487,1674,'depend_de',41,82,1,1358,'Le Terme sacristie depend de La Rubrique Edifices cultuels catholiques '),(2498,1679,'depend_de',41,83,1,1362,'Le Terme salle capitulaire depend de La Rubrique Bâtiments conventuels ou monastiques '),(2501,1682,'depend_de',41,77,1,1364,'Le Terme salle d\'armes depend de La Rubrique Spectacle, sport '),(2516,1697,'depend_de',41,81,1,1377,'Le Terme sanctuaire depend de La Rubrique Cultes païens '),(2518,1701,'depend_de',41,81,1,1379,'Le Terme sanctuaire germanique depend de La Rubrique Cultes païens '),(2519,1702,'depend_de',41,81,1,1380,'Le Terme sanctuaire métroaque depend de La Rubrique Cultes païens '),(2525,1704,'depend_de',41,86,1,1382,'Le Terme sarcophage depend de La Rubrique Funéraire '),(2527,1705,'depend_de',41,86,1,1383,'Le Terme sarcophage de plâtre depend de La Rubrique Funéraire '),(2528,1706,'depend_de',41,86,1,1384,'Le Terme sarcophage de plomb depend de La Rubrique Funéraire '),(2535,1714,'depend_de',41,89,1,1392,'Le Terme savonnerie depend de La Rubrique Industrie '),(2537,1716,'depend_de',41,74,1,1394,'Le Terme schola depend de La Rubrique Pouvoir civil, justice '),(2541,1719,'depend_de',41,87,1,1397,'Le Terme scories depend de La Rubrique Artisanat '),(2544,1722,'depend_de',41,80,1,1399,'Le Terme séchoir depend de La Rubrique Habitat privé '),(2545,1722,'depend_de',41,87,1,1399,'Le Terme séchoir depend de La Rubrique Artisanat '),(2547,1724,'depend_de',41,91,1,1401,'Le Terme sédiments depend de La Rubrique Formations naturelles '),(2550,1726,'depend_de',41,75,1,1403,'Le Terme séminaire depend de La Rubrique Education, culture '),(2556,1728,'depend_de',41,86,1,1406,'Le Terme sépulture depend de La Rubrique Funéraire '),(2557,1729,'depend_de',41,86,1,1407,'Le Terme sépulture animale depend de La Rubrique Funéraire '),(2559,1731,'depend_de',41,86,1,1409,'Le Terme sépulture collective depend de La Rubrique Funéraire '),(2562,1737,'depend_de',41,86,1,1411,'Le Terme sépulture de nourrisson depend de La Rubrique Funéraire '),(2566,1734,'depend_de',41,86,1,1415,'Le Terme sépulture d\'enfant depend de La Rubrique Funéraire '),(2578,1746,'depend_de',41,86,1,1422,'Le Terme sépulture isolée depend de La Rubrique Funéraire '),(2580,1747,'depend_de',41,86,1,1423,'Le Terme sépulture juive depend de La Rubrique Funéraire '),(2582,1749,'depend_de',41,86,1,1425,'Le Terme sépulture multiple depend de La Rubrique Funéraire '),(2584,1751,'depend_de',41,86,1,1427,'Le Terme sépulture privilégiée depend de La Rubrique Funéraire '),(2585,1752,'depend_de',41,86,1,1428,'Le Terme sépulture protestante depend de La Rubrique Funéraire '),(2588,1756,'depend_de',41,86,1,1430,'Le Terme sépulture sous lauzes depend de La Rubrique Funéraire '),(2589,1757,'depend_de',41,86,1,1431,'Le Terme sépulture sous tuiles depend de La Rubrique Funéraire '),(2601,1766,'depend_de',41,74,1,1440,'Le Terme silo depend de La Rubrique Pouvoir civil, justice '),(2602,1766,'depend_de',41,79,1,1440,'Le Terme silo depend de La Rubrique Commerce, échanges quand stockage public'),(2603,1766,'depend_de',41,80,1,1440,'Le Terme silo depend de La Rubrique Habitat privé '),(2608,1766,'depend_de',41,88,1,1440,'Le Terme silo depend de La Rubrique Agriculture, élevage '),(2617,1774,'depend_de',41,65,1,1447,'Le Terme soutènement depend de La Rubrique Aménagements des berges et voies d\'eau '),(2618,1774,'depend_de',41,66,1,1447,'Le Terme soutènement depend de La Rubrique Aménagements du relief '),(2628,1776,'depend_de',41,80,1,1449,'Le Terme souterrain depend de La Rubrique Habitat privé '),(2630,1778,'depend_de',41,73,1,1451,'Le Terme square depend de La Rubrique Espaces publics aménagés '),(2644,1787,'depend_de',41,79,1,1459,'Le Terme stockage depend de La Rubrique Commerce, échanges '),(2645,1787,'depend_de',41,80,1,1459,'Le Terme stockage depend de La Rubrique Habitat privé '),(2646,1787,'depend_de',41,87,1,1459,'Le Terme stockage depend de La Rubrique Artisanat '),(2647,1787,'depend_de',41,88,1,1459,'Le Terme stockage depend de La Rubrique Agriculture, élevage '),(2652,1794,'depend_de',41,88,1,1464,'Le Terme structure de séchage depend de La Rubrique Agriculture, élevage '),(2661,1801,'depend_de',41,85,1,1471,'Le Terme synagogue depend de La Rubrique Cultes autres que catholique '),(2665,1807,'depend_de',41,87,1,1475,'Le Terme tabletterie depend de La Rubrique Artisanat '),(2671,1810,'depend_de',41,70,1,1478,'Le Terme talus depend de La Rubrique Système défensif urbain '),(2672,1810,'depend_de',41,71,1,1478,'Le Terme talus depend de La Rubrique Structures fortifiées '),(2679,1812,'depend_de',41,91,1,1480,'Le Terme talweg depend de La Rubrique Formations naturelles '),(2681,1813,'depend_de',41,87,1,1481,'Le Terme tannerie depend de La Rubrique Artisanat '),(2682,1813,'depend_de',41,89,1,1481,'Le Terme tannerie depend de La Rubrique Industrie '),(2685,1817,'depend_de',41,87,1,1484,'Le Terme teinturerie depend de La Rubrique Artisanat '),(2689,1819,'depend_de',41,74,1,1486,'Le Terme temple depend de La Rubrique Pouvoir civil, justice quand temple du culte officiel'),(2690,1819,'depend_de',41,81,1,1486,'Le Terme temple depend de La Rubrique Cultes païens '),(2693,1821,'depend_de',41,85,1,1488,'Le Terme temple protestant depend de La Rubrique Cultes autres que catholique '),(2694,1822,'depend_de',41,78,1,1489,'Le Terme tepidarium depend de La Rubrique Bains '),(2697,1825,'depend_de',41,91,1,1491,'Le Terme terrain inondable depend de La Rubrique Formations naturelles '),(2700,1829,'depend_de',41,64,1,1494,'Le Terme terrain vague depend de La Rubrique Espaces libres '),(2711,1830,'depend_de',41,66,1,1495,'Le Terme terrasse depend de La Rubrique Aménagements du relief '),(2727,1841,'depend_de',41,64,1,1504,'Le Terme terres noires depend de La Rubrique Espaces libres '),(2729,1843,'depend_de',41,87,1,1506,'Le Terme tessonière depend de La Rubrique Artisanat '),(2731,1844,'depend_de',41,89,1,1507,'Le Terme tessonnière depend de La Rubrique Industrie '),(2734,1846,'depend_de',41,87,1,1508,'Le Terme textile depend de La Rubrique Artisanat '),(2735,1846,'depend_de',41,89,1,1508,'Le Terme textile depend de La Rubrique Industrie '),(2738,1848,'depend_de',41,77,1,1509,'Le Terme théâtre depend de La Rubrique Spectacle, sport '),(2740,1850,'depend_de',41,78,1,1510,'Le Terme thermes depend de La Rubrique Bains '),(2741,1850,'depend_de',41,80,1,1510,'Le Terme thermes depend de La Rubrique Habitat privé '),(2744,1853,'depend_de',41,87,1,1513,'Le Terme tissage depend de La Rubrique Artisanat '),(2752,1864,'depend_de',41,86,1,1521,'Le Terme tombe double depend de La Rubrique Funéraire '),(2757,1870,'depend_de',41,86,1,1526,'Le Terme tombe rupestre depend de La Rubrique Funéraire '),(2760,1873,'depend_de',41,86,1,1529,'Le Terme tombeau depend de La Rubrique Funéraire '),(2766,1877,'depend_de',41,70,1,1533,'Le Terme tour depend de La Rubrique Système défensif urbain '),(2767,1877,'depend_de',41,71,1,1533,'Le Terme tour depend de La Rubrique Structures fortifiées '),(2769,1877,'depend_de',41,80,1,1533,'Le Terme tour depend de La Rubrique Habitat privé si tour servant d\'habitation'),(2772,1878,'depend_de',41,87,1,1534,'Le Terme tour de potier depend de La Rubrique Artisanat '),(2783,1887,'depend_de',41,87,1,1542,'Le Terme traitement de l\'argile depend de La Rubrique Artisanat '),(2784,1888,'depend_de',41,63,1,1543,'Le Terme trame urbaine depend de La Rubrique Voies '),(2796,1901,'depend_de',41,63,1,1553,'Le Terme trottoir depend de La Rubrique Voies '),(2805,1905,'depend_de',41,87,1,1556,'Le Terme tuilerie depend de La Rubrique Artisanat '),(2807,1907,'depend_de',41,86,1,1558,'Le Terme tumulus depend de La Rubrique Funéraire '),(2808,1908,'depend_de',41,67,1,1559,'Le Terme tunnel depend de La Rubrique Franchissements '),(2813,1914,'depend_de',41,86,1,1563,'Le Terme urne depend de La Rubrique Funéraire '),(2815,1917,'depend_de',41,89,1,1564,'Le Terme usine depend de La Rubrique Industrie '),(2817,1920,'depend_de',41,86,1,1566,'Le Terme ustrinum depend de La Rubrique Funéraire '),(2818,1921,'depend_de',41,91,1,1568,'Le Terme vallée secondaire depend de La Rubrique Formations naturelles '),(2820,1923,'depend_de',41,70,1,1570,'Le Terme vallum depend de La Rubrique Système défensif urbain '),(2821,1924,'depend_de',41,65,1,1571,'Le Terme vanne depend de La Rubrique Aménagements des berges et voies d\'eau '),(2830,1931,'depend_de',41,87,1,1577,'Le Terme verre depend de La Rubrique Artisanat '),(2836,1938,'depend_de',41,69,1,1582,'Le Terme vide sanitaire depend de La Rubrique Collecteurs, évacuations '),(2840,1939,'depend_de',41,88,1,1583,'Le Terme vigne depend de La Rubrique Agriculture, élevage '),(2842,1941,'depend_de',41,80,1,1585,'Le Terme villa depend de La Rubrique Habitat privé '),(2843,1941,'depend_de',41,88,1,1585,'Le Terme villa depend de La Rubrique Agriculture, élevage '),(2849,1947,'depend_de',41,65,1,1590,'Le Terme vivier depend de La Rubrique Aménagements des berges et voies d\'eau '),(2852,1947,'depend_de',41,88,1,1590,'Le Terme vivier depend de La Rubrique Agriculture, élevage '),(2853,1948,'depend_de',41,63,1,1591,'Le Terme voie depend de La Rubrique Voies '),(2886,1969,'depend_de',41,91,1,1609,'Le Terme zone humide depend de La Rubrique Formations naturelles '),(2891,1972,'depend_de',41,91,1,1612,'Le Terme zone marécageuse depend de La Rubrique Formations naturelles '),(2893,357,'depend_de',41,63,1,1614,'Le Terme cadastration depend de La Rubrique Voies '),(2894,620,'depend_de',41,63,1,1615,'Le Terme croix depend de La Rubrique Voies '),(2895,1325,'depend_de',41,63,1,1616,'Le Terme milliaire depend de La Rubrique Voies '),(2896,1490,'depend_de',41,63,1,1617,'Le Terme place royale depend de La Rubrique Voies '),(2897,1490,'depend_de',41,73,1,1617,'Le Terme place royale depend de La Rubrique Espaces publics aménagés '),(2908,378,'depend_de',41,65,1,1628,'Le Terme canal souterrain depend de La Rubrique Aménagements des berges et voies d\'eau '),(2914,1106,'depend_de',41,65,1,1634,'Le Terme hangar à bateaux depend de La Rubrique Aménagements des berges et voies d\'eau '),(2915,1352,'depend_de',41,65,1,1635,'Le Terme moulin à marée depend de La Rubrique Aménagements des berges et voies d\'eau '),(2917,1515,'depend_de',41,65,1,1636,'Le Terme pompe depend de La Rubrique Aménagements des berges et voies d\'eau '),(2918,1608,'depend_de',41,65,1,1637,'Le Terme rampe de halage depend de La Rubrique Aménagements des berges et voies d\'eau '),(2920,1660,'depend_de',41,65,1,1345,'Le Terme rivière depend de La Rubrique Aménagements des berges et voies d\'eau '),(2928,1908,'depend_de',41,66,1,1559,'Le Terme tunnel depend de La Rubrique Aménagements du relief '),(2929,1517,'depend_de',41,67,1,1645,'Le Terme pont de bateaux depend de La Rubrique Franchissements '),(2931,1935,'depend_de',41,67,1,1647,'Le Terme viaduc depend de La Rubrique Franchissements '),(2932,146,'depend_de',41,68,1,1648,'Le Terme aménagement de source depend de La Rubrique Adductions d\'eau '),(2936,719,'depend_de',41,68,1,554,'Le Terme déversoir depend de La Rubrique Adductions d\'eau '),(2938,1269,'depend_de',41,68,1,1653,'Le Terme maison de source depend de La Rubrique Adductions d\'eau '),(2940,1512,'depend_de',41,68,1,1655,'Le Terme point d\'eau aménagé depend de La Rubrique Adductions d\'eau '),(2941,1515,'depend_de',41,68,1,1636,'Le Terme pompe depend de La Rubrique Adductions d\'eau '),(2942,1587,'depend_de',41,68,1,1656,'Le Terme puits monumental depend de La Rubrique Adductions d\'eau '),(2950,1913,'depend_de',41,69,1,1661,'Le Terme urinoir depend de La Rubrique Collecteurs, évacuations '),(2951,1936,'depend_de',41,69,1,1580,'Le Terme vidange depend de La Rubrique Collecteurs, évacuations '),(2953,318,'depend_de',41,70,1,1663,'Le Terme berme depend de La Rubrique Système défensif urbain '),(2957,933,'depend_de',41,70,1,739,'Le Terme forteresse depend de La Rubrique Système défensif urbain '),(2959,1090,'depend_de',41,70,1,1668,'Le Terme habitat fortifié depend de La Rubrique Système défensif urbain '),(2967,283,'depend_de',41,71,1,164,'Le Terme bastille depend de La Rubrique Structures fortifiées '),(2968,318,'depend_de',41,71,1,1663,'Le Terme berme depend de La Rubrique Structures fortifiées '),(2969,386,'depend_de',41,71,1,250,'Le Terme caponnière depend de La Rubrique Structures fortifiées '),(2971,521,'depend_de',41,71,1,1674,'Le Terme clocher-donjon depend de La Rubrique Structures fortifiées '),(2972,521,'depend_de',41,82,1,1674,'Le Terme clocher-donjon depend de La Rubrique Edifices cultuels catholiques '),(2975,794,'depend_de',41,71,1,622,'Le Terme enceinte canoniale depend de La Rubrique Structures fortifiées '),(2976,799,'depend_de',41,71,1,627,'Le Terme enceinte en bois depend de La Rubrique Structures fortifiées '),(2978,1090,'depend_de',41,71,1,1668,'Le Terme habitat fortifié depend de La Rubrique Structures fortifiées '),(2979,1411,'depend_de',41,71,1,1133,'Le Terme ouvrage de défense depend de La Rubrique Structures fortifiées '),(2981,1529,'depend_de',41,71,1,1240,'Le Terme porte d\'eau depend de La Rubrique Structures fortifiées '),(2982,1596,'depend_de',41,71,1,1676,'Le Terme qasba depend de La Rubrique Structures fortifiées '),(2983,1619,'depend_de',41,71,1,1309,'Le Terme redoute depend de La Rubrique Structures fortifiées '),(2985,1923,'depend_de',41,71,1,1570,'Le Terme vallum depend de La Rubrique Structures fortifiées '),(2986,306,'depend_de',41,72,1,1677,'Le Terme bâtiment militaire depend de La Rubrique Garnisons, casernements '),(2990,932,'depend_de',41,72,1,738,'Le Terme fort depend de La Rubrique Garnisons, casernements '),(2991,1109,'depend_de',41,72,1,890,'Le Terme hôpital militaire depend de La Rubrique Garnisons, casernements '),(2992,1201,'depend_de',41,72,1,1679,'Le Terme latrines militaires depend de La Rubrique Garnisons, casernements '),(2993,1210,'depend_de',41,72,1,972,'Le Terme levée de terre depend de La Rubrique Garnisons, casernements '),(2995,1557,'depend_de',41,72,1,1265,'Le Terme prétoire depend de La Rubrique Garnisons, casernements '),(2996,1560,'depend_de',41,72,1,1681,'Le Terme principia depend de La Rubrique Garnisons, casernements '),(2997,1562,'depend_de',41,72,1,1682,'Le Terme prison militaire depend de La Rubrique Garnisons, casernements '),(2999,1640,'depend_de',41,72,1,1327,'Le Terme rempart de terre depend de La Rubrique Garnisons, casernements '),(3002,1828,'depend_de',41,72,1,1685,'Le Terme terrain militaire depend de La Rubrique Garnisons, casernements '),(3004,167,'depend_de',41,73,1,1687,'Le Terme arc triomphal depend de La Rubrique Espaces publics aménagés '),(3006,368,'depend_de',41,73,1,237,'Le Terme calvaire depend de La Rubrique Espaces publics aménagés '),(3007,620,'depend_de',41,73,1,1615,'Le Terme croix depend de La Rubrique Espaces publics aménagés '),(3011,1900,'depend_de',41,73,1,1690,'Le Terme trophée depend de La Rubrique Espaces publics aménagés '),(3013,449,'depend_de',41,74,1,1692,'Le Terme chancellerie depend de La Rubrique Pouvoir civil, justice '),(3014,731,'depend_de',41,74,1,1693,'Le Terme douane depend de La Rubrique Pouvoir civil, justice '),(3015,1049,'depend_de',41,74,1,842,'Le Terme gare depend de La Rubrique Pouvoir civil, justice '),(3016,1057,'depend_de',41,74,1,1694,'Le Terme gibet depend de La Rubrique Pouvoir civil, justice '),(3017,1060,'depend_de',41,74,1,1695,'Le Terme glacière communale depend de La Rubrique Pouvoir civil, justice '),(3018,1330,'depend_de',41,74,1,1696,'Le Terme monnaie depend de La Rubrique Pouvoir civil, justice '),(3020,1175,'depend_de',41,74,1,1698,'Le Terme intendance depend de La Rubrique Pouvoir civil, justice '),(3021,1763,'depend_de',41,74,1,1700,'Le Terme siège du pouvoir public depend de La Rubrique Pouvoir civil, justice '),(3023,1121,'depend_de',41,74,1,1701,'Le Terme hôtel du gouverneur depend de La Rubrique Pouvoir civil, justice '),(3024,1212,'depend_de',41,74,1,1702,'Le Terme lieu d\'exécution depend de La Rubrique Pouvoir civil, justice '),(3032,1528,'depend_de',41,74,1,1238,'Le Terme porte depend de La Rubrique Pouvoir civil, justice '),(3033,1537,'depend_de',41,74,1,1709,'Le Terme poste depend de La Rubrique Pouvoir civil, justice '),(3035,1631,'depend_de',41,74,1,1710,'Le Terme relais de poste depend de La Rubrique Pouvoir civil, justice '),(3036,1697,'depend_de',41,74,1,1377,'Le Terme sanctuaire depend de La Rubrique Pouvoir civil, justice '),(3037,173,'depend_de',41,75,1,1711,'Le Terme archives depend de La Rubrique Education, culture '),(3039,752,'depend_de',41,75,1,1713,'Le Terme édifice culturel depend de La Rubrique Education, culture '),(3042,1289,'depend_de',41,75,1,1716,'Le Terme manécanterie depend de La Rubrique Education, culture '),(3044,1912,'depend_de',41,75,1,1718,'Le Terme université depend de La Rubrique Education, culture '),(3045,614,'depend_de',41,76,1,1719,'Le Terme couvent hospitalier depend de La Rubrique Santé '),(3046,614,'depend_de',41,83,1,1719,'Le Terme couvent hospitalier depend de La Rubrique Bâtiments conventuels ou monastiques '),(3047,866,'depend_de',41,76,1,1720,'Le Terme établissement de cure depend de La Rubrique Santé '),(3048,868,'depend_de',41,76,1,690,'Le Terme établissement thermal depend de La Rubrique Santé '),(3049,1262,'depend_de',41,83,1,1017,'Le Terme maison de charité depend de La Rubrique Bâtiments conventuels ou monastiques '),(3052,1402,'depend_de',41,76,1,1722,'Le Terme orphelinat depend de La Rubrique Santé '),(3054,371,'depend_de',41,77,1,1724,'Le Terme campus depend de La Rubrique Spectacle, sport '),(3055,1085,'depend_de',41,77,1,873,'Le Terme gymnase depend de La Rubrique Spectacle, sport '),(3056,1185,'depend_de',41,77,1,1725,'Le Terme jeu de mail depend de La Rubrique Spectacle, sport '),(3057,1290,'depend_de',41,77,1,1038,'Le Terme manège depend de La Rubrique Spectacle, sport '),(3058,1781,'depend_de',41,77,1,1726,'Le Terme stade depend de La Rubrique Spectacle, sport '),(3059,1849,'depend_de',41,77,1,1727,'Le Terme théâtre-amphithéâtre depend de La Rubrique Spectacle, sport '),(3063,1125,'depend_de',41,78,1,900,'Le Terme hôtellerie depend de La Rubrique Bains '),(3068,390,'depend_de',41,79,1,1733,'Le Terme cargaison depend de La Rubrique Commerce, échanges '),(3070,725,'depend_de',41,79,1,560,'Le Terme dock depend de La Rubrique Commerce, échanges '),(3071,1111,'depend_de',41,79,1,1734,'Le Terme horreum depend de La Rubrique Commerce, échanges '),(3076,1947,'depend_de',41,79,1,1590,'Le Terme vivier depend de La Rubrique Commerce, échanges '),(3077,123,'depend_de',41,80,1,1738,'Le Terme aire de mouture depend de La Rubrique Habitat privé '),(3079,228,'depend_de',41,80,1,1739,'Le Terme autel domestique depend de La Rubrique Habitat privé '),(3084,946,'depend_de',41,80,1,1743,'Le Terme fosse à dolium depend de La Rubrique Habitat privé '),(3093,1443,'depend_de',41,80,1,1751,'Le Terme pars urbana depend de La Rubrique Habitat privé '),(3097,1524,'depend_de',41,80,1,1234,'Le Terme porche depend de La Rubrique Habitat privé '),(3098,1524,'depend_de',41,82,1,1234,'Le Terme porche depend de La Rubrique Edifices cultuels catholiques '),(3101,1556,'depend_de',41,80,1,1264,'Le Terme pressoir depend de La Rubrique Habitat privé '),(3102,1564,'depend_de',41,80,1,1754,'Le Terme production domestique depend de La Rubrique Habitat privé '),(3108,1693,'depend_de',41,80,1,1374,'Le Terme salle souterraine depend de La Rubrique Habitat privé '),(3114,228,'depend_de',41,81,1,1739,'Le Terme autel domestique depend de La Rubrique Cultes païens '),(3115,120,'depend_de',41,81,1,25,'Le Terme aire de crémation depend de La Rubrique Cultes païens '),(3116,270,'depend_de',41,81,1,1762,'Le Terme bassin cultuel depend de La Rubrique Cultes païens '),(3118,419,'depend_de',41,81,1,1764,'Le Terme cavité à offrandes depend de La Rubrique Cultes païens '),(3121,1327,'depend_de',41,81,1,1765,'Le Terme mithraeum depend de La Rubrique Cultes païens '),(3124,1698,'depend_de',41,81,1,1767,'Le Terme sanctuaire de carrefour depend de La Rubrique Cultes païens '),(3127,602,'depend_de',41,82,1,446,'Le Terme cour depend de La Rubrique Edifices cultuels catholiques '),(3129,928,'depend_de',41,82,1,1769,'Le Terme fonts baptismaux depend de La Rubrique Edifices cultuels catholiques '),(3132,991,'depend_de',41,82,1,790,'Le Terme four à cloche depend de La Rubrique Edifices cultuels catholiques '),(3133,1371,'depend_de',41,82,1,1772,'Le Terme narthex depend de La Rubrique Edifices cultuels catholiques '),(3135,313,'depend_de',41,83,1,1774,'Le Terme béguinage depend de La Rubrique Bâtiments conventuels ou monastiques '),(3140,615,'depend_de',41,83,1,1777,'Le Terme couvent mendiant depend de La Rubrique Bâtiments conventuels ou monastiques '),(3144,1091,'depend_de',41,83,1,1779,'Le Terme habitat religieux depend de La Rubrique Bâtiments conventuels ou monastiques '),(3155,1647,'depend_de',41,83,1,1333,'Le Terme résidence depend de La Rubrique Bâtiments conventuels ou monastiques '),(3156,1693,'depend_de',41,83,1,1374,'Le Terme salle souterraine depend de La Rubrique Bâtiments conventuels ou monastiques '),(3161,901,'depend_de',41,84,1,1789,'Le Terme fabrique d\'église depend de La Rubrique Bâtiments écclésiastiques '),(3162,1066,'depend_de',41,84,1,1790,'Le Terme grange aux dîmes depend de La Rubrique Bâtiments écclésiastiques '),(3165,1570,'depend_de',41,84,1,1272,'Le Terme puisard depend de La Rubrique Bâtiments écclésiastiques '),(3167,1651,'depend_de',41,83,1,1792,'Le Terme résidence pontificale depend de La Rubrique Bâtiments conventuels ou monastiques '),(3168,1693,'depend_de',41,84,1,1374,'Le Terme salle souterraine depend de La Rubrique Bâtiments écclésiastiques '),(3171,1062,'depend_de',41,85,1,1794,'Le Terme gleza depend de La Rubrique Cultes autres que catholique '),(3172,1340,'depend_de',41,85,1,1795,'Le Terme mosquée depend de La Rubrique Cultes autres que catholique '),(3173,229,'depend_de',41,86,1,1796,'Le Terme autel funéraire depend de La Rubrique Funéraire '),(3176,441,'depend_de',41,86,1,1799,'Le Terme chambre funéraire depend de La Rubrique Funéraire '),(3182,620,'depend_de',41,86,1,1615,'Le Terme croix depend de La Rubrique Funéraire '),(3183,767,'depend_de',41,86,1,597,'Le Terme église cémetériale depend de La Rubrique Funéraire '),(3184,973,'depend_de',41,86,1,1804,'Le Terme fosse funéraire depend de La Rubrique Funéraire '),(3188,1154,'depend_de',41,86,1,1808,'Le Terme inhumation assise depend de La Rubrique Funéraire '),(3194,1230,'depend_de',41,86,1,1814,'Le Terme lit funéraire depend de La Rubrique Funéraire '),(3195,1317,'depend_de',41,86,1,1815,'Le Terme mensa depend de La Rubrique Funéraire '),(3197,1375,'depend_de',41,86,1,1816,'Le Terme nécropole d\'animaux depend de La Rubrique Funéraire '),(3202,1753,'depend_de',41,86,1,1821,'Le Terme sépulture réutilisée depend de La Rubrique Funéraire '),(3203,1755,'depend_de',41,86,1,1822,'Le Terme sépulture secondaire depend de La Rubrique Funéraire '),(3204,1856,'depend_de',41,86,1,1823,'Le Terme tombe à char depend de La Rubrique Funéraire '),(3210,100,'depend_de',41,87,1,1829,'Le Terme abri de chantier depend de La Rubrique Artisanat '),(3222,243,'depend_de',41,87,1,129,'Le Terme bac à pigment depend de La Rubrique Artisanat '),(3224,583,'depend_de',41,87,1,1841,'Le Terme conserverie depend de La Rubrique Artisanat '),(3225,585,'depend_de',41,87,1,1842,'Le Terme construction navale depend de La Rubrique Artisanat '),(3226,598,'depend_de',41,87,1,1843,'Le Terme corroyeur depend de La Rubrique Artisanat '),(3227,737,'depend_de',41,87,1,1844,'Le Terme draperie depend de La Rubrique Artisanat '),(3229,783,'depend_de',41,87,1,1845,'Le Terme émailleur depend de La Rubrique Artisanat '),(3230,899,'depend_de',41,87,1,1846,'Le Terme fabrication du garum depend de La Rubrique Artisanat '),(3232,996,'depend_de',41,87,1,1848,'Le Terme four à plomb depend de La Rubrique Artisanat '),(3233,1017,'depend_de',41,87,1,1849,'Le Terme four en batterie depend de La Rubrique Artisanat '),(3235,1315,'depend_de',41,87,1,1851,'Le Terme mélangeur à mortier depend de La Rubrique Artisanat '),(3236,1352,'depend_de',41,87,1,1635,'Le Terme moulin à marée depend de La Rubrique Artisanat '),(3238,1441,'depend_de',41,87,1,1853,'Le Terme parcheminier depend de La Rubrique Artisanat '),(3239,1565,'depend_de',41,87,1,1854,'Le Terme production salicole depend de La Rubrique Artisanat '),(3242,1662,'depend_de',41,87,1,1856,'Le Terme rouissage depend de La Rubrique Artisanat '),(3243,1675,'depend_de',41,87,1,1857,'Le Terme salaison depend de La Rubrique Artisanat '),(3244,1793,'depend_de',41,87,1,1858,'Le Terme structure de cuisson depend de La Rubrique Artisanat '),(3245,1875,'depend_de',41,87,1,1859,'Le Terme tonnellerie depend de La Rubrique Artisanat '),(3247,1927,'depend_de',41,87,1,1861,'Le Terme vase de foulon depend de La Rubrique Artisanat '),(3248,1946,'depend_de',41,87,1,1862,'Le Terme vitrail depend de La Rubrique Artisanat '),(3250,96,'depend_de',41,88,1,5,'Le Terme abreuvoir depend de La Rubrique Agriculture, élevage '),(3251,113,'depend_de',41,88,1,1864,'Le Terme aire boisée depend de La Rubrique Agriculture, élevage '),(3255,907,'depend_de',41,88,1,1867,'Le Terme fenil depend de La Rubrique Agriculture, élevage '),(3256,919,'depend_de',41,88,1,725,'Le Terme fond de cabane depend de La Rubrique Agriculture, élevage '),(3258,983,'depend_de',41,88,1,1869,'Le Terme fouloir depend de La Rubrique Agriculture, élevage '),(3260,1051,'depend_de',41,88,1,1871,'Le Terme garenne depend de La Rubrique Agriculture, élevage '),(3261,1098,'depend_de',41,88,1,881,'Le Terme haie depend de La Rubrique Agriculture, élevage '),(3262,1128,'depend_de',41,88,1,1872,'Le Terme houblonnerie depend de La Rubrique Agriculture, élevage '),(3264,1197,'depend_de',41,88,1,1873,'Le Terme landes depend de La Rubrique Agriculture, élevage '),(3266,1395,'depend_de',41,88,1,1874,'Le Terme oliveraie depend de La Rubrique Agriculture, élevage '),(3267,1404,'depend_de',41,88,1,1875,'Le Terme oseraie depend de La Rubrique Agriculture, élevage '),(3268,1433,'depend_de',41,88,1,1876,'Le Terme palmeraie depend de La Rubrique Agriculture, élevage '),(3270,1556,'depend_de',41,88,1,1264,'Le Terme pressoir depend de La Rubrique Agriculture, élevage '),(3272,1790,'depend_de',41,88,1,1879,'Le Terme structure agricole depend de La Rubrique Agriculture, élevage '),(3276,1294,'depend_de',41,89,1,1883,'Le Terme manufacture de porcelaine depend de La Rubrique Industrie '),(3277,1545,'depend_de',41,89,1,1884,'Le Terme poudrerie depend de La Rubrique Industrie '),(3278,1602,'depend_de',41,89,1,1885,'Le Terme raffinerie depend de La Rubrique Industrie '),(3279,1887,'depend_de',41,89,1,1542,'Le Terme traitement de l\'argile depend de La Rubrique Industrie '),(3281,175,'depend_de',41,90,1,1886,'Le Terme ardoisière depend de La Rubrique Extraction '),(3283,1676,'depend_de',41,90,1,1359,'Le Terme saline depend de La Rubrique Extraction '),(3297,113,'depend_de',41,91,1,1864,'Le Terme aire boisée depend de La Rubrique Formations naturelles '),(3302,858,'depend_de',41,91,1,682,'Le Terme espace vert depend de La Rubrique Formations naturelles si espace non aménagé'),(3303,916,'depend_de',41,91,1,1904,'Le Terme fleuve depend de La Rubrique Formations naturelles '),(3304,1134,'depend_de',41,91,1,1905,'Le Terme île depend de La Rubrique Formations naturelles '),(3306,1193,'depend_de',41,91,1,1907,'Le Terme lagune depend de La Rubrique Formations naturelles '),(3308,1303,'depend_de',41,91,1,1909,'Le Terme marais littoral depend de La Rubrique Formations naturelles '),(3309,1314,'depend_de',41,91,1,1910,'Le Terme méandre depend de La Rubrique Formations naturelles '),(3310,1370,'depend_de',41,91,1,1911,'Le Terme nappe phréatique depend de La Rubrique Formations naturelles '),(3312,1566,'depend_de',41,91,1,1913,'Le Terme progradation depend de La Rubrique Formations naturelles '),(3316,1833,'depend_de',41,91,1,1915,'Le Terme terrasse naturelle depend de La Rubrique Formations naturelles '),(3317,1885,'depend_de',41,91,1,1916,'Le Terme tourbière depend de La Rubrique Formations naturelles '),(3318,1893,'depend_de',41,91,1,1917,'Le Terme transgression depend de La Rubrique Formations naturelles '),(3324,94,'secondaire_de',42,611,1,3,'Le Terme abbaye Terme secondaire de le Terme couvent'),(3325,95,'secondaire_de',42,94,1,4,'Le Terme abbaye fortifiée Terme secondaire de le Terme abbaye'),(3326,106,'remplacer_par',43,107,1,14,'Le Terme adduction Terme à remplacer par le Terme adduction d\'eau'),(3327,114,'remplacer_par',43,1213,1,20,'Le Terme aire cultuelle Terme à remplacer par le Terme lieu de culte'),(3328,125,'remplacer_par',43,1787,1,29,'Le Terme aire de stockage Terme à remplacer par le Terme stockage'),(3329,115,'remplacer_par',43,1766,1,31,'Le Terme aire d\'ensilage Terme à remplacer par le Terme silo'),(3330,129,'en_instance',44,986,1,33,'Le Terme alandier Terme en instance le Terme four'),(3331,132,'remplacer_par',43,1665,1,36,'Le Terme allée Terme à remplacer par le Terme rue'),(3332,137,'remplacer_par',43,1445,1,41,'Le Terme ambitus Terme à remplacer par le Terme passage'),(3333,138,'remplacer_par',43,145,1,42,'Le Terme aménagement bas de pente Terme à remplacer par le Terme aménagement de relief'),(3334,143,'remplacer_par',43,1302,1,46,'Le Terme aménagement de marais Terme à remplacer par le Terme marais asséché'),(3335,147,'remplacer_par',43,1526,1,50,'Le Terme aménagement portuaire Terme à remplacer par le Terme port'),(3336,149,'secondaire_de',42,755,1,52,'Le Terme amphithéâtre Terme secondaire de le Terme édifice de spectacle'),(3337,151,'remplacer_par',43,675,1,54,'Le Terme annexe Terme à remplacer par le Terme dépendance'),(3338,157,'remplacer_par',43,1384,1,60,'Le Terme aplanissement Terme à remplacer par le Terme nivellement'),(3339,160,'en_instance',44,675,1,65,'Le Terme appentis Terme en instance le Terme dépendance'),(3340,170,'remplacer_par',43,1774,1,75,'Le Terme arche de soutènement Terme à remplacer par le Terme soutènement'),(3341,180,'remplacer_par',43,602,1,81,'Le Terme arrière-cour Terme à remplacer par le Terme cour'),(3342,181,'remplacer_par',43,939,1,82,'Le Terme arrière-fossé Terme à remplacer par le Terme fossé'),(3343,191,'en_instance',44,290,1,88,'Le Terme artisanat du bâtiment Terme en instance le Terme bâtiment'),(3344,200,'remplacer_par',43,1302,1,91,'Le Terme assèchement Terme à remplacer par le Terme marais asséché'),(3345,210,'secondaire_de',42,427,1,100,'Le Terme atelier de potier Terme secondaire de le Terme céramique'),(3346,215,'secondaire_de',42,1905,1,105,'Le Terme atelier de tuilier Terme secondaire de le Terme tuilerie'),(3347,220,'remplacer_par',43,1023,1,109,'Le Terme atre Terme à remplacer par le Terme foyer'),(3348,222,'secondaire_de',42,268,1,111,'Le Terme atrium Terme secondaire de le Terme bassin'),(3349,227,'remplacer_par',43,1334,1,116,'Le Terme autel Terme à remplacer par le Terme monument commémoratif'),(3350,233,'remplacer_par',43,1410,1,119,'Le Terme avant-mur Terme à remplacer par le Terme ouvrage avancé'),(3351,235,'remplacer_par',43,1526,1,121,'Le Terme avant-port Terme à remplacer par le Terme port'),(3352,236,'remplacer_par',43,1665,1,122,'Le Terme avenue Terme à remplacer par le Terme rue'),(3353,244,'remplacer_par',43,1887,1,130,'Le Terme bac de traitement de l\'argile Terme à remplacer par le Terme traitement de l\'argile'),(3354,250,'remplacer_par',43,248,1,136,'Le Terme bains publics Terme à remplacer par le Terme bains'),(3355,252,'remplacer_par',43,1850,1,138,'Le Terme balnéaire Terme à remplacer par le Terme thermes'),(3356,253,'remplacer_par',43,1850,1,139,'Le Terme balneum Terme à remplacer par le Terme thermes'),(3357,262,'remplacer_par',43,261,1,148,'Le Terme bas-foyer Terme à remplacer par le Terme bas-fourneau'),(3358,271,'remplacer_par',43,268,1,156,'Le Terme bassin d\'agrément Terme à remplacer par le Terme bassin'),(3359,276,'remplacer_par',43,736,1,159,'Le Terme bassin de drainage Terme à remplacer par le Terme drainage'),(3360,277,'remplacer_par',43,984,1,160,'Le Terme bassin de foulon Terme à remplacer par le Terme foulon'),(3361,279,'remplacer_par',43,1787,1,161,'Le Terme bassin de stockage Terme à remplacer par le Terme stockage'),(3362,282,'remplacer_par',43,222,1,163,'Le Terme bassin impluvium Terme à remplacer par le Terme atrium'),(3363,286,'remplacer_par',43,284,1,166,'Le Terme bastionnage Terme à remplacer par le Terme bastion'),(3364,288,'remplacer_par',43,833,1,168,'Le Terme bateau Terme à remplacer par le Terme épave'),(3365,290,'remplacer_par',43,1087,1,170,'Le Terme bâtiment Terme à remplacer par le Terme habitat'),(3366,298,'remplacer_par',43,611,1,178,'Le Terme bâtiment conventuel Terme à remplacer par le Terme couvent'),(3367,301,'remplacer_par',43,571,1,179,'Le Terme bâtiment de service Terme à remplacer par le Terme communs'),(3368,307,'remplacer_par',43,611,1,184,'Le Terme bâtiment monastique Terme à remplacer par le Terme couvent'),(3369,309,'remplacer_par',43,761,1,186,'Le Terme bâtiment public Terme à remplacer par le Terme édifice public'),(3370,311,'secondaire_de',42,1502,1,187,'Le Terme batterie Terme secondaire de le Terme plate-forme'),(3371,314,'remplacer_par',43,1659,1,189,'Le Terme berge Terme à remplacer par le Terme rive'),(3372,315,'remplacer_par',43,141,1,190,'Le Terme berge aménagée Terme à remplacer par le Terme aménagement de berge'),(3373,327,'remplacer_par',43,1325,1,1618,'Le Terme borne milliaire Terme à remplacer par le Terme milliaire'),(3374,329,'remplacer_par',43,925,1,201,'Le Terme borne-fontaine Terme à remplacer par le Terme fontaine'),(3375,354,'remplacer_par',43,919,1,225,'Le Terme cabane enterrée Terme à remplacer par le Terme fond de cabane'),(3376,367,'remplacer_par',43,366,1,236,'Le Terme cale de halage Terme à remplacer par le Terme cale'),(3377,368,'secondaire_de',42,620,1,237,'Le Terme calvaire Terme secondaire de le Terme croix'),(3378,373,'remplacer_par',43,778,1,243,'Le Terme canal d\'évacuation Terme à remplacer par le Terme égout'),(3379,388,'remplacer_par',43,387,1,252,'Le Terme captage de source Terme à remplacer par le Terme captage'),(3380,389,'secondaire_de',42,1665,1,253,'Le Terme cardo Terme secondaire de le Terme rue'),(3381,392,'remplacer_par',43,1665,1,255,'Le Terme carrefour Terme à remplacer par le Terme rue'),(3382,399,'remplacer_par',43,398,1,261,'Le Terme caserne de pompiers Terme à remplacer par le Terme caserne'),(3383,403,'remplacer_par',43,789,1,263,'Le Terme castrum Terme à remplacer par le Terme enceinte'),(3384,414,'remplacer_par',43,413,1,271,'Le Terme caveau collectif Terme à remplacer par le Terme caveau'),(3385,416,'remplacer_par',43,413,1,273,'Le Terme caveau maçonné Terme à remplacer par le Terme caveau'),(3386,417,'remplacer_par',43,413,1,274,'Le Terme caveau-ossuaire Terme à remplacer par le Terme caveau'),(3387,421,'secondaire_de',42,1787,1,280,'Le Terme cellier Terme secondaire de le Terme stockage'),(3388,424,'en_instance',44,986,1,283,'Le Terme cendrier Terme en instance le Terme four'),(3389,433,'secondaire_de',42,431,1,291,'Le Terme cercueil de bois Terme secondaire de le Terme cercueil'),(3390,434,'secondaire_de',42,431,1,292,'Le Terme cercueil de plomb Terme secondaire de le Terme cercueil'),(3391,448,'remplacer_par',43,446,1,303,'Le Terme champ de tir Terme à remplacer par le Terme champ de manoeuvre'),(3392,456,'remplacer_par',43,772,1,309,'Le Terme chapelle cémétériale Terme à remplacer par le Terme église funéraire'),(3393,469,'remplacer_par',43,471,1,321,'Le Terme château Terme à remplacer par le Terme château fort'),(3394,477,'secondaire_de',42,1665,1,330,'Le Terme chaussée Terme secondaire de le Terme rue'),(3395,479,'secondaire_de',42,1948,1,332,'Le Terme chemin Terme secondaire de le Terme voie'),(3396,485,'remplacer_par',43,383,1,339,'Le Terme chenal d\'écoulement Terme à remplacer par le Terme caniveau'),(3397,491,'remplacer_par',43,501,1,344,'Le Terme cimetière abbatial Terme à remplacer par le Terme cimetière de communauté religieuse'),(3398,492,'remplacer_par',43,501,1,345,'Le Terme cimetière canonial Terme à remplacer par le Terme cimetière de communauté religieuse'),(3399,494,'remplacer_par',43,490,1,347,'Le Terme cimetière communal Terme à remplacer par le Terme cimetière'),(3400,495,'remplacer_par',43,501,1,348,'Le Terme cimetière conventuel Terme à remplacer par le Terme cimetière de communauté religieuse'),(3401,499,'remplacer_par',43,501,1,349,'Le Terme cimetière de communauté Terme à remplacer par le Terme cimetière de communauté religieuse'),(3402,498,'remplacer_par',43,497,1,355,'Le Terme cimetière d\'hôpital Terme à remplacer par le Terme cimetière d\'établissement hospitalier'),(3403,508,'remplacer_par',43,501,1,359,'Le Terme cimetière monastique Terme à remplacer par le Terme cimetière de communauté religieuse'),(3404,510,'remplacer_par',43,501,1,362,'Le Terme cimetière prieural Terme à remplacer par le Terme cimetière de communauté religieuse'),(3405,524,'remplacer_par',43,523,1,376,'Le Terme cloître à galerie Terme à remplacer par le Terme cloître'),(3406,527,'remplacer_par',43,526,1,379,'Le Terme cloître cathédral Terme à remplacer par le Terme cloître canonial'),(3407,528,'remplacer_par',43,526,1,380,'Le Terme cloître épiscopal Terme à remplacer par le Terme cloître canonial'),(3408,530,'remplacer_par',43,534,1,382,'Le Terme coffrage Terme à remplacer par le Terme coffre'),(3409,531,'remplacer_par',43,536,1,383,'Le Terme coffrage de bois Terme à remplacer par le Terme coffre de bois'),(3410,532,'en_instance',44,540,1,385,'Le Terme coffrage de pierres Terme en instance le Terme coffre de pierres'),(3411,537,'remplacer_par',43,534,1,390,'Le Terme coffre de briques Terme à remplacer par le Terme coffre'),(3412,542,'remplacer_par',43,534,1,395,'Le Terme coffre de tegulae Terme à remplacer par le Terme coffre'),(3413,543,'en_instance',44,534,1,397,'Le Terme coffre de tuiles Terme en instance le Terme coffre'),(3414,548,'remplacer_par',43,534,1,402,'Le Terme coffre funéraire Terme à remplacer par le Terme coffre'),(3415,549,'secondaire_de',42,534,1,403,'Le Terme coffre maçonné Terme secondaire de le Terme coffre'),(3416,553,'remplacer_par',43,552,1,406,'Le Terme collecteur-égout Terme à remplacer par le Terme collecteur'),(3417,558,'remplacer_par',43,1476,1,411,'Le Terme colombier Terme à remplacer par le Terme pigeonnier'),(3418,559,'remplacer_par',43,1533,1,412,'Le Terme colonnade Terme à remplacer par le Terme portique'),(3419,569,'secondaire_de',42,611,1,422,'Le Terme commanderie Terme secondaire de le Terme couvent'),(3420,573,'remplacer_par',43,379,1,426,'Le Terme conduit Terme à remplacer par le Terme canalisation'),(3421,578,'remplacer_par',43,379,1,428,'Le Terme conduite Terme à remplacer par le Terme canalisation'),(3422,579,'remplacer_par',43,1846,1,429,'Le Terme confection Terme à remplacer par le Terme textile'),(3423,584,'remplacer_par',43,1087,1,433,'Le Terme construction Terme à remplacer par le Terme habitat'),(3424,595,'remplacer_par',43,1528,1,441,'Le Terme corps de garde Terme à remplacer par le Terme porte'),(3425,601,'remplacer_par',43,1776,1,445,'Le Terme couloir souterrain Terme à remplacer par le Terme souterrain'),(3426,608,'remplacer_par',43,607,1,452,'Le Terme cours d\'eau fossile Terme à remplacer par le Terme cours d\'eau'),(3427,620,'secondaire_de',42,1334,1,1615,'Le Terme croix Terme secondaire de le Terme monument commémoratif'),(3428,630,'remplacer_par',43,844,1,469,'Le Terme culture Terme à remplacer par le Terme espace agricole'),(3429,633,'remplacer_par',43,1554,1,471,'Le Terme cure Terme à remplacer par le Terme presbytère'),(3430,635,'remplacer_par',43,517,1,473,'Le Terme cuve Terme à remplacer par le Terme citerne'),(3431,645,'secondaire_de',42,1813,1,482,'Le Terme cuve de tanneur Terme secondaire de le Terme tannerie'),(3432,647,'remplacer_par',43,1704,1,484,'Le Terme cuve monolithe Terme à remplacer par le Terme sarcophage'),(3433,649,'remplacer_par',43,651,1,486,'Le Terme dalle Terme à remplacer par le Terme dalle funéraire'),(3434,650,'remplacer_par',43,651,1,487,'Le Terme dalle épigraphe Terme à remplacer par le Terme dalle funéraire'),(3435,652,'remplacer_par',43,651,1,489,'Le Terme dalle gravée Terme à remplacer par le Terme dalle funéraire'),(3436,653,'remplacer_par',43,379,1,490,'Le Terme dalot Terme à remplacer par le Terme canalisation'),(3437,656,'remplacer_par',43,161,1,492,'Le Terme débarcadère Terme à remplacer par le Terme appontement'),(3438,663,'remplacer_par',43,662,1,499,'Le Terme décharge organisée Terme à remplacer par le Terme décharge'),(3439,664,'remplacer_par',43,662,1,1625,'Le Terme décharge publique Terme à remplacer par le Terme décharge'),(3440,668,'secondaire_de',42,1665,1,503,'Le Terme decumanus Terme secondaire de le Terme rue'),(3441,672,'remplacer_par',43,1092,1,508,'Le Terme demeure Terme à remplacer par le Terme habitation'),(3442,678,'remplacer_par',43,675,1,514,'Le Terme dépense Terme à remplacer par le Terme dépendance'),(3443,679,'remplacer_par',43,827,1,515,'Le Terme dépôt Terme à remplacer par le Terme entrepôt'),(3444,716,'remplacer_par',43,662,1,1626,'Le Terme dépotoir municipal Terme à remplacer par le Terme décharge'),(3445,725,'secondaire_de',42,827,1,560,'Le Terme dock Terme secondaire de le Terme entrepôt'),(3446,726,'remplacer_par',43,844,1,562,'Le Terme domaine agricole Terme à remplacer par le Terme espace agricole'),(3447,727,'secondaire_de',42,1092,1,563,'Le Terme domus Terme secondaire de le Terme habitation'),(3448,732,'remplacer_par',43,939,1,566,'Le Terme douve Terme à remplacer par le Terme fossé'),(3449,733,'remplacer_par',43,939,1,567,'Le Terme douves Terme à remplacer par le Terme fossé'),(3450,744,'remplacer_par',43,338,1,577,'Le Terme échoppe Terme à remplacer par le Terme boutique'),(3451,750,'remplacer_par',43,1087,1,583,'Le Terme édifice Terme à remplacer par le Terme habitat'),(3452,754,'remplacer_par',43,751,1,586,'Le Terme édifice de culte Terme à remplacer par le Terme édifice cultuel'),(3453,760,'remplacer_par',43,1087,1,591,'Le Terme édifice privé Terme à remplacer par le Terme habitat'),(3454,764,'remplacer_par',43,93,1,594,'Le Terme église abbatiale Terme à remplacer par le Terme abbatiale'),(3455,766,'remplacer_par',43,404,1,596,'Le Terme église cathédrale Terme à remplacer par le Terme cathédrale'),(3456,768,'remplacer_par',43,555,1,598,'Le Terme église collégiale Terme à remplacer par le Terme collégiale'),(3457,769,'secondaire_de',42,763,1,599,'Le Terme église conventuelle Terme secondaire de le Terme église'),(3458,775,'secondaire_de',42,763,1,605,'Le Terme église prieurale Terme secondaire de le Terme église'),(3459,784,'remplacer_par',43,161,1,612,'Le Terme embarcadère Terme à remplacer par le Terme appontement'),(3460,793,'remplacer_par',43,789,1,621,'Le Terme enceinte bastionnée Terme à remplacer par le Terme enceinte'),(3461,796,'remplacer_par',43,797,1,624,'Le Terme enceinte claustrale Terme à remplacer par le Terme enceinte conventuelle'),(3462,802,'remplacer_par',43,798,1,630,'Le Terme enceinte fossoyée Terme à remplacer par le Terme enceinte cultuelle'),(3463,803,'remplacer_par',43,797,1,631,'Le Terme enceinte monastique Terme à remplacer par le Terme enceinte conventuelle'),(3464,807,'remplacer_par',43,1437,1,635,'Le Terme enclos à bestiaux Terme à remplacer par le Terme parc à bétail'),(3465,814,'remplacer_par',43,720,1,641,'Le Terme endiguement Terme à remplacer par le Terme digue'),(3466,825,'remplacer_par',43,1850,1,650,'Le Terme ensemble thermal Terme à remplacer par le Terme thermes'),(3467,835,'en_instance',44,1076,1,660,'Le Terme épier Terme en instance le Terme grenier'),(3468,853,'remplacer_par',43,1972,1,678,'Le Terme espace marécageux Terme à remplacer par le Terme zone marécageuse'),(3469,854,'remplacer_par',43,852,1,679,'Le Terme espace non bâti Terme à remplacer par le Terme espace libre'),(3470,860,'secondaire_de',42,1486,1,684,'Le Terme esplanade Terme secondaire de le Terme place'),(3471,865,'remplacer_par',43,1850,1,688,'Le Terme établissement balnéaire Terme à remplacer par le Terme thermes'),(3472,871,'remplacer_par',43,872,1,693,'Le Terme étuve Terme à remplacer par le Terme étuves'),(3473,902,'remplacer_par',43,903,1,710,'Le Terme faïence Terme à remplacer par le Terme faïencerie'),(3474,906,'remplacer_par',43,335,1,715,'Le Terme fausse-braie Terme à remplacer par le Terme boulevard'),(3475,912,'remplacer_par',43,210,1,720,'Le Terme figlina Terme à remplacer par le Terme atelier de potier'),(3476,917,'remplacer_par',43,445,1,723,'Le Terme foirail Terme à remplacer par le Terme champ de foire'),(3477,918,'remplacer_par',43,445,1,724,'Le Terme foire Terme à remplacer par le Terme champ de foire'),(3478,926,'remplacer_par',43,925,1,733,'Le Terme fontaine publique Terme à remplacer par le Terme fontaine'),(3479,934,'remplacer_par',43,789,1,740,'Le Terme fortification Terme à remplacer par le Terme enceinte'),(3480,935,'remplacer_par',43,284,1,741,'Le Terme fortification bastionnée Terme à remplacer par le Terme bastion'),(3481,937,'secondaire_de',42,1486,1,743,'Le Terme forum Terme secondaire de le Terme place'),(3482,938,'remplacer_par',43,1728,1,744,'Le Terme fosse Terme à remplacer par le Terme sépulture'),(3483,940,'remplacer_par',43,383,1,1623,'Le Terme fossé (de rue ou de voie) Terme à remplacer par le Terme caniveau'),(3484,941,'remplacer_par',43,150,1,746,'Le Terme fosse à amphores Terme à remplacer par le Terme amphore'),(3485,949,'remplacer_par',43,1143,1,753,'Le Terme fosse à incinération Terme à remplacer par le Terme incinération'),(3486,952,'remplacer_par',43,1728,1,757,'Le Terme fosse anthropomorphe Terme à remplacer par le Terme sépulture'),(3487,955,'remplacer_par',43,552,1,759,'Le Terme fossé collecteur Terme à remplacer par le Terme collecteur'),(3488,958,'remplacer_par',43,1200,1,762,'Le Terme fosse d\'aisance Terme à remplacer par le Terme latrines'),(3489,967,'remplacer_par',43,736,1,764,'Le Terme fossé de drainage Terme à remplacer par le Terme drainage'),(3490,972,'remplacer_par',43,690,1,769,'Le Terme fosse dépotoir Terme à remplacer par le Terme dépotoir'),(3491,961,'remplacer_par',43,383,1,771,'Le Terme fossé d\'évacuation Terme à remplacer par le Terme caniveau'),(3492,975,'secondaire_de',42,1222,1,775,'Le Terme fossé parcellaire Terme secondaire de le Terme limite parcellaire'),(3493,976,'remplacer_par',43,957,1,776,'Le Terme fosse rituelle Terme à remplacer par le Terme fosse cultuelle'),(3494,977,'remplacer_par',43,939,1,777,'Le Terme fossé sec Terme à remplacer par le Terme fossé'),(3495,982,'remplacer_par',43,1766,1,782,'Le Terme fosse-silo Terme à remplacer par le Terme silo'),(3496,986,'remplacer_par',43,1016,1,785,'Le Terme four Terme à remplacer par le Terme four domestique'),(3497,989,'secondaire_de',42,427,1,788,'Le Terme four à céramique Terme secondaire de le Terme céramique'),(3498,1009,'secondaire_de',42,427,1,807,'Le Terme four de potier Terme secondaire de le Terme céramique'),(3499,1012,'secondaire_de',42,1905,1,810,'Le Terme four de tuilier Terme secondaire de le Terme tuilerie'),(3500,1019,'en_instance',44,986,1,816,'Le Terme fourneau Terme en instance le Terme four'),(3501,1021,'remplacer_par',43,1007,1,818,'Le Terme fournette Terme à remplacer par le Terme four de faïencier'),(3502,1025,'remplacer_par',43,1023,1,822,'Le Terme foyer domestique Terme à remplacer par le Terme foyer'),(3503,1026,'remplacer_par',43,483,1,823,'Le Terme foyer-cheminée Terme à remplacer par le Terme cheminée'),(3504,1033,'remplacer_par',43,1032,1,826,'Le Terme friche urbaine Terme à remplacer par le Terme friche'),(3505,1037,'remplacer_par',43,882,1,830,'Le Terme front de taille Terme à remplacer par le Terme extraction'),(3506,1039,'remplacer_par',43,1533,1,832,'Le Terme galerie Terme à remplacer par le Terme portique'),(3507,1048,'remplacer_par',43,1787,1,841,'Le Terme garde-manger Terme à remplacer par le Terme stockage'),(3508,1055,'remplacer_par',43,398,1,846,'Le Terme gendarmerie Terme à remplacer par le Terme caserne'),(3509,1076,'remplacer_par',43,827,1,863,'Le Terme grenier Terme à remplacer par le Terme entrepôt'),(3510,1081,'remplacer_par',43,1082,1,868,'Le Terme groupe cathédral Terme à remplacer par le Terme groupe épiscopal'),(3511,1084,'remplacer_par',43,1542,1,872,'Le Terme guichet Terme à remplacer par le Terme poterne'),(3512,1102,'remplacer_par',43,1257,1,885,'Le Terme halle échevinale Terme à remplacer par le Terme maison communale'),(3513,1105,'remplacer_par',43,675,1,888,'Le Terme hangar Terme à remplacer par le Terme dépendance'),(3514,1114,'remplacer_par',43,1122,1,894,'Le Terme hôtel Terme à remplacer par le Terme hôtel particulier'),(3515,1117,'remplacer_par',43,1330,1,895,'Le Terme hôtel de la monnaie Terme à remplacer par le Terme monnaie'),(3516,1122,'secondaire_de',42,1092,1,897,'Le Terme hôtel particulier Terme secondaire de le Terme habitation'),(3517,1126,'remplacer_par',43,1125,1,901,'Le Terme hôtellerie monastique Terme à remplacer par le Terme hôtellerie'),(3518,1127,'remplacer_par',43,1128,1,902,'Le Terme houblon Terme à remplacer par le Terme houblonnerie'),(3519,1137,'remplacer_par',43,1135,1,913,'Le Terme Îlot urbain Terme à remplacer par le Terme îlot'),(3520,1140,'remplacer_par',43,1666,1,915,'Le Terme impasse Terme à remplacer par le Terme ruelle'),(3521,1150,'remplacer_par',43,305,1,923,'Le Terme industrie Terme à remplacer par le Terme bâtiment industriel'),(3522,1158,'remplacer_par',43,150,1,928,'Le Terme inhumation en amphore Terme à remplacer par le Terme amphore'),(3523,1159,'remplacer_par',43,1746,1,930,'Le Terme inhumation isolée Terme à remplacer par le Terme sépulture isolée'),(3524,1166,'remplacer_par',43,1167,1,934,'Le Terme inscription Terme à remplacer par le Terme inscription funéraire'),(3525,1172,'remplacer_par',43,305,1,940,'Le Terme installation industrielle Terme à remplacer par le Terme bâtiment industriel'),(3526,1184,'remplacer_par',43,720,1,951,'Le Terme jetée Terme à remplacer par le Terme digue'),(3527,1194,'remplacer_par',43,1846,1,959,'Le Terme laine Terme à remplacer par le Terme textile'),(3528,1195,'remplacer_par',43,1319,1,960,'Le Terme laiton Terme à remplacer par le Terme métal'),(3529,1202,'remplacer_par',43,1200,1,964,'Le Terme latrines publiques Terme à remplacer par le Terme latrines'),(3530,1205,'remplacer_par',43,1204,1,967,'Le Terme lavoir public Terme à remplacer par le Terme lavoir'),(3531,1208,'remplacer_par',43,720,1,970,'Le Terme levée Terme à remplacer par le Terme digue'),(3532,1209,'remplacer_par',43,141,1,971,'Le Terme levée de berge Terme à remplacer par le Terme aménagement de berge'),(3533,1219,'remplacer_par',43,1222,1,981,'Le Terme limite de parcelle Terme à remplacer par le Terme limite parcellaire'),(3534,1232,'remplacer_par',43,1658,1,993,'Le Terme littoral Terme à remplacer par le Terme rivage'),(3535,1237,'remplacer_par',43,1532,1,996,'Le Terme loge Terme à remplacer par le Terme porterie'),(3536,1248,'secondaire_de',42,1305,1,1006,'Le Terme macellum Terme secondaire de le Terme marché'),(3537,1250,'remplacer_par',43,827,1,1008,'Le Terme magasin Terme à remplacer par le Terme entrepôt'),(3538,1254,'remplacer_par',43,1092,1,1011,'Le Terme maison Terme à remplacer par le Terme habitation'),(3539,1271,'remplacer_par',43,1257,1,1024,'Le Terme maison de ville Terme à remplacer par le Terme maison communale'),(3540,1278,'remplacer_par',43,302,1,1028,'Le Terme maison du trésorier Terme à remplacer par le Terme bâtiment ecclésiastique'),(3541,1285,'secondaire_de',42,1092,1,1034,'Le Terme maison-tour Terme secondaire de le Terme habitation'),(3542,1291,'remplacer_par',43,1647,1,1039,'Le Terme manoir Terme à remplacer par le Terme résidence'),(3543,1293,'remplacer_par',43,903,1,1041,'Le Terme manufacture de faïence Terme à remplacer par le Terme faïencerie'),(3544,1299,'remplacer_par',43,1298,1,1045,'Le Terme maraîcher Terme à remplacer par le Terme maraîchage'),(3545,1311,'remplacer_par',43,1616,1,1056,'Le Terme matériau de construction Terme à remplacer par le Terme récupération de matériau'),(3546,1320,'remplacer_par',43,1319,1,1061,'Le Terme metallurgie Terme à remplacer par le Terme métal'),(3547,1325,'secondaire_de',42,326,1,1616,'Le Terme milliaire Terme secondaire de le Terme borne'),(3548,1328,'remplacer_par',43,720,1,1067,'Le Terme môle Terme à remplacer par le Terme digue'),(3549,1329,'remplacer_par',43,611,1,1068,'Le Terme monastère Terme à remplacer par le Terme couvent'),(3550,1331,'remplacer_par',43,1334,1,1069,'Le Terme monument Terme à remplacer par le Terme monument commémoratif'),(3551,1335,'remplacer_par',43,627,1,1073,'Le Terme monument des eaux Terme à remplacer par le Terme culte des eaux'),(3552,1338,'remplacer_par',43,1334,1,1619,'Le Terme monuments aux morts Terme à remplacer par le Terme monument commémoratif'),(3553,1345,'remplacer_par',43,991,1,1081,'Le Terme moule à cloche Terme à remplacer par le Terme four à cloche'),(3554,1346,'remplacer_par',43,1344,1,1082,'Le Terme moule à flans monétaire Terme à remplacer par le Terme moule'),(3555,1356,'remplacer_par',43,141,1,1090,'Le Terme mur de berge Terme à remplacer par le Terme aménagement de berge'),(3556,1357,'remplacer_par',43,797,1,1091,'Le Terme mur de clôture Terme à remplacer par le Terme enceinte conventuelle'),(3557,1359,'remplacer_par',43,529,1,1092,'Le Terme mur de jardin Terme à remplacer par le Terme clôture'),(3558,1360,'remplacer_par',43,141,1,1093,'Le Terme mur de soutènement Terme à remplacer par le Terme aménagement de berge'),(3559,1355,'remplacer_par',43,1217,1,1095,'Le Terme mur d\'enclos Terme à remplacer par le Terme limite de cimetière'),(3560,1364,'remplacer_par',43,789,1,1097,'Le Terme muraille Terme à remplacer par le Terme enceinte'),(3561,1372,'remplacer_par',43,1483,1,1101,'Le Terme natatio Terme à remplacer par le Terme piscine'),(3562,1374,'remplacer_par',43,501,1,1103,'Le Terme nécropole conventuelle Terme à remplacer par le Terme cimetière de communauté religieuse'),(3563,1377,'remplacer_par',43,497,1,1105,'Le Terme nécropole d\'hôpital Terme à remplacer par le Terme cimetière d\'établissement hospitalier'),(3564,1379,'remplacer_par',43,501,1,1106,'Le Terme nécropole monastique Terme à remplacer par le Terme cimetière de communauté religieuse'),(3565,1388,'remplacer_par',43,1087,1,1115,'Le Terme occupation Terme à remplacer par le Terme habitat'),(3566,1389,'remplacer_par',43,844,1,1116,'Le Terme occupation agricole Terme à remplacer par le Terme espace agricole'),(3567,1401,'secondaire_de',42,1665,1,1126,'Le Terme ornière Terme secondaire de le Terme rue'),(3568,1419,'remplacer_par',43,1647,1,1139,'Le Terme palais comtal Terme à remplacer par le Terme résidence'),(3569,1422,'remplacer_par',43,1257,1,1142,'Le Terme palais de la commune Terme à remplacer par le Terme maison communale'),(3570,1424,'remplacer_par',43,1647,1,1143,'Le Terme palais des comtes Terme à remplacer par le Terme résidence'),(3571,1425,'remplacer_par',43,1650,1,1144,'Le Terme palais épiscopal Terme à remplacer par le Terme résidence épiscopale'),(3572,1436,'remplacer_par',43,1177,1,1154,'Le Terme parc Terme à remplacer par le Terme jardin'),(3573,1442,'secondaire_de',42,1941,1,1159,'Le Terme pars rustica Terme secondaire de le Terme villa'),(3574,1448,'remplacer_par',43,1516,1,1164,'Le Terme passerelle Terme à remplacer par le Terme pont'),(3575,1451,'remplacer_par',43,602,1,1168,'Le Terme patio Terme à remplacer par le Terme cour'),(3576,1452,'remplacer_par',43,1551,1,1169,'Le Terme pâture Terme à remplacer par le Terme pré'),(3577,1468,'remplacer_par',43,287,1,1185,'Le Terme pessière Terme à remplacer par le Terme batardeau'),(3578,1470,'remplacer_par',43,1092,1,1186,'Le Terme pièce résidentielle Terme à remplacer par le Terme habitation'),(3579,1472,'remplacer_par',43,213,1,1188,'Le Terme pierre Terme à remplacer par le Terme atelier de taille de pierre'),(3580,1473,'remplacer_par',43,651,1,1189,'Le Terme pierre tombale Terme à remplacer par le Terme dalle funéraire'),(3581,1477,'remplacer_par',43,1516,1,1193,'Le Terme pile Terme à remplacer par le Terme pont'),(3582,1489,'remplacer_par',43,1486,1,1204,'Le Terme place publique Terme à remplacer par le Terme place'),(3583,1490,'secondaire_de',42,1486,1,1617,'Le Terme place royale Terme secondaire de le Terme place'),(3584,1491,'remplacer_par',43,1486,1,1620,'Le Terme placette Terme à remplacer par le Terme place'),(3585,1501,'remplacer_par',43,1023,1,1214,'Le Terme plaque-foyer Terme à remplacer par le Terme foyer'),(3586,1502,'secondaire_de',42,1830,1,1216,'Le Terme plate-forme Terme secondaire de le Terme terrasse'),(3587,1504,'remplacer_par',43,651,1,1217,'Le Terme plate-tombe Terme à remplacer par le Terme dalle funéraire'),(3588,1523,'remplacer_par',43,1294,1,1233,'Le Terme porcelaine Terme à remplacer par le Terme manufacture de porcelaine'),(3589,1531,'remplacer_par',43,1528,1,1241,'Le Terme porte fortifiée Terme à remplacer par le Terme porte'),(3590,1540,'remplacer_par',43,1177,1,1249,'Le Terme potager Terme à remplacer par le Terme jardin'),(3591,1543,'remplacer_par',43,210,1,1252,'Le Terme potier Terme à remplacer par le Terme atelier de potier'),(3592,1544,'remplacer_par',43,1545,1,1253,'Le Terme poudre Terme à remplacer par le Terme poudrerie'),(3593,1550,'remplacer_par',43,1551,1,1258,'Le Terme prairie Terme à remplacer par le Terme pré'),(3594,1552,'remplacer_par',43,1177,1,1260,'Le Terme préau Terme à remplacer par le Terme jardin'),(3595,1575,'remplacer_par',43,1587,1,1277,'Le Terme puits à dromos Terme à remplacer par le Terme puits monumental'),(3596,1577,'remplacer_par',43,1580,1,1279,'Le Terme puits à offrandes Terme à remplacer par le Terme puits cultuel'),(3597,1588,'remplacer_par',43,1570,1,1287,'Le Terme puits perle Terme à remplacer par le Terme puisard'),(3598,1590,'remplacer_par',43,1574,1,1289,'Le Terme puits public Terme à remplacer par le Terme puits'),(3599,1591,'remplacer_par',43,1580,1,1290,'Le Terme puits rituel Terme à remplacer par le Terme puits cultuel'),(3600,1592,'remplacer_par',43,1580,1,1291,'Le Terme puits votif Terme à remplacer par le Terme puits cultuel'),(3601,1627,'remplacer_par',43,611,1,1317,'Le Terme refuge Terme à remplacer par le Terme couvent'),(3602,1628,'remplacer_par',43,379,1,1318,'Le Terme regard Terme à remplacer par le Terme canalisation'),(3603,1638,'remplacer_par',43,675,1,1325,'Le Terme remise Terme à remplacer par le Terme dépendance'),(3604,1639,'remplacer_par',43,789,1,1326,'Le Terme rempart Terme à remplacer par le Terme enceinte'),(3605,1644,'remplacer_par',43,1787,1,1329,'Le Terme réserve de terre Terme à remplacer par le Terme stockage'),(3606,1643,'remplacer_par',43,517,1,1330,'Le Terme réserve d\'eau Terme à remplacer par le Terme citerne'),(3607,1645,'remplacer_par',43,1787,1,1331,'Le Terme réserve domestique Terme à remplacer par le Terme stockage'),(3608,1646,'remplacer_par',43,517,1,1332,'Le Terme réservoir Terme à remplacer par le Terme citerne'),(3609,1652,'remplacer_par',43,1647,1,1337,'Le Terme résidence princière Terme à remplacer par le Terme résidence'),(3610,1657,'remplacer_par',43,778,1,1342,'Le Terme rigole d\'évacuation Terme à remplacer par le Terme égout'),(3611,1660,'secondaire_de',42,607,1,1345,'Le Terme rivière Terme secondaire de le Terme cours d\'eau'),(3612,1663,'remplacer_par',43,1948,1,1347,'Le Terme route Terme à remplacer par le Terme voie'),(3613,1665,'secondaire_de',42,1948,1,1349,'Le Terme rue Terme secondaire de le Terme voie'),(3614,1666,'secondaire_de',42,1948,1,1350,'Le Terme ruelle Terme secondaire de le Terme voie'),(3615,1668,'remplacer_par',43,372,1,1352,'Le Terme ruisseau canalisé Terme à remplacer par le Terme canal'),(3616,1669,'remplacer_par',43,1671,1,1353,'Le Terme sable Terme à remplacer par le Terme sablière'),(3617,1670,'remplacer_par',43,922,1,1354,'Le Terme sable de fonderie Terme à remplacer par le Terme fonderie'),(3618,1673,'en_instance',44,227,1,1357,'Le Terme sacellum Terme en instance le Terme autel'),(3619,1678,'remplacer_par',43,1092,1,1361,'Le Terme salle Terme à remplacer par le Terme habitation'),(3620,1680,'remplacer_par',43,365,1,1363,'Le Terme salle chaude Terme à remplacer par le Terme caldarium'),(3621,1683,'remplacer_par',43,248,1,1365,'Le Terme salle de bain Terme à remplacer par le Terme bains'),(3622,1688,'remplacer_par',43,755,1,1370,'Le Terme salle de spectacle Terme à remplacer par le Terme édifice de spectacle'),(3623,1690,'remplacer_par',43,1679,1,1372,'Le Terme salle du chapitre Terme à remplacer par le Terme salle capitulaire'),(3624,1708,'remplacer_par',43,1704,1,1387,'Le Terme sarcophage en remploi Terme à remplacer par le Terme sarcophage'),(3625,1711,'remplacer_par',43,1704,1,1389,'Le Terme sarcophage réutilisé Terme à remplacer par le Terme sarcophage'),(3626,1713,'remplacer_par',43,1714,1,1391,'Le Terme savon Terme à remplacer par le Terme savonnerie'),(3627,1720,'remplacer_par',43,1719,1,1398,'Le Terme scories métalliques Terme à remplacer par le Terme scories'),(3628,1727,'en_instance',44,595,1,1404,'Le Terme sénéchaussée Terme en instance le Terme corps de garde'),(3629,1730,'remplacer_par',43,501,1,1408,'Le Terme sépulture canoniale Terme à remplacer par le Terme cimetière de communauté religieuse'),(3630,1738,'remplacer_par',43,1737,1,1412,'Le Terme sépulture de nouveau né Terme à remplacer par le Terme sépulture de nourrisson'),(3631,1739,'remplacer_par',43,1728,1,1413,'Le Terme sépulture de pélerin Terme à remplacer par le Terme sépulture'),(3632,1744,'remplacer_par',43,1728,1,1421,'Le Terme sépulture en fosse Terme à remplacer par le Terme sépulture'),(3633,1748,'remplacer_par',43,534,1,1424,'Le Terme sépulture maçonnée Terme à remplacer par le Terme coffre'),(3634,1750,'remplacer_par',43,1737,1,1426,'Le Terme sépulture périnatale Terme à remplacer par le Terme sépulture de nourrisson'),(3635,1754,'remplacer_par',43,1870,1,1429,'Le Terme sépulture rupestre Terme à remplacer par le Terme tombe rupestre'),(3636,1766,'secondaire_de',42,1787,1,1440,'Le Terme silo Terme secondaire de le Terme stockage'),(3637,1770,'remplacer_par',43,117,1,1443,'Le Terme sol de circulation Terme à remplacer par le Terme aire de circulation'),(3638,1772,'remplacer_par',43,387,1,1445,'Le Terme source Terme à remplacer par le Terme captage'),(3639,1773,'remplacer_par',43,406,1,1446,'Le Terme sous-sol Terme à remplacer par le Terme cave'),(3640,1775,'remplacer_par',43,141,1,1448,'Le Terme soutènement de berge Terme à remplacer par le Terme aménagement de berge'),(3641,1777,'remplacer_par',43,514,1,1450,'Le Terme spina Terme à remplacer par le Terme cirque'),(3642,1783,'remplacer_par',43,1334,1,1455,'Le Terme statue Terme à remplacer par le Terme monument commémoratif'),(3643,1784,'en_instance',44,1331,1,1456,'Le Terme stèle Terme en instance le Terme monument'),(3644,1785,'en_instance',44,1784,1,1457,'Le Terme stèle funéraire Terme en instance le Terme stèle'),(3645,1789,'remplacer_par',43,751,1,1461,'Le Terme structure à fonction cultuelle Terme à remplacer par le Terme édifice cultuel'),(3646,1791,'remplacer_par',43,1171,1,1462,'Le Terme structure artisanale Terme à remplacer par le Terme installation artisanale'),(3647,1797,'remplacer_par',43,1533,1,1467,'Le Terme stylobate Terme à remplacer par le Terme portique'),(3648,1800,'remplacer_par',43,117,1,1470,'Le Terme surface de circulation Terme à remplacer par le Terme aire de circulation'),(3649,1802,'remplacer_par',43,357,1,1621,'Le Terme système cadastral Terme à remplacer par le Terme cadastration'),(3650,1807,'secondaire_de',42,1403,1,1475,'Le Terme tabletterie Terme secondaire de le Terme os'),(3651,1808,'remplacer_par',43,213,1,1476,'Le Terme taille de la pierre Terme à remplacer par le Terme atelier de taille de pierre'),(3652,1813,'secondaire_de',42,624,1,1481,'Le Terme tannerie Terme secondaire de le Terme cuir'),(3653,1823,'remplacer_par',43,844,1,1490,'Le Terme terrain agricole Terme à remplacer par le Terme espace agricole'),(3654,1826,'remplacer_par',43,852,1,1492,'Le Terme terrain libre Terme à remplacer par le Terme espace libre'),(3655,1827,'remplacer_par',43,1972,1,1493,'Le Terme terrain marécageux Terme à remplacer par le Terme zone marécageuse'),(3656,1831,'remplacer_par',43,1502,1,1496,'Le Terme terrasse d\'artillerie Terme à remplacer par le Terme plate-forme'),(3657,1835,'remplacer_par',43,178,1,1499,'Le Terme terre Terme à remplacer par le Terme argile'),(3658,1838,'remplacer_par',43,1506,1,1501,'Le Terme terre libre Terme à remplacer par le Terme pleine terre'),(3659,1839,'remplacer_par',43,1502,1,1502,'Le Terme terre-plein Terme à remplacer par le Terme plate-forme'),(3660,1848,'secondaire_de',42,755,1,1509,'Le Terme théâtre Terme secondaire de le Terme édifice de spectacle'),(3661,1851,'remplacer_par',43,249,1,1511,'Le Terme thermes privés Terme à remplacer par le Terme bains privés'),(3662,1852,'remplacer_par',43,1850,1,1512,'Le Terme thermes publics Terme à remplacer par le Terme thermes'),(3663,1855,'remplacer_par',43,1728,1,1515,'Le Terme tombe Terme à remplacer par le Terme sépulture'),(3664,1861,'remplacer_par',43,534,1,1518,'Le Terme tombe coffrée Terme à remplacer par le Terme coffre'),(3665,1862,'remplacer_par',43,1731,1,1519,'Le Terme tombe collective Terme à remplacer par le Terme sépulture collective'),(3666,1863,'remplacer_par',43,534,1,1520,'Le Terme tombe construite Terme à remplacer par le Terme coffre'),(3667,1867,'remplacer_par',43,534,1,1524,'Le Terme tombe maçonnée Terme à remplacer par le Terme coffre'),(3668,1871,'remplacer_par',43,1756,1,1527,'Le Terme tombe sous lauzes Terme à remplacer par le Terme sépulture sous lauzes'),(3669,1872,'remplacer_par',43,1757,1,1528,'Le Terme tombe sous tuiles Terme à remplacer par le Terme sépulture sous tuiles'),(3670,1878,'secondaire_de',42,427,1,1534,'Le Terme tour de potier Terme secondaire de le Terme céramique'),(3671,1884,'remplacer_par',43,1885,1,1535,'Le Terme tourbe Terme à remplacer par le Terme tourbière'),(3672,1881,'remplacer_par',43,1528,1,1539,'Le Terme tour-porte Terme à remplacer par le Terme porte'),(3673,1882,'remplacer_par',43,1542,1,1540,'Le Terme tour-poterne Terme à remplacer par le Terme poterne'),(3674,1890,'remplacer_par',43,735,1,1545,'Le Terme tranchée de drainage Terme à remplacer par le Terme drain'),(3675,1891,'remplacer_par',43,479,1,1622,'Le Terme tranchée-chemin Terme à remplacer par le Terme chemin'),(3676,1892,'remplacer_par',43,763,1,1546,'Le Terme transept Terme à remplacer par le Terme église'),(3677,1895,'remplacer_par',43,1319,1,1548,'Le Terme travailleur de métaux Terme à remplacer par le Terme métal'),(3678,1897,'remplacer_par',43,1177,1,1550,'Le Terme treille Terme à remplacer par le Terme jardin'),(3679,1898,'remplacer_par',43,605,1,1551,'Le Terme tribunal Terme à remplacer par le Terme cour de justice'),(3680,1899,'remplacer_par',43,1092,1,1552,'Le Terme triclinium Terme à remplacer par le Terme habitation'),(3681,1901,'secondaire_de',42,1665,1,1553,'Le Terme trottoir Terme secondaire de le Terme rue'),(3682,1904,'remplacer_par',43,1905,1,1555,'Le Terme tuile Terme à remplacer par le Terme tuilerie'),(3683,1906,'remplacer_par',43,1905,1,1557,'Le Terme tuilier Terme à remplacer par le Terme tuilerie'),(3684,1909,'remplacer_par',43,379,1,1560,'Le Terme tuyau Terme à remplacer par le Terme canalisation'),(3685,1926,'remplacer_par',43,134,1,1573,'Le Terme vase Terme à remplacer par le Terme alluvions'),(3686,1928,'remplacer_par',43,687,1,1574,'Le Terme vase funéraire Terme à remplacer par le Terme dépôt funéraire'),(3687,1929,'remplacer_par',43,1666,1,1575,'Le Terme venelle Terme à remplacer par le Terme ruelle'),(3688,1930,'remplacer_par',43,1177,1,1576,'Le Terme verger Terme à remplacer par le Terme jardin'),(3689,1932,'remplacer_par',43,1931,1,1578,'Le Terme verrerie Terme à remplacer par le Terme verre'),(3690,1933,'remplacer_par',43,1931,1,1579,'Le Terme verrier Terme à remplacer par le Terme verre'),(3691,1952,'remplacer_par',43,1948,1,1624,'Le Terme voie secondaire Terme à remplacer par le Terme voie'),(3692,1956,'remplacer_par',43,844,1,1597,'Le Terme zone agraire Terme à remplacer par le Terme espace agricole'),(3693,1957,'remplacer_par',43,844,1,1598,'Le Terme zone agricole Terme à remplacer par le Terme espace agricole'),(3694,1960,'remplacer_par',43,1526,1,1599,'Le Terme zone de chargement Terme à remplacer par le Terme port'),(3695,1962,'remplacer_par',43,1793,1,1601,'Le Terme zone de cuisson Terme à remplacer par le Terme structure de cuisson'),(3696,1963,'remplacer_par',43,844,1,1602,'Le Terme zone de culture Terme à remplacer par le Terme espace agricole'),(3697,1965,'remplacer_par',43,690,1,1604,'Le Terme zone de décharge Terme à remplacer par le Terme dépotoir'),(3698,1967,'remplacer_par',43,1787,1,1605,'Le Terme zone de stockage Terme à remplacer par le Terme stockage'),(3699,1959,'remplacer_par',43,662,1,1608,'Le Terme zone d\'épandage Terme à remplacer par le Terme décharge'),(3700,1975,'remplacer_par',43,1526,1,1613,'Le Terme zone portuaire Terme à remplacer par le Terme port'),(3701,140,'remplacer_par',43,372,1,1627,'Le Terme aménagement d\'une voie fluviale Terme à remplacer par le Terme canal'),(3702,381,'remplacer_par',43,372,1,1629,'Le Terme canalisation d\'une rivière Terme à remplacer par le Terme canal'),(3703,576,'remplacer_par',43,718,1,1630,'Le Terme conduit de dérivation Terme à remplacer par le Terme dérivation'),(3704,577,'remplacer_par',43,378,1,1631,'Le Terme conduit souterrain d\'une rivière Terme à remplacer par le Terme canal souterrain'),(3705,654,'remplacer_par',43,268,1,1632,'Le Terme darse Terme à remplacer par le Terme bassin'),(3706,862,'remplacer_par',43,720,1,1633,'Le Terme estacade Terme à remplacer par le Terme digue'),(3707,1655,'remplacer_par',43,268,1,1638,'Le Terme retenue Terme à remplacer par le Terme bassin'),(3708,1955,'remplacer_par',43,378,1,1639,'Le Terme voûtement de rivière Terme à remplacer par le Terme canal souterrain'),(3709,587,'remplacer_par',43,1774,1,1640,'Le Terme contreforts Terme à remplacer par le Terme soutènement'),(3710,1606,'remplacer_par',43,1605,1,1641,'Le Terme rampe d\'accès Terme à remplacer par le Terme rampe'),(3711,1609,'remplacer_par',43,1774,1,1642,'Le Terme rampe de soutènement Terme à remplacer par le Terme soutènement'),(3712,1629,'remplacer_par',43,879,1,1643,'Le Terme rehaussement de niveau Terme à remplacer par le Terme exhaussement'),(3713,1637,'remplacer_par',43,1634,1,1644,'Le Terme remblayage Terme à remplacer par le Terme remblai'),(3714,1845,'remplacer_par',43,1516,1,1646,'Le Terme tête de pont Terme à remplacer par le Terme pont'),(3715,273,'remplacer_par',43,268,1,1649,'Le Terme bassin d\'atrium Terme à remplacer par le Terme bassin'),(3716,274,'remplacer_par',43,387,1,1650,'Le Terme bassin de captage Terme à remplacer par le Terme captage'),(3717,402,'remplacer_par',43,470,1,1651,'Le Terme castellum divisiorum Terme à remplacer par le Terme château d\'eau'),(3718,1031,'remplacer_par',43,379,1,1652,'Le Terme frette Terme à remplacer par le Terme canalisation'),(3719,1415,'remplacer_par',43,379,1,1654,'Le Terme ouvrage hydraulique souterrain Terme à remplacer par le Terme canalisation'),(3720,1595,'remplacer_par',43,1574,1,1657,'Le Terme puteus public Terme à remplacer par le Terme puits'),(3721,382,'remplacer_par',43,735,1,1658,'Le Terme canaux de drainage Terme à remplacer par le Terme drain'),(3722,574,'remplacer_par',43,778,1,1659,'Le Terme conduit d\'évacuation des eaux usées Terme à remplacer par le Terme égout'),(3723,779,'remplacer_par',43,552,1,1660,'Le Terme égout-collecteur Terme à remplacer par le Terme collecteur'),(3724,285,'remplacer_par',43,284,1,1662,'Le Terme bastion d\'artillerie Terme à remplacer par le Terme bastion'),(3725,401,'remplacer_par',43,933,1,1664,'Le Terme castellum Terme à remplacer par le Terme forteresse'),(3726,589,'remplacer_par',43,939,1,1665,'Le Terme contrevallation Terme à remplacer par le Terme fossé'),(3727,915,'remplacer_par',43,609,1,1666,'Le Terme flanc Terme à remplacer par le Terme courtine'),(3728,953,'remplacer_par',43,939,1,1667,'Le Terme fossé armé Terme à remplacer par le Terme fossé'),(3729,1358,'remplacer_par',43,789,1,1669,'Le Terme mur de défense Terme à remplacer par le Terme enceinte'),(3730,1362,'remplacer_par',43,789,1,1670,'Le Terme mur de ville Terme à remplacer par le Terme enceinte'),(3731,1503,'remplacer_par',43,1502,1,1671,'Le Terme plate-forme de tir Terme à remplacer par le Terme plate-forme'),(3732,1623,'remplacer_par',43,1410,1,1672,'Le Terme réduit Terme à remplacer par le Terme ouvrage avancé'),(3733,1803,'remplacer_par',43,1411,1,1673,'Le Terme système de défense Terme à remplacer par le Terme ouvrage de défense'),(3734,729,'remplacer_par',43,728,1,1675,'Le Terme donjon double Terme à remplacer par le Terme donjon'),(3735,447,'remplacer_par',43,446,1,1678,'Le Terme champ de mars Terme à remplacer par le Terme champ de manoeuvre'),(3736,1295,'remplacer_par',43,827,1,1680,'Le Terme manutention Terme à remplacer par le Terme entrepôt'),(3737,1600,'remplacer_par',43,398,1,1683,'Le Terme quartier militaire Terme à remplacer par le Terme caserne'),(3738,1824,'remplacer_par',43,446,1,1684,'Le Terme terrain de manoeuvre Terme à remplacer par le Terme champ de manoeuvre'),(3739,108,'remplacer_par',43,937,1,1686,'Le Terme agora Terme à remplacer par le Terme forum'),(3740,1488,'remplacer_par',43,1486,1,1688,'Le Terme place du marché Terme à remplacer par le Terme place'),(3741,1569,'remplacer_par',43,1533,1,1689,'Le Terme propylées Terme à remplacer par le Terme portique'),(3742,334,'remplacer_par',43,634,1,1691,'Le Terme bouleuterion Terme à remplacer par le Terme curie'),(3743,1116,'remplacer_par',43,1175,1,1697,'Le Terme hôtel de l\'intendance Terme à remplacer par le Terme intendance'),(3744,1120,'remplacer_par',43,1763,1,1699,'Le Terme hôtel des trésoriers Terme à remplacer par le Terme siège du pouvoir public'),(3745,1238,'remplacer_par',43,1260,1,1703,'Le Terme loge des marchands Terme à remplacer par le Terme maison corporative'),(3746,1253,'remplacer_par',43,1118,1,1704,'Le Terme mairie Terme à remplacer par le Terme hôtel de ville'),(3747,1259,'remplacer_par',43,1647,1,1705,'Le Terme maison comtale Terme à remplacer par le Terme résidence'),(3748,1264,'remplacer_par',43,1561,1,1706,'Le Terme maison de détention Terme à remplacer par le Terme prison'),(3749,1272,'en_instance',44,595,1,1707,'Le Terme maison des archers Terme en instance le Terme corps de garde'),(3750,1409,'remplacer_par',43,1561,1,1708,'Le Terme oubliette Terme à remplacer par le Terme prison'),(3751,300,'remplacer_par',43,752,1,1712,'Le Terme bâtiment culturel Terme à remplacer par le Terme édifice culturel'),(3752,356,'remplacer_par',43,319,1,1714,'Le Terme cabinet de lecture Terme à remplacer par le Terme bibliothèque'),(3753,1115,'remplacer_par',43,752,1,1715,'Le Terme hôtel de l\'académie Terme à remplacer par le Terme édifice culturel'),(3754,1423,'remplacer_par',43,1368,1,1717,'Le Terme palais des Beaux-Arts Terme à remplacer par le Terme musée'),(3755,614,'secondaire_de',42,611,1,1719,'Le Terme couvent hospitalier Terme secondaire de le Terme couvent'),(3756,1275,'remplacer_par',43,1402,1,1721,'Le Terme maison des orphelines Terme à remplacer par le Terme orphelinat'),(3757,1849,'secondaire_de',42,755,1,1727,'Le Terme théâtre-amphithéâtre Terme secondaire de le Terme édifice de spectacle'),(3758,821,'remplacer_par',43,1850,1,1728,'Le Terme ensemble balnéaire Terme à remplacer par le Terme thermes'),(3759,1601,'remplacer_par',43,1850,1,1729,'Le Terme quartier thermal Terme à remplacer par le Terme thermes'),(3760,1691,'remplacer_par',43,1034,1,1730,'Le Terme salle froide Terme à remplacer par le Terme frigidarium'),(3761,1694,'remplacer_par',43,1822,1,1731,'Le Terme salle tiède Terme à remplacer par le Terme tepidarium'),(3762,1934,'remplacer_par',43,158,1,1732,'Le Terme vestiaire Terme à remplacer par le Terme apodyterium'),(3763,1111,'secondaire_de',42,827,1,1734,'Le Terme horreum Terme secondaire de le Terme entrepôt'),(3764,1234,'remplacer_par',43,297,1,1735,'Le Terme local à fonction commerciale Terme à remplacer par le Terme bâtiment commercial'),(3765,1235,'remplacer_par',43,338,1,1736,'Le Terme local de vente Terme à remplacer par le Terme boutique'),(3766,1815,'remplacer_par',43,223,1,1737,'Le Terme taverne Terme à remplacer par le Terme auberge'),(3767,281,'remplacer_par',43,268,1,1740,'Le Terme bassin froid Terme à remplacer par le Terme bassin'),(3768,628,'remplacer_par',43,228,1,1741,'Le Terme culte domestique Terme à remplacer par le Terme autel domestique'),(3769,855,'remplacer_par',43,1087,1,1742,'Le Terme espace privé Terme à remplacer par le Terme habitat'),(3770,974,'remplacer_par',43,939,1,1744,'Le Terme fossé palissadé Terme à remplacer par le Terme fossé'),(3771,1027,'remplacer_par',43,1023,1,1745,'Le Terme foyer-construit Terme à remplacer par le Terme foyer'),(3772,1028,'remplacer_par',43,1023,1,1746,'Le Terme foyer-plaque Terme à remplacer par le Terme foyer'),(3773,1139,'remplacer_par',43,1138,1,1747,'Le Terme immeuble de rapport Terme à remplacer par le Terme immeuble'),(3774,1281,'remplacer_par',43,1122,1,1748,'Le Terme maison noble Terme à remplacer par le Terme hôtel particulier'),(3775,1365,'remplacer_par',43,529,1,1749,'Le Terme muret de jardin Terme à remplacer par le Terme clôture'),(3776,1366,'remplacer_par',43,529,1,1750,'Le Terme murette Terme à remplacer par le Terme clôture'),(3777,1443,'secondaire_de',42,1941,1,1751,'Le Terme pars urbana Terme secondaire de le Terme villa'),(3778,1453,'remplacer_par',43,1092,1,1752,'Le Terme pavillon Terme à remplacer par le Terme habitation'),(3779,1469,'remplacer_par',43,1092,1,1753,'Le Terme pièce d\'habitation Terme à remplacer par le Terme habitation'),(3780,1593,'remplacer_par',43,690,1,1755,'Le Terme puits-dépotoir Terme à remplacer par le Terme dépotoir'),(3781,1585,'remplacer_par',43,1574,1,1756,'Le Terme puits domestique Terme à remplacer par le Terme puits'),(3782,1681,'remplacer_par',43,1092,1,1757,'Le Terme salle d\'apparat Terme à remplacer par le Terme habitation'),(3783,1700,'remplacer_par',43,228,1,1758,'Le Terme sanctuaire domestique Terme à remplacer par le Terme autel domestique'),(3784,1767,'remplacer_par',43,1766,1,1759,'Le Terme silo aérien Terme à remplacer par le Terme silo'),(3785,1902,'remplacer_par',43,1087,1,1760,'Le Terme trou de poteau Terme à remplacer par le Terme habitat'),(3786,230,'en_instance',44,227,1,1761,'Le Terme autel votif Terme en instance le Terme autel'),(3787,299,'remplacer_par',43,751,1,1763,'Le Terme bâtiment cultuel Terme à remplacer par le Terme édifice cultuel'),(3788,1578,'remplacer_par',43,1580,1,1766,'Le Terme puits à usage cultuel Terme à remplacer par le Terme puits cultuel'),(3789,642,'remplacer_par',43,928,1,1768,'Le Terme cuve baptismale Terme à remplacer par le Terme fonts baptismaux'),(3790,762,'remplacer_par',43,751,1,1770,'Le Terme édifice religieux Terme à remplacer par le Terme édifice cultuel'),(3791,822,'remplacer_par',43,1082,1,1771,'Le Terme ensemble cathédral Terme à remplacer par le Terme groupe épiscopal'),(3792,310,'remplacer_par',43,526,1,1773,'Le Terme bâtiments canoniaux Terme à remplacer par le Terme cloître canonial'),(3793,313,'secondaire_de',42,611,1,1774,'Le Terme béguinage Terme secondaire de le Terme couvent'),(3794,464,'remplacer_par',43,526,1,1775,'Le Terme chapitre épiscopal Terme à remplacer par le Terme cloître canonial'),(3795,612,'remplacer_par',43,611,1,1776,'Le Terme couvent de la Contre Réforme Terme à remplacer par le Terme couvent'),(3796,615,'secondaire_de',42,611,1,1777,'Le Terme couvent mendiant Terme secondaire de le Terme couvent'),(3797,813,'remplacer_par',43,1559,1,1778,'Le Terme enclos prieural Terme à remplacer par le Terme prieuré'),(3798,1094,'remplacer_par',43,1256,1,1780,'Le Terme habitation canoniale Terme à remplacer par le Terme maison canoniale'),(3799,1095,'remplacer_par',43,1277,1,1781,'Le Terme habitation du prieur Terme à remplacer par le Terme maison du prieur'),(3800,1119,'remplacer_par',43,1256,1,1782,'Le Terme hôtel des chanoines Terme à remplacer par le Terme maison canoniale'),(3801,1255,'remplacer_par',43,1242,1,1783,'Le Terme maison abbatiale Terme à remplacer par le Terme logis abbatial'),(3802,1273,'remplacer_par',43,1256,1,1784,'Le Terme maison des chanoinesses Terme à remplacer par le Terme maison canoniale'),(3803,1312,'remplacer_par',43,226,1,1785,'Le Terme matricule Terme à remplacer par le Terme aumônerie'),(3804,1417,'remplacer_par',43,1242,1,1786,'Le Terme palais abbatial Terme à remplacer par le Terme logis abbatial'),(3805,172,'remplacer_par',43,875,1,1787,'Le Terme archevêché Terme à remplacer par le Terme évêché'),(3806,756,'remplacer_par',43,302,1,1788,'Le Terme édifice ecclésiastique Terme à remplacer par le Terme bâtiment ecclésiastique'),(3807,1096,'remplacer_par',43,302,1,1791,'Le Terme habitation ecclésiastique Terme à remplacer par le Terme bâtiment ecclésiastique'),(3808,278,'remplacer_par',43,247,1,1793,'Le Terme bassin de purification Terme à remplacer par le Terme bain rituel'),(3809,550,'remplacer_par',43,534,1,1797,'Le Terme coffre non maçonné Terme à remplacer par le Terme coffre'),(3810,432,'remplacer_par',43,431,1,1798,'Le Terme cercueil cloué Terme à remplacer par le Terme cercueil'),(3811,444,'remplacer_par',43,1373,1,1800,'Le Terme champ d\'urne Terme à remplacer par le Terme nécropole'),(3812,500,'remplacer_par',43,501,1,1801,'Le Terme cimetière de communauté canonial Terme à remplacer par le Terme cimetière de communauté religieuse'),(3813,504,'remplacer_par',43,501,1,1802,'Le Terme cimetière du couvent Terme à remplacer par le Terme cimetière de communauté religieuse'),(3814,533,'en_instance',44,541,1,1803,'Le Terme coffrage en plâtre Terme en instance le Terme coffre de plâtre'),(3815,1107,'remplacer_par',43,1728,1,1805,'Le Terme heroum Terme à remplacer par le Terme sépulture'),(3816,1148,'remplacer_par',43,351,1,1806,'Le Terme incinération primaire Terme à remplacer par le Terme bustum'),(3817,1149,'remplacer_par',43,1920,1,1807,'Le Terme incinération secondaire Terme à remplacer par le Terme ustrinum'),(3818,1157,'remplacer_par',43,501,1,1809,'Le Terme inhumation de religieuses Terme à remplacer par le Terme cimetière de communauté religieuse'),(3819,1161,'remplacer_par',43,1728,1,1810,'Le Terme inhumation pleine terre Terme à remplacer par le Terme sépulture'),(3820,1163,'remplacer_par',43,1757,1,1811,'Le Terme inhumation sous tuiles Terme à remplacer par le Terme sépulture sous tuiles'),(3821,1196,'remplacer_par',43,651,1,1812,'Le Terme lame funéraire Terme à remplacer par le Terme dalle funéraire'),(3822,1225,'remplacer_par',43,1224,1,1813,'Le Terme linceul avec chaux Terme à remplacer par le Terme linceul'),(3823,1378,'remplacer_par',43,501,1,1817,'Le Terme nécropole de communauté religieuse Terme à remplacer par le Terme cimetière de communauté religieuse'),(3824,1709,'remplacer_par',43,1704,1,1818,'Le Terme sarcophage monolithe Terme à remplacer par le Terme sarcophage'),(3825,1732,'remplacer_par',43,1870,1,1819,'Le Terme sépulture creusée dans le rocher Terme à remplacer par le Terme tombe rupestre'),(3826,1745,'remplacer_par',43,1506,1,1820,'Le Terme sépulture en pleine terre Terme à remplacer par le Terme pleine terre'),(3827,1858,'remplacer_par',43,534,1,1824,'Le Terme tombe avec lauzes Terme à remplacer par le Terme coffre'),(3828,1859,'remplacer_par',43,534,1,1825,'Le Terme tombe bâtie Terme à remplacer par le Terme coffre'),(3829,1869,'remplacer_par',43,1751,1,1826,'Le Terme tombe privilégiée Terme à remplacer par le Terme sépulture privilégiée'),(3830,1915,'remplacer_par',43,1914,1,1827,'Le Terme urne cinéraire Terme à remplacer par le Terme urne'),(3831,1916,'remplacer_par',43,1914,1,1828,'Le Terme urne funéraire Terme à remplacer par le Terme urne'),(3832,185,'remplacer_par',43,427,1,1830,'Le Terme artisanat céramique Terme à remplacer par le Terme céramique'),(3833,187,'remplacer_par',43,130,1,1831,'Le Terme artisanat de l\'alimentation Terme à remplacer par le Terme alimentation'),(3834,188,'remplacer_par',43,1403,1,1832,'Le Terme artisanat de l\'os Terme à remplacer par le Terme os'),(3835,189,'en_instance',44,908,1,1833,'Le Terme artisanat de travail du fer Terme en instance le Terme fer'),(3836,192,'remplacer_par',43,323,1,1834,'Le Terme artisanat du bois Terme à remplacer par le Terme bois'),(3837,193,'remplacer_par',43,624,1,1835,'Le Terme artisanat du cuir Terme à remplacer par le Terme cuir'),(3838,194,'remplacer_par',43,1319,1,1836,'Le Terme artisanat du métal Terme à remplacer par le Terme métal'),(3839,195,'remplacer_par',43,1931,1,1837,'Le Terme artisanat du verre Terme à remplacer par le Terme verre'),(3840,196,'remplacer_par',43,1319,1,1838,'Le Terme artisanat métallurgique Terme à remplacer par le Terme métal'),(3841,197,'remplacer_par',43,1846,1,1839,'Le Terme artisanat textile Terme à remplacer par le Terme textile'),(3842,209,'en_instance',44,908,1,1919,'Le Terme atelier de grillage du minerai de fer Terme en instance le Terme fer'),(3843,451,'remplacer_par',43,119,1,1840,'Le Terme chantier de construction Terme à remplacer par le Terme aire de construction'),(3844,598,'secondaire_de',42,1813,1,1843,'Le Terme corroyeur Terme secondaire de le Terme tannerie'),(3845,965,'remplacer_par',43,991,1,1847,'Le Terme fosse de coulée de cloche Terme à remplacer par le Terme four à cloche'),(3846,1053,'remplacer_par',43,899,1,1850,'Le Terme garum Terme à remplacer par le Terme fabrication du garum'),(3847,1392,'remplacer_par',43,201,1,1852,'Le Terme officine Terme à remplacer par le Terme atelier'),(3848,1441,'secondaire_de',42,1813,1,1853,'Le Terme parcheminier Terme secondaire de le Terme tannerie'),(3849,1641,'remplacer_par',43,585,1,1855,'Le Terme réparation de navires Terme à remplacer par le Terme construction navale'),(3850,1662,'secondaire_de',42,1846,1,1856,'Le Terme rouissage Terme secondaire de le Terme textile'),(3851,1918,'remplacer_par',43,1675,1,1860,'Le Terme usine de salaison Terme à remplacer par le Terme salaison'),(3852,1966,'remplacer_par',43,690,1,1863,'Le Terme zone de rejets Terme à remplacer par le Terme dépotoir'),(3853,374,'remplacer_par',43,1176,1,1865,'Le Terme canal d\'irrigation Terme à remplacer par le Terme irrigation'),(3854,631,'remplacer_par',43,1298,1,1866,'Le Terme culture maraîchère Terme à remplacer par le Terme maraîchage'),(3855,959,'remplacer_par',43,806,1,1868,'Le Terme fossé d\'enclos Terme à remplacer par le Terme enclos'),(3856,1030,'remplacer_par',43,1947,1,1870,'Le Terme frayère Terme à remplacer par le Terme vivier'),(3857,1438,'remplacer_par',43,1547,1,1877,'Le Terme parc à volaille Terme à remplacer par le Terme poulailler'),(3858,1721,'remplacer_par',43,124,1,1878,'Le Terme séchage Terme à remplacer par le Terme aire de séchage'),(3859,1836,'remplacer_par',43,844,1,1880,'Le Terme terre agricole Terme à remplacer par le Terme espace agricole'),(3860,1974,'remplacer_par',43,1551,1,1881,'Le Terme zone pastorale Terme à remplacer par le Terme pré'),(3861,217,'remplacer_par',43,305,1,1882,'Le Terme atelier industriel Terme à remplacer par le Terme bâtiment industriel'),(3862,394,'remplacer_par',43,1326,1,1887,'Le Terme carrière souterraine Terme à remplacer par le Terme mine'),(3863,883,'remplacer_par',43,175,1,1888,'Le Terme extraction d\'ardoise Terme à remplacer par le Terme ardoisière'),(3864,884,'remplacer_par',43,178,1,1889,'Le Terme extraction d\'argile Terme à remplacer par le Terme argile'),(3865,885,'remplacer_par',43,364,1,1890,'Le Terme extraction de calcaire Terme à remplacer par le Terme calcaire'),(3866,886,'remplacer_par',43,616,1,1891,'Le Terme extraction de craie Terme à remplacer par le Terme craie'),(3867,887,'remplacer_par',43,1063,1,1892,'Le Terme extraction de gneiss Terme à remplacer par le Terme gneiss'),(3868,888,'remplacer_par',43,1069,1,1893,'Le Terme extraction de granit Terme à remplacer par le Terme granit'),(3869,889,'remplacer_par',43,1074,1,1894,'Le Terme extraction de gravier Terme à remplacer par le Terme gravière'),(3870,890,'remplacer_par',43,1080,1,1895,'Le Terme extraction de grès Terme à remplacer par le Terme grès'),(3871,891,'remplacer_par',43,1223,1,1896,'Le Terme extraction de limon Terme à remplacer par le Terme limon'),(3872,892,'remplacer_par',43,1236,1,1897,'Le Terme extraction de loess Terme à remplacer par le Terme loess'),(3873,893,'en_instance',44,1472,1,1898,'Le Terme extraction de pierre Terme en instance le Terme pierre'),(3874,894,'remplacer_par',43,1671,1,1899,'Le Terme extraction de sable Terme à remplacer par le Terme sablière'),(3875,895,'remplacer_par',43,1676,1,1900,'Le Terme extraction de sel Terme à remplacer par le Terme saline'),(3876,128,'remplacer_par',43,1972,1,1901,'Le Terme aire marécageuse Terme à remplacer par le Terme zone marécageuse'),(3877,316,'remplacer_par',43,1659,1,1902,'Le Terme berge non aménagée Terme à remplacer par le Terme rive'),(3878,599,'remplacer_par',43,1658,1,1903,'Le Terme côte Terme à remplacer par le Terme rivage'),(3879,916,'secondaire_de',42,607,1,1904,'Le Terme fleuve Terme secondaire de le Terme cours d\'eau'),(3880,1165,'remplacer_par',43,1382,1,1906,'Le Terme inondation Terme à remplacer par le Terme niveau de crue'),(3881,1228,'remplacer_par',43,607,1,1908,'Le Terme lit Terme à remplacer par le Terme cours d\'eau'),(3882,1303,'secondaire_de',42,1300,1,1909,'Le Terme marais littoral Terme secondaire de le Terme marais'),(3883,1427,'remplacer_par',43,607,1,1912,'Le Terme paléo-ruisseau Terme à remplacer par le Terme cours d\'eau'),(3884,1847,'remplacer_par',43,1812,1,1914,'Le Terme thalweg Terme à remplacer par le Terme talweg'),(3885,1973,'remplacer_par',43,1300,1,1918,'Le Terme zone palustre Terme à remplacer par le Terme marais'),(3886,2129,'depend_de',41,2193,1,1,'La précision \"17-Cuisson plutôt oxydante (contient 10, 11, 12, 14, 16)\" Dépend de la Production \"1-Vernis argileux, greses\" '),(3887,2130,'depend_de',41,2193,1,2,'La précision \"18-Cuisson plutôt réductrice (contient 13 et15)\" Dépend de la Production \"1-Vernis argileux, greses\" '),(3888,2131,'depend_de',41,2194,1,3,'La précision \"20-Polissage extérieur\" Dépend de la Production \"2-Polissage sur pâte cuite en atmosphère oxydante\" '),(3889,2132,'depend_de',41,2194,1,4,'La précision \"21-Polissage extérieur - intérieur\" Dépend de la Production \"2-Polissage sur pâte cuite en atmosphère oxydante\" '),(3890,2133,'depend_de',41,2194,1,5,'La précision \"23-Polissage par outil métalique laissant des traînées d\'oxyde brunes\" Dépend de la Production \"2-Polissage sur pâte cuite en atmosphère oxydante\" '),(3891,2134,'depend_de',41,2194,1,6,'La précision \"24-Polissage intérieur\" Dépend de la Production \"2-Polissage sur pâte cuite en atmosphère oxydante\" '),(3892,2135,'depend_de',41,2195,1,7,'La précision \"30-Polissage extérieur\" Dépend de la Production \"3-Polissage sur pâte cuite en atmosphère réductrice\" '),(3893,2136,'depend_de',41,2195,1,8,'La précision \"31-Polissage extérieur - intérieur\" Dépend de la Production \"3-Polissage sur pâte cuite en atmosphère réductrice\" '),(3894,2137,'depend_de',41,2195,1,9,'La précision \"32-Polissage intérieur\" Dépend de la Production \"3-Polissage sur pâte cuite en atmosphère réductrice\" '),(3895,2138,'depend_de',41,2195,1,10,'La précision \"33-Polissage extérieur - peinture extérieur\" Dépend de la Production \"3-Polissage sur pâte cuite en atmosphère réductrice\" '),(3896,2139,'depend_de',41,2195,1,11,'La précision \"331-Production dite de \"Tating\"\" Dépend de la Production \"3-Polissage sur pâte cuite en atmosphère réductrice\" '),(3897,2140,'depend_de',41,2196,1,12,'La précision \"40-Engobage extérieur\" Dépend de la Production \"4-Engobage et film\" '),(3898,2141,'depend_de',41,2196,1,13,'La précision \"41-Engobage intérieur\" Dépend de la Production \"4-Engobage et film\" '),(3899,2142,'depend_de',41,2196,1,14,'La précision \"42-Engobage extérieur et polissage\" Dépend de la Production \"4-Engobage et film\" '),(3900,2143,'depend_de',41,2196,1,15,'La précision \"43-Engobage intérieur - extérieur\" Dépend de la Production \"4-Engobage et film\" '),(3901,2144,'depend_de',41,2197,1,16,'La précision \"50-Peinture visible, non caractérisée\" Dépend de la Production \"5-Décor peint\" '),(3902,2145,'depend_de',41,2197,1,17,'La précision \"51-Motif\" Dépend de la Production \"5-Décor peint\" '),(3903,2146,'depend_de',41,2197,1,18,'La précision \"52-\"Coulures\"\" Dépend de la Production \"5-Décor peint\" '),(3904,2147,'depend_de',41,2197,1,19,'La précision \"53-\"Virgules\"\" Dépend de la Production \"5-Décor peint\" '),(3905,2148,'depend_de',41,2197,1,20,'La précision \"54-\"Flammules\" en faisceux croisés\" Dépend de la Production \"5-Décor peint\" '),(3906,2149,'depend_de',41,2197,1,21,'La précision \"55-\"Flammules\" en faisceux\" Dépend de la Production \"5-Décor peint\" '),(3907,2150,'depend_de',41,2197,1,22,'La précision \"56-Décore peint, non caractérisé, puis polissage\" Dépend de la Production \"5-Décor peint\" '),(3908,2151,'depend_de',41,2197,1,23,'La précision \"58-Motif peint, puis polissage\" Dépend de la Production \"5-Décor peint\" '),(3909,2152,'depend_de',41,2198,1,24,'La précision \"60-Glaçure extérieur\" Dépend de la Production \"6-Glaçure plombifère\" '),(3910,2153,'depend_de',41,2198,1,25,'La précision \"61-Glaçure extérieur - intérieur\" Dépend de la Production \"6-Glaçure plombifère\" '),(3911,2154,'depend_de',41,2198,1,26,'La précision \"62-Glaçure intérieur\" Dépend de la Production \"6-Glaçure plombifère\" '),(3912,2155,'depend_de',41,2198,1,27,'La précision \"63-Glaçure sur décor peint ou modelé à la barbotine colorée\" Dépend de la Production \"6-Glaçure plombifère\" '),(3913,2156,'depend_de',41,2198,1,28,'La précision \"69-Glaçure sur engobe\" Dépend de la Production \"6-Glaçure plombifère\" '),(3914,2157,'depend_de',41,2199,1,29,'La précision \"70-Pâte grésée non glaçurée (\"ouverte\")\" Dépend de la Production \"7-Gres, gresage\" '),(3915,2158,'depend_de',41,2199,1,30,'La précision \"71-Grès en pâte claire\" Dépend de la Production \"7-Gres, gresage\" '),(3916,2159,'depend_de',41,2199,1,31,'La précision \"72-Pâte grésée, glaçurée (regroupe 72 et 73)\" Dépend de la Production \"7-Gres, gresage\" '),(3917,2160,'depend_de',41,2199,1,32,'La précision \"73-grès glaçuré (sel, cendre)\" Dépend de la Production \"7-Gres, gresage\" '),(3918,2161,'depend_de',41,2199,1,33,'La précision \"74-Pâte glaçuré et oxydes colorés (\"grès azuré\")\" Dépend de la Production \"7-Gres, gresage\" '),(3919,2162,'depend_de',41,2199,1,34,'La précision \"76-Grès en pâte brune\" Dépend de la Production \"7-Gres, gresage\" '),(3920,2163,'depend_de',41,2199,1,35,'La précision \"77-Pâte grésée, décor \"a sgraffiato\" glaçué\" Dépend de la Production \"7-Gres, gresage\" '),(3921,2164,'depend_de',41,2199,1,36,'La précision \"79-Pâte grésée, décor \"à la corne\"\" Dépend de la Production \"7-Gres, gresage\" '),(3922,2165,'depend_de',41,2202,1,37,'La précision \"999-* à déterminer\" Dépend de la Production \"999-* à déterminer\" '),(3949,2292,'depend_de',41,2291,1,2,'La Forme Forme ouverte depend_de la Forme Verre 16ème-17ème'),(3950,2293,'depend_de',41,2292,1,2,'La Forme Coupe depend_de la Forme Forme ouverte'),(3954,40,'presence_de',2304,23,1,1,''),(3955,92,'',2304,2096,1,1,'Présence de Reste animaux dans abattoir'),(3956,466,'',2304,2096,1,2,''),(3957,1437,'',2304,2096,1,3,''),(3958,2290,'depend_de',41,23,1,0,'La Categorie Elément recueilli - Forme  Dépend de la Categorie Elément recueilli - Nature / Matière'),(3959,2290,'depend_de',41,2290,1,0,'La Categorie Elément recueilli - Forme  Dépend de de la Categorie Elément recueilli - Forme '),(3960,2291,'',41,2100,1,1,''),(3961,2326,'',41,2291,1,3,''),(3962,2292,'depend_de',41,2291,1,2,'La Forme Forme ouverte depend_de la Forme Verre 16ème-17ème'),(3963,2293,'depend_de',41,2292,1,2,'La Forme Coupe depend_de la Forme Forme ouverte'),(3964,2306,'depend_de',41,2292,1,4,'La Forme Gobelet depend_de la Forme Forme ouverte'),(3965,2307,'depend_de',41,2292,1,5,'La Forme Vase depend_de la Forme Forme ouverte'),(3966,2308,'depend_de',41,2292,1,6,'La Forme Verre à pied depend_de la Forme Forme ouverte'),(3967,2309,'depend_de',41,2292,1,7,'La Forme Verre à jambe depend_de la Forme Forme ouverte'),(3968,2310,'depend_de',41,2292,1,8,'La Forme Plat depend_de la Forme Forme ouverte'),(3969,2311,'depend_de',41,2293,1,9,'La Forme Drageoir depend_de la Forme Coupe'),(3970,2312,'depend_de',41,2293,1,10,'La Forme Tasse depend_de la Forme Coupe'),(3971,2313,'depend_de',41,2293,1,11,'La Forme Coupe à pied depend_de la Forme Coupe'),(3972,2314,'depend_de',41,2293,1,12,'La Forme Coupelle depend_de la Forme Coupe'),(3973,2315,'depend_de',41,2308,1,13,'La Forme Verre biconique depend_de la Forme Verre à pied'),(3974,2316,'depend_de',41,2308,1,14,'La Forme Verre bitronconique depend_de la Forme Verre à pied'),(3975,2317,'depend_de',41,2308,1,15,'La Forme Verre conique depend_de la Forme Verre à pied'),(3976,2318,'depend_de',41,2308,1,16,'La Forme Verre tronconique depend_de la Forme Verre à pied'),(3977,2319,'depend_de',41,2308,1,17,'La Forme Verre campagniforme depend_de la Forme Verre à pied'),(3978,2320,'depend_de',41,2308,1,18,'La Forme Verre cylindrique depend_de la Forme Verre à pied'),(3979,2321,'depend_de',41,2308,1,19,'La Forme Verre hémisphérique depend_de la Forme Verre à pied'),(3980,2322,'depend_de',41,2309,1,20,'La Forme Verre à tige depend_de la Forme Verre à jambe'),(3981,2323,'depend_de',41,2309,1,21,'La Forme Verre à bouton depend_de la Forme Verre à jambe'),(3982,2324,'depend_de',41,2309,1,22,'La Forme Verre à balustre depend_de la Forme Verre à jambe'),(3983,2325,'depend_de',41,2309,1,23,'La Forme Verre à serpents depend_de la Forme Verre à jambe'),(3984,2326,'depend_de',41,2291,1,24,'La Forme Forme fermée depend_de la Forme Verre 16ème-17ème'),(3985,2327,'depend_de',41,2326,1,25,'La Forme Aiguière depend_de la Forme Forme fermée'),(3986,2328,'depend_de',41,2326,1,26,'La Forme Albarello depend_de la Forme Forme fermée'),(3987,2329,'depend_de',41,2326,1,27,'La Forme Bouteille depend_de la Forme Forme fermée'),(3988,2330,'depend_de',41,2326,1,28,'La Forme Burette depend_de la Forme Forme fermée'),(3989,2331,'depend_de',41,2326,1,29,'La Forme Carafe depend_de la Forme Forme fermée'),(3990,2294,'depend_de',41,2326,1,30,'La Forme Cruche depend_de la Forme Forme fermée'),(3991,2332,'depend_de',41,2326,1,31,'La Forme Curcubite depend_de la Forme Forme fermée'),(3992,2333,'depend_de',41,2326,1,32,'La Forme Kuttrolf depend_de la Forme Forme fermée'),(3993,2295,'depend_de',41,2326,1,33,'La Forme Gourde depend_de la Forme Forme fermée'),(3994,2334,'depend_de',41,2326,1,34,'La Forme Flasque depend_de la Forme Forme fermée'),(3995,2296,'depend_de',41,2329,1,39,'La Forme Bouteille à pied depend_de la Forme Bouteille'),(3996,2327,'',41,2292,1,1,''),(3997,2306,'',41,2292,1,9,''),(3998,2307,'',41,2292,1,10,''),(3999,2308,'',41,2292,1,11,''),(4000,2309,'',41,2292,1,12,''),(4001,2310,'',41,2292,1,13,''),(4002,2328,'',41,2326,1,35,''),(4003,2296,'',41,2326,1,36,''),(4004,2330,'',41,2326,1,37,''),(4005,2331,'',41,2326,1,38,''),(4006,2294,'',41,2326,1,39,''),(4007,2332,'',41,2326,1,40,''),(4008,2333,'',41,2326,1,41,''),(4009,2295,'',41,2326,1,42,''),(4010,2334,'',41,2326,1,43,''),(4011,2307,'',41,2326,1,44,''),(4012,2311,'',41,2293,1,13,''),(4013,2312,'',41,2293,1,14,''),(4014,2313,'',41,2293,1,15,''),(4015,2314,'',41,2293,1,16,''),(4016,2338,'depend_de',41,2338,1,2338,'Emplacement depend de Emplacement'),(4017,2338,'depend_de',41,2338,1,0,'Emplacement depend de Emplacement'),(4020,2342,'',41,2340,1,1,''),(4021,2341,'',41,2340,1,2,''),(4022,2340,'',41,2339,1,1,''),(4023,2343,'',41,2342,1,1,''),(4024,2344,'',41,2342,1,2,''),(4025,2342,'',41,2341,1,1,''),(4026,2344,'',41,2343,1,1,''),(4027,2339,'',41,2534,1,1,'Type Site - Type Racine'),(4028,2536,'',41,2535,1,1,'Relation causal sur la qualité des points marqués en Rugby');
/*!40000 ALTER TABLE `fsn_categorie_relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fsn_entiteadmin`
--

DROP TABLE IF EXISTS `fsn_entiteadmin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fsn_entiteadmin` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identifiant Entité Administrative',
  `organisme_id` int(11) unsigned NOT NULL COMMENT 'Organisme',
  `description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_organisme_id` (`organisme_id`),
  CONSTRAINT `fk_fsn_organisme` FOREIGN KEY (`organisme_id`) REFERENCES `fsn_organisme` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fsn_entiteadmin`
--

LOCK TABLES `fsn_entiteadmin` WRITE;
/*!40000 ALTER TABLE `fsn_entiteadmin` DISABLE KEYS */;
INSERT INTO `fsn_entiteadmin` VALUES (1,1,'SOGETI');
/*!40000 ALTER TABLE `fsn_entiteadmin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fsn_historique`
--

DROP TABLE IF EXISTS `fsn_historique`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fsn_historique` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categorie_object` varchar(45) COLLATE utf8_unicode_ci NOT NULL COMMENT 'module fonction FSN (sf/ us / er / oa ... )',
  `login` varchar(8) COLLATE utf8_unicode_ci NOT NULL COMMENT 'login utilisateur ayant effectuer la modification',
  `type_action` varchar(10) CHARACTER SET utf8 NOT NULL COMMENT 'type de modification CRUD',
  `cle_metier` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_historique` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'date de modification',
  `data_modified` mediumtext COLLATE utf8_unicode_ci NOT NULL COMMENT 'information AVANT modification, au format JSON ',
  `description` text COLLATE utf8_unicode_ci COMMENT 'remarques complementaire precisant la modification',
  `entiteadmin_id` int(11) NOT NULL DEFAULT '1' COMMENT 'Entite Administrative de reference',
  `sitefouille_id` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Site Fouillé',
  PRIMARY KEY (`id`),
  KEY `entiteadmin_id` (`entiteadmin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31071 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fsn_historique`
--

LOCK TABLES `fsn_historique` WRITE;
/*!40000 ALTER TABLE `fsn_historique` DISABLE KEYS */;
/*!40000 ALTER TABLE `fsn_historique` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fsn_organisme`
--

DROP TABLE IF EXISTS `fsn_organisme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fsn_organisme` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identifiant',
  `nom` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `nomabrege` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `type_id` int(11) NOT NULL COMMENT 'Type organisme',
  `adresse1` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adresse2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp` int(10) DEFAULT NULL COMMENT 'Code Postale',
  `ville` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Ville',
  `pays` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'france' COMMENT 'Pays',
  `telephone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Telephone',
  `fax` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Fax',
  `email` varchar(320) CHARACTER SET armscii8 DEFAULT NULL COMMENT 'Email de Contact',
  `url` text CHARACTER SET ascii COMMENT 'url Site Web',
  `correspondant_id` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Intervenant référant',
  `description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nomabrege_UNIQUE` (`nomabrege`),
  UNIQUE KEY `nom_UNIQUE` (`nom`),
  KEY `type_id` (`type_id`),
  KEY `correspondant_id` (`correspondant_id`),
  CONSTRAINT `FK_organisme_type_id` FOREIGN KEY (`type_id`) REFERENCES `fsn_categorie` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fsn_organisme`
--

LOCK TABLES `fsn_organisme` WRITE;
/*!40000 ALTER TABLE `fsn_organisme` DISABLE KEYS */;
INSERT INTO `fsn_organisme` VALUES (1,'SOGETI France','SOGETI',2336,'','22-24 rue du Gouverneur Général Eboué 00',92130,'Issy les Moulineaux','France','','','','',NULL,'');
/*!40000 ALTER TABLE `fsn_organisme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `homogeneite`
--

DROP TABLE IF EXISTS `homogeneite`;
/*!50001 DROP VIEW IF EXISTS `homogeneite`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `homogeneite` (
  `id` tinyint NOT NULL,
  `homogeneite` tinyint NOT NULL,
  `visible` tinyint NOT NULL,
  `ordre` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ihm`
--

DROP TABLE IF EXISTS `ihm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ihm` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `NomIHM` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `Description` varchar(110) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `NomIHM_UNIQUE` (`NomIHM`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ihm`
--

LOCK TABLES `ihm` WRITE;
/*!40000 ALTER TABLE `ihm` DISABLE KEYS */;
INSERT INTO `ihm` VALUES (1,'oa-gestion','Gestion des Opérations Archéologiques'),(2,'oa-formulaire','Nouvelle et Modification Opération Archeologique'),(3,'oa-detail','Détail Opération Archeologique'),(4,'us-gestion','Gestion des Unités Stratigraphiques'),(5,'us-formulaire','Nouvelle et Modification Unité Stratigraphique'),(6,'us-detail','Détail Unité Stratigraphique'),(7,'er-gestion','Gestion des Eléments Recueillis'),(8,'er-formulaire','Nouvel et Modification Elément Recueilli'),(9,'er-detail','Détail Elément Recueilli'),(10,'phase-gestion','Gestion des Phases Chronologiques'),(11,'phase-formulaire','Nouvelle et Modification Phase Chronologiques'),(12,'phase-detail','Détail des phases Chronologiques'),(13,'intervenant-gestion','Gestion des Intervenants'),(14,'intervenant-formulaire','Nouvel et Modification Intervenant'),(15,'intervenant-detail','Détail Intervenant'),(16,'reference-gestion','Gestion des Listes de Références'),(17,'sf-gestion','Gestion des Sites Fouillés'),(18,'sf-formulaire','Nouveau et Modification Site fouillé'),(19,'sf-detail','Détail Site fouillé'),(20,'public','Restitutions de Données'),(21,'picasa','Open Picasa'),(22,'profil-gestion','[ Admin ] - Création et Suppression des Profils'),(23,'profil-formulaire','[ Admin ] - Modification des Profils'),(24,'profil-habilitation','[ Admin ] - Gestion des Habilitations'),(25,'user-gestion','[ Admin ] - Création et Suppression des Utilisateurs || [ User ] - Accès à la Modification du Mot De Passe'),(26,'user-formulaire','[ Admin ] - Modification des Utilisateurs || [ User ] - Modification du Mot De Passe'),(27,'fai-gestion','Gestion des FAI'),(28,'fai-formulaire','Nouvelle et Modification FAI'),(29,'fai-detail','Détail FAI'),(30,'sitefouille-visu','A saisir.'),(31,'oa-listsimple','A saisir.'),(32,'us-listsimple','A saisir.'),(33,'dashboard_sitefouille-list','A saisir.'),(34,'dashboard_sitefouille-show','A saisir.'),(35,'dashboard-show','A saisir.'),(36,'dashboard_us-usnetwork','A saisir.'),(37,'dashboard_oa-list','A saisir.'),(38,'dashboard_sitefouille-intervention','A saisir.'),(39,'dashboard_us-ustimeline','A saisir.'),(40,'index-debug','A saisir.'),(41,'dashboard_us-count','A saisir.'),(42,'dashboard_sitefouille-cloudpoints','A saisir.'),(43,'dashboard_elementrecueilli-count','A saisir.'),(44,'analyser_sitefouille-show','A saisir.'),(45,'analyser_sitefouille-cloudpoints','A saisir.'),(46,'analyser_us-periodetimeline','A saisir.'),(47,'analyser_elementrecueilli-fonction','A saisir.'),(48,'fsn_historique-edit','A saisir.'),(49,'fsn_categorie-gestion','A saisir.'),(50,'conteneur-gestion','A saisir.'),(51,'emplacement-gestion','A saisir.'),(52,'emplacement-getsite','A saisir.'),(53,'traitement-edit','A saisir.'),(54,'analyser_sitefouille-list','A saisir.'),(55,'fsn_historique-import','A saisir.'),(56,'traitement-delete','A saisir.'),(57,'user-listsimple','A saisir.'),(58,'fsn_entiteadmin-edit','A saisir.'),(59,'fsn_entiteadmin-delete','A saisir.'),(60,'entiteadministratif-gestion','A saisir'),(61,'organisme_fsn','A saisir'),(62,'fsn_organisme-edit','A saisir.'),(63,'fsn_organisme-delete','A saisir'),(64,'historique_fsn','Historique'),(65,'emplacement-test','A saisir.'),(66,'user-reinitmdp','Réinitialisation du mot de passe'),(67,'oa-show','A saisir.'),(68,'user-suppression','Suppression de compte Utilisateur'),(69,'us-show','A saisir.'),(70,'dashboard_us-list','A saisir.'),(71,'dashboard_us-show','A saisir.'),(72,'dashboard_elementrecueilli-list','A saisir.'),(73,'dashboard_phasechrono-list','A saisir.'),(74,'fsn_categorie-edit','A saisir.'),(75,'fsn_categorie_relation-edit','A saisir.'),(76,'dashboard-zenphotoversion','A saisir.'),(77,'user-edit','A saisir.'),(78,'fsn_categorie_relation-list','A saisir.'),(79,'elementrecueilli-edit','A saisir.'),(80,'conteneur-delete','A saisir.'),(81,'conteneur-edit','A saisir.'),(82,'dashboard_participation-show','A saisir.'),(83,'fsn_organisme-show','A saisir.'),(84,'oa-edit','A saisir.'),(85,'us-edit','A saisir.'),(86,'profil-edit','A saisir.'),(87,'profil-delete','A saisir.'),(88,'fsn_historique-listsimple','A saisir.'),(89,'sitefouille-select','A saisir.'),(90,'intervenant-edit','A saisir.'),(91,'us-delete','A saisir.'),(92,'index-setting','A saisir.'),(93,'ihm-edit','A saisir.'),(94,'sitefouille-delete','A saisir.'),(95,'connexion-logout','A saisir.'),(96,'sitefouille-edit','A saisir.'),(97,'elementrecueilli-show','A saisir.'),(98,'fai-edit','A saisir.'),(99,'homogeneite-edit','A saisir.'),(100,'sitefouille-show','A saisir.'),(101,'sitefouille-showi','A saisir.'),(102,'phasechrono-edit','A saisir.'),(103,'fsn_categorie-list','A saisir.'),(104,'intervenant-delete','A saisir.'),(105,'intervenant-show','A saisir.'),(106,'oa-delete','A saisir.'),(107,'fai-delete','A saisir.'),(108,'elementrecueilli-genererqrcode','A saisir.'),(109,'intervention_us-list','A saisir.'),(110,'topo_us-list','A saisir.'),(111,'emplacement-genererqrcode','A saisir.'),(112,'conteneur-genererqrcode','A saisir.'),(113,'fai-show','A saisir.'),(114,'phasechrono-delete','A saisir.'),(115,'phasechrono-show','A saisir.'),(116,'superposition-listposterieur','A saisir.'),(117,'connexion-login','A saisir.'),(118,'sitefouille-selection','A saisir.'),(119,'inclusion-listinclusions','A saisir.'),(120,'intervention_us-listinterventions','A saisir.'),(121,'fsn_historique-show','A saisir.'),(122,'elementrecueilli-delete','A saisir.'),(123,'conteneur-show','A saisir.'),(124,'dashboard-us','A saisir.'),(125,'dashboard-elementrecueilli','A saisir.'),(126,'user-delete','A saisir.'),(127,'user-show','A saisir.'),(128,'conteneur-genererqrcodegroupe','A saisir.'),(129,'dashboard-interventions','A saisir.'),(130,'us-showerfromus','A saisir.'),(131,'superposition-show','A saisir.'),(132,'synchronisation-show','A saisir.'),(133,'traitement-list','A saisir.');
/*!40000 ALTER TABLE `ihm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `import_tmp_us_sylvius`
--

DROP TABLE IF EXISTS `import_tmp_us_sylvius`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `import_tmp_us_sylvius` (
  `id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `identification` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `debut` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instformdeb_aaaa` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instformdeb_mm` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instformdeb_jj` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instformdeb_ad` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instformfin_aaaa` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instformfin_mm` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instformfin_jj` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instformfin_ad` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `taq_aaaa` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `taq_mm` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `taq_jj` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `taq_ad` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tpq_aaaa` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tpq_mm` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tpq_jj` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tpq_ad` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `taq` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tpq` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dureeinstformdeb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dureeinstformfin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `commentaire` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `occupation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `couleur` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `interpretation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contexte_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `consistance_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `methodefouille_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `elementmineral_tuiles` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `elementmineral_brique` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `elementorganique_charbon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `elementorganique_cendre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `elementmineral_platre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descriptionorganique` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descriptionmineral` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `materielisole` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `materielconserve_poterie` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `materielconserve_osanimal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `materielconserve_construc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `homogeneite_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sitefouille_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `import_tmp_us_sylvius`
--

LOCK TABLES `import_tmp_us_sylvius` WRITE;
/*!40000 ALTER TABLE `import_tmp_us_sylvius` DISABLE KEYS */;
/*!40000 ALTER TABLE `import_tmp_us_sylvius` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inclusion`
--

DROP TABLE IF EXISTS `inclusion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inclusion` (
  `inclusion1_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `inclusion2_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_key` (`inclusion1_id`,`inclusion2_id`),
  KEY `FK_inclusion2_us` (`inclusion2_id`),
  CONSTRAINT `FK_inclusion1_us` FOREIGN KEY (`inclusion1_id`) REFERENCES `us` (`id`),
  CONSTRAINT `FK_inclusion2_us` FOREIGN KEY (`inclusion2_id`) REFERENCES `us` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='<double-click to overwrite multiple objects>';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inclusion`
--

LOCK TABLES `inclusion` WRITE;
/*!40000 ALTER TABLE `inclusion` DISABLE KEYS */;
/*!40000 ALTER TABLE `inclusion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `interfait`
--

DROP TABLE IF EXISTS `interfait`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interfait` (
  `typeinterfait_id` int(11) NOT NULL,
  `fai_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fai_id_1` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `incertitude` tinyint(1) NOT NULL DEFAULT '0',
  `precisions` text COLLATE utf8_unicode_ci,
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_key` (`fai_id`,`fai_id_1`),
  KEY `FK_fai_id_1` (`fai_id_1`),
  CONSTRAINT `FK_fai_id` FOREIGN KEY (`fai_id`) REFERENCES `fai` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_fai_id_1` FOREIGN KEY (`fai_id_1`) REFERENCES `fai` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interfait`
--

LOCK TABLES `interfait` WRITE;
/*!40000 ALTER TABLE `interfait` DISABLE KEYS */;
/*!40000 ALTER TABLE `interfait` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `intervenant`
--

DROP TABLE IF EXISTS `intervenant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `intervenant` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `titre` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nom` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adresse` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp` int(5) DEFAULT NULL,
  `ville` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `organisme_id` int(11) unsigned NOT NULL COMMENT 'Organisme de rattachement',
  `commentaire` text COLLATE utf8_unicode_ci,
  `metier_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNI_id` (`id`),
  UNIQUE KEY `uk_nom_prenom_organisme` (`nom`,`prenom`,`organisme_id`),
  KEY `IDX_intervenant_metier_id` (`metier_id`),
  KEY `organisme_id` (`organisme_id`),
  CONSTRAINT `FK_intervenant_metier_id` FOREIGN KEY (`metier_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_intervenant_organisme_id` FOREIGN KEY (`organisme_id`) REFERENCES `fsn_organisme` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='<double-click to overwrite multiple objects>';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `intervenant`
--

LOCK TABLES `intervenant` WRITE;
/*!40000 ALTER TABLE `intervenant` DISABLE KEYS */;
INSERT INTO `intervenant` VALUES ('f197c3aa-dd69-bf8e-7eae-72b3f951d12d','Mr','Administrateur','Ad','Ministrateur',0,'','','',1,'',NULL);
/*!40000 ALTER TABLE `intervenant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `intervention_us`
--

DROP TABLE IF EXISTS `intervention_us`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `intervention_us` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `intervenant_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `us_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `debut` date DEFAULT NULL,
  `fin` date DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_intervention_us_intervenant_id` (`intervenant_id`),
  KEY `FK_interventon_us_us_id` (`us_id`),
  KEY `FK_interventon_us_type` (`type_id`),
  CONSTRAINT `FK_intervention_us_intervenant_id` FOREIGN KEY (`intervenant_id`) REFERENCES `intervenant` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_interventon_us_type` FOREIGN KEY (`type_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_interventon_us_us_id` FOREIGN KEY (`us_id`) REFERENCES `us` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `intervention_us`
--

LOCK TABLES `intervention_us` WRITE;
/*!40000 ALTER TABLE `intervention_us` DISABLE KEYS */;
/*!40000 ALTER TABLE `intervention_us` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `methodefouille`
--

DROP TABLE IF EXISTS `methodefouille`;
/*!50001 DROP VIEW IF EXISTS `methodefouille`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `methodefouille` (
  `id` tinyint NOT NULL,
  `methodefouille` tinyint NOT NULL,
  `visible` tinyint NOT NULL,
  `ordre` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `metier`
--

DROP TABLE IF EXISTS `metier`;
/*!50001 DROP VIEW IF EXISTS `metier`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `metier` (
  `id` tinyint NOT NULL,
  `metier` tinyint NOT NULL,
  `visible` tinyint NOT NULL,
  `ordre` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `nature`
--

DROP TABLE IF EXISTS `nature`;
/*!50001 DROP VIEW IF EXISTS `nature`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `nature` (
  `id` tinyint NOT NULL,
  `nature` tinyint NOT NULL,
  `visible` tinyint NOT NULL,
  `ordre` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `naturematiere`
--

DROP TABLE IF EXISTS `naturematiere`;
/*!50001 DROP VIEW IF EXISTS `naturematiere`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `naturematiere` (
  `id` tinyint NOT NULL,
  `naturematiere` tinyint NOT NULL,
  `visible` tinyint NOT NULL,
  `ordre` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `naturematiere_fonction`
--

DROP TABLE IF EXISTS `naturematiere_fonction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `naturematiere_fonction` (
  `naturematiere_id` int(11) NOT NULL,
  `fonction_id` int(11) NOT NULL,
  PRIMARY KEY (`naturematiere_id`,`fonction_id`),
  UNIQUE KEY `UNI_naturematiere_id_fonction_id` (`naturematiere_id`,`fonction_id`),
  KEY `IDX_naturematiere_fonction_naturematiere_id` (`naturematiere_id`),
  KEY `IDX_naturematiere_fonction_fonction_id` (`fonction_id`),
  CONSTRAINT `FK_naturematiere_fonction_fonction_id` FOREIGN KEY (`fonction_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_naturematiere_fonction_naturematiere_id` FOREIGN KEY (`naturematiere_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='<double-click to overwrite multiple objects>';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `naturematiere_fonction`
--

LOCK TABLES `naturematiere_fonction` WRITE;
/*!40000 ALTER TABLE `naturematiere_fonction` DISABLE KEYS */;
/*!40000 ALTER TABLE `naturematiere_fonction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oa`
--

DROP TABLE IF EXISTS `oa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oa` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sitefouille_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `identification` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `arretedesignation` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `arreteprescription` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `debut` date NOT NULL,
  `fin` date NOT NULL,
  `oastatut_id` int(11) DEFAULT NULL,
  `nature_id` int(11) DEFAULT NULL,
  `conditionfouille` text COLLATE utf8_unicode_ci,
  `organismeratachement` varchar(80) COLLATE utf8_unicode_ci DEFAULT 'Unité d''archeologie de la ville de Saint-Denis',
  `maitriseouvrage` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `surfacefouille` int(11) DEFAULT NULL,
  `commentaire` text COLLATE utf8_unicode_ci,
  `entiteadmin_id` int(11) NOT NULL COMMENT 'Entite Administrative de reference',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNI_oa_id` (`id`),
  UNIQUE KEY `UNI_oa_identification` (`identification`),
  KEY `IDX_oa_sitefouille_id` (`sitefouille_id`),
  KEY `IDX_oa_raisonurgence_id` (`oastatut_id`),
  KEY `IDX_oa_nature_id` (`nature_id`),
  KEY `entiteadmin_id` (`entiteadmin_id`),
  CONSTRAINT `FK_oa_nature_id` FOREIGN KEY (`nature_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_oa_oastatut_id` FOREIGN KEY (`oastatut_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_oa_sitefouille_id` FOREIGN KEY (`sitefouille_id`) REFERENCES `sitefouille` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='<double-click to overwrite multiple objects>';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oa`
--

LOCK TABLES `oa` WRITE;
/*!40000 ALTER TABLE `oa` DISABLE KEYS */;
/*!40000 ALTER TABLE `oa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oa_doc`
--

DROP TABLE IF EXISTS `oa_doc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oa_doc` (
  `oa_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `doc_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`oa_id`,`doc_id`),
  UNIQUE KEY `UNI_oa_id_doc_id` (`oa_id`,`doc_id`),
  KEY `IDX_oa_doc_oa_id` (`oa_id`),
  KEY `IDX_oa_doc_doc_id` (`doc_id`),
  CONSTRAINT `FK_oa_doc_doc` FOREIGN KEY (`doc_id`) REFERENCES `document` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_oa_doc_oa` FOREIGN KEY (`oa_id`) REFERENCES `oa` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oa_doc`
--

LOCK TABLES `oa_doc` WRITE;
/*!40000 ALTER TABLE `oa_doc` DISABLE KEYS */;
/*!40000 ALTER TABLE `oa_doc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `oastatut`
--

DROP TABLE IF EXISTS `oastatut`;
/*!50001 DROP VIEW IF EXISTS `oastatut`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `oastatut` (
  `id` tinyint NOT NULL,
  `oastatut` tinyint NOT NULL,
  `visible` tinyint NOT NULL,
  `ordre` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `participation`
--

DROP TABLE IF EXISTS `participation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participation` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `intervenant_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `oa_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `debut` date NOT NULL,
  `fin` date NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_participation_intervenant_id` (`intervenant_id`),
  KEY `FK_participation_oa_id` (`oa_id`),
  KEY `FK_participation_role_id` (`role_id`),
  CONSTRAINT `FK_participation_intervenant_id` FOREIGN KEY (`intervenant_id`) REFERENCES `intervenant` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_participation_oa_id` FOREIGN KEY (`oa_id`) REFERENCES `oa` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_participation_role_id` FOREIGN KEY (`role_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participation`
--

LOCK TABLES `participation` WRITE;
/*!40000 ALTER TABLE `participation` DISABLE KEYS */;
/*!40000 ALTER TABLE `participation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phasechrono`
--

DROP TABLE IF EXISTS `phasechrono`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phasechrono` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `identification` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `datatdebbas_aaaa` int(4) DEFAULT NULL,
  `datatdebhaut_aaaa` int(4) DEFAULT NULL,
  `datatfinbas_aaaa` int(4) DEFAULT NULL,
  `datatfinhaut_aaaa` int(4) DEFAULT NULL,
  `episodeurbain_id` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sitefouille_id` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `episode_urbain` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNI_id` (`id`),
  UNIQUE KEY `uk_identification_sitefouille` (`identification`,`sitefouille_id`),
  KEY `IDX_phasechrono_episodeurbain_id` (`episodeurbain_id`),
  KEY `FK_phasechrono_sitefouille_idx` (`sitefouille_id`),
  CONSTRAINT `FK_phasechrono_episodeurbain` FOREIGN KEY (`episodeurbain_id`) REFERENCES `episodeurbain` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_phasechrono_sitefouille` FOREIGN KEY (`sitefouille_id`) REFERENCES `sitefouille` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='<double-click to overwrite multiple objects>';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phasechrono`
--

LOCK TABLES `phasechrono` WRITE;
/*!40000 ALTER TABLE `phasechrono` DISABLE KEYS */;
/*!40000 ALTER TABLE `phasechrono` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `precisionproduction`
--

DROP TABLE IF EXISTS `precisionproduction`;
/*!50001 DROP VIEW IF EXISTS `precisionproduction`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `precisionproduction` (
  `id` tinyint NOT NULL,
  `precision` tinyint NOT NULL,
  `visible` tinyint NOT NULL,
  `ordre` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `prod_precisionprod`
--

DROP TABLE IF EXISTS `prod_precisionprod`;
/*!50001 DROP VIEW IF EXISTS `prod_precisionprod`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `prod_precisionprod` (
  `prod_id` tinyint NOT NULL,
  `precisionprod_id` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `production`
--

DROP TABLE IF EXISTS `production`;
/*!50001 DROP VIEW IF EXISTS `production`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `production` (
  `id` tinyint NOT NULL,
  `production` tinyint NOT NULL,
  `visible` tinyint NOT NULL,
  `ordre` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `profil`
--

DROP TABLE IF EXISTS `profil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profil` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nomprofil` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Description` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `uk_Nomprofil` (`Nomprofil`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profil`
--

LOCK TABLES `profil` WRITE;
/*!40000 ALTER TABLE `profil` DISABLE KEYS */;
INSERT INTO `profil` VALUES (1,'Administrateur','Administrateur'),(2,'Fouilleur','Fouilleur'),(4,'Gestionnaire de collection','Gestionnaire de collection'),(5,'Fouilleur benevole','Fouilleur bénévole'),(7,'profil_test','mon profil à moi'),(8,'mon profil ','mon profil à moi ');
/*!40000 ALTER TABLE `profil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profil_ihm`
--

DROP TABLE IF EXISTS `profil_ihm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profil_ihm` (
  `profil_id` int(11) NOT NULL,
  `ihm_id` int(11) NOT NULL,
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_key` (`profil_id`,`ihm_id`),
  KEY `FK_profil_ihm_ihm_id_idx` (`ihm_id`),
  CONSTRAINT `FK_profil_ihm_ihm_id` FOREIGN KEY (`ihm_id`) REFERENCES `ihm` (`Id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_profil_ihm_profil_id` FOREIGN KEY (`profil_id`) REFERENCES `profil` (`Id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profil_ihm`
--

LOCK TABLES `profil_ihm` WRITE;
/*!40000 ALTER TABLE `profil_ihm` DISABLE KEYS */;
INSERT INTO `profil_ihm` VALUES (1,1,'0ce10018-91cc-11e5-8f2e-54ee75347586'),(1,2,'0ce10390-91cc-11e5-8f2e-54ee75347586'),(1,3,'0ce10453-91cc-11e5-8f2e-54ee75347586'),(1,4,'0ce104e2-91cc-11e5-8f2e-54ee75347586'),(1,5,'0ce10568-91cc-11e5-8f2e-54ee75347586'),(1,6,'0ce105e3-91cc-11e5-8f2e-54ee75347586'),(1,7,'0ce10659-91cc-11e5-8f2e-54ee75347586'),(1,8,'0ce106cf-91cc-11e5-8f2e-54ee75347586'),(1,9,'0ce10745-91cc-11e5-8f2e-54ee75347586'),(1,10,'0ce107bb-91cc-11e5-8f2e-54ee75347586'),(1,11,'0ce10827-91cc-11e5-8f2e-54ee75347586'),(1,12,'0ce10893-91cc-11e5-8f2e-54ee75347586'),(1,13,'0ce108f9-91cc-11e5-8f2e-54ee75347586'),(1,14,'0ce10c95-91cc-11e5-8f2e-54ee75347586'),(1,15,'0ce10d43-91cc-11e5-8f2e-54ee75347586'),(1,16,'0ce10db4-91cc-11e5-8f2e-54ee75347586'),(1,17,'0ce10e20-91cc-11e5-8f2e-54ee75347586'),(1,18,'0ce10e91-91cc-11e5-8f2e-54ee75347586'),(1,19,'0ce10efd-91cc-11e5-8f2e-54ee75347586'),(1,20,'0ce10f6e-91cc-11e5-8f2e-54ee75347586'),(1,21,'0ce10fd4-91cc-11e5-8f2e-54ee75347586'),(1,22,'0ce11040-91cc-11e5-8f2e-54ee75347586'),(1,23,'0ce110a7-91cc-11e5-8f2e-54ee75347586'),(1,24,'0ce1110d-91cc-11e5-8f2e-54ee75347586'),(1,25,'0ce11174-91cc-11e5-8f2e-54ee75347586'),(1,26,'0ce111db-91cc-11e5-8f2e-54ee75347586'),(1,27,'0ce11246-91cc-11e5-8f2e-54ee75347586'),(1,28,'0ce112ad-91cc-11e5-8f2e-54ee75347586'),(1,29,'0ce11314-91cc-11e5-8f2e-54ee75347586'),(1,30,'0ce1137a-91cc-11e5-8f2e-54ee75347586'),(1,31,'0ce113e1-91cc-11e5-8f2e-54ee75347586'),(1,32,'0ce11448-91cc-11e5-8f2e-54ee75347586'),(1,33,'0ce114a9-91cc-11e5-8f2e-54ee75347586'),(1,34,'0ce11515-91cc-11e5-8f2e-54ee75347586'),(1,35,'0ce11571-91cc-11e5-8f2e-54ee75347586'),(1,36,'0ce115d8-91cc-11e5-8f2e-54ee75347586'),(1,37,'0ce11639-91cc-11e5-8f2e-54ee75347586'),(1,38,'0ce116a0-91cc-11e5-8f2e-54ee75347586'),(1,39,'0ce11701-91cc-11e5-8f2e-54ee75347586'),(1,40,'0ce11768-91cc-11e5-8f2e-54ee75347586'),(1,41,'0ce117ca-91cc-11e5-8f2e-54ee75347586'),(1,42,'0ce11830-91cc-11e5-8f2e-54ee75347586'),(1,43,'0ce11897-91cc-11e5-8f2e-54ee75347586'),(1,44,'0ce118fe-91cc-11e5-8f2e-54ee75347586'),(1,45,'0ce11969-91cc-11e5-8f2e-54ee75347586'),(1,46,'0ce119d0-91cc-11e5-8f2e-54ee75347586'),(1,47,'0ce11a37-91cc-11e5-8f2e-54ee75347586'),(1,48,'0ce11a98-91cc-11e5-8f2e-54ee75347586'),(1,49,'0ce11aff-91cc-11e5-8f2e-54ee75347586'),(1,50,'0ce11b6a-91cc-11e5-8f2e-54ee75347586'),(1,51,'0ce11bd1-91cc-11e5-8f2e-54ee75347586'),(1,52,'0ce11c3d-91cc-11e5-8f2e-54ee75347586'),(1,53,'0ce11ca3-91cc-11e5-8f2e-54ee75347586'),(1,54,'0ce11d14-91cc-11e5-8f2e-54ee75347586'),(1,55,'0ce11d7b-91cc-11e5-8f2e-54ee75347586'),(1,56,'0ce11de2-91cc-11e5-8f2e-54ee75347586'),(1,57,'0ce11e53-91cc-11e5-8f2e-54ee75347586'),(1,58,'0ce11eb9-91cc-11e5-8f2e-54ee75347586'),(1,59,'0ce11f20-91cc-11e5-8f2e-54ee75347586'),(1,60,'0ce11f8c-91cc-11e5-8f2e-54ee75347586'),(1,61,'0ce11ff7-91cc-11e5-8f2e-54ee75347586'),(1,62,'0ce12063-91cc-11e5-8f2e-54ee75347586'),(1,63,'0ce120cf-91cc-11e5-8f2e-54ee75347586'),(1,64,'0ce1213b-91cc-11e5-8f2e-54ee75347586'),(1,65,'0ce121a6-91cc-11e5-8f2e-54ee75347586'),(1,66,'0ce1221c-91cc-11e5-8f2e-54ee75347586'),(1,67,'0ce12341-91cc-11e5-8f2e-54ee75347586'),(1,68,'0ce123c6-91cc-11e5-8f2e-54ee75347586'),(1,69,'0ce1243c-91cc-11e5-8f2e-54ee75347586'),(1,70,'0ce124a8-91cc-11e5-8f2e-54ee75347586'),(1,71,'0ce12921-91cc-11e5-8f2e-54ee75347586'),(1,72,'0ce129cf-91cc-11e5-8f2e-54ee75347586'),(1,73,'0ce12a4a-91cc-11e5-8f2e-54ee75347586'),(1,74,'0ce12abb-91cc-11e5-8f2e-54ee75347586'),(1,75,'0ce12b31-91cc-11e5-8f2e-54ee75347586'),(1,76,'0ce12b9d-91cc-11e5-8f2e-54ee75347586'),(1,77,'0ce12c09-91cc-11e5-8f2e-54ee75347586'),(1,78,'0ce12c74-91cc-11e5-8f2e-54ee75347586'),(1,79,'0ce12ce5-91cc-11e5-8f2e-54ee75347586'),(1,80,'0ce12d56-91cc-11e5-8f2e-54ee75347586'),(1,81,'0ce12dc2-91cc-11e5-8f2e-54ee75347586'),(1,82,'0ce12e33-91cc-11e5-8f2e-54ee75347586'),(1,83,'0ce12ea4-91cc-11e5-8f2e-54ee75347586'),(1,84,'0ce12f0a-91cc-11e5-8f2e-54ee75347586'),(1,85,'0ce12f76-91cc-11e5-8f2e-54ee75347586'),(1,86,'0ce12fe2-91cc-11e5-8f2e-54ee75347586'),(1,87,'0ce1304e-91cc-11e5-8f2e-54ee75347586'),(1,88,'0ce130b9-91cc-11e5-8f2e-54ee75347586'),(1,89,'0ce13130-91cc-11e5-8f2e-54ee75347586'),(1,90,'0ce1319b-91cc-11e5-8f2e-54ee75347586'),(1,128,''),(2,1,'0ce1320c-91cc-11e5-8f2e-54ee75347586'),(2,3,'0ce13282-91cc-11e5-8f2e-54ee75347586'),(2,4,'0ce132ee-91cc-11e5-8f2e-54ee75347586'),(2,5,'0ce1335a-91cc-11e5-8f2e-54ee75347586'),(2,6,'0ce133c6-91cc-11e5-8f2e-54ee75347586'),(2,7,'0ce13436-91cc-11e5-8f2e-54ee75347586'),(2,8,'0ce1373d-91cc-11e5-8f2e-54ee75347586'),(2,9,'0ce137cd-91cc-11e5-8f2e-54ee75347586'),(2,10,'0ce13839-91cc-11e5-8f2e-54ee75347586'),(2,11,'0ce138af-91cc-11e5-8f2e-54ee75347586'),(2,12,'0ce1391b-91cc-11e5-8f2e-54ee75347586'),(2,13,'0ce13981-91cc-11e5-8f2e-54ee75347586'),(2,14,'0ce139ed-91cc-11e5-8f2e-54ee75347586'),(2,15,'0ce13a5e-91cc-11e5-8f2e-54ee75347586'),(2,17,'0ce13aca-91cc-11e5-8f2e-54ee75347586'),(2,19,'0ce13b35-91cc-11e5-8f2e-54ee75347586'),(2,20,'0ce13ba6-91cc-11e5-8f2e-54ee75347586'),(2,21,'0ce13c12-91cc-11e5-8f2e-54ee75347586'),(2,25,'0ce13c79-91cc-11e5-8f2e-54ee75347586'),(2,26,'0ce13ce4-91cc-11e5-8f2e-54ee75347586'),(2,27,'0ce13d50-91cc-11e5-8f2e-54ee75347586'),(2,28,'0ce13dbc-91cc-11e5-8f2e-54ee75347586'),(2,29,'0ce13e28-91cc-11e5-8f2e-54ee75347586'),(2,64,'0ce13e99-91cc-11e5-8f2e-54ee75347586'),(4,26,'0ce13f04-91cc-11e5-8f2e-54ee75347586'),(4,27,'0ce13f75-91cc-11e5-8f2e-54ee75347586'),(5,26,'0ce13fe1-91cc-11e5-8f2e-54ee75347586'),(5,27,'0ce14052-91cc-11e5-8f2e-54ee75347586'),(7,1,'0ce140c3-91cc-11e5-8f2e-54ee75347586'),(7,2,'0ce14139-91cc-11e5-8f2e-54ee75347586');
/*!40000 ALTER TABLE `profil_ihm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `releve`
--

DROP TABLE IF EXISTS `releve`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `releve` (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `croquis_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `sitefouille_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_releve_croquis` (`croquis_id`),
  KEY `idx_releve_sitefouille` (`sitefouille_id`),
  CONSTRAINT `FK_releve_croquis` FOREIGN KEY (`croquis_id`) REFERENCES `croquis` (`id`),
  CONSTRAINT `FK_releve_sitefouille` FOREIGN KEY (`sitefouille_id`) REFERENCES `sitefouille` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `releve`
--

LOCK TABLES `releve` WRITE;
/*!40000 ALTER TABLE `releve` DISABLE KEYS */;
/*!40000 ALTER TABLE `releve` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requeteur`
--

DROP TABLE IF EXISTS `requeteur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `requeteur` (
  `id` varchar(38) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `titre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `critere` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `requete` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `modified` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNI_requeteur_user_id_titre` (`user_id`,`titre`),
  CONSTRAINT `FK_REQUETEUR_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requeteur`
--

LOCK TABLES `requeteur` WRITE;
/*!40000 ALTER TABLE `requeteur` DISABLE KEYS */;
/*!40000 ALTER TABLE `requeteur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `role`
--

DROP TABLE IF EXISTS `role`;
/*!50001 DROP VIEW IF EXISTS `role`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `role` (
  `id` tinyint NOT NULL,
  `role` tinyint NOT NULL,
  `visible` tinyint NOT NULL,
  `ordre` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `sitefouille`
--

DROP TABLE IF EXISTS `sitefouille`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitefouille` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `nom` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `nomabrege` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `numerosite` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `departement` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `commune` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adresse` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `anneecadastre` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sectionparcelle` text COLLATE utf8_unicode_ci,
  `proprietaire` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `protectionjuridique` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `surfaceestime` int(10) unsigned DEFAULT NULL,
  `commentaire` text COLLATE utf8_unicode_ci,
  `lambert_x` decimal(10,3) DEFAULT NULL,
  `lambert_y` decimal(10,3) DEFAULT NULL,
  `altitude` decimal(6,2) DEFAULT NULL,
  `fsn_entiteadmin_id` int(11) NOT NULL DEFAULT '1',
  `datation_debut` int(11) NOT NULL DEFAULT '-5000',
  `datation_fin` int(11) NOT NULL DEFAULT '2015',
  `bary_x` float DEFAULT NULL,
  `bary_y` float DEFAULT NULL,
  `bary_z` float DEFAULT NULL,
  `contour` polygon DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNI_id` (`id`),
  UNIQUE KEY `UNI_nomabrege` (`nomabrege`),
  UNIQUE KEY `UNI_nom` (`nom`),
  UNIQUE KEY `UNI_numerosite` (`numerosite`),
  KEY `fk_sitefouille_fsn_entiteadmin_id_idx` (`fsn_entiteadmin_id`),
  CONSTRAINT `fk_sitefouille_fsn_entiteadmin_id` FOREIGN KEY (`fsn_entiteadmin_id`) REFERENCES `fsn_entiteadmin` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='<double-click to overwrite multiple objects>';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitefouille`
--

LOCK TABLES `sitefouille` WRITE;
/*!40000 ALTER TABLE `sitefouille` DISABLE KEYS */;
/*!40000 ALTER TABLE `sitefouille` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitefouille_zpwg_images`
--

DROP TABLE IF EXISTS `sitefouille_zpwg_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitefouille_zpwg_images` (
  `id` varchar(36) NOT NULL,
  `zpwg_images_id` mediumint(8) unsigned NOT NULL,
  `sitefouille_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sitefouille_zpwg_images_UNI1` (`zpwg_images_id`,`sitefouille_id`),
  KEY `sitefouille_zpwg_images_sitefouille_id_idx` (`sitefouille_id`),
  CONSTRAINT `sitefouille_zpwg_images_sitefouille_id` FOREIGN KEY (`sitefouille_id`) REFERENCES `sitefouille` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `sitefouille_zpwg_images_zpwg_images_id` FOREIGN KEY (`zpwg_images_id`) REFERENCES `zpwg_images` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitefouille_zpwg_images`
--

LOCK TABLES `sitefouille_zpwg_images` WRITE;
/*!40000 ALTER TABLE `sitefouille_zpwg_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `sitefouille_zpwg_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `sptype`
--

DROP TABLE IF EXISTS `sptype`;
/*!50001 DROP VIEW IF EXISTS `sptype`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `sptype` (
  `id` tinyint NOT NULL,
  `sptype` tinyint NOT NULL,
  `visible` tinyint NOT NULL,
  `ordre` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `statut`
--

DROP TABLE IF EXISTS `statut`;
/*!50001 DROP VIEW IF EXISTS `statut`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `statut` (
  `id` tinyint NOT NULL,
  `statut` tinyint NOT NULL,
  `visible` tinyint NOT NULL,
  `ordre` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `superposition`
--

DROP TABLE IF EXISTS `superposition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `superposition` (
  `anterieur_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `posterieur_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `incertitude` tinyint(1) NOT NULL DEFAULT '0',
  `sptype_id` int(11) DEFAULT NULL,
  `observation` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `duree_min_certain` int(11) DEFAULT NULL,
  `duree_max_certain` int(11) DEFAULT NULL,
  `duree_min_estim` int(11) DEFAULT NULL,
  `duree_max_estim` int(11) DEFAULT NULL,
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_key` (`anterieur_id`,`posterieur_id`),
  KEY `IDX_superposition_anterieur_id` (`anterieur_id`),
  KEY `IDX_superposition_posterieur_id` (`posterieur_id`),
  KEY `IDX_superposition_sptype_id` (`sptype_id`),
  CONSTRAINT `FK_superposition_anterieur_id` FOREIGN KEY (`anterieur_id`) REFERENCES `us` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_superposition_posterieur_id` FOREIGN KEY (`posterieur_id`) REFERENCES `us` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_superposition_sptype` FOREIGN KEY (`sptype_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `superposition`
--

LOCK TABLES `superposition` WRITE;
/*!40000 ALTER TABLE `superposition` DISABLE KEYS */;
/*!40000 ALTER TABLE `superposition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `synchronisation`
--

DROP TABLE IF EXISTS `synchronisation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `synchronisation` (
  `synchro1_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `synchro2_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `incertitude` tinyint(1) NOT NULL DEFAULT '0',
  `synchrotype_id` int(11) DEFAULT NULL,
  `observation` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_key` (`synchro1_id`,`synchro2_id`),
  KEY `IDX_synchronisation_synchrotype_id` (`synchrotype_id`),
  KEY `FK_synchro2_us` (`synchro2_id`),
  CONSTRAINT `FK_synchro1_us` FOREIGN KEY (`synchro1_id`) REFERENCES `us` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_synchro2_us` FOREIGN KEY (`synchro2_id`) REFERENCES `us` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_synchro_synchrotype` FOREIGN KEY (`synchrotype_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='<double-click to overwrite multiple objects>';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `synchronisation`
--

LOCK TABLES `synchronisation` WRITE;
/*!40000 ALTER TABLE `synchronisation` DISABLE KEYS */;
/*!40000 ALTER TABLE `synchronisation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `synchrotype`
--

DROP TABLE IF EXISTS `synchrotype`;
/*!50001 DROP VIEW IF EXISTS `synchrotype`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `synchrotype` (
  `id` tinyint NOT NULL,
  `synchrotype` tinyint NOT NULL,
  `visible` tinyint NOT NULL,
  `ordre` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `topo_er`
--

DROP TABLE IF EXISTS `topo_er`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topo_er` (
  `er_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `topo_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`er_id`,`topo_id`),
  UNIQUE KEY `UNI_er_id_topo_id` (`er_id`,`topo_id`),
  KEY `IDX_topo_er_topo_id` (`topo_id`),
  KEY `IDX_topo_er_er_id` (`er_id`),
  CONSTRAINT `FK_topo_er_er_id` FOREIGN KEY (`er_id`) REFERENCES `elementrecueilli` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_topo_er_topo_id` FOREIGN KEY (`topo_id`) REFERENCES `topographie` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `topo_er`
--

LOCK TABLES `topo_er` WRITE;
/*!40000 ALTER TABLE `topo_er` DISABLE KEYS */;
/*!40000 ALTER TABLE `topo_er` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `topo_sf`
--

DROP TABLE IF EXISTS `topo_sf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topo_sf` (
  `sitefouille_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `topo_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`sitefouille_id`,`topo_id`),
  UNIQUE KEY `UNI_sitefouille_id_topo_id` (`sitefouille_id`,`topo_id`),
  KEY `IDX_topo_sf_topo_id` (`topo_id`),
  KEY `IDX_topo_sf_sitefouille_id` (`sitefouille_id`),
  CONSTRAINT `FK_position_sitefouille` FOREIGN KEY (`sitefouille_id`) REFERENCES `sitefouille` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_position_topo` FOREIGN KEY (`topo_id`) REFERENCES `topographie` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `topo_sf`
--

LOCK TABLES `topo_sf` WRITE;
/*!40000 ALTER TABLE `topo_sf` DISABLE KEYS */;
/*!40000 ALTER TABLE `topo_sf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `topo_us`
--

DROP TABLE IF EXISTS `topo_us`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topo_us` (
  `us_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `topo_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`us_id`,`topo_id`),
  UNIQUE KEY `UNI_us_id_topo_id` (`us_id`,`topo_id`),
  KEY `IDX_topo_us_topo_id` (`topo_id`),
  KEY `IDX_topo_us_us_id` (`us_id`),
  CONSTRAINT `FK_topo_us_topo_id` FOREIGN KEY (`topo_id`) REFERENCES `topographie` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_topo_us_us_id` FOREIGN KEY (`us_id`) REFERENCES `us` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `topo_us`
--

LOCK TABLES `topo_us` WRITE;
/*!40000 ALTER TABLE `topo_us` DISABLE KEYS */;
/*!40000 ALTER TABLE `topo_us` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `topographie`
--

DROP TABLE IF EXISTS `topographie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topographie` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `x` float DEFAULT NULL,
  `y` float DEFAULT NULL,
  `z` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNI_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='<double-click to overwrite multiple objects>';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `topographie`
--

LOCK TABLES `topographie` WRITE;
/*!40000 ALTER TABLE `topographie` DISABLE KEYS */;
/*!40000 ALTER TABLE `topographie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trait_inter`
--

DROP TABLE IF EXISTS `trait_inter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trait_inter` (
  `trait_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `inter_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_key` (`trait_id`,`inter_id`),
  KEY `IDX_trait_inter_inter_id` (`inter_id`),
  KEY `IDX_trait_inter_trait_id` (`trait_id`),
  CONSTRAINT `FK_trait_inter_intervenant` FOREIGN KEY (`inter_id`) REFERENCES `intervenant` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trait_inter`
--

LOCK TABLES `trait_inter` WRITE;
/*!40000 ALTER TABLE `trait_inter` DISABLE KEYS */;
/*!40000 ALTER TABLE `trait_inter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `traitement`
--

DROP TABLE IF EXISTS `traitement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traitement` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `nom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ref_labo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `debut` date DEFAULT NULL,
  `fin` date DEFAULT NULL,
  `lieu` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `observation` text COLLATE utf8_unicode_ci,
  `etat_id` int(11) NOT NULL,
  `intervenant_id` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `er_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNI_id` (`id`),
  UNIQUE KEY `uni_nom` (`nom`),
  KEY `IDX_traitement_type_id` (`type_id`),
  KEY `IDX_traitement_etat_id` (`etat_id`),
  KEY `FK_traitement_er_id` (`er_id`),
  KEY `FK_traitement_intervenant_id_idx` (`intervenant_id`),
  CONSTRAINT `FK_traitement_er_id` FOREIGN KEY (`er_id`) REFERENCES `elementrecueilli` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_traitement_etat_id` FOREIGN KEY (`etat_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_traitement_intervenant_id` FOREIGN KEY (`intervenant_id`) REFERENCES `intervenant` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_traitement_type_id` FOREIGN KEY (`type_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `traitement`
--

LOCK TABLES `traitement` WRITE;
/*!40000 ALTER TABLE `traitement` DISABLE KEYS */;
/*!40000 ALTER TABLE `traitement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `typeasso`
--

DROP TABLE IF EXISTS `typeasso`;
/*!50001 DROP VIEW IF EXISTS `typeasso`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `typeasso` (
  `id` tinyint NOT NULL,
  `type` tinyint NOT NULL,
  `visible` tinyint NOT NULL,
  `ordre` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `typedocument`
--

DROP TABLE IF EXISTS `typedocument`;
/*!50001 DROP VIEW IF EXISTS `typedocument`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `typedocument` (
  `id` tinyint NOT NULL,
  `typedoc` tinyint NOT NULL,
  `visible` tinyint NOT NULL,
  `ordre` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `typeintervention`
--

DROP TABLE IF EXISTS `typeintervention`;
/*!50001 DROP VIEW IF EXISTS `typeintervention`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `typeintervention` (
  `id` tinyint NOT NULL,
  `typeinterv` tinyint NOT NULL,
  `visible` tinyint NOT NULL,
  `ordre` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `typetraitement`
--

DROP TABLE IF EXISTS `typetraitement`;
/*!50001 DROP VIEW IF EXISTS `typetraitement`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `typetraitement` (
  `id` tinyint NOT NULL,
  `type` tinyint NOT NULL,
  `visible` tinyint NOT NULL,
  `ordre` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `us`
--

DROP TABLE IF EXISTS `us`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `us` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `identification` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `debut` date DEFAULT NULL,
  `fin` date DEFAULT NULL,
  `commentaire` text COLLATE utf8_unicode_ci,
  `occupation` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `couleur` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `interpretation` text COLLATE utf8_unicode_ci,
  `contexte_id` int(11) DEFAULT NULL,
  `consistance_id` int(11) DEFAULT NULL,
  `methodefouille_id` int(11) DEFAULT NULL,
  `elementmineral_tuiles` tinyint(1) DEFAULT NULL,
  `elementmineral_brique` tinyint(1) DEFAULT NULL,
  `elementorganique_charbon` tinyint(1) DEFAULT NULL,
  `elementorganique_cendre` tinyint(1) DEFAULT NULL,
  `elementmineral_platre` tinyint(1) DEFAULT NULL,
  `descriptionorganique` text COLLATE utf8_unicode_ci,
  `descriptionmineral` text COLLATE utf8_unicode_ci,
  `materielisole` tinyint(1) DEFAULT NULL,
  `materielconserve_poterie` tinyint(1) DEFAULT NULL,
  `materielconserve_osanimal` tinyint(1) DEFAULT NULL,
  `materielconserve_construc` tinyint(1) DEFAULT NULL,
  `homogeneite_id` int(11) DEFAULT NULL,
  `sitefouille_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `phase_id` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `taq_certain` int(11) NOT NULL DEFAULT '2015',
  `tpq_certain` int(11) DEFAULT NULL,
  `inst_org_plaf_certain` int(11) DEFAULT NULL,
  `inst_org_planch_certain` int(11) NOT NULL DEFAULT '-6000',
  `duree_min_certain` int(11) DEFAULT NULL,
  `duree_max_certain` int(11) DEFAULT NULL,
  `taq_estim` int(11) DEFAULT NULL,
  `tpq_estim` int(11) DEFAULT NULL,
  `inst_org_plaf_estim` int(11) DEFAULT NULL,
  `inst_org_planch_estim` int(11) DEFAULT NULL,
  `duree_min_estim` int(11) DEFAULT NULL,
  `duree_max_estim` int(11) DEFAULT NULL,
  `croquis_id` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bary_x` float DEFAULT NULL,
  `bary_y` float DEFAULT NULL,
  `bary_z` float DEFAULT NULL,
  `contour` polygon DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNI_id` (`id`),
  UNIQUE KEY `identification_UNIQUE` (`identification`),
  KEY `IDX_us_typeus_id` (`contexte_id`),
  KEY `IDX_us_constituant_id` (`consistance_id`),
  KEY `IDX_us_espacefouille_id` (`sitefouille_id`),
  KEY `IDX_us_methodefouille_id` (`methodefouille_id`),
  KEY `IDX_us_homogeneite_id` (`homogeneite_id`),
  KEY `idx_us_croquis` (`croquis_id`),
  KEY `IDX_us_phase_id` (`phase_id`),
  CONSTRAINT `FK_us_constituant_id` FOREIGN KEY (`consistance_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_us_contexte_id` FOREIGN KEY (`contexte_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_us_croquis` FOREIGN KEY (`croquis_id`) REFERENCES `croquis` (`id`),
  CONSTRAINT `FK_us_homogeneite_id` FOREIGN KEY (`homogeneite_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_us_methodefouille_id` FOREIGN KEY (`methodefouille_id`) REFERENCES `fsn_categorie` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_us_phase_id` FOREIGN KEY (`phase_id`) REFERENCES `phasechrono` (`id`),
  CONSTRAINT `FK_us_sitefouille_id` FOREIGN KEY (`sitefouille_id`) REFERENCES `sitefouille` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `us`
--

LOCK TABLES `us` WRITE;
/*!40000 ALTER TABLE `us` DISABLE KEYS */;
/*!40000 ALTER TABLE `us` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `us_doc`
--

DROP TABLE IF EXISTS `us_doc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `us_doc` (
  `us_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `doc_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`us_id`,`doc_id`),
  UNIQUE KEY `UNI_us_id_doc_us` (`us_id`,`doc_id`),
  KEY `IDX_us_doc_doc_id` (`doc_id`),
  KEY `IDX_us_doc_us_id` (`us_id`),
  CONSTRAINT `FK_us_doc_doc_id` FOREIGN KEY (`doc_id`) REFERENCES `document` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_us_doc_us_id` FOREIGN KEY (`us_id`) REFERENCES `us` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `us_doc`
--

LOCK TABLES `us_doc` WRITE;
/*!40000 ALTER TABLE `us_doc` DISABLE KEYS */;
/*!40000 ALTER TABLE `us_doc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `us_oa`
--

DROP TABLE IF EXISTS `us_oa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `us_oa` (
  `us_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `oa_id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`us_id`,`oa_id`),
  UNIQUE KEY `UNI_us_id_oa_id` (`us_id`,`oa_id`),
  KEY `FK_oa_id` (`oa_id`),
  CONSTRAINT `FK_oa_id` FOREIGN KEY (`oa_id`) REFERENCES `oa` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_us_id` FOREIGN KEY (`us_id`) REFERENCES `us` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `us_oa`
--

LOCK TABLES `us_oa` WRITE;
/*!40000 ALTER TABLE `us_oa` DISABLE KEYS */;
/*!40000 ALTER TABLE `us_oa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `us_zpwg_images`
--

DROP TABLE IF EXISTS `us_zpwg_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `us_zpwg_images` (
  `id` varchar(36) NOT NULL,
  `zpwg_images_id` mediumint(8) unsigned NOT NULL,
  `us_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `us_zpwg_images_UNI1` (`zpwg_images_id`,`us_id`),
  KEY `us_zpwg_images_us_id_idx` (`us_id`),
  CONSTRAINT `us_zpwg_images_us_id` FOREIGN KEY (`us_id`) REFERENCES `us` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `us_zpwg_images_zpwg_images_id` FOREIGN KEY (`zpwg_images_id`) REFERENCES `zpwg_images` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `us_zpwg_images`
--

LOCK TABLES `us_zpwg_images` WRITE;
/*!40000 ALTER TABLE `us_zpwg_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `us_zpwg_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `mdp` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `nom` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prenom` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profil_id` int(11) DEFAULT NULL,
  `entiteadmin_id` int(11) NOT NULL COMMENT 'Entite Administrative de reference',
  `intervenant_id` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Information Complementaire',
  `sitefouille_id` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNI_id` (`id`),
  UNIQUE KEY `uk_login` (`login`),
  KEY `FK_user_profil_id_idx` (`profil_id`),
  KEY `entiteadmin_id` (`entiteadmin_id`),
  KEY `FK_user_intervenantl_id_idx` (`intervenant_id`),
  CONSTRAINT `FK_user_intervenantl_id` FOREIGN KEY (`intervenant_id`) REFERENCES `intervenant` (`id`),
  CONSTRAINT `FK_user_profil_id` FOREIGN KEY (`profil_id`) REFERENCES `profil` (`Id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'administ','5c508f5a3db3eb94329f8b0b513783eb48659f3c','','',1,1,'f197c3aa-dd69-bf8e-7eae-72b3f951d12d',NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_caddie`
--

DROP TABLE IF EXISTS `zpwg_caddie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_caddie` (
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `element_id` mediumint(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`element_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_caddie`
--

LOCK TABLES `zpwg_caddie` WRITE;
/*!40000 ALTER TABLE `zpwg_caddie` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_caddie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_categories`
--

DROP TABLE IF EXISTS `zpwg_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_categories` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `id_uppercat` smallint(5) unsigned DEFAULT NULL,
  `comment` text,
  `dir` varchar(255) DEFAULT NULL,
  `rank` smallint(5) unsigned DEFAULT NULL,
  `status` enum('public','private') NOT NULL DEFAULT 'public',
  `site_id` tinyint(4) unsigned DEFAULT NULL,
  `visible` enum('true','false') NOT NULL DEFAULT 'true',
  `representative_picture_id` mediumint(8) unsigned DEFAULT NULL,
  `uppercats` varchar(255) NOT NULL DEFAULT '',
  `commentable` enum('true','false') NOT NULL DEFAULT 'true',
  `global_rank` varchar(255) DEFAULT NULL,
  `image_order` varchar(128) DEFAULT NULL,
  `permalink` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `lastmodified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `community_user` mediumint(8) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_i3` (`permalink`),
  KEY `categories_i2` (`id_uppercat`),
  KEY `lastmodified` (`lastmodified`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_categories`
--

LOCK TABLES `zpwg_categories` WRITE;
/*!40000 ALTER TABLE `zpwg_categories` DISABLE KEYS */;
INSERT INTO `zpwg_categories` VALUES (1,'Community',NULL,NULL,NULL,1,'public',NULL,'true',NULL,'1','true','1',NULL,NULL,'2016-07-27 14:27:48',NULL),(2,'File Uploader',NULL,'Created by the File Uploader plugin','file_uploader',2,'private',1,'true',NULL,'2','true','2',NULL,NULL,'2016-07-27 14:28:38',NULL);
/*!40000 ALTER TABLE `zpwg_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_category_banner`
--

DROP TABLE IF EXISTS `zpwg_category_banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_category_banner` (
  `category_id` smallint(5) unsigned NOT NULL,
  `image` varchar(255) NOT NULL,
  `deep` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_category_banner`
--

LOCK TABLES `zpwg_category_banner` WRITE;
/*!40000 ALTER TABLE `zpwg_category_banner` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_category_banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_comments`
--

DROP TABLE IF EXISTS `zpwg_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_comments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `image_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `author` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `author_id` mediumint(8) unsigned DEFAULT NULL,
  `anonymous_id` varchar(45) NOT NULL,
  `website_url` varchar(255) DEFAULT NULL,
  `content` longtext,
  `validated` enum('true','false') NOT NULL DEFAULT 'false',
  `validation_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `comments_i2` (`validation_date`),
  KEY `comments_i1` (`image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_comments`
--

LOCK TABLES `zpwg_comments` WRITE;
/*!40000 ALTER TABLE `zpwg_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_community_pendings`
--

DROP TABLE IF EXISTS `zpwg_community_pendings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_community_pendings` (
  `image_id` mediumint(8) unsigned NOT NULL,
  `state` varchar(255) NOT NULL,
  `added_on` datetime NOT NULL,
  `validated_by` mediumint(8) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_community_pendings`
--

LOCK TABLES `zpwg_community_pendings` WRITE;
/*!40000 ALTER TABLE `zpwg_community_pendings` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_community_pendings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_community_permissions`
--

DROP TABLE IF EXISTS `zpwg_community_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_community_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `group_id` smallint(5) unsigned DEFAULT NULL,
  `user_id` mediumint(8) unsigned DEFAULT NULL,
  `category_id` smallint(5) unsigned DEFAULT NULL,
  `user_album` enum('true','false') NOT NULL DEFAULT 'false',
  `recursive` enum('true','false') NOT NULL DEFAULT 'true',
  `create_subcategories` enum('true','false') NOT NULL DEFAULT 'false',
  `moderated` enum('true','false') NOT NULL DEFAULT 'true',
  `nb_photos` int(11) DEFAULT NULL,
  `storage` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_community_permissions`
--

LOCK TABLES `zpwg_community_permissions` WRITE;
/*!40000 ALTER TABLE `zpwg_community_permissions` DISABLE KEYS */;
INSERT INTO `zpwg_community_permissions` VALUES (1,'any_registered_user',NULL,NULL,1,'false','true','true','true',NULL,NULL);
/*!40000 ALTER TABLE `zpwg_community_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_config`
--

DROP TABLE IF EXISTS `zpwg_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_config` (
  `param` varchar(40) NOT NULL DEFAULT '',
  `value` text,
  `comment` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`param`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='configuration table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_config`
--

LOCK TABLES `zpwg_config` WRITE;
/*!40000 ALTER TABLE `zpwg_config` DISABLE KEYS */;
INSERT INTO `zpwg_config` VALUES ('activate_comments','true','Global parameter for usage of comments system'),('AdminTools','a:3:{s:12:\"default_open\";b:1;s:15:\"closed_position\";s:4:\"left\";s:17:\"public_quick_edit\";b:1;}',NULL),('allow_user_customization','true','allow users to customize their gallery?'),('allow_user_registration','true','allow visitors to register?'),('blk_menubar','','Menubar options'),('c13y_ignore','a:2:{s:7:\"version\";s:5:\"2.8.2\";s:4:\"list\";a:0:{}}','List of ignored anomalies'),('comments_author_mandatory','false','Comment author is mandatory'),('comments_email_mandatory','false','Comment email is mandatory'),('comments_enable_website','true','Enable \"website\" field on add comment form'),('comments_forall','false','even guest not registered can post comments'),('comments_order','ASC','comments order on picture page and cie'),('comments_validation','false','administrators validate users comments before becoming visible'),('community','a:1:{s:11:\"user_albums\";b:0;}',NULL),('community_cache_key','sST7h4dQXIbLMSSjpJNF',NULL),('contactmeta','',NULL),('data_dir_checked','1',NULL),('derivatives','a:4:{s:1:\"d\";a:9:{s:6:\"square\";O:16:\"DerivativeParams\":3:{s:13:\"last_mod_time\";i:1469628773;s:6:\"sizing\";O:12:\"SizingParams\":3:{s:10:\"ideal_size\";a:2:{i:0;i:120;i:1;i:120;}s:8:\"max_crop\";i:1;s:8:\"min_size\";a:2:{i:0;i:120;i:1;i:120;}}s:7:\"sharpen\";i:0;}s:5:\"thumb\";O:16:\"DerivativeParams\":3:{s:13:\"last_mod_time\";i:1469628773;s:6:\"sizing\";O:12:\"SizingParams\":3:{s:10:\"ideal_size\";a:2:{i:0;i:144;i:1;i:144;}s:8:\"max_crop\";i:0;s:8:\"min_size\";N;}s:7:\"sharpen\";i:0;}s:6:\"2small\";O:16:\"DerivativeParams\":3:{s:13:\"last_mod_time\";i:1469628773;s:6:\"sizing\";O:12:\"SizingParams\":3:{s:10:\"ideal_size\";a:2:{i:0;i:240;i:1;i:240;}s:8:\"max_crop\";i:0;s:8:\"min_size\";N;}s:7:\"sharpen\";i:0;}s:6:\"xsmall\";O:16:\"DerivativeParams\":3:{s:13:\"last_mod_time\";i:1469628773;s:6:\"sizing\";O:12:\"SizingParams\":3:{s:10:\"ideal_size\";a:2:{i:0;i:432;i:1;i:324;}s:8:\"max_crop\";i:0;s:8:\"min_size\";N;}s:7:\"sharpen\";i:0;}s:5:\"small\";O:16:\"DerivativeParams\":3:{s:13:\"last_mod_time\";i:1469628773;s:6:\"sizing\";O:12:\"SizingParams\":3:{s:10:\"ideal_size\";a:2:{i:0;i:576;i:1;i:432;}s:8:\"max_crop\";i:0;s:8:\"min_size\";N;}s:7:\"sharpen\";i:0;}s:6:\"medium\";O:16:\"DerivativeParams\":3:{s:13:\"last_mod_time\";i:1469628773;s:6:\"sizing\";O:12:\"SizingParams\":3:{s:10:\"ideal_size\";a:2:{i:0;i:792;i:1;i:594;}s:8:\"max_crop\";i:0;s:8:\"min_size\";N;}s:7:\"sharpen\";i:0;}s:5:\"large\";O:16:\"DerivativeParams\":3:{s:13:\"last_mod_time\";i:1469628773;s:6:\"sizing\";O:12:\"SizingParams\":3:{s:10:\"ideal_size\";a:2:{i:0;i:1008;i:1;i:756;}s:8:\"max_crop\";i:0;s:8:\"min_size\";N;}s:7:\"sharpen\";i:0;}s:6:\"xlarge\";O:16:\"DerivativeParams\":3:{s:13:\"last_mod_time\";i:1469628773;s:6:\"sizing\";O:12:\"SizingParams\":3:{s:10:\"ideal_size\";a:2:{i:0;i:1224;i:1;i:918;}s:8:\"max_crop\";i:0;s:8:\"min_size\";N;}s:7:\"sharpen\";i:0;}s:7:\"xxlarge\";O:16:\"DerivativeParams\":3:{s:13:\"last_mod_time\";i:1469628773;s:6:\"sizing\";O:12:\"SizingParams\":3:{s:10:\"ideal_size\";a:2:{i:0;i:1656;i:1;i:1242;}s:8:\"max_crop\";i:0;s:8:\"min_size\";N;}s:7:\"sharpen\";i:0;}}s:1:\"q\";i:95;s:1:\"w\";O:15:\"WatermarkParams\":7:{s:4:\"file\";s:0:\"\";s:8:\"min_size\";a:2:{i:0;i:500;i:1;i:500;}s:4:\"xpos\";i:50;s:4:\"ypos\";i:50;s:7:\"xrepeat\";i:0;s:7:\"yrepeat\";i:0;s:7:\"opacity\";i:100;}s:1:\"c\";a:0:{}}',NULL),('elegant','a:3:{s:11:\"p_main_menu\";s:2:\"on\";s:12:\"p_pict_descr\";s:2:\"on\";s:14:\"p_pict_comment\";s:3:\"off\";}',NULL),('email_admin_on_comment','false','Send an email to the administrators when a valid comment is entered'),('email_admin_on_comment_deletion','false','Send an email to the administrators when a comment is deleted'),('email_admin_on_comment_edition','false','Send an email to the administrators when a comment is modified'),('email_admin_on_comment_validation','true','Send an email to the administrators when a comment requires validation'),('email_admin_on_new_user','false','Send an email to theadministrators when a user registers'),('extents_for_templates','a:0:{}','Actived template-extension(s)'),('file_uploader','a:3:{s:7:\"new_tab\";s:1:\"1\";s:9:\"overwrite\";s:1:\"0\";s:18:\"pdf2tab_extensions\";a:10:{i:0;s:3:\"pdf\";i:1;s:4:\"docx\";i:2;s:3:\"doc\";i:3;s:3:\"odt\";i:4;s:4:\"xlsx\";i:5;s:3:\"xls\";i:6;s:3:\"ods\";i:7;s:4:\"pptx\";i:8;s:3:\"ppt\";i:9;s:3:\"odp\";}}','File Uploader plugin parameters'),('Fotorama','a:20:{s:15:\"allowfullscreen\";s:6:\"native\";s:3:\"fit\";s:9:\"scaledown\";s:10:\"transition\";s:5:\"slide\";s:7:\"shadows\";b:0;s:8:\"autoplay\";b:1;s:19:\"stopautoplayontouch\";b:0;s:4:\"loop\";b:1;s:3:\"nav\";s:5:\"false\";s:14:\"fullscreen_nav\";s:5:\"false\";s:15:\"only_fullscreen\";b:0;s:14:\"enable_caption\";b:0;s:11:\"thumbheight\";i:64;s:15:\"replace_picture\";b:0;s:26:\"replace_picture_only_users\";b:0;s:25:\"clicktransition_crossfade\";b:1;s:12:\"close_button\";b:0;s:6:\"resize\";b:0;s:6:\"period\";i:4000;s:11:\"info_button\";b:0;s:12:\"square_thumb\";b:1;}',NULL),('gallery_locked','false','Lock your gallery temporary for non admin users'),('gallery_title','Une galerie Piwigo de plus','Title at top of each page and for RSS feed'),('gdThumb','a:12:{s:6:\"height\";i:200;s:6:\"margin\";i:10;s:13:\"nb_image_page\";i:80;s:9:\"big_thumb\";b:1;s:16:\"big_thumb_noinpw\";b:0;s:15:\"cache_big_thumb\";b:1;s:15:\"normalize_title\";b:0;s:6:\"method\";s:4:\"crop\";s:16:\"thumb_mode_album\";s:6:\"bottom\";s:16:\"thumb_mode_photo\";s:6:\"bottom\";s:14:\"thumb_metamode\";s:6:\"merged\";s:11:\"no_wordwrap\";b:0;}',NULL),('gmaps_api_key','','Google Maps API key'),('GThumb','a:8:{s:6:\"height\";i:200;s:6:\"margin\";i:10;s:13:\"nb_image_page\";i:80;s:9:\"big_thumb\";b:1;s:15:\"cache_big_thumb\";b:1;s:6:\"method\";s:4:\"crop\";s:22:\"show_thumbnail_caption\";b:1;s:21:\"show_score_in_caption\";b:0;}','GThumb plugin parameters'),('gvideo','a:10:{s:8:\"autoplay\";i:0;s:5:\"width\";i:640;s:6:\"height\";i:360;s:16:\"sync_description\";i:1;s:9:\"sync_tags\";i:1;s:5:\"vimeo\";a:4:{s:5:\"title\";i:1;s:8:\"portrait\";i:1;s:6:\"byline\";i:1;s:5:\"color\";s:6:\"00adef\";}s:11:\"dailymotion\";a:3:{s:4:\"logo\";i:1;s:5:\"title\";i:1;s:5:\"color\";s:6:\"F7FFFD\";}s:7:\"youtube\";a:0:{}s:3:\"wat\";a:0:{}s:5:\"wideo\";a:0:{}}',NULL),('header_manager','a:6:{s:5:\"width\";i:1000;s:6:\"height\";i:150;s:5:\"image\";s:6:\"random\";s:7:\"display\";s:10:\"image_only\";s:17:\"banner_on_picture\";b:1;s:10:\"keep_ratio\";b:1;}',NULL),('history_admin','false','keep a history of administrator visits on your website'),('history_guest','true','keep a history of guest visits on your website'),('index_created_date_icon','true','Display calendar by creation date icon'),('index_flat_icon','false','Display flat icon'),('index_new_icon','true','Display new icons next albums and pictures'),('index_posted_date_icon','true','Display calendar by posted date'),('index_slideshow_icon','true','Display slideshow icon'),('index_sort_order_input','true','Display image order selection list'),('jplayer_autoplay','false','autoplay media in jplayer plugin'),('jplayer_representative_as_poster','false','Use thumbnail as poster to be displayed in player before playing'),('jplayer_skin','bm','Skin used by the piwigo-jplayer plugin'),('log','true','keep an history of visits on your website'),('mail_theme','clear',NULL),('media_icon_advanced','a:4:{s:8:\"position\";s:11:\"bottomright\";s:9:\"xposition\";s:10:\"right: 5px\";s:9:\"yposition\";s:11:\"bottom: 5px\";s:7:\"opacity\";i:100;}','Media Icon plugin advanced parameters'),('media_icon_general','a:4:{s:6:\"styles\";a:6:{s:4:\"logo\";s:4:\"Logo\";s:4:\"page\";s:4:\"Page\";s:6:\"folder\";s:6:\"Folder\";s:11:\"foldercolor\";s:12:\"Folder color\";s:13:\"folderpicture\";s:14:\"Folder picture\";s:7:\"cracked\";s:7:\"Cracked\";}s:5:\"style\";s:4:\"logo\";s:7:\"support\";a:11:{s:7:\"youtube\";a:2:{s:4:\"name\";s:14:\"Youtube videos\";s:5:\"infos\";s:33:\"Videos added with Embedded Videos\";}s:5:\"vimeo\";a:2:{s:4:\"name\";s:12:\"Vimeo videos\";s:5:\"infos\";s:33:\"Videos added with Embedded Videos\";}s:11:\"dailymotion\";a:2:{s:4:\"name\";s:18:\"Dailymotion videos\";s:5:\"infos\";s:33:\"Videos added with Embedded Videos\";}s:5:\"wideo\";a:2:{s:4:\"name\";s:12:\"Wideo videos\";s:5:\"infos\";s:33:\"Videos added with Embedded Videos\";}s:3:\"wat\";a:2:{s:4:\"name\";s:10:\"Wat videos\";s:5:\"infos\";s:33:\"Videos added with Embedded Videos\";}s:5:\"video\";a:2:{s:4:\"name\";s:6:\"Videos\";s:5:\"infos\";s:48:\"Files supported: webm, webmv, ogv, m4v, flv, mp4\";}s:5:\"music\";a:2:{s:4:\"name\";s:6:\"Musics\";s:5:\"infos\";s:52:\"Files supported: mp3, ogg, oga, m4a, webma, fla, wav\";}s:3:\"pdf\";a:2:{s:4:\"name\";s:9:\"pdf files\";s:5:\"infos\";s:20:\"Files supported: pdf\";}s:8:\"document\";a:2:{s:4:\"name\";s:9:\"Documents\";s:5:\"infos\";s:34:\"Files supported: doc, docx and odt\";}s:11:\"spreadsheet\";a:2:{s:4:\"name\";s:12:\"Spreadsheets\";s:5:\"infos\";s:34:\"Files supported: xls, xlsx and ods\";}s:12:\"presentation\";a:2:{s:4:\"name\";s:13:\"Presentations\";s:5:\"infos\";s:34:\"Files supported: ppt, pptx and odp\";}}s:6:\"active\";a:11:{s:7:\"youtube\";i:1;s:5:\"vimeo\";i:1;s:11:\"dailymotion\";i:1;s:5:\"wideo\";i:1;s:3:\"wat\";i:1;s:5:\"video\";i:1;s:5:\"music\";i:1;s:3:\"pdf\";i:1;s:8:\"document\";i:1;s:11:\"spreadsheet\";i:1;s:12:\"presentation\";i:1;}}','Media Icon plugin general parameters'),('menubar_filter_icon','false','Display filter icon'),('meta 2.0.5','1','MAJ meta'),('meta 2.1.0','1','MAJ meta'),('mobile_theme','smartpocket',NULL),('mobile_theme_for_tablets','a:1:{s:7:\"devices\";a:3:{s:6:\"mobile\";s:1:\"1\";s:6:\"tablet\";s:1:\"1\";s:7:\"desktop\";s:1:\"0\";}}','Mobile Theme for Tablets plugin parameters'),('mp_plugin','true,false,false,260,400','Plugin music_player : evidence, header, footer, hauteur, largeur'),('nbm_complementary_mail_content','','Complementary mail content for notification by mail'),('nbm_send_detailed_content','true','Send detailed content for notification by mail'),('nbm_send_html_mail','true','Send mail on HTML format for notification by mail'),('nbm_send_mail_as','','Send mail as param value for notification by mail'),('nbm_send_recent_post_dates','true','Send recent post by dates for notification by mail'),('nb_categories_page','12','Param for categories pagination'),('nb_comment_page','10','number of comments to display on each page'),('no_photo_yet','false',NULL),('obligatory_user_mail_address','false','Mail address is obligatory for users'),('order_by','ORDER BY date_available DESC, file ASC, id ASC','default photo order'),('order_by_inside_category','ORDER BY date_available DESC, file ASC, id ASC','default photo order inside category'),('original_resize','false',NULL),('original_resize_maxheight','2016',NULL),('original_resize_maxwidth','2016',NULL),('original_resize_quality','95',NULL),('osm_conf','a:8:{s:11:\"right_panel\";a:7:{s:7:\"enabled\";b:1;s:10:\"add_before\";s:7:\"Average\";s:6:\"height\";s:3:\"200\";s:4:\"zoom\";i:12;s:4:\"link\";s:8:\"Location\";s:7:\"linkcss\";N;s:7:\"showosm\";b:1;}s:9:\"left_menu\";a:11:{s:7:\"enabled\";b:1;s:4:\"link\";s:10:\"OSWORLDMAP\";s:5:\"popup\";i:0;s:14:\"popupinfo_name\";b:1;s:13:\"popupinfo_img\";b:1;s:14:\"popupinfo_link\";b:1;s:17:\"popupinfo_comment\";b:1;s:16:\"popupinfo_author\";b:1;s:4:\"zoom\";i:2;s:6:\"center\";s:3:\"0,0\";s:6:\"layout\";i:2;}s:20:\"category_description\";a:4:{s:7:\"enabled\";b:1;s:5:\"index\";i:0;s:6:\"height\";s:3:\"200\";s:5:\"width\";s:4:\"auto\";}s:9:\"main_menu\";a:2:{s:7:\"enabled\";b:0;s:6:\"height\";s:3:\"200\";}s:3:\"gpx\";a:2:{s:6:\"height\";s:3:\"500\";s:5:\"width\";s:3:\"320\";}s:5:\"batch\";a:2:{s:13:\"global_height\";s:3:\"200\";s:11:\"unit_height\";s:3:\"200\";}s:3:\"map\";a:7:{s:9:\"baselayer\";s:6:\"mapnik\";s:15:\"custombaselayer\";N;s:18:\"custombaselayerurl\";N;s:11:\"noworldwarp\";b:0;s:11:\"attrleaflet\";b:1;s:11:\"attrimagery\";b:1;s:10:\"attrplugin\";b:1;}s:3:\"pin\";a:7:{s:3:\"pin\";i:1;s:7:\"pinpath\";s:0:\"\";s:7:\"pinsize\";s:0:\"\";s:13:\"pinshadowpath\";s:0:\"\";s:13:\"pinshadowsize\";s:0:\"\";s:9:\"pinoffset\";s:0:\"\";s:14:\"pinpopupoffset\";s:0:\"\";}}','Configuration settings for piwigo-openstreetmap plugin'),('page_banner','<h1>%gallery_title%</h1>\n\n<p>Bienvenue sur ma galerie photo</p>','html displayed on the top each page of your gallery'),('pfemail_last_check','2016-07-27 16:32:27',NULL),('PhotoSphere','a:4:{s:9:\"raw_width\";i:6144;s:12:\"display_help\";b:1;s:9:\"auto_anim\";b:1;s:12:\"display_icon\";b:1;}',NULL),('picture_download_icon','true','Display download icon on picture page'),('picture_favorite_icon','true','Display favorite icon on picture page'),('picture_informations','a:11:{s:6:\"author\";b:1;s:10:\"created_on\";b:1;s:9:\"posted_on\";b:1;s:10:\"dimensions\";b:0;s:4:\"file\";b:0;s:8:\"filesize\";b:0;s:4:\"tags\";b:1;s:10:\"categories\";b:1;s:6:\"visits\";b:1;s:12:\"rating_score\";b:1;s:13:\"privacy_level\";b:1;}','Information displayed on picture page'),('picture_menu','false','Show menubar on picture page'),('picture_metadata_icon','true','Display metadata icon on picture page'),('picture_navigation_icons','true','Display navigation icons on picture page'),('picture_navigation_thumb','true','Display navigation thumbnails on picture page'),('picture_slideshow_icon','true','Display slideshow icon on picture page'),('piwigo_db_version','2.8',NULL),('Preload','a:2:{s:10:\"imageCount\";i:1;s:12:\"squareThumbs\";b:1;}',NULL),('rate','true','Rating pictures feature is enabled'),('rate_anonymous','true','Rating pictures feature is also enabled for visitors'),('secret_key','d2ec60e833ad7229d0dee8f0a9e36d66','a secret key specific to the gallery for internal use'),('see_photos_by_user_color','#ffffff',NULL),('see_photos_by_user_limit','1000',NULL),('see_photos_by_user_nbphotos','0',NULL),('see_photos_by_user_order','username ASC',NULL),('see_photos_by_user_shape','sphere',NULL),('see_photos_by_user_show','1',NULL),('see_photos_by_user_show_user_home','1',NULL),('smartpocket','a:2:{s:4:\"loop\";b:1;s:8:\"autohide\";i:5000;}',NULL),('threed','a:4:{s:10:\"chromeCast\";i:0;s:9:\"openGraph\";i:0;s:14:\"video_autoplay\";i:0;s:17:\"video_description\";s:0:\"\";}',NULL),('thumbnail_tooltip','a:9:{s:12:\"display_name\";b:1;s:6:\"value1\";s:8:\"tn_type1\";s:6:\"value2\";s:8:\"tn_type8\";s:6:\"value3\";s:8:\"tn_type4\";s:6:\"value4\";s:4:\"none\";s:6:\"value5\";s:4:\"none\";s:6:\"value6\";s:4:\"none\";s:9:\"separator\";s:1:\"1\";s:18:\"display_author_cat\";s:1:\"1\";}','Thumbnail Tooltip plugin parameters'),('updates_ignored','a:3:{s:7:\"plugins\";a:0:{}s:6:\"themes\";a:0:{}s:9:\"languages\";a:0:{}}','Extensions ignored for update'),('user_can_delete_comment','false','administrators can allow user delete their own comments'),('user_can_edit_comment','false','administrators can allow user edit their own comments'),('vjs_conf','a:11:{s:4:\"skin\";s:16:\"vjs-default-skin\";s:10:\"max_height\";s:3:\"720\";s:7:\"preload\";s:4:\"auto\";s:8:\"controls\";b:1;s:8:\"autoplay\";b:0;s:4:\"loop\";b:0;s:6:\"volume\";s:1:\"1\";s:7:\"upscale\";b:0;s:7:\"plugins\";a:3:{s:10:\"zoomrotate\";b:0;s:10:\"thumbnails\";b:0;s:9:\"watermark\";b:0;}s:6:\"player\";s:17:\"vjs-player.tpl.v5\";s:8:\"metadata\";b:1;}','Configuration settings for piwigo-videojs plugin'),('vjs_customcss','','Custom CSS used by the piwigo-videojs plugin'),('week_starts_on','monday','Monday may not be the first day of the week'),('wired_for_sound','plugins/WiredForSound/mp3/,100,44,1,0','Parametres du plugin WiredForSound');
/*!40000 ALTER TABLE `zpwg_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_favorites`
--

DROP TABLE IF EXISTS `zpwg_favorites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_favorites` (
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `image_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_favorites`
--

LOCK TABLES `zpwg_favorites` WRITE;
/*!40000 ALTER TABLE `zpwg_favorites` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_favorites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_group_access`
--

DROP TABLE IF EXISTS `zpwg_group_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_group_access` (
  `group_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `cat_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`group_id`,`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_group_access`
--

LOCK TABLES `zpwg_group_access` WRITE;
/*!40000 ALTER TABLE `zpwg_group_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_group_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_groups`
--

DROP TABLE IF EXISTS `zpwg_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_groups` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `is_default` enum('true','false') NOT NULL DEFAULT 'false',
  `lastmodified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `pfemail_notify` enum('true','false') DEFAULT 'false',
  PRIMARY KEY (`id`),
  UNIQUE KEY `groups_ui1` (`name`),
  KEY `lastmodified` (`lastmodified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_groups`
--

LOCK TABLES `zpwg_groups` WRITE;
/*!40000 ALTER TABLE `zpwg_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_history`
--

DROP TABLE IF EXISTS `zpwg_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL DEFAULT '1970-01-01',
  `time` time NOT NULL DEFAULT '00:00:00',
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `IP` varchar(15) NOT NULL DEFAULT '',
  `section` enum('categories','tags','search','list','favorites','most_visited','best_rated','recent_pics','recent_cats','most_downloaded') DEFAULT NULL,
  `category_id` smallint(5) DEFAULT NULL,
  `tag_ids` varchar(50) DEFAULT NULL,
  `image_id` mediumint(8) DEFAULT NULL,
  `summarized` enum('true','false') DEFAULT 'false',
  `image_type` enum('picture','high','other') DEFAULT NULL,
  `format_id` int(11) unsigned DEFAULT NULL,
  `auth_key_id` int(11) unsigned DEFAULT NULL,
  `thumbnail_tooltip` enum('true','false') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `history_i1` (`summarized`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_history`
--

LOCK TABLES `zpwg_history` WRITE;
/*!40000 ALTER TABLE `zpwg_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_history_summary`
--

DROP TABLE IF EXISTS `zpwg_history_summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_history_summary` (
  `year` smallint(4) NOT NULL DEFAULT '0',
  `month` tinyint(2) DEFAULT NULL,
  `day` tinyint(2) DEFAULT NULL,
  `hour` tinyint(2) DEFAULT NULL,
  `nb_pages` int(11) DEFAULT NULL,
  UNIQUE KEY `history_summary_ymdh` (`year`,`month`,`day`,`hour`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_history_summary`
--

LOCK TABLES `zpwg_history_summary` WRITE;
/*!40000 ALTER TABLE `zpwg_history_summary` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_history_summary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_image_category`
--

DROP TABLE IF EXISTS `zpwg_image_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_image_category` (
  `image_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `category_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `rank` mediumint(8) unsigned DEFAULT NULL,
  PRIMARY KEY (`image_id`,`category_id`),
  KEY `image_category_i1` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_image_category`
--

LOCK TABLES `zpwg_image_category` WRITE;
/*!40000 ALTER TABLE `zpwg_image_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_image_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_image_format`
--

DROP TABLE IF EXISTS `zpwg_image_format`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_image_format` (
  `format_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `image_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ext` varchar(255) NOT NULL,
  `filesize` mediumint(9) unsigned DEFAULT NULL,
  PRIMARY KEY (`format_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_image_format`
--

LOCK TABLES `zpwg_image_format` WRITE;
/*!40000 ALTER TABLE `zpwg_image_format` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_image_format` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_image_tag`
--

DROP TABLE IF EXISTS `zpwg_image_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_image_tag` (
  `image_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `tag_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`image_id`,`tag_id`),
  KEY `image_tag_i1` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_image_tag`
--

LOCK TABLES `zpwg_image_tag` WRITE;
/*!40000 ALTER TABLE `zpwg_image_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_image_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_image_video`
--

DROP TABLE IF EXISTS `zpwg_image_video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_image_video` (
  `picture_id` mediumint(8) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `type` varchar(64) NOT NULL,
  `video_id` varchar(128) NOT NULL,
  `width` smallint(9) DEFAULT NULL,
  `height` smallint(9) DEFAULT NULL,
  `autoplay` tinyint(1) DEFAULT NULL,
  `embed` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_image_video`
--

LOCK TABLES `zpwg_image_video` WRITE;
/*!40000 ALTER TABLE `zpwg_image_video` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_image_video` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_images`
--

DROP TABLE IF EXISTS `zpwg_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_images` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `file` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `date_available` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `date_creation` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `comment` text,
  `author` varchar(255) DEFAULT NULL,
  `hit` int(10) unsigned NOT NULL DEFAULT '0',
  `filesize` mediumint(9) unsigned DEFAULT NULL,
  `width` smallint(9) unsigned DEFAULT NULL,
  `height` smallint(9) unsigned DEFAULT NULL,
  `coi` char(4) DEFAULT NULL COMMENT 'center of interest',
  `representative_ext` varchar(4) DEFAULT NULL,
  `date_metadata_update` date DEFAULT NULL,
  `rating_score` float(5,2) unsigned DEFAULT NULL,
  `path` varchar(255) NOT NULL DEFAULT '',
  `storage_category_id` smallint(5) unsigned DEFAULT NULL,
  `level` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `md5sum` char(32) DEFAULT NULL,
  `added_by` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `rotation` tinyint(3) unsigned DEFAULT NULL,
  `latitude` double(8,6) DEFAULT NULL,
  `longitude` double(9,6) DEFAULT NULL,
  `lastmodified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `download_counter` int(10) unsigned NOT NULL DEFAULT '0',
  `is_gvideo` tinyint(1) NOT NULL DEFAULT '0',
  `is_sphere` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `images_i2` (`date_available`),
  KEY `images_i3` (`rating_score`),
  KEY `images_i4` (`hit`),
  KEY `images_i5` (`date_creation`),
  KEY `images_i1` (`storage_category_id`),
  KEY `images_i6` (`latitude`),
  KEY `lastmodified` (`lastmodified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_images`
--

LOCK TABLES `zpwg_images` WRITE;
/*!40000 ALTER TABLE `zpwg_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_languages`
--

DROP TABLE IF EXISTS `zpwg_languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_languages` (
  `id` varchar(64) NOT NULL DEFAULT '',
  `version` varchar(64) NOT NULL DEFAULT '0',
  `name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_languages`
--

LOCK TABLES `zpwg_languages` WRITE;
/*!40000 ALTER TABLE `zpwg_languages` DISABLE KEYS */;
INSERT INTO `zpwg_languages` VALUES ('af_ZA','2.8.2','Afrikaans [ZA]'),('ar_EG','2.8.2','العربية (مصر) [EG]'),('ar_MA','2.8.2','العربية [MA]'),('ar_SA','2.8.2','العربية [AR]'),('az_AZ','2.8.2','Azərbaycanca [AZ]'),('bg_BG','2.8.2','Български [BG]'),('bn_IN','2.8.2','বাংলা[IN]'),('br_FR','2.8.2','Brezhoneg [FR]'),('ca_ES','2.8.2','Català [CA]'),('cs_CZ','2.8.2','Česky [CZ]'),('da_DK','2.8.2','Dansk [DK]'),('de_DE','2.8.2','Deutsch [DE]'),('dv_MV','2.8.2','Dhivehi [MV]'),('el_GR','2.8.2','Ελληνικά [GR]'),('en_GB','2.8.2','English [GB]'),('en_UK','2.8.2','English [UK]'),('en_US','2.8.2','English [US]'),('eo_EO','2.8.2','Esperanto [EO]'),('es_AR','2.8.2','Argentina [AR]'),('es_ES','2.8.2','Español [ES]'),('es_MX','2.8.2','México [MX]'),('et_EE','2.8.2','Estonian [EE]'),('eu_ES','2.8.2','Euskara [ES]'),('fa_IR','2.8.2','پارسی [IR]'),('fi_FI','2.8.2','Finnish [FI]'),('fr_CA','2.8.2','Français [QC]'),('fr_FR','2.8.2','Français [FR]'),('ga_IE','2.8.2','Gaeilge [IE]'),('gl_ES','2.8.2','Galego [ES]'),('gu_IN','2.8.2','ગુજરાતી[IN]'),('he_IL','2.8.2','עברית [IL]'),('hr_HR','2.8.2','Hrvatski [HR]'),('hu_HU','2.8.2','Magyar [HU]'),('id_ID','2.8.2','Bahasa Indonesia [ID]'),('is_IS','2.8.2','Íslenska [IS]'),('it_IT','2.8.2','Italiano [IT]'),('ja_JP','2.8.2','日本語 [JP]'),('ka_GE','2.8.2','ქართული [GE]'),('km_KH','2.8.2','ខ្មែរ [KH]'),('kn_IN','2.8.2','ಕನ್ನಡ [IN]'),('kok_IN','2.8.2','कोंकणी [IN]'),('ko_KR','2.8.2','한국어 [KR]'),('lb_LU','2.8.2','Lëtzebuergesch [LU]'),('lt_LT','2.8.2','Lietuviu [LT]'),('lv_LV','2.8.2','Latviešu [LV]'),('mk_MK','2.8.2','Македонски [MK]'),('mn_MN','2.8.2','Монгол [MN]'),('ms_MY','2.8.2','Malay [MY]'),('nb_NO','2.8.2','Norsk bokmål [NO]'),('nl_NL','2.8.2','Nederlands [NL]'),('nn_NO','2.8.2','Norwegian nynorsk [NO]'),('pl_PL','2.8.2','Polski [PL]'),('pt_BR','2.8.2','Brasil [BR]'),('pt_PT','2.8.2','Português [PT]'),('ro_RO','2.8.2','Română [RO]'),('ru_RU','2.8.2','Русский [RU]'),('sh_RS','2.8.2','Srpski [SR]'),('sk_SK','2.8.2','Slovensky [SK]'),('sl_SI','2.8.2','Slovenšcina [SI]'),('sr_RS','2.8.2','Српски [SR]'),('sv_SE','2.8.2','Svenska [SE]'),('ta_IN','2.8.2','தமிழ் [IN]'),('th_TH','2.8.2','ภาษาไทย [TH]'),('tr_TR','2.8.2','Türkçe [TR]'),('uk_UA','2.8.2','Українська [UA]'),('vi_VN','2.8.2','Tiếng Việt [VN]'),('wo_SN','2.8.2','Wolof [SN]'),('zh_CN','2.8.2','简体中文 [CN]'),('zh_HK','2.8.2','中文 (香港) [HK]'),('zh_TW','2.8.2','中文 (繁體) [TW]');
/*!40000 ALTER TABLE `zpwg_languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_meta`
--

DROP TABLE IF EXISTS `zpwg_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_meta` (
  `id` smallint(5) unsigned NOT NULL,
  `metaname` varchar(255) NOT NULL,
  `metaval` longtext NOT NULL,
  `metatype` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_meta`
--

LOCK TABLES `zpwg_meta` WRITE;
/*!40000 ALTER TABLE `zpwg_meta` DISABLE KEYS */;
INSERT INTO `zpwg_meta` VALUES (1,'author','','name'),(2,'keywords','','name'),(3,'Description','','name'),(4,'robots','follow','name');
/*!40000 ALTER TABLE `zpwg_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_meta_ap`
--

DROP TABLE IF EXISTS `zpwg_meta_ap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_meta_ap` (
  `id` smallint(5) unsigned NOT NULL,
  `metaKeyap` varchar(255) NOT NULL,
  `metadesap` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_meta_ap`
--

LOCK TABLES `zpwg_meta_ap` WRITE;
/*!40000 ALTER TABLE `zpwg_meta_ap` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_meta_ap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_meta_cat`
--

DROP TABLE IF EXISTS `zpwg_meta_cat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_meta_cat` (
  `id` smallint(5) unsigned NOT NULL,
  `metaKeycat` varchar(255) NOT NULL,
  `metadescat` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_meta_cat`
--

LOCK TABLES `zpwg_meta_cat` WRITE;
/*!40000 ALTER TABLE `zpwg_meta_cat` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_meta_cat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_meta_img`
--

DROP TABLE IF EXISTS `zpwg_meta_img`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_meta_img` (
  `id` smallint(5) unsigned NOT NULL,
  `metaKeyimg` varchar(255) NOT NULL,
  `metadesimg` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_meta_img`
--

LOCK TABLES `zpwg_meta_img` WRITE;
/*!40000 ALTER TABLE `zpwg_meta_img` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_meta_img` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_metaperso`
--

DROP TABLE IF EXISTS `zpwg_metaperso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_metaperso` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `metaname` varchar(255) NOT NULL,
  `metaval` longtext NOT NULL,
  `metatype` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_metaperso`
--

LOCK TABLES `zpwg_metaperso` WRITE;
/*!40000 ALTER TABLE `zpwg_metaperso` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_metaperso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_mp_music`
--

DROP TABLE IF EXISTS `zpwg_mp_music`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_mp_music` (
  `id` bigint(4) NOT NULL AUTO_INCREMENT,
  `pl_id` bigint(4) NOT NULL,
  `rang` bigint(4) NOT NULL,
  `url` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_mp_music`
--

LOCK TABLES `zpwg_mp_music` WRITE;
/*!40000 ALTER TABLE `zpwg_mp_music` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_mp_music` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_mp_playlist`
--

DROP TABLE IF EXISTS `zpwg_mp_playlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_mp_playlist` (
  `id` bigint(4) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `url` varchar(255) NOT NULL,
  `texte` tinytext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_mp_playlist`
--

LOCK TABLES `zpwg_mp_playlist` WRITE;
/*!40000 ALTER TABLE `zpwg_mp_playlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_mp_playlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_old_permalinks`
--

DROP TABLE IF EXISTS `zpwg_old_permalinks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_old_permalinks` (
  `cat_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `permalink` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `date_deleted` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `last_hit` datetime DEFAULT NULL,
  `hit` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`permalink`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_old_permalinks`
--

LOCK TABLES `zpwg_old_permalinks` WRITE;
/*!40000 ALTER TABLE `zpwg_old_permalinks` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_old_permalinks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_osm_places`
--

DROP TABLE IF EXISTS `zpwg_osm_places`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_osm_places` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `latitude` double(8,6) NOT NULL,
  `longitude` double(8,6) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `parentId` mediumint(8) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_osm_places`
--

LOCK TABLES `zpwg_osm_places` WRITE;
/*!40000 ALTER TABLE `zpwg_osm_places` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_osm_places` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_pfemail_mailboxes`
--

DROP TABLE IF EXISTS `zpwg_pfemail_mailboxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_pfemail_mailboxes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(255) NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `category_id` smallint(5) unsigned DEFAULT NULL,
  `moderated` enum('true','false') NOT NULL DEFAULT 'true',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_pfemail_mailboxes`
--

LOCK TABLES `zpwg_pfemail_mailboxes` WRITE;
/*!40000 ALTER TABLE `zpwg_pfemail_mailboxes` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_pfemail_mailboxes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_pfemail_pendings`
--

DROP TABLE IF EXISTS `zpwg_pfemail_pendings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_pfemail_pendings` (
  `image_id` mediumint(8) unsigned NOT NULL,
  `state` varchar(255) NOT NULL,
  `added_on` datetime NOT NULL,
  `validated_by` mediumint(8) unsigned DEFAULT NULL,
  `from_name` varchar(255) DEFAULT NULL,
  `from_address` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_pfemail_pendings`
--

LOCK TABLES `zpwg_pfemail_pendings` WRITE;
/*!40000 ALTER TABLE `zpwg_pfemail_pendings` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_pfemail_pendings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_plugins`
--

DROP TABLE IF EXISTS `zpwg_plugins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_plugins` (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `state` enum('inactive','active') NOT NULL DEFAULT 'inactive',
  `version` varchar(64) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_plugins`
--

LOCK TABLES `zpwg_plugins` WRITE;
/*!40000 ALTER TABLE `zpwg_plugins` DISABLE KEYS */;
INSERT INTO `zpwg_plugins` VALUES ('AdminTools','active','2.8.2'),('File_Uploader','active','2.7.a'),('Fotorama','active','2.7.r'),('GDThumb','active','1.0.19'),('GThumb','active','2.8.a'),('LocalFilesEditor','active','2.8.2'),('Media_Icon','active','2.7.a'),('Mobile_Theme_for_Tablets','active','2.7.a'),('Panoramas','active','2.7.a'),('PhotoSphere','active','0.2.0'),('Preload','active','0.3.3'),('TakeATour','inactive','2.8.2'),('ThreeD','active','2.7'),('ThumbnailTooltip','active','1.5.2'),('WiredForSound','active','2.7.a'),('autocorrect_filename','active','1.1.0'),('automatic_size','active','2.8.a'),('community','active','2.8.a'),('download_counter','active','2.8.a'),('edit_filename','active','1.0.1'),('exif_view','active','2.8.a'),('export_data','active','2.8.a'),('gvideo','active','2.7.2'),('header_manager','active','1.2.0'),('language_switch','active','2.8.2'),('meta','active','2.8.a'),('most_downloaded','active','0.0.1'),('music_player','active','2.3.4'),('photo_from_email','active','2.7.a'),('piwigo-jplayer','active','0.6'),('piwigo-openstreetmap','active','2.8.b'),('piwigo-videojs','active','2.8.b'),('prevnext','active','1.2'),('read_metadata','active','2.8.b'),('rotateImage','active','2.7.a'),('rv_gmaps','active','2.7.b'),('rv_menutree','active','2.7.a'),('see_my_photos','active','2.8.a'),('see_photos_by_user','active','2.8.a'),('uploadt1menu','active','2.8.a'),('url_uploader','active','1.2.2'),('write_metadata','active','2.8.a');
/*!40000 ALTER TABLE `zpwg_plugins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_rate`
--

DROP TABLE IF EXISTS `zpwg_rate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_rate` (
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `element_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `anonymous_id` varchar(45) NOT NULL DEFAULT '',
  `rate` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '1970-01-01',
  PRIMARY KEY (`element_id`,`user_id`,`anonymous_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_rate`
--

LOCK TABLES `zpwg_rate` WRITE;
/*!40000 ALTER TABLE `zpwg_rate` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_rate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_search`
--

DROP TABLE IF EXISTS `zpwg_search`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_search` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `last_seen` date DEFAULT NULL,
  `rules` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_search`
--

LOCK TABLES `zpwg_search` WRITE;
/*!40000 ALTER TABLE `zpwg_search` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_search` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_sessions`
--

DROP TABLE IF EXISTS `zpwg_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_sessions` (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `data` mediumtext NOT NULL,
  `expiration` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_sessions`
--

LOCK TABLES `zpwg_sessions` WRITE;
/*!40000 ALTER TABLE `zpwg_sessions` DISABLE KEYS */;
INSERT INTO `zpwg_sessions` VALUES ('7F00j6enhb9e1sp4evb0rfk6bikot3','pwg_uid|i:1;pwg_device|s:7:\"desktop\";pwg_mobile_theme|b:0;need_update|b:0;extensions_need_update|a:0:{}incompatible_plugins|a:1:{s:10:\"~~expire~~\";i:1469629949;}pwg_multiview|a:8:{s:7:\"view_as\";s:1:\"1\";s:5:\"theme\";s:7:\"elegant\";s:4:\"lang\";s:5:\"fr_FR\";s:12:\"show_queries\";b:0;s:10:\"debug_l10n\";b:0;s:14:\"debug_template\";b:0;s:22:\"template_combine_files\";b:1;s:10:\"no_history\";b:0;}','2016-07-27 16:32:27');
/*!40000 ALTER TABLE `zpwg_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_sites`
--

DROP TABLE IF EXISTS `zpwg_sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_sites` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `galleries_url` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sites_ui1` (`galleries_url`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_sites`
--

LOCK TABLES `zpwg_sites` WRITE;
/*!40000 ALTER TABLE `zpwg_sites` DISABLE KEYS */;
INSERT INTO `zpwg_sites` VALUES (1,'./galleries/');
/*!40000 ALTER TABLE `zpwg_sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_tags`
--

DROP TABLE IF EXISTS `zpwg_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_tags` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `url_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `lastmodified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `tags_i1` (`url_name`),
  KEY `lastmodified` (`lastmodified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_tags`
--

LOCK TABLES `zpwg_tags` WRITE;
/*!40000 ALTER TABLE `zpwg_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_themes`
--

DROP TABLE IF EXISTS `zpwg_themes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_themes` (
  `id` varchar(64) NOT NULL DEFAULT '',
  `version` varchar(64) NOT NULL DEFAULT '0',
  `name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_themes`
--

LOCK TABLES `zpwg_themes` WRITE;
/*!40000 ALTER TABLE `zpwg_themes` DISABLE KEYS */;
INSERT INTO `zpwg_themes` VALUES ('elegant','2.8.2','elegant'),('smartpocket','2.8.2','Smart Pocket');
/*!40000 ALTER TABLE `zpwg_themes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_upgrade`
--

DROP TABLE IF EXISTS `zpwg_upgrade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_upgrade` (
  `id` varchar(20) NOT NULL DEFAULT '',
  `applied` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_upgrade`
--

LOCK TABLES `zpwg_upgrade` WRITE;
/*!40000 ALTER TABLE `zpwg_upgrade` DISABLE KEYS */;
INSERT INTO `zpwg_upgrade` VALUES ('100','2016-07-27 16:12:27','upgrade included in installation'),('101','2016-07-27 16:12:27','upgrade included in installation'),('102','2016-07-27 16:12:27','upgrade included in installation'),('103','2016-07-27 16:12:27','upgrade included in installation'),('104','2016-07-27 16:12:27','upgrade included in installation'),('105','2016-07-27 16:12:27','upgrade included in installation'),('106','2016-07-27 16:12:27','upgrade included in installation'),('107','2016-07-27 16:12:27','upgrade included in installation'),('108','2016-07-27 16:12:27','upgrade included in installation'),('109','2016-07-27 16:12:27','upgrade included in installation'),('110','2016-07-27 16:12:27','upgrade included in installation'),('111','2016-07-27 16:12:27','upgrade included in installation'),('112','2016-07-27 16:12:27','upgrade included in installation'),('113','2016-07-27 16:12:27','upgrade included in installation'),('114','2016-07-27 16:12:27','upgrade included in installation'),('115','2016-07-27 16:12:27','upgrade included in installation'),('116','2016-07-27 16:12:27','upgrade included in installation'),('117','2016-07-27 16:12:27','upgrade included in installation'),('118','2016-07-27 16:12:27','upgrade included in installation'),('119','2016-07-27 16:12:27','upgrade included in installation'),('120','2016-07-27 16:12:27','upgrade included in installation'),('121','2016-07-27 16:12:27','upgrade included in installation'),('122','2016-07-27 16:12:27','upgrade included in installation'),('123','2016-07-27 16:12:27','upgrade included in installation'),('124','2016-07-27 16:12:27','upgrade included in installation'),('125','2016-07-27 16:12:27','upgrade included in installation'),('126','2016-07-27 16:12:27','upgrade included in installation'),('127','2016-07-27 16:12:27','upgrade included in installation'),('128','2016-07-27 16:12:27','upgrade included in installation'),('129','2016-07-27 16:12:27','upgrade included in installation'),('130','2016-07-27 16:12:27','upgrade included in installation'),('131','2016-07-27 16:12:27','upgrade included in installation'),('132','2016-07-27 16:12:27','upgrade included in installation'),('133','2016-07-27 16:12:27','upgrade included in installation'),('134','2016-07-27 16:12:27','upgrade included in installation'),('135','2016-07-27 16:12:27','upgrade included in installation'),('136','2016-07-27 16:12:27','upgrade included in installation'),('137','2016-07-27 16:12:27','upgrade included in installation'),('138','2016-07-27 16:12:27','upgrade included in installation'),('139','2016-07-27 16:12:27','upgrade included in installation'),('140','2016-07-27 16:12:27','upgrade included in installation'),('141','2016-07-27 16:12:27','upgrade included in installation'),('142','2016-07-27 16:12:27','upgrade included in installation'),('143','2016-07-27 16:12:27','upgrade included in installation'),('144','2016-07-27 16:12:27','upgrade included in installation'),('145','2016-07-27 16:12:27','upgrade included in installation'),('146','2016-07-27 16:12:27','upgrade included in installation'),('147','2016-07-27 16:12:27','upgrade included in installation'),('148','2016-07-27 16:12:27','upgrade included in installation'),('61','2016-07-27 16:12:27','upgrade included in installation'),('62','2016-07-27 16:12:27','upgrade included in installation'),('63','2016-07-27 16:12:27','upgrade included in installation'),('64','2016-07-27 16:12:27','upgrade included in installation'),('65','2016-07-27 16:12:27','upgrade included in installation'),('66','2016-07-27 16:12:27','upgrade included in installation'),('67','2016-07-27 16:12:27','upgrade included in installation'),('68','2016-07-27 16:12:27','upgrade included in installation'),('69','2016-07-27 16:12:27','upgrade included in installation'),('70','2016-07-27 16:12:27','upgrade included in installation'),('71','2016-07-27 16:12:27','upgrade included in installation'),('72','2016-07-27 16:12:27','upgrade included in installation'),('73','2016-07-27 16:12:27','upgrade included in installation'),('74','2016-07-27 16:12:27','upgrade included in installation'),('75','2016-07-27 16:12:27','upgrade included in installation'),('76','2016-07-27 16:12:27','upgrade included in installation'),('77','2016-07-27 16:12:27','upgrade included in installation'),('78','2016-07-27 16:12:27','upgrade included in installation'),('79','2016-07-27 16:12:27','upgrade included in installation'),('80','2016-07-27 16:12:27','upgrade included in installation'),('81','2016-07-27 16:12:27','upgrade included in installation'),('82','2016-07-27 16:12:27','upgrade included in installation'),('83','2016-07-27 16:12:27','upgrade included in installation'),('84','2016-07-27 16:12:27','upgrade included in installation'),('85','2016-07-27 16:12:27','upgrade included in installation'),('86','2016-07-27 16:12:27','upgrade included in installation'),('87','2016-07-27 16:12:27','upgrade included in installation'),('88','2016-07-27 16:12:27','upgrade included in installation'),('89','2016-07-27 16:12:27','upgrade included in installation'),('90','2016-07-27 16:12:27','upgrade included in installation'),('91','2016-07-27 16:12:27','upgrade included in installation'),('92','2016-07-27 16:12:27','upgrade included in installation'),('93','2016-07-27 16:12:27','upgrade included in installation'),('94','2016-07-27 16:12:27','upgrade included in installation'),('95','2016-07-27 16:12:27','upgrade included in installation'),('96','2016-07-27 16:12:27','upgrade included in installation'),('97','2016-07-27 16:12:27','upgrade included in installation'),('98','2016-07-27 16:12:27','upgrade included in installation'),('99','2016-07-27 16:12:27','upgrade included in installation');
/*!40000 ALTER TABLE `zpwg_upgrade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_user_access`
--

DROP TABLE IF EXISTS `zpwg_user_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_user_access` (
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `cat_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_user_access`
--

LOCK TABLES `zpwg_user_access` WRITE;
/*!40000 ALTER TABLE `zpwg_user_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_user_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_user_auth_keys`
--

DROP TABLE IF EXISTS `zpwg_user_auth_keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_user_auth_keys` (
  `auth_key_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `auth_key` varchar(255) NOT NULL,
  `user_id` mediumint(8) unsigned NOT NULL,
  `created_on` datetime NOT NULL,
  `duration` int(11) unsigned DEFAULT NULL,
  `expired_on` datetime NOT NULL,
  PRIMARY KEY (`auth_key_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_user_auth_keys`
--

LOCK TABLES `zpwg_user_auth_keys` WRITE;
/*!40000 ALTER TABLE `zpwg_user_auth_keys` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_user_auth_keys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_user_cache`
--

DROP TABLE IF EXISTS `zpwg_user_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_user_cache` (
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `need_update` enum('true','false') NOT NULL DEFAULT 'true',
  `cache_update_time` int(10) unsigned NOT NULL DEFAULT '0',
  `forbidden_categories` mediumtext,
  `nb_total_images` mediumint(8) unsigned DEFAULT NULL,
  `last_photo_date` datetime DEFAULT NULL,
  `nb_available_tags` int(5) DEFAULT NULL,
  `nb_available_comments` int(5) DEFAULT NULL,
  `image_access_type` enum('NOT IN','IN') NOT NULL DEFAULT 'NOT IN',
  `image_access_list` mediumtext,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_user_cache`
--

LOCK TABLES `zpwg_user_cache` WRITE;
/*!40000 ALTER TABLE `zpwg_user_cache` DISABLE KEYS */;
INSERT INTO `zpwg_user_cache` VALUES (1,'false',1469628747,'0',0,NULL,0,0,'NOT IN','0');
/*!40000 ALTER TABLE `zpwg_user_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_user_cache_categories`
--

DROP TABLE IF EXISTS `zpwg_user_cache_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_user_cache_categories` (
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `cat_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `date_last` datetime DEFAULT NULL,
  `max_date_last` datetime DEFAULT NULL,
  `nb_images` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `count_images` mediumint(8) unsigned DEFAULT '0',
  `nb_categories` mediumint(8) unsigned DEFAULT '0',
  `count_categories` mediumint(8) unsigned DEFAULT '0',
  `user_representative_picture_id` mediumint(8) unsigned DEFAULT NULL,
  PRIMARY KEY (`user_id`,`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_user_cache_categories`
--

LOCK TABLES `zpwg_user_cache_categories` WRITE;
/*!40000 ALTER TABLE `zpwg_user_cache_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_user_cache_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_user_feed`
--

DROP TABLE IF EXISTS `zpwg_user_feed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_user_feed` (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `last_check` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_user_feed`
--

LOCK TABLES `zpwg_user_feed` WRITE;
/*!40000 ALTER TABLE `zpwg_user_feed` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_user_feed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_user_group`
--

DROP TABLE IF EXISTS `zpwg_user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_user_group` (
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `group_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`group_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_user_group`
--

LOCK TABLES `zpwg_user_group` WRITE;
/*!40000 ALTER TABLE `zpwg_user_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_user_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_user_infos`
--

DROP TABLE IF EXISTS `zpwg_user_infos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_user_infos` (
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `nb_image_page` smallint(3) unsigned NOT NULL DEFAULT '15',
  `status` enum('webmaster','admin','normal','generic','guest') NOT NULL DEFAULT 'guest',
  `language` varchar(50) NOT NULL DEFAULT 'en_UK',
  `expand` enum('true','false') NOT NULL DEFAULT 'false',
  `show_nb_comments` enum('true','false') NOT NULL DEFAULT 'false',
  `show_nb_hits` enum('true','false') NOT NULL DEFAULT 'false',
  `recent_period` tinyint(3) unsigned NOT NULL DEFAULT '7',
  `theme` varchar(255) NOT NULL DEFAULT 'elegant',
  `registration_date` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `enabled_high` enum('true','false') NOT NULL DEFAULT 'true',
  `level` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `activation_key` varchar(255) DEFAULT NULL,
  `activation_key_expire` datetime DEFAULT NULL,
  `lastmodified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  KEY `lastmodified` (`lastmodified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_user_infos`
--

LOCK TABLES `zpwg_user_infos` WRITE;
/*!40000 ALTER TABLE `zpwg_user_infos` DISABLE KEYS */;
INSERT INTO `zpwg_user_infos` VALUES (1,15,'webmaster','fr_FR','false','false','false',7,'elegant','2016-07-27 16:12:27','true',8,NULL,NULL,'2016-07-27 14:12:27'),(2,15,'guest','fr_FR','false','false','false',7,'elegant','2016-07-27 16:12:27','true',0,NULL,NULL,'2016-07-27 14:12:27');
/*!40000 ALTER TABLE `zpwg_user_infos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_user_mail_notification`
--

DROP TABLE IF EXISTS `zpwg_user_mail_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_user_mail_notification` (
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `check_key` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `enabled` enum('true','false') NOT NULL DEFAULT 'false',
  `last_send` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_mail_notification_ui1` (`check_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_user_mail_notification`
--

LOCK TABLES `zpwg_user_mail_notification` WRITE;
/*!40000 ALTER TABLE `zpwg_user_mail_notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_user_mail_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_users`
--

DROP TABLE IF EXISTS `zpwg_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_users` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `password` varchar(255) DEFAULT NULL,
  `mail_address` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_ui1` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_users`
--

LOCK TABLES `zpwg_users` WRITE;
/*!40000 ALTER TABLE `zpwg_users` DISABLE KEYS */;
INSERT INTO `zpwg_users` VALUES (1,'root','02ddc8825acc09beb1ffd2e738140926','fsn.sogeti@gmail.com'),(2,'guest',NULL,NULL);
/*!40000 ALTER TABLE `zpwg_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_wfs_img_cat_sound`
--

DROP TABLE IF EXISTS `zpwg_wfs_img_cat_sound`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_wfs_img_cat_sound` (
  `image_id` mediumint(8) unsigned DEFAULT NULL,
  `cat_id` mediumint(8) DEFAULT NULL,
  `sound_id` smallint(5) NOT NULL,
  `volume` smallint(3) unsigned DEFAULT NULL,
  KEY `image_id` (`image_id`),
  KEY `cat_id` (`cat_id`),
  KEY `sound_id` (`sound_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_wfs_img_cat_sound`
--

LOCK TABLES `zpwg_wfs_img_cat_sound` WRITE;
/*!40000 ALTER TABLE `zpwg_wfs_img_cat_sound` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_wfs_img_cat_sound` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zpwg_wfs_sounds`
--

DROP TABLE IF EXISTS `zpwg_wfs_sounds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zpwg_wfs_sounds` (
  `id` smallint(5) NOT NULL,
  `file` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zpwg_wfs_sounds`
--

LOCK TABLES `zpwg_wfs_sounds` WRITE;
/*!40000 ALTER TABLE `zpwg_wfs_sounds` DISABLE KEYS */;
/*!40000 ALTER TABLE `zpwg_wfs_sounds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'fsn_sog'
--

--
-- Dumping routines for database 'fsn_sog'
--
/*!50003 DROP FUNCTION IF EXISTS `hierarchy_emplacement_connect_by_prior` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `hierarchy_emplacement_connect_by_prior`(value INT) RETURNS int(11)
    READS SQL DATA
BEGIN

        DECLARE _id INT;

        DECLARE _parent INT;

        DECLARE _next INT;

        DECLARE CONTINUE HANDLER FOR NOT FOUND SET @id = NULL;



        SET _parent = @id;

        SET _id = -1;



        IF @id IS NULL THEN

                RETURN NULL;

        END IF;



        LOOP

                SELECT  MIN(id)

                INTO    @id

                FROM   emplacement

                WHERE   COALESCE(parent_id, 0) = _parent

                        AND id > _id;

                IF @id IS NOT NULL OR _parent = @start_with THEN

                        SET @level = @level + 1;

                        RETURN @id;

                END IF;

                SET @level := @level - 1;

                SELECT  id, COALESCE(parent_id, 0)

                INTO    _id, _parent

                FROM    emplacement

                WHERE   id = _parent;

        END LOOP;       

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `hierarchy_emplacement_connect_by_prior_with_level` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `hierarchy_emplacement_connect_by_prior_with_level`(value INT, maxlevel INT) RETURNS int(11)
    READS SQL DATA
BEGIN

        DECLARE _id INT;

        DECLARE _parent INT;

        DECLARE _next INT;

        DECLARE _i INT;

        DECLARE CONTINUE HANDLER FOR NOT FOUND SET @id = NULL;



        SET _parent = @id;

        SET _id = -1;

        SET _i = 0;



        IF @id IS NULL THEN

                RETURN NULL;

        END IF;



        LOOP

                SELECT  MIN(id)

                INTO    @id

                FROM    emplacement

                WHERE   parent_id = _parent

                        AND id > _id

                        AND COALESCE(@level < maxlevel, TRUE);

                IF @id IS NOT NULL OR _parent = @start_with THEN

                        SET @level = @level + 1;

                        RETURN @id;

                END IF;

                SET @level := @level - 1;

                SELECT  id, parent_id

                INTO    _id, _parent

                FROM    emplacement

                WHERE   id = _parent;

                SET _i = _i + 1;

        END LOOP;

        RETURN NULL;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `hierarchy_emplacement_sys_connect_by_path` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `hierarchy_emplacement_sys_connect_by_path`(sparator TEXT, node INT) RETURNS text CHARSET utf8 COLLATE utf8_unicode_ci
    READS SQL DATA
BEGIN

    DECLARE _path TEXT;

    DECLARE _cpath TEXT;

    DECLARE _id INT;

    DECLARE EXIT HANDLER FOR NOT FOUND RETURN _path;

    SET _id = COALESCE(node, @id);

    SET _path = _id;

    LOOP

        SELECT  parent_id

            INTO    _id

        FROM    emplacement

        WHERE   id = _id

            AND COALESCE(id <> @start_with, TRUE);

        SET _path = CONCAT(_id, sparator, _path);

    END LOOP;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `translate` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `translate`(V_string VARCHAR(255), V_from VARCHAR(255), V_to VARCHAR(255)) RETURNS varchar(255) CHARSET utf8 COLLATE utf8_unicode_ci
    DETERMINISTIC
BEGIN

DECLARE i INT;

SET i = CHAR_LENGTH(V_from);

WHILE i > 0 DO 

  SET V_string = REPLACE(V_string, SUBSTR(V_from, i, 1), SUBSTR(V_to, i, 1));

  SET i = i - 1;

END WHILE;

RETURN V_string;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `caracterepate`
--

/*!50001 DROP TABLE IF EXISTS `caracterepate`*/;
/*!50001 DROP VIEW IF EXISTS `caracterepate`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `caracterepate` AS select `fsn_categorie`.`id` AS `id`,`fsn_categorie`.`categorie_value` AS `caracterepate`,`fsn_categorie`.`visible` AS `visible`,`fsn_categorie`.`ordre` AS `ordre` from `fsn_categorie` where ((`fsn_categorie`.`categorie_type` = 'elementpoterie_caracterepate') and (`fsn_categorie`.`visible` = 1)) order by `fsn_categorie`.`ordre` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `classe`
--

/*!50001 DROP TABLE IF EXISTS `classe`*/;
/*!50001 DROP VIEW IF EXISTS `classe`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `classe` AS select `fsn_categorie`.`id` AS `id`,`fsn_categorie`.`categorie_value` AS `classe`,`fsn_categorie`.`visible` AS `visible`,`fsn_categorie`.`ordre` AS `ordre` from `fsn_categorie` where ((`fsn_categorie`.`categorie_type` = 'elementrecueilli_classe') and (`fsn_categorie`.`visible` = 1)) order by `fsn_categorie`.`ordre` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `classification`
--

/*!50001 DROP TABLE IF EXISTS `classification`*/;
/*!50001 DROP VIEW IF EXISTS `classification`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `classification` AS select `fsn_categorie`.`id` AS `id`,`fsn_categorie`.`categorie_value` AS `classification`,`fsn_categorie`.`visible` AS `visible`,`fsn_categorie`.`ordre` AS `ordre` from `fsn_categorie` where ((`fsn_categorie`.`categorie_type` = 'elementpoterie_classification') and (`fsn_categorie`.`visible` = 1)) order by `fsn_categorie`.`ordre` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `consistance`
--

/*!50001 DROP TABLE IF EXISTS `consistance`*/;
/*!50001 DROP VIEW IF EXISTS `consistance`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `consistance` AS select `fsn_categorie`.`id` AS `id`,`fsn_categorie`.`categorie_value` AS `consistance`,`fsn_categorie`.`visible` AS `visible`,`fsn_categorie`.`ordre` AS `ordre` from `fsn_categorie` where ((`fsn_categorie`.`categorie_type` = 'us_consistance') and (`fsn_categorie`.`visible` = 1)) order by `fsn_categorie`.`ordre` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contexte`
--

/*!50001 DROP TABLE IF EXISTS `contexte`*/;
/*!50001 DROP VIEW IF EXISTS `contexte`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contexte` AS select `fsn_categorie`.`id` AS `id`,`fsn_categorie`.`categorie_value` AS `contexte`,`fsn_categorie`.`visible` AS `visible`,`fsn_categorie`.`ordre` AS `ordre` from `fsn_categorie` where ((`fsn_categorie`.`categorie_type` = 'us_contexte') and (`fsn_categorie`.`visible` = 1)) order by `fsn_categorie`.`ordre` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `etat`
--

/*!50001 DROP TABLE IF EXISTS `etat`*/;
/*!50001 DROP VIEW IF EXISTS `etat`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `etat` AS select `fsn_categorie`.`id` AS `id`,`fsn_categorie`.`categorie_value` AS `etat`,`fsn_categorie`.`visible` AS `visible`,`fsn_categorie`.`ordre` AS `ordre` from `fsn_categorie` where ((`fsn_categorie`.`categorie_type` = 'elementrecueilli_etat') and (`fsn_categorie`.`visible` = 1)) order by `fsn_categorie`.`ordre` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `etattraitemen`
--

/*!50001 DROP TABLE IF EXISTS `etattraitemen`*/;
/*!50001 DROP VIEW IF EXISTS `etattraitemen`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `etattraitemen` AS select `fsn_categorie`.`id` AS `id`,`fsn_categorie`.`categorie_value` AS `etat`,`fsn_categorie`.`visible` AS `visible`,`fsn_categorie`.`ordre` AS `ordre` from `fsn_categorie` where ((`fsn_categorie`.`categorie_type` = 'traitement_etat') and (`fsn_categorie`.`visible` = 1)) order by `fsn_categorie`.`ordre` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `etattraitement`
--

/*!50001 DROP TABLE IF EXISTS `etattraitement`*/;
/*!50001 DROP VIEW IF EXISTS `etattraitement`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `etattraitement` AS select `fsn_categorie`.`id` AS `id`,`fsn_categorie`.`categorie_value` AS `etat`,`fsn_categorie`.`visible` AS `visible` from `fsn_categorie` where ((`fsn_categorie`.`categorie_type` = 'traitement_etat') and (`fsn_categorie`.`visible` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `extorganisme`
--

/*!50001 DROP TABLE IF EXISTS `extorganisme`*/;
/*!50001 DROP VIEW IF EXISTS `extorganisme`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `extorganisme` AS select `fsn_organisme`.`id` AS `id`,`fsn_organisme`.`nom` AS `nom`,concat(coalesce(`fsn_organisme`.`adresse1`,''),' ',coalesce(`fsn_organisme`.`adresse2`,''),' ',coalesce(`fsn_organisme`.`cp`,''),' ',coalesce(`fsn_organisme`.`ville`,'')) AS `adresse` from `fsn_organisme` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `fonction`
--

/*!50001 DROP TABLE IF EXISTS `fonction`*/;
/*!50001 DROP VIEW IF EXISTS `fonction`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `fonction` AS select `fsn_categorie`.`id` AS `id`,`fsn_categorie`.`categorie_value` AS `fonction`,`fsn_categorie`.`visible` AS `visible`,`fsn_categorie`.`ordre` AS `ordre` from `fsn_categorie` where ((`fsn_categorie`.`categorie_type` = 'elementrecueilli_fonction') and (`fsn_categorie`.`visible` = 1)) order by `fsn_categorie`.`ordre` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `forme`
--

/*!50001 DROP TABLE IF EXISTS `forme`*/;
/*!50001 DROP VIEW IF EXISTS `forme`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `forme` AS select `fsn_categorie`.`id` AS `id`,`fsn_categorie`.`categorie_value` AS `description`,`fsn_categorie`.`visible` AS `visible`,`fsn_categorie`.`ordre` AS `ordre` from `fsn_categorie` where ((`fsn_categorie`.`categorie_type` = 'elementpoterie_forme') and (`fsn_categorie`.`visible` = 1)) order by `fsn_categorie`.`ordre` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `homogeneite`
--

/*!50001 DROP TABLE IF EXISTS `homogeneite`*/;
/*!50001 DROP VIEW IF EXISTS `homogeneite`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `homogeneite` AS select `fsn_categorie`.`id` AS `id`,`fsn_categorie`.`categorie_value` AS `homogeneite`,`fsn_categorie`.`visible` AS `visible`,`fsn_categorie`.`ordre` AS `ordre` from `fsn_categorie` where ((`fsn_categorie`.`categorie_type` = 'us_homogeneite') and (`fsn_categorie`.`visible` = 1)) order by `fsn_categorie`.`ordre` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `methodefouille`
--

/*!50001 DROP TABLE IF EXISTS `methodefouille`*/;
/*!50001 DROP VIEW IF EXISTS `methodefouille`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `methodefouille` AS select `fsn_categorie`.`id` AS `id`,`fsn_categorie`.`categorie_value` AS `methodefouille`,`fsn_categorie`.`visible` AS `visible`,`fsn_categorie`.`ordre` AS `ordre` from `fsn_categorie` where ((`fsn_categorie`.`categorie_type` = 'us_methodefouille') and (`fsn_categorie`.`visible` = 1)) order by `fsn_categorie`.`ordre` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `metier`
--

/*!50001 DROP TABLE IF EXISTS `metier`*/;
/*!50001 DROP VIEW IF EXISTS `metier`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `metier` AS select `fsn_categorie`.`id` AS `id`,`fsn_categorie`.`categorie_value` AS `metier`,`fsn_categorie`.`visible` AS `visible`,`fsn_categorie`.`ordre` AS `ordre` from `fsn_categorie` where ((`fsn_categorie`.`categorie_type` = 'intervenant_metier') and (`fsn_categorie`.`visible` = 1)) order by `fsn_categorie`.`ordre` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `nature`
--

/*!50001 DROP TABLE IF EXISTS `nature`*/;
/*!50001 DROP VIEW IF EXISTS `nature`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `nature` AS select `fsn_categorie`.`id` AS `id`,`fsn_categorie`.`categorie_value` AS `nature`,`fsn_categorie`.`visible` AS `visible`,`fsn_categorie`.`ordre` AS `ordre` from `fsn_categorie` where ((`fsn_categorie`.`categorie_type` = 'oa_nature') and (`fsn_categorie`.`visible` = 1)) order by `fsn_categorie`.`ordre` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `naturematiere`
--

/*!50001 DROP TABLE IF EXISTS `naturematiere`*/;
/*!50001 DROP VIEW IF EXISTS `naturematiere`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `naturematiere` AS select `fsn_categorie`.`id` AS `id`,`fsn_categorie`.`categorie_value` AS `naturematiere`,`fsn_categorie`.`visible` AS `visible`,`fsn_categorie`.`ordre` AS `ordre` from `fsn_categorie` where ((`fsn_categorie`.`categorie_type` = 'elementrecueilli_naturematiere') and (`fsn_categorie`.`visible` = 1)) order by `fsn_categorie`.`ordre` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `oastatut`
--

/*!50001 DROP TABLE IF EXISTS `oastatut`*/;
/*!50001 DROP VIEW IF EXISTS `oastatut`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `oastatut` AS select `fsn_categorie`.`id` AS `id`,`fsn_categorie`.`categorie_value` AS `oastatut`,`fsn_categorie`.`visible` AS `visible`,`fsn_categorie`.`ordre` AS `ordre` from `fsn_categorie` where ((`fsn_categorie`.`categorie_type` = 'oa_statut') and (`fsn_categorie`.`visible` = 1)) order by `fsn_categorie`.`ordre` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `precisionproduction`
--

/*!50001 DROP TABLE IF EXISTS `precisionproduction`*/;
/*!50001 DROP VIEW IF EXISTS `precisionproduction`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `precisionproduction` AS select `fsn_categorie`.`id` AS `id`,`fsn_categorie`.`categorie_value` AS `precision`,`fsn_categorie`.`visible` AS `visible`,`fsn_categorie`.`ordre` AS `ordre` from `fsn_categorie` where ((`fsn_categorie`.`categorie_type` = 'elementpoterie_precisionproduction') and (`fsn_categorie`.`visible` = 1)) order by `fsn_categorie`.`ordre` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `prod_precisionprod`
--

/*!50001 DROP TABLE IF EXISTS `prod_precisionprod`*/;
/*!50001 DROP VIEW IF EXISTS `prod_precisionprod`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `prod_precisionprod` AS select `c`.`id` AS `prod_id`,`cp`.`id` AS `precisionprod_id` from ((`fsn_categorie` `c` join `fsn_categorie_relation` `r`) join `fsn_categorie` `cp`) where ((`r`.`element_b` = `c`.`id`) and (`cp`.`id` = `r`.`element_a`) and (`c`.`categorie_type` = 'elementpoterie_production') and (`cp`.`categorie_type` = 'elementpoterie_precisionproduction')) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `production`
--

/*!50001 DROP TABLE IF EXISTS `production`*/;
/*!50001 DROP VIEW IF EXISTS `production`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `production` AS select `fsn_categorie`.`id` AS `id`,`fsn_categorie`.`categorie_value` AS `production`,`fsn_categorie`.`visible` AS `visible`,`fsn_categorie`.`ordre` AS `ordre` from `fsn_categorie` where ((`fsn_categorie`.`categorie_type` = 'elementpoterie_production') and (`fsn_categorie`.`visible` = 1)) order by `fsn_categorie`.`ordre` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `role`
--

/*!50001 DROP TABLE IF EXISTS `role`*/;
/*!50001 DROP VIEW IF EXISTS `role`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `role` AS select `fsn_categorie`.`id` AS `id`,`fsn_categorie`.`categorie_value` AS `role`,`fsn_categorie`.`visible` AS `visible`,`fsn_categorie`.`ordre` AS `ordre` from `fsn_categorie` where ((`fsn_categorie`.`categorie_type` = 'participation_role') and (`fsn_categorie`.`visible` = 1)) order by `fsn_categorie`.`ordre` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `sptype`
--

/*!50001 DROP TABLE IF EXISTS `sptype`*/;
/*!50001 DROP VIEW IF EXISTS `sptype`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `sptype` AS select `fsn_categorie`.`id` AS `id`,`fsn_categorie`.`categorie_value` AS `sptype`,`fsn_categorie`.`visible` AS `visible`,`fsn_categorie`.`ordre` AS `ordre` from `fsn_categorie` where ((`fsn_categorie`.`categorie_type` = 'us_superposition_type') and (`fsn_categorie`.`visible` = 1)) order by `fsn_categorie`.`ordre` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `statut`
--

/*!50001 DROP TABLE IF EXISTS `statut`*/;
/*!50001 DROP VIEW IF EXISTS `statut`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `statut` AS select `fsn_categorie`.`id` AS `id`,`fsn_categorie`.`categorie_value` AS `statut`,`fsn_categorie`.`visible` AS `visible`,`fsn_categorie`.`ordre` AS `ordre` from `fsn_categorie` where ((`fsn_categorie`.`categorie_type` = 'elementrecueilli_statut') and (`fsn_categorie`.`visible` = 1)) order by `fsn_categorie`.`ordre` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `synchrotype`
--

/*!50001 DROP TABLE IF EXISTS `synchrotype`*/;
/*!50001 DROP VIEW IF EXISTS `synchrotype`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `synchrotype` AS select `fsn_categorie`.`id` AS `id`,`fsn_categorie`.`categorie_value` AS `synchrotype`,`fsn_categorie`.`visible` AS `visible`,`fsn_categorie`.`ordre` AS `ordre` from `fsn_categorie` where ((`fsn_categorie`.`categorie_type` = 'us_synchronisation_type') and (`fsn_categorie`.`visible` = 1)) order by `fsn_categorie`.`ordre` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `typeasso`
--

/*!50001 DROP TABLE IF EXISTS `typeasso`*/;
/*!50001 DROP VIEW IF EXISTS `typeasso`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `typeasso` AS select `fsn_categorie`.`id` AS `id`,`fsn_categorie`.`categorie_value` AS `type`,`fsn_categorie`.`visible` AS `visible`,`fsn_categorie`.`ordre` AS `ordre` from `fsn_categorie` where ((`fsn_categorie`.`categorie_type` = 'elementrecueilli_association_type') and (`fsn_categorie`.`visible` = 1)) order by `fsn_categorie`.`ordre` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `typedocument`
--

/*!50001 DROP TABLE IF EXISTS `typedocument`*/;
/*!50001 DROP VIEW IF EXISTS `typedocument`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `typedocument` AS select `fsn_categorie`.`id` AS `id`,`fsn_categorie`.`categorie_value` AS `typedoc`,`fsn_categorie`.`visible` AS `visible`,`fsn_categorie`.`ordre` AS `ordre` from `fsn_categorie` where ((`fsn_categorie`.`categorie_type` = 'document_type') and (`fsn_categorie`.`visible` = 1)) order by `fsn_categorie`.`ordre` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `typeintervention`
--

/*!50001 DROP TABLE IF EXISTS `typeintervention`*/;
/*!50001 DROP VIEW IF EXISTS `typeintervention`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `typeintervention` AS select `fsn_categorie`.`id` AS `id`,`fsn_categorie`.`categorie_value` AS `typeinterv`,`fsn_categorie`.`visible` AS `visible`,`fsn_categorie`.`ordre` AS `ordre` from `fsn_categorie` where ((`fsn_categorie`.`categorie_type` = 'intervention_type') and (`fsn_categorie`.`visible` = 1)) order by `fsn_categorie`.`ordre` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `typetraitement`
--

/*!50001 DROP TABLE IF EXISTS `typetraitement`*/;
/*!50001 DROP VIEW IF EXISTS `typetraitement`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `typetraitement` AS select `fsn_categorie`.`id` AS `id`,`fsn_categorie`.`categorie_value` AS `type`,`fsn_categorie`.`visible` AS `visible`,`fsn_categorie`.`ordre` AS `ordre` from `fsn_categorie` where ((`fsn_categorie`.`categorie_type` = 'traitement_type') and (`fsn_categorie`.`visible` = 1)) order by `fsn_categorie`.`ordre` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-10 14:59:27


-- Update FSN V4.0
drop table if exists couche_log;
drop table if exists `log`;
drop table if exists contour_troncon;
drop table if exists contour_unite_sondage;
drop table if exists troncon_us;
drop table if exists troncon;
drop table if exists unitesondage;


create table unitesondage(
    id varchar(36) primary key not null,
    identification varchar(36) not null,
    sitefouille_id varchar(36) not null,
    `type` varchar(8) not null,
    numero_tranchee varchar(36) default null, /* utilisé uniquemenet si type_unitesondage == fenetre */
    nom varchar(32),
    longueur int,
    largeur int,
    profondeur int,
    date_debut date,
    date_fin date,
    description text,
    foreign key(sitefouille_id) references sitefouille(id) on delete restrict on update cascade,
    foreign key(numero_tranchee) references unitesondage(id) on delete cascade on update cascade
) DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;

create table troncon(
    id varchar(36) primary key not null,
    identification varchar(36) not null,
    unitesondage_id varchar(36) not null,
    fai_id varchar(36),
    nom varchar(32),
    longueur int,
    largeur int,
    profondeur int,
    date_debut date,
    date_fin date,
    description text,
    foreign key(unitesondage_id) references unitesondage(id) on delete restrict on update cascade,
    foreign key(fai_id) references fai(id)
) DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;

create table `log`(
    id varchar(36) primary key not null,
    identification varchar(36) not null,
    unitesondage_id varchar(36) not null,
    nom varchar(32),
    description text,
    foreign key(unitesondage_id) references unitesondage(id) on delete restrict on update cascade
) DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;

create table couche_log(
    id varchar(36) primary key not null,
    log_id varchar(36) not null,
    num_sequence int not null,
    epaisseur int,
    couleur varchar(16),
    description varchar(128),
    us_id varchar(36),
    foreign key(log_id) references `log`(id) on delete restrict on update cascade,
    foreign key(us_id) references us(id)
) DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;

create table contour_unite_sondage(
    id varchar(36) primary key not null,
    unitesondage_id varchar(36) not null,
    x float,
    y float,
    z float,
    num_sequence int not null,
    foreign key(unitesondage_id) references unitesondage(id) on delete restrict on update cascade
) DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;

create table contour_troncon(
    id varchar(36) primary key not null,
    troncon_id varchar(36) not null,
    x float,
    y float,
    z float,
    num_sequence int not null,
    foreign key(troncon_id) references troncon(id) on delete restrict on update cascade
) DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;

/*
 * Association entre troncon et unite stratigraphique: relation N:N
 */
create table troncon_us(
    id_troncon varchar(36) not null,
    id_us varchar(36) not null,
    primary key(id_troncon, id_us),
    foreign key(id_troncon) references troncon(id),
    foreign key(id_us) references us(id)
) DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;

insert into fsn_categorie (categorie_object,categorie_type,categorie_key,categorie_value,system,visible,ordre) values
('unitesondage','unitesondage_type','a3ee9be4-3d0a-113f-2793-19918f5d00b3','Tranchée',0,1,1),
('unitesondage','unitesondage_type','c1204bf7-edc6-8d88-6d9b-ec252d936026','Fenêtre',0,1,2);

drop view if exists type_unitesondage;

CREATE VIEW type_unitesondage AS
SELECT id, categorie_object, categorie_type, categorie_key, categorie_value, visible, ordre
FROM `fsn_categorie`
WHERE categorie_object='unitesondage' AND categorie_type='unitesondage_type' AND visible=1
ORDER BY ordre;

drop table if exists fai_extension_value;

CREATE TABLE fai_extension_value (
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    fai_id VARCHAR(36) NOT NULL,
    extension_id INT(11) NOT NULL,
    extension_value VARCHAR(100)
) COLLATE=utf8_unicode_ci ENGINE=INNODB;

ALTER TABLE fai_extension_value
ADD FOREIGN KEY (fai_id) REFERENCES fai(id);

ALTER TABLE fai_extension_value
ADD FOREIGN KEY (extension_id) REFERENCES fsn_categorie(id);

drop view if exists fai_extension;

CREATE VIEW fai_extension as (SELECT b.id id, b.categorie_value nom, a.id valeur_id, a.categorie_value valeur, a.categorie_key valeur_key, c.element_b type_id FROM `fsn_categorie_relation` left join fsn_categorie as a on a.id = fsn_categorie_relation.element_a left join fsn_categorie as b on b.id = fsn_categorie_relation.element_b left join fsn_categorie_relation as c on c.element_a=b.id  where c.element_b in (select id from fsn_categorie where categorie_type="fai_terme") and b.categorie_type='fai_extension');

INSERT INTO `fsn_categorie` (`id`, `categorie_object`, `categorie_type`, `categorie_key`, `categorie_value`, `color`, `system`, `visible`, `ordre`, `old_id`, `description`) VALUES
(2882, 'fai', 'fai_extension_valeur', '59089e61-2fe7-f2da-64a1-50173dcea268', 'Texte', NULL, 1, 1, 1, 2882, NULL),
(2887, 'fai', 'fai_extension_valeur', '05c33c09-8556-9519-73c9-372c5fb496a0', 'Booléen', NULL, 1, 1, 4, 2887, NULL),
(2888, 'fai', 'fai_extension_valeur', '3de370a2-999a-3238-b59f-f7628fb18951', 'Date', NULL, 1, 1, 5, 2888, NULL),
(2890, 'fai', 'fai_extension_valeur', '38633cee-86c7-65bf-8949-03d68ad5b94d', 'Numérique', NULL, 1, 1, 6, 2890, NULL);

INSERT INTO `fsn_categorie` (`id`, `categorie_object`, `categorie_type`, `categorie_key`, `categorie_value`, `color`, `system`, `visible`, `ordre`, `old_id`, `description`) VALUES
(2872, 'fai', 'categorie_type', 'fai_extension_valeur', 'Fait archéologique - extension valeur', NULL, 1, 1, 1, NULL, NULL),
(2880, 'fai', 'categorie_type', 'fai_extension', 'Fait archéologique -extension nom', NULL, 1, 1, 1, NULL, NULL),
(2917, 'fai', 'categorie_type', 'fai_extension_liste_valeur', 'Fait archéologique - extension liste valeur', NULL, 1, 1, 1, NULL, NULL);

INSERT INTO `fsn_categorie` (`id`, `categorie_object`, `categorie_type`, `categorie_key`, `categorie_value`, `color`, `system`, `visible`, `ordre`, `old_id`, `description`) VALUES
(2873, 'categorie', 'categorie_relation', 'de_type', 'de type', NULL, 1, 1, 1, NULL, NULL);

INSERT INTO `fsn_categorie_relation` (`element_a`, `type_relation`, `type_relation_id`, `element_b`, `visible`, `ordre`, `description`) VALUES
(2872, 'de_type', 2873, 2880, 1, 1, NULL),
(2917, 'de_type', 2873, 2880, 1, 1, '');
INSERT INTO `fsn_categorie_relation` (`id`, `element_a`, `type_relation`, `type_relation_id`, `element_b`, `visible`, `ordre`, `description`) VALUES
(4042, 2880, 'depend_de', 41, 40, 1, 1, NULL);

ALTER TABLE `fai_extension_value` CHANGE `id` `id` VARCHAR(36) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
