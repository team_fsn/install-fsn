#!/bin/bash
set -e

echo "Mise à jour"
sudo apt-get update -qq
sudo apt-get -yqq upgrade

sudo apt-get autoremove -yqq
sudo apt-get autoclean -yqq

echo "Installation des outils"
sudo apt-get install -yqq software-properties-common vim unzip htop gdebi curl make

echo "Installation de MySQL"
sudo apt-get install -yqq mysql-server

echo "Installation de apache (2.4)"
sudo apt-get install -yqq apache2
sudo a2enmod rewrite
sudo service apache2 restart

echo "suppression de l'index par défaut"
sudo rm -f /var/www/html/index.html

echo "installation de php (5.5.9)"
sudo apt-get install -yqq php5 imagemagick php5-imagick php5-curl php5-mcrypt php5-mysql php5-gd

echo "installation de java 8"
sudo apt-add-repository ppa:webupd8team/java -y
sudo apt-get update -qq
sudo apt-get install -yqq oracle-java8-installer

echo "installation de python (2.7)"
sudo add-apt-repository ppa:fkrull/deadsnakes -y
sudo apt-get update -qq
sudo apt-get install -yqq python2.7

sudo add-apt-repository ppa:ubuntugis/ubuntugis-unstable -y
echo "Installtion de gdal (2.1.0)"
sudo apt-get update -qq
sudo apt-get install -yqq gdal-bin python-gdal

echo "Installation de instant reality (2.8)"
sudo wget -q http://doc.instantreality.org/media/uploads/downloads/2.8.0/InstantReality-Ubuntu-14.04-x64-2.8.0.38619.deb
sudo gdebi -nq InstantReality-Ubuntu-14.04-x64-2.8.0.38619.deb
sudo rm -f InstantReality-Ubuntu-14.04-x64-2.8.0.38619.deb
echo "Terminé !"