#!/bin/bash
set -e

echo "Configuration du host name"

source ./prod.properties

sudo echo "127.0.0.1 ${FSN_DOMAIN}" >> "/etc/hosts"

echo "Fin de la configuration"